package de.tum.lmt.texturerecognizerappas;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.List;
import java.util.Set;

/**
 * Created by Kev94 on 05.10.2016.
 */
public class DialogFinalStepFragment extends DialogFragment {

    private static final String TAG = DialogFinalStepFragment.class.getSimpleName();

    private View mButtonShare;
    private View mButtonClassification;
    private View mButtonNewRecording;

    private ClassificationTask.iOnClassificationTaskFinishedListener mClassificationListener;
    private AlertDialog mClassificationProgressDialog;
    private long mClassificationTimeStart;
    private long mClassificationTimeStop;

    private Features mFeatures;

    private Animation mClickAnimation;

    static DialogFinalStepFragment newInstance(Features features) {
        DialogFinalStepFragment f = new DialogFinalStepFragment();

        Bundle args = new Bundle();
        args.putParcelable("features", features);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View content = inflater.inflate(R.layout.dialog_final_step, null);

        mFeatures = getArguments().getParcelable("features");

        Log.i("FinalStepDialog", "MFCC1 fs: " + mFeatures.getMFCC1());

        mButtonShare = content.findViewById(R.id.button_share_final_step);
        mButtonShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mButtonShare.startAnimation(mClickAnimation);

                showSendDialog();
                dismiss();
            }
        });

        mButtonClassification = content.findViewById(R.id.button_classification_final_step);
        mButtonClassification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mButtonClassification.startAnimation(mClickAnimation);

                SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

                String classificationMethod = sharedPrefs.getString(Constants.PREF_KEY_CLASSIFICATION_METHOD, null);

                String neighborhoodString = sharedPrefs.getString(Constants.PREF_KEY_NEIGHBORHOOD, null);
                int neighborhood = Constants.DEFAULT_NEIGHBORHOOD;
                if(neighborhoodString != null) {
                    neighborhood = Integer.valueOf(neighborhoodString);
                }

                Set<String> featureSet = sharedPrefs.getStringSet(Constants.PREF_KEY_CLASSIfICATION_FEATURES, null);
                String featurePath = sharedPrefs.getString(Constants.PREF_KEY_LOGGING_DIR, null) + Constants.DATA_TO_SEND_FOLDER_NAME + File.separator;

                if(classificationMethod == null) {
                    Log.e("Classification", "classification parameters not set");
                    Toast.makeText(getActivity(), getString(R.string.parameters_missing), Toast.LENGTH_LONG).show();
                } else {
                    ClassificationTask classifierTask = new ClassificationTask(featurePath, mFeatures, classificationMethod, neighborhood, featureSet, mClassificationListener);
                    classifierTask.execute();

                    mClassificationTimeStart = System.currentTimeMillis();

                    showProgressDialog();
                }
            }
        });

        mButtonNewRecording = content.findViewById(R.id.button_new_recording_final_step);
        mButtonNewRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mButtonNewRecording.startAnimation(mClickAnimation);

                /*Intent mStartActivity = new Intent(getActivity(), MainActivity.class);
                int mPendingIntentId = 123456;
                PendingIntent mPendingIntent = PendingIntent.getActivity(getActivity(), mPendingIntentId,    mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager mgr = (AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);
                mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, mPendingIntent);
                System.exit(0);*/
                dismiss();
                getActivity().finish();
            }
        });

        mClickAnimation = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.click_on_item);

        mClassificationListener = new ClassificationTask.iOnClassificationTaskFinishedListener() {
            @Override
            public void onClassifierTaskFinished(List<Texture.Sample> result) {

                mClassificationTimeStop = System.currentTimeMillis();

                Toast.makeText(getActivity().getApplicationContext(), "Classification took " + (mClassificationTimeStop - mClassificationTimeStart) + " ms.", Toast.LENGTH_LONG).show();

                if(mClassificationProgressDialog != null) {
                    mClassificationProgressDialog.dismiss();
                }

                SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                String classificationMethod = sharedPrefs.getString(Constants.PREF_KEY_CLASSIFICATION_METHOD, null);

                if(result == null) {
                    Log.e("Classification", "Classfication not successful");
                    Toast.makeText(getActivity(), getString(R.string.classification_null), Toast.LENGTH_LONG).show();
                } else {
                    showClassificationListDialog(result, classificationMethod);
                }
            }
        };

        builder.setView(content);

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    private void showSendDialog() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        String loggingDirPath = sharedPrefs.getString(Constants.PREF_KEY_LOGGING_DIR, null);

        if(loggingDirPath != null) {
            DialogSendFragment dialogSend = DialogSendFragment.newInstance(loggingDirPath);
            dialogSend.show(getFragmentManager(), "dialogSend");
        } else {
            Log.e(TAG, "loggingDirPath null");
        }
    }

    private void showClassificationListDialog(List<Texture.Sample> textures, String classificationMethod) {
        DialogClassificationListFragment dialogClassificationList = new DialogClassificationListFragment(textures, classificationMethod);
        dialogClassificationList.show(getFragmentManager(), "dialogClassificationList");
    }

    private void showProgressDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        RelativeLayout rl = new RelativeLayout(getActivity());

        ProgressBar progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);
        RelativeLayout.LayoutParams progressBarParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView text = new TextView(getActivity());
        text.setText(R.string.classification_progress);
        RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        textParams.addRule(RelativeLayout.CENTER_VERTICAL);

        rl.addView(progressBar, progressBarParams);
        rl.addView(text, textParams);

        builder.setView(rl);

        mClassificationProgressDialog = builder.create();
        mClassificationProgressDialog = builder.show();

        mClassificationProgressDialog.setCanceledOnTouchOutside(false);
    }
}
