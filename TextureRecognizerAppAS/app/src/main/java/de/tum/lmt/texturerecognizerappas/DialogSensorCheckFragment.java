package de.tum.lmt.texturerecognizerappas;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import java.io.IOException;
import java.util.HashMap;

public class DialogSensorCheckFragment extends DialogFragment {

    Context mContext;

    SensorManager mSensorManager;
    Bluetooth mBT;

    HashMap<String, Boolean> mAvailabilityMap;

    Handler mTimeHandler;

    int[] mCheckData = new int[]{0,0};

    boolean mCheckExternSensors;

    static DialogSensorCheckFragment newInstance(boolean checkExternSensors) {
        DialogSensorCheckFragment f = new DialogSensorCheckFragment();

        Bundle args = new Bundle();
        args.putBoolean("checkExternSensors", checkExternSensors);
        f.setArguments(args);

        return f;
    }

    public interface iOnAvailabilityCheckedListener {
        public void onAvailabilityChecked(HashMap map);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View content = inflater.inflate(R.layout.dialog_sensor_check, null);

        mContext = getActivity().getApplicationContext();
        mBT = new Bluetooth(mContext, true);

        mAvailabilityMap = new HashMap<>();

        mTimeHandler = new Handler();

        mCheckExternSensors = getArguments().getBoolean("checkExternSensors");

        mSensorManager = (SensorManager) mContext.getSystemService(mContext.SENSOR_SERVICE);

        builder.setView(content);
        builder.setMessage(getString(R.string.message_sensor_check));

        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();

        checkInternalSensors();

        if(mCheckExternSensors) {
            checkExternalSensors();
        } else {

            mAvailabilityMap.put(Constants.FSR_FILENAME, false);
            mAvailabilityMap.put(Constants.EXTERN_ACCEL_FILENAME, false);

            iOnAvailabilityCheckedListener activity = (iOnAvailabilityCheckedListener) getActivity();
            activity.onAvailabilityChecked(mAvailabilityMap);

            dismiss();
        }
    }

    public void checkInternalSensors() {

        Log.i("check", "check Internal Sensors");

        if(mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            Log.i("check", "accel ok");
            mAvailabilityMap.put(Constants.ACCEL_FILENAME, true);
        } else {
            Log.i("check", "accel not ok");
            mAvailabilityMap.put(Constants.ACCEL_FILENAME, false);
        }

        if(mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION) != null) {
            Log.i("check", "lin accel ok");
            mAvailabilityMap.put(Constants.LIN_ACCEL_FILENAME, true);
        } else {
            Log.i("check", "lin accel not ok");
            mAvailabilityMap.put(Constants.LIN_ACCEL_FILENAME, false);
        }

        if(mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY) != null) {
            Log.i("check", "grav ok");
            mAvailabilityMap.put(Constants.GRAV_FILENAME, true);
        } else {
            Log.i("check", "grav not ok");
            mAvailabilityMap.put(Constants.GRAV_FILENAME, false);
        }

        if(mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null) {
            Log.i("check", "gyro ok");
            mAvailabilityMap.put(Constants.GYRO_FILENAME, true);
        } else {
            Log.i("check", "gyro not ok");
            mAvailabilityMap.put(Constants.GYRO_FILENAME, false);
        }

        if(mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null) {
            Log.i("check", "magnet ok");
            mAvailabilityMap.put(Constants.MAGNET_FILENAME, true);
        } else {
            Log.i("check", "magnet not ok");
            mAvailabilityMap.put(Constants.MAGNET_FILENAME, false);
        }

        if(mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR) != null) {
            Log.i("check", "rotvec ok");
            mAvailabilityMap.put(Constants.ROTVEC_FILENAME, true);
        } else {
            Log.i("check", "rotvec not ok");
            mAvailabilityMap.put(Constants.ROTVEC_FILENAME, false);
        }
    }

    public void checkExternalSensors() {

        Log.i("check", "check external sensors");

        boolean success = false;

        mBT.findBluetooth();
        try {
            mBT.openBT();
            success = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(success == true) {
            try {
                mBT.sendData("c");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        mTimeHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    mBT.closeBT();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                mCheckData = mBT.getCheckData();

                if(mCheckData[0] == 1) {
                    Log.i("check", "extern accel ok");
                    mAvailabilityMap.put(Constants.EXTERN_ACCEL_FILENAME, true);
                } else {
                    Log.i("check", "extern accel not ok");
                    mAvailabilityMap.put(Constants.EXTERN_ACCEL_FILENAME, false);
                }

                if(mCheckData[1] == 1) {
                    Log.i("check", "fsr ok");
                    mAvailabilityMap.put(Constants.FSR_FILENAME, true);
                } else {
                    Log.i("check", "fsr not ok");
                    mAvailabilityMap.put(Constants.FSR_FILENAME, false);
                }

                iOnAvailabilityCheckedListener activity = (iOnAvailabilityCheckedListener) getActivity();
                activity.onAvailabilityChecked(mAvailabilityMap);

                dismiss();

            }
        }, 1000);
    }

}
