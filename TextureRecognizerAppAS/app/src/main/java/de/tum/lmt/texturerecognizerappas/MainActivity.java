package de.tum.lmt.texturerecognizerappas;

import android.app.Activity;
import android.app.DialogFragment;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;

public class MainActivity extends Activity
		implements DialogSensorFragment.iOnDialogButtonClickListener, DialogSensorCheckFragment.iOnAvailabilityCheckedListener,
		DialogEnableBluetoothFragment.iOnEnableBluetoothDialogButtonPressed, DialogTextureChoiceFragment.iOnTextureChosen,
		DialogNewEntryFragment.iOnNewEntry, DialogDatabaseSettingsHintFragment.iOnDatabaseSettingsHintDialogFinished {

	private static final String TAG = MainActivity.class.getSimpleName();

	//onActivityResult()
	private static final int SETTINGS_CODE = 0;
	private static final int BLUETOOTH_CODE = 1;

	//UI Elements
	private ImageButton mButtonStart;
	private Button mButtonGallery;
	private Button mButtonNewTexture;
	private ImageButton mButtonSettings;
	
	private static boolean mDatabaseMode = false;
	private String mPathToStorage;
	private static File mLoggingDir;

	private SensorManager mSensorManager;

	private Bluetooth mBT;

	private boolean mAccelAvailable = false;
	private boolean mLinAccelAvailable = false;
	private boolean mGravAvailable = false;
	private boolean mGyroAvailable = false;
	private boolean mMagnetAvailable = false;
	private boolean mRotVecAvailable = false;
	private boolean mExternAccelAvailable = true;
	private boolean mFSRAvailable = true;

	@Override
	public void onEnableBluetoothDialogButtonPressed(String which) {
		if(which.equals(getString(R.string.yes))) {
			Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBluetooth, BLUETOOTH_CODE);
		} else if (which.equals(getString(R.string.no))) {
			DialogSensorCheckFragment checkDialog = DialogSensorCheckFragment.newInstance(false);
			checkDialog.show(getFragmentManager(), "checkDialog");
		}
	}

	@Override
	public void onAvailabilityChecked(HashMap map) {
		mAccelAvailable = (boolean)map.get(Constants.ACCEL_FILENAME);
		mLinAccelAvailable = (boolean)map.get(Constants.LIN_ACCEL_FILENAME);
		mGravAvailable = (boolean)map.get(Constants.GRAV_FILENAME);
		mGyroAvailable = (boolean)map.get(Constants.GYRO_FILENAME);
		mMagnetAvailable = (boolean)map.get(Constants.MAGNET_FILENAME);
		mRotVecAvailable = (boolean)map.get(Constants.ROTVEC_FILENAME);
		mExternAccelAvailable = (boolean)map.get(Constants.EXTERN_ACCEL_FILENAME);
		mFSRAvailable = (boolean)map.get(Constants.FSR_FILENAME);

		DialogSensorFragment sensorDialog = DialogSensorFragment.newInstance(mAccelAvailable, mLinAccelAvailable, mGravAvailable, mGyroAvailable, mMagnetAvailable, mRotVecAvailable, mExternAccelAvailable, mFSRAvailable);
		sensorDialog.show(getFragmentManager(), "sensorDialog");
	}

	@Override
	public void onFinishSensorDialog(String buttonClicked) {
		
		if (buttonClicked.equals(getString(R.string.close))) {
            finish();
        } else if (buttonClicked.equals(getString(R.string.ok))) {


        } else if (buttonClicked.equals(getString(R.string.settings))) {

			startSettingsActivity();
		}
	}

	@Override
	public void onDatabaseSettingsHintDialogFinished(String buttonClicked) {

		if(buttonClicked.equals(getString(R.string.settings))) {

			startSettingsActivity();

		} else if(buttonClicked.equals(getString(R.string.cancel))) {

		}
	}

	@Override
	public void onTextureChosen(File dir, String name, int number) {
		mLoggingDir = dir;

		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = sharedPrefs.edit();
		editor.putString(Constants.PREF_KEY_TEXTURE_NAME, name);
		editor.putInt(Constants.PREF_KEY_TEXTURE_NUMBER, number);
		editor.putString(Constants.PREF_KEY_LOGGING_DIR, dir.getAbsolutePath());
		editor.commit();

		startCameraActivity();

		//startCalibrationActivity();
	}

	@Override
	public void onNewEntry(File dir, String name) {

		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = sharedPrefs.edit();
		editor.putString(Constants.PREF_KEY_TEXTURE_NAME, name);
		editor.putInt(Constants.PREF_KEY_TEXTURE_NUMBER, 1);
		editor.putString(Constants.PREF_KEY_LOGGING_DIR, dir.getAbsolutePath());
		editor.commit();
		Log.i("name", "name: " + sharedPrefs.getString(Constants.PREF_KEY_TEXTURE_NAME, "default"));

		mLoggingDir = dir;
		//change
		//startLoggingActivity();
		startCameraActivity();
	}
	
	/**
	 * taken from http://stackoverflow.com/questions/3394765/how-to-check-available-space-on-android-device-on-mini-sd-card
	 * @return
	 */
	public static long megabytesAvailable() {
		StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
		long bytesAvailable = (long)stat.getBlockSizeLong() *(long)stat.getAvailableBlocksLong();
		long megAvailable = bytesAvailable / 1048576;
		Log.i(TAG, "Megs :" + megAvailable);
		return megAvailable;
	}
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

		if (megabytesAvailable() < 500) { // MB
			Toast.makeText(getBaseContext(),  "Not enough space on the SDcard, please clean up. Only " + megabytesAvailable() + " MB left", 
					Toast.LENGTH_LONG).show();
		}

		mSensorManager = (SensorManager) this.getSystemService(this.SENSOR_SERVICE);

		mBT = new Bluetooth(this);

		if(mBT.isAvailable()) {

			if(!mBT.isEnabled()) {
				DialogFragment enableBTDialog = new DialogEnableBluetoothFragment();
				enableBTDialog.show(getFragmentManager(), "enableBTDialog");
			} else {
				DialogSensorCheckFragment checkDialog = DialogSensorCheckFragment.newInstance(true);
				checkDialog.show(getFragmentManager(), "checkDialog");
			}
		} else {
			Toast.makeText(this, getString(R.string.no_bluetooth), Toast.LENGTH_LONG).show();
		}
    }

	@Override
	protected void onStart() {
		super.onStart();

	}

    @Override
	protected void onResume() {
		super.onResume();

		final SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		mDatabaseMode = sharedPrefs.getBoolean(Constants.PREF_KEY_MODE_SELECT, false);

		mButtonStart = (ImageButton) findViewById(R.id.button_start);
		mButtonStart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mLoggingDir = loggingDir();
				SharedPreferences.Editor editor = sharedPrefs.edit();
				editor.putString(Constants.PREF_KEY_LOGGING_DIR, loggingDir().getAbsolutePath());
				editor.commit();

				startCameraActivity();
				//startCalibrationActivity();
			}
		});

		mButtonGallery = (Button) findViewById(R.id.button_gallery);
		mButtonGallery.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startGalleryActivity();
			}
		});

		mButtonSettings = (ImageButton) findViewById(R.id.button_prefs);
		mButtonSettings.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startSettingsActivity();
			}
		});

		mButtonNewTexture = (Button) findViewById(R.id.button_new_texture);
		mButtonNewTexture.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if(checkIfAllSensorsSelected()) {
					mLoggingDir = loggingDir();
					SharedPreferences.Editor editor = sharedPrefs.edit();
					editor.putString(Constants.PREF_KEY_LOGGING_DIR, loggingDir().getAbsolutePath());
					editor.commit();
					showNewEntryDialog(mLoggingDir);
				} else {
					showDatabaseSettingsHintDialog();
				}
			}
		});

		if(mDatabaseMode) {
			mButtonNewTexture.setVisibility(View.VISIBLE);
			mButtonStart.setVisibility(View.GONE);
		}
		else {
			mButtonStart.setVisibility(View.VISIBLE);
			mButtonNewTexture.setVisibility(View.GONE);
		}

		// show used memory
		{
			final Runtime runtime = Runtime.getRuntime();
			final long usedMemInMB=(runtime.totalMemory() - runtime.freeMemory()) / 1048576L;
			final long maxHeapSizeInMB=runtime.maxMemory() / 1048576L;
			Log.i(TAG, "TRActivity, Used in MB " + Long.toString(usedMemInMB) + ", total  " + Long.toString(maxHeapSizeInMB));
		}
	}
    
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	
    	if(requestCode == BLUETOOTH_CODE) {

			if(resultCode == RESULT_OK) {
				Toast.makeText(this, getString(R.string.toast_enabled_bt), Toast.LENGTH_LONG).show();

				DialogSensorCheckFragment checkDialog = DialogSensorCheckFragment.newInstance(true);
				checkDialog.show(getFragmentManager(), "checkDialog");

			} else if(resultCode == RESULT_CANCELED) {
				Toast.makeText(this, getString(R.string.toast_error_bt), Toast.LENGTH_LONG).show();

				SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

				SharedPreferences.Editor editor = sharedPrefs.edit();
				editor.putBoolean(Constants.PREF_KEY_EXTERN_ACCEL, false);
			}
    	}
    }

	private boolean checkIfAllSensorsSelected() {

		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		boolean useAccel = sharedPrefs.getBoolean(Constants.PREF_KEY_ACCEL_SELECT, false);
		boolean useLinAccel = sharedPrefs.getBoolean(Constants.PREF_KEY_LIN_ACCEL_SELECT, false);
		boolean useGrav = sharedPrefs.getBoolean(Constants.PREF_KEY_GRAV_SELECT, false);
		boolean useGyro = sharedPrefs.getBoolean(Constants.PREF_KEY_GYRO_SELECT, false);
		boolean useMagnet = sharedPrefs.getBoolean(Constants.PREF_KEY_MAGNET_SELECT, false);
		boolean useRotVec = sharedPrefs.getBoolean(Constants.PREF_KEY_ROTVEC_SELECT, false);
		boolean useExternAccel = sharedPrefs.getBoolean(Constants.PREF_KEY_EXTERN_ACCEL, false);
		boolean useFSR = sharedPrefs.getBoolean(Constants.PREF_KEY_FSR, false);

		return (useAccel && useLinAccel && useGrav && useGyro && useMagnet && useRotVec && useExternAccel && useFSR);
	}

	protected void showTrainDialog() {

		mLoggingDir = loggingDir();
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		String classificationMethod = sharedPrefs.getString(Constants.PREF_KEY_CLASSIFICATION_METHOD, null);
		DialogTrainFragment dialogTrain = DialogTrainFragment.newInstance(mLoggingDir.getAbsolutePath(), classificationMethod);
		dialogTrain.show(getFragmentManager(), "dialogTrain");
	}

	protected void showNewEntryDialog(File dir) {

		DialogNewEntryFragment newEntryDialog = DialogNewEntryFragment.newInstance(dir.getAbsolutePath(), 0);
		newEntryDialog.show(getFragmentManager(), "DialogNewEntryFragment");
	}

	protected void showDatabaseSettingsHintDialog() {

		DialogFragment databaseSettingsHintDialog = new DialogDatabaseSettingsHintFragment();
		databaseSettingsHintDialog.show(getFragmentManager(), "DialogDatabaseSettingsHintFragment");
	}

	protected void startSettingsActivity() {
		Intent settingsActivity = new Intent(MainActivity.this, SettingsActivity.class);

		settingsActivity.putExtra("accelAvailable", mAccelAvailable);
		settingsActivity.putExtra("linAccelAvailable", mLinAccelAvailable);
		settingsActivity.putExtra("gravAvailable", mGravAvailable);
		settingsActivity.putExtra("gyroAvailable", mGyroAvailable);
		settingsActivity.putExtra("magnetAvailable", mMagnetAvailable);
		settingsActivity.putExtra("rotVecAvailable", mRotVecAvailable);
		settingsActivity.putExtra("externAccelAvailable", mExternAccelAvailable);
		settingsActivity.putExtra("fsrAvailable", mFSRAvailable);

		startActivityForResult(settingsActivity, SETTINGS_CODE);
	}

	protected void startGalleryActivity() {
		Intent galleryActivity = new Intent(MainActivity.this, GalleryActivity.class);
		startActivity(galleryActivity);
	}

	protected void startCameraActivity() {
		Intent cameraIntent = new Intent(MainActivity.this, CameraActivity.class);
		startActivity(cameraIntent);
	}

	/*protected void startCalibrationActivity() {
		Intent calibrationIntent = new Intent(MainActivity.this, SensorCalibrationActivity.class);
		startActivity(calibrationIntent);
	}*/

	protected void startLoggingActivity() {
		Intent loggingIntent = new Intent(MainActivity.this, SensorLoggingActivity.class);
		startActivity(loggingIntent);
	}

	private static String get2DigitString(int val){
		String valStr=""+val;
		if(val<10){
			valStr="0"+val;
		}
		return valStr;
	}

	private File loggingDir() {
		File dir;

		mPathToStorage = Constants.PATH_TO_STORAGE;
		String prefix = Environment.getExternalStorageDirectory().getPath();
		mPathToStorage =  prefix + "/" + mPathToStorage;

		if(!mDatabaseMode) {
			mPathToStorage = mPathToStorage.concat(Constants.ANALYSIS_FOLDER_NAME);
		}
		else {
			mPathToStorage = mPathToStorage.concat(Constants.DATABASE_FOLDER_NAME);
		}

		File logPath = new File(mPathToStorage);
		
		if (!logPath.exists()) {
			logPath.mkdirs();
		}
		
		if(!logPath.exists())
		{
			((TextView) findViewById(R.id.textview_error)).setText("Not logging! Storage directory " + logPath.getPath() + 
					" does not exist.");
			return null;
		}
		else {
			Log.i(TAG, "storage dir: " + logPath.getAbsolutePath());
		}

		Calendar rightNow = Calendar.getInstance();

		if(!mDatabaseMode) {
			dir = getLogFolder(mPathToStorage, rightNow);
		}
		else {
			dir = new File(mPathToStorage);
		}

		if (!dir.exists()) {
			Log.i(TAG, "does not exist, creating folder " + dir.getName() + "...");
			if (!dir.mkdirs())
				Log.i(TAG, "failure on creating");
		}

		return dir;
	}

	private static File getLogFolder(String pathToStorage, Calendar rightNow){

		return new File(pathToStorage + rightNow.get(Calendar.YEAR)
				+get2DigitString(rightNow.get(Calendar.MONTH)+1)
				+get2DigitString(rightNow.get(Calendar.DAY_OF_MONTH))+"_"
				+get2DigitString(rightNow.get(Calendar.HOUR_OF_DAY))
				+get2DigitString(rightNow.get(Calendar.MINUTE))
				+get2DigitString(rightNow.get(Calendar.SECOND)));
	}

	public static File getLoggingDir() {

		return mLoggingDir;
	}
}
