package de.tum.lmt.texturerecognizerappas;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class CameraActivity extends Activity{
	private static final String TAG = CameraActivity.class.getSimpleName();
	
	//UI Elements
	private ImageButton mButtonCamera;
	private ImageView mFlash;
	private ImageView mIdealFrame;
	private FrameLayout mFrameLayout;

	private AlertDialog mProcessingDialog;

	private RotationTask.iOnRotatedListener mOnRotatedListener;

	private boolean mButtonCameraFirstClick = true;
	
	//Files and Paths
	private File mLoggingDir = MainActivity.getLoggingDir();
	private String mDataToSendPath = MainActivity.getLoggingDir().getAbsolutePath() + Constants.DATA_TO_SEND_FOLDER_NAME;
	private File mDataToSendDir;
	
	//Camera, Preview and Pictures
	private Camera mCamera;
	private int mDefaultCameraId;
	private boolean mDataReady = false;
	private PictureCallback mPicture;
	private int mNumberOfPicturesTaken = 0;
	private boolean mPictureTaken = false;
	private CameraPreview mPreview;
	private int mHeight;
	private int mWidth;
	private int mFrameWidth;
	private int mFrameHeight;
	
	private Handler mTimerHandler;
	
	private String mGapFiller;
	
	private Vibrator mVibrator;

	private boolean mDatabaseMode;
	private String mTextureName;
	private int mTextureNumber;

	private int mNumberOfClicks = 0;

	/**
	 * taken from http://stackoverflow.com/questions/3170691/how-to-get-current-memory-usage-in-android
	 * @return
	 */
	long getUsedMemory() {
		final Runtime runtime = Runtime.getRuntime();
		final long usedMemInMB=(runtime.totalMemory() - runtime.freeMemory()) / 1048576L;
		final long maxHeapSizeInMB=runtime.maxMemory() / 1048576L;
		Log.i(TAG, "Used in MB " + Long.toString(usedMemInMB) + ", total  " + Long.toString(maxHeapSizeInMB));
		return maxHeapSizeInMB;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		getUsedMemory();
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera);

		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		mDatabaseMode = sharedPrefs.getBoolean(Constants.PREF_KEY_MODE_SELECT, false);
		mTextureName = sharedPrefs.getString(Constants.PREF_KEY_TEXTURE_NAME, null);
		mTextureNumber = sharedPrefs.getInt(Constants.PREF_KEY_TEXTURE_NUMBER, 0);

		initializePictureCallback();

		mCamera = getCameraInstance();

		if(mCamera == null) {
			Toast.makeText(this, getString(R.string.message_open_failed), Toast.LENGTH_LONG).show();
		}

		setupCameraSimple();

		mDataToSendDir = new File(mDataToSendPath);

		if(!mDataToSendDir.exists()) {
			mDataToSendDir.mkdirs();
		}

		mFrameLayout = (FrameLayout) findViewById(R.id.framelayout_camera);

		mIdealFrame = (ImageView) findViewById(R.id.ideal_frame);

		//wait until FrameLayout is drawn, so that its dimensions can be read
		mFrameLayout.post(new Runnable() {
			@Override
			public void run() {

				ViewGroup.LayoutParams params = mIdealFrame.getLayoutParams();
				params.width = (int)Math.round(mFrameLayout.getWidth() * 0.6);
				params.height = (int)Math.round(mFrameLayout.getHeight() * 0.6);
				mIdealFrame.setLayoutParams(params);
				mIdealFrame.bringToFront();
			}
		});


		mButtonCamera = (ImageButton) findViewById(R.id.button_camera);
		mButtonCamera.bringToFront();
		mButtonCamera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (mNumberOfPicturesTaken < 2) {
					if (mCamera != null) {
						takePicture();
					}
				} else {
					startRecordingActivity();
					//startSensorLoggingActivty();
					finish();
				}
			}
		});

		mFlash = (ImageView) findViewById(R.id.flash);
		mFlash.bringToFront();
		mFlash.setVisibility(View.GONE);

		mOnRotatedListener = new RotationTask.iOnRotatedListener() {
			@Override
			public void onRotated() {

				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						mProcessingDialog.dismiss();

						mButtonCamera.setEnabled(true);
						mButtonCamera.setClickable(true);

						Resources resources = getApplicationContext().getResources();
						mButtonCamera.setImageDrawable(resources.getDrawable(R.drawable.start));

					}
				});

			}
		};

	}

	private void startSensorLoggingActivty() {
		Intent recordingActivity = new Intent(this, SensorLoggingActivity.class);
		startActivity(recordingActivity);
	}

	private void startRecordingActivity() {
		Intent recordingActivity = new Intent(this, RecordingActivity.class);
		startActivity(recordingActivity);
	}

	private void takePicture() {
		mCamera.takePicture(null, null, mPicture);
	}

	private void initializePictureCallback() {
		Log.i(TAG, "Calling initializePictureCallback");
		mPicture = new PictureCallback() {

			@Override
			public void onPictureTaken(byte[] data, Camera camera) {

				mButtonCamera.setClickable(false);
				mButtonCamera.setEnabled(false);

				//save the picture
				File pictureFile = getOutputMediaFile();
				if (pictureFile == null){
					Log.d(TAG, "Error creating media file, check storage permissions "); //e.getMessage
					return;
				}

				try {
					FileOutputStream fos = new FileOutputStream(pictureFile);
					fos.write(data);
					fos.close();
				} catch (FileNotFoundException e) {
					Log.d(TAG, "File not found: " + e.getMessage());
				} catch (IOException e) {
					Log.d(TAG, "Error accessing file: " + e.getMessage());
				}

				pictureFile = null;

				mPictureTaken = true;
				mNumberOfPicturesTaken++;

				//also save the picture without Flash in the Data To Send Folder
				if(mNumberOfPicturesTaken == 1) {
					/*File pictureFileToSend = getOutputMediaFileToSend();
						if (pictureFile == null){
							Log.d(TAG, "Error creating media file, check storage permissions "); //e.getMessage
							return;
						}

						try {
							FileOutputStream fos = new FileOutputStream(pictureFileToSend);
							fos.write(data);
							fos.close();
						} catch (FileNotFoundException e) {
							Log.d(TAG, "File not found: " + e.getMessage());
						} catch (IOException e) {
							Log.d(TAG, "Error accessing file: " + e.getMessage());
						}*/
				}

				mCamera.startPreview();

				if((mNumberOfPicturesTaken%2) == 1) {

					mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

					mVibrator.vibrate(200);

					activateFlash();

					mTimerHandler = new Handler();

					mTimerHandler.postDelayed(new Runnable() {
						@Override
						public void run() {
							takePicture();
						}
					}, Constants.DURATION_BREAK_PICTURE);

				} else if(((mNumberOfPicturesTaken%2) == 0)) {

					mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

					mVibrator.vibrate(200);

					String pathToPicture = null;
					String pathToPictureFlash = null;

					if(mDatabaseMode) {
						//first two lines for image database
						/*pathToPicture = mLoggingDir.getAbsolutePath() + File.separator + mTextureName + "_" + mTextureNumber + "_" + Constants.CAMERA_NO_FLASH_FILENAME + "Img" + (mNumberOfPicturesTaken-2) + Constants.JPG_FILE_EXTENSION;
						pathToPictureFlash = mLoggingDir.getAbsolutePath() + File.separator + mTextureName + "_" + mTextureNumber + "_" + Constants.CAMERA_FLASH_FILENAME + "Img" + (mNumberOfPicturesTaken-2) + Constants.JPG_FILE_EXTENSION;*/
						pathToPicture = mLoggingDir.getAbsolutePath() + File.separator + mTextureName + "_" + mTextureNumber + "_" + Constants.CAMERA_NO_FLASH_FILENAME + Constants.JPG_FILE_EXTENSION;
						pathToPictureFlash = mLoggingDir.getAbsolutePath() + File.separator + mTextureName + "_" + mTextureNumber + "_" + Constants.CAMERA_FLASH_FILENAME + Constants.JPG_FILE_EXTENSION;
					} else {
						pathToPicture = mLoggingDir.getAbsolutePath() + File.separator + Constants.CAMERA_NO_FLASH_FILENAME + Constants.JPG_FILE_EXTENSION;
						pathToPictureFlash = mLoggingDir.getAbsolutePath() + File.separator + Constants.CAMERA_FLASH_FILENAME + Constants.JPG_FILE_EXTENSION;
					}

					showProcessingDialog();

					RotationTask rotationTask = new RotationTask(mOnRotatedListener);
					rotationTask.execute(pathToPicture, pathToPictureFlash);

					//rotatePicture(pathToPicture);
					//rotatePicture(pathToPictureFlash);
					//						rotatePicture(dataToSendDir + "/" + pictureDisplayFilename + fileExtension);

					releaseCamera();
					if (mPreview != null) {
						mPreview = null;
					}

				}
			}
		};
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(!mDataReady) {
			mCamera = getCameraInstance();

			if(mCamera == null) {
				Toast.makeText(this, getString(R.string.message_open_failed), Toast.LENGTH_LONG).show();
			}

			setupCameraSimple();//setupCamera();
		}
	}

	protected void showCancelDialog(String gapFiller) {
		DialogCancelFragment cancelDialog = DialogCancelFragment.newInstanceOf(gapFiller, mPictureTaken);
		cancelDialog.show(getFragmentManager(), "DialogSensorCheckFragment");
	}

	protected void showContinueDialog() {
		DialogContinueFragment continueDialog = DialogContinueFragment.newInstance("camera");
		continueDialog.show(getFragmentManager(), "DialogContinueFragment");
	}

	@SuppressWarnings("deprecation")
	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open();
		}
		catch (Exception e){

		}
		return c;
	}

	private Camera.Size getTheRightSize(Camera.Parameters params) {
		List<Camera.Size> sizesPicture = params.getSupportedPictureSizes();


		int minNumberPixels = 999999999;
		Camera.Size bestSize = null;
		for(Camera.Size size : sizesPicture) {
			if (size.width>Constants.DESIRED_CAMERA_IMAGE_WIDTH && size.height>Constants.DESIRED_IMAGE_CAMERA_HEIGHT) {
				int currentNumPixels = size.width * size.height;
				if (currentNumPixels<minNumberPixels) {
					minNumberPixels = currentNumPixels;
					bestSize = size;
				}
			}
		}
		if (bestSize == null) { // just get the best size
			int maxNumberPixels = 0;
			for(Camera.Size size : sizesPicture) {
				Log.i(TAG, "Sizes " + size.width + "x" + size.height);
				int currentNumPixels = size.width * size.height;
				if (currentNumPixels>=maxNumberPixels) {
					bestSize = size;
					maxNumberPixels = currentNumPixels;
				}
			}
		}
		//int max_size = 4096;//openglrender throws an exception, in case we load a bitmap with size bigger than 4096

		Log.i(TAG, "width x height: " + bestSize.width + " x " + bestSize.height);

		return bestSize;
	}

	protected void setupCameraSimple() {
		if(mCamera != null){
			Camera.Parameters params = mCamera.getParameters();
			Log.d(TAG, "getSupportedPreviewSizes()");
			//List<android.hardware.Camera.Size> sizes = params.getSupportedPreviewSizes();
			List<Camera.Size> sizes = params.getSupportedPictureSizes();

			if (sizes != null) {

				Camera.Size bestSize = getTheRightSize(params);

				params.setPictureSize(bestSize.width, bestSize.height);

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
					params.setRecordingHint(true);

				if (params.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
					params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
				}

				try {
					mCamera.setParameters(params);
				}
				catch (Exception e) {
					Log.e(TAG, "Catching exception in camera parameters");
				}

				params = mCamera.getParameters();

				mFrameWidth = params.getPreviewSize().width;
				mFrameHeight = params.getPreviewSize().height;

				Log.i(TAG, "frame " + mFrameWidth + "x" + mFrameHeight);
			}

			int result = getSurfaceOrientation();
			Log.i(TAG, "camera orientation " + result);
			if( mCamera != null)
				mCamera.setDisplayOrientation(result);

			mPreview = new CameraPreview(this, mCamera);
			FrameLayout preview = (FrameLayout) findViewById(R.id.framelayout_camera);

			preview.addView(mPreview);

			mDataReady = true;

		}
	}

	public void activateFlash() {
		if(mCamera != null) {
			Camera.Parameters params;
			params = mCamera.getParameters();

			if(params.getSupportedFlashModes().contains(Camera.Parameters.FLASH_MODE_TORCH)) {
				params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
			}
			mCamera.setParameters(params);
			mFlash.setVisibility(View.VISIBLE);
		}
	}

	@Override
	protected void onPause() {

		super.onPause();
		releaseCamera();
		if(mPreview != null)
			mPreview = null;

		// release background resource
		getUsedMemory();

		mProcessingDialog.dismiss();
	}

	@Override
	protected void onStop() {
		super.onStop();
		releaseCamera();
		if(mPreview != null)
			mPreview = null;
	}

	private void releaseCamera() {
		if(mCamera != null) {
			mCamera.release();
			mCamera = null;
		}
	}

	private File getOutputMediaFile() {
		File mediaFile = null;
		if(mNumberOfPicturesTaken%2 == 0) {
			if(mDatabaseMode) {
				//first line for image database
				//mediaFile = new File(mLoggingDir.getPath() + File.separator + mTextureName + "_" + mTextureNumber + "_" + Constants.CAMERA_NO_FLASH_FILENAME + "Img" + mNumberOfPicturesTaken + Constants.JPG_FILE_EXTENSION);
				mediaFile = new File(mLoggingDir.getPath() + File.separator + mTextureName + "_" + mTextureNumber + "_" + Constants.CAMERA_NO_FLASH_FILENAME + Constants.JPG_FILE_EXTENSION);
			} else {
				mediaFile = new File(mLoggingDir.getPath() + File.separator + Constants.CAMERA_NO_FLASH_FILENAME + Constants.JPG_FILE_EXTENSION);
			}
		}
		else if(mNumberOfPicturesTaken%2 == 1) {
			if(mDatabaseMode) {
				//first line for image database
				//mediaFile = new File(mLoggingDir.getPath() + File.separator + mTextureName + "_" + mTextureNumber + "_" + Constants.CAMERA_FLASH_FILENAME + "Img" + (mNumberOfPicturesTaken-1) + Constants.JPG_FILE_EXTENSION);
				mediaFile = new File(mLoggingDir.getPath() + File.separator + mTextureName + "_" + mTextureNumber + "_" + Constants.CAMERA_FLASH_FILENAME + Constants.JPG_FILE_EXTENSION);
			} else {
				mediaFile = new File(mLoggingDir.getPath() + File.separator + Constants.CAMERA_FLASH_FILENAME + Constants.JPG_FILE_EXTENSION);
			}		}

		return mediaFile;
	}

	@SuppressWarnings("deprecation")
	public int getSurfaceOrientation() {
		// Adapt rotation to current orientation
		Camera.CameraInfo info = new Camera.CameraInfo();
		Camera.getCameraInfo( mDefaultCameraId, info);
		int rotation = this.getWindowManager().getDefaultDisplay().getRotation();
		int degrees = 0;
		switch (rotation) {
		case Surface.ROTATION_0: degrees = 0; break;
		case Surface.ROTATION_90: degrees = 90; break;
		case Surface.ROTATION_180: degrees = 180; break;
		case Surface.ROTATION_270: degrees = 270; break;
		}

		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360;  // compensate the mirror
		} else {  // back-facing
			result = (info.orientation - degrees + 360) % 360;
		}

		return result;
	}

	protected void showProcessingDialog() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		LayoutInflater inflater = this.getLayoutInflater();
		View content = inflater.inflate(R.layout.dialog_processing, null);

		builder.setView(content);

		mProcessingDialog = builder.create();
		mProcessingDialog = builder.show();

		mProcessingDialog.setCanceledOnTouchOutside(false);
	}
}
