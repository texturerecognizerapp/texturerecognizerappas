package de.tum.lmt.texturerecognizerappas;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by Kev94 on 27.03.2016.
 */
public class DialogTrainFragment extends DialogFragment {

    String mLoggingDir;
    String mClassificationMethod;

    Button mButtonOk;
    ProgressBar mProgressbar;
    TextView mTextView;

    //Task listener
    private TrainTask.iOnTrainedListener mOnTrainedListener;

    //http://developer.android.com/reference/android/app/DialogFragment.html
    static DialogTrainFragment newInstance(String loggingDir, String clasificationMethod) {
        DialogTrainFragment f = new DialogTrainFragment();

        Bundle args = new Bundle();
        args.putString("loggingDir", loggingDir);
        args.putString("classificationMethod" , clasificationMethod);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View content = inflater.inflate(R.layout.dialog_train, null);

        mLoggingDir = getArguments().getString("loggingDir");
        mClassificationMethod = getArguments().getString("classificationMethod");

        mProgressbar = (ProgressBar) content.findViewById(R.id.progressbar_train);
        mTextView = (TextView) content.findViewById(R.id.textview_train);

        mButtonOk = (Button) content.findViewById(R.id.button_ok_train);
        mButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        builder.setView(content);

        mOnTrainedListener = new TrainTask.iOnTrainedListener() {
			@Override
			public void onTrained() {
                mProgressbar.setVisibility(View.GONE);
                mTextView.setText(R.string.textview_train_done);
                mButtonOk.setEnabled(true);
			}
		};

        TrainTask trainTask = new TrainTask(mLoggingDir, mClassificationMethod, mOnTrainedListener);
        trainTask.execute();

        return builder.create();
    }
}
