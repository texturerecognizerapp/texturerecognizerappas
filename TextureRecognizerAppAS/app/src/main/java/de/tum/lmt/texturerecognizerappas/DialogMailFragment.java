package de.tum.lmt.texturerecognizerappas;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

public class DialogMailFragment extends DialogFragment {

	private static final String TAG = DialogMailFragment.class.getSimpleName();

	private String mBaseDirPath;

	private FileHandler mFileHandler;

	//http://developer.android.com/reference/android/app/DialogFragment.html
	static DialogMailFragment newInstance(String dir) {
		DialogMailFragment f = new DialogMailFragment();

		Bundle args = new Bundle();
		args.putString("dir", dir);
		f.setArguments(args);

		return f;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		mBaseDirPath = getArguments().getString("dir");

		mFileHandler = new FileHandler();
		
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View content = inflater.inflate(R.layout.dialog_mail, null);
		
		final EditText edittextAddress = (EditText) content.findViewById(R.id.edittext_dialog_mail_address);
		final EditText edittextSubject = (EditText) content.findViewById(R.id.edittext_dialog_mail_subject);
		final EditText edittextText = (EditText) content.findViewById(R.id.edittext_dialog_mail_text);
		
		builder.setView(content)
	   		   .setPositiveButton(R.string.send, new DialogInterface.OnClickListener() {
	   		
	   			   @Override
	   			   public void onClick(DialogInterface dialog, int which) {

	   				   createZipFile();

	   				   File dir = new File(mBaseDirPath + File.separator + Constants.ZIP_FILE_NAME);
	   				   
	   				   Intent intent_email = new Intent(Intent.ACTION_SEND);
	   				   intent_email.setType("plain/text");
	   				   intent_email.putExtra(Intent.EXTRA_EMAIL, edittextAddress.getText().toString());
	   				   intent_email.putExtra(Intent.EXTRA_SUBJECT, edittextSubject.getText().toString());
	   				   intent_email.putExtra(Intent.EXTRA_TEXT, edittextText.getText().toString());
	   				   intent_email.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(dir));
	   				   
	   				   startActivity(Intent.createChooser(intent_email, "Send email..."));

					   //deleteFiles();
					   //dir.delete();
	   			   }
	   		   })
	   		   .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
		
	   			   @Override
	   			   public void onClick(DialogInterface dialog, int which) {

	   			   }
	   		   });

		return builder.create();
	}
	
	//from http://examples.javacodegeeks.com/core-java/util/zip/create-zip-file-from-directory-with-zipoutputstream/
	
	public void createZipFile() {
		
		File zipFile = new File(mBaseDirPath + File.separator + Constants.ZIP_FILE_NAME);
		
		if(!zipFile.getParentFile().exists()) {
			zipFile.getParentFile().mkdirs();
		}
		
		String zipFilePath = zipFile.getAbsolutePath();

		try {
			// create byte buffer
			byte[] buffer = new byte[1024];

			FileOutputStream fos = new FileOutputStream(zipFilePath);

			ZipOutputStream zos = new ZipOutputStream(fos);

			List<File> srcFiles = mFileHandler.getFileListTwoLevels(new File(mBaseDirPath));

			int indexToRemove = -1;

			for(int i = 0; i < srcFiles.size(); i++) {
				if(srcFiles.get(i).getName().contains(".zip")) {
					indexToRemove = i;
				}
			}

			if(indexToRemove != -1) {
				srcFiles.remove(indexToRemove);
			}

			for (File file:srcFiles) {

				if(!file.isDirectory()) {
					Log.i(TAG, "Adding file: " + file.getName());
					FileInputStream fis = new FileInputStream(file);

					// begin writing a new ZIP entry, positions the stream to the start of the entry data
					zos.putNextEntry(new ZipEntry(file.getName()));

					int length;
					while ((length = fis.read(buffer)) > 0) {
						zos.write(buffer, 0, length);
					}

					zos.closeEntry();

					// close the InputStream

					fis.close();
				}
				else {
					for(File deeperFile : file.listFiles()) {
						Log.i(TAG, "Adding file: " + deeperFile.getName());
						FileInputStream fis = new FileInputStream(deeperFile);

						// begin writing a new ZIP entry, positions the stream to the start of the entry data
						zos.putNextEntry(new ZipEntry(deeperFile.getName()));

						int length;
						while ((length = fis.read(buffer)) > 0) {
							zos.write(buffer, 0, length);
						}

						zos.closeEntry();

						// close the InputStream

						fis.close();
					}
				}
			}

			// close the ZipOutputStream
			zos.close();

		}

		catch (IOException ioe) {

			System.out.println("Error creating zip file" + ioe);

		}

	}

	//don't know what this was for
	/*public void deleteFiles() {
		
		//Not Sent Folder
		File filesToDelete = new File(MainActivity.getLoggingDir().getAbsolutePath());
		File[] files = filesToDelete.listFiles();
		
		if(files != null) {
			for(File file : files) {
				if(!file.isDirectory()) {
				   file.delete();
			   }
			   else {
				   for(File child : file.listFiles()) {
					   child.delete();
				   }
				   file.delete();
			   }
		   }
		}
		
		filesToDelete.delete();
	}*/

}
