package de.tum.lmt.texturerecognizerappas;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by Kev94 on 19.05.2016.
 */
public class DialogDatabaseSettingsHintFragment extends DialogFragment {

    public interface iOnDatabaseSettingsHintDialogFinished {
        public void onDatabaseSettingsHintDialogFinished(String buttonClicked);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(getString(R.string.database_settings_hint))
                .setPositiveButton(R.string.settings, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        iOnDatabaseSettingsHintDialogFinished activity = (iOnDatabaseSettingsHintDialogFinished) getActivity();
                        activity.onDatabaseSettingsHintDialogFinished(getString(R.string.settings));
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        iOnDatabaseSettingsHintDialogFinished activity = (iOnDatabaseSettingsHintDialogFinished) getActivity();
                        activity.onDatabaseSettingsHintDialogFinished(getString(R.string.cancel));

                    }
                });

        return builder.create();
    }

}
