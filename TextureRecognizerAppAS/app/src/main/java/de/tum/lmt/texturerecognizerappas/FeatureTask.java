package de.tum.lmt.texturerecognizerappas;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kev94 on 21.03.2016.
 */
public class FeatureTask extends AsyncTask<Void, Void, OutOfMemoryError> {

    private iOnFeatureTaskFinishedListener mListener;

    private Context mContext;
    private Byte[] mRawSoundDataTap;
    private Byte[] mRawSoundDataMove;
    private SensorLog mAccelLogTap;
    private SensorLog mAccelLogMove;
    private List<SensorLog> mExternAccelLogListTap;
    private List<SensorLog> mExternAccelLogListMove;

    FeatureComputer mFeatureComputer;
    Features mFeatures;

    public FeatureTask(Context context, iOnFeatureTaskFinishedListener listener,
                       Byte[] rawSoundDataTap, Byte[] rawSoundDataMove, SensorLog accelLogTap, SensorLog accelLogMove,
                       List<SensorLog> externAccelLogListTap, List<SensorLog> externAccelLogListMove) {

        mContext = context;
        mRawSoundDataTap = rawSoundDataTap;
        mRawSoundDataMove = rawSoundDataMove;
        mAccelLogTap = accelLogTap;
        mAccelLogMove = accelLogMove;
        mExternAccelLogListTap = externAccelLogListTap;
        mExternAccelLogListMove = externAccelLogListMove;

        mListener = listener;

    }

    public interface iOnFeatureTaskFinishedListener {
        public void onFeatureTaskFinished(Features features);
    }

    @Override
    protected OutOfMemoryError doInBackground(Void... params) {

        mFeatureComputer = new FeatureComputer(mContext, mRawSoundDataTap, mRawSoundDataMove, mAccelLogTap, mAccelLogMove, mExternAccelLogListTap, mExternAccelLogListMove);

        mFeatures = mFeatureComputer.computeFeatures();

        if(mFeatures == null) {
            Log.e("FeatureTask", "features null");
        }

        Log.i("FeatureTask", "MFCC1: " + mFeatures.getMFCC1());

        return null;
    }

    @Override
    protected void onPostExecute(OutOfMemoryError Result) {

        if (Result != null) {
            Toast.makeText(mContext, Result.getMessage(), Toast.LENGTH_LONG).show();
        } else if(mListener != null) {
            if(mFeatures == null) {
                Log.e("FeatureTask", "features on post execute null");
            }
            mListener.onFeatureTaskFinished(mFeatures);

        } else {
            Log.e("FeatureTask", "listener null");
        }
    }
}
