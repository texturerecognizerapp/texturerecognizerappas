package de.tum.lmt.texturerecognizerappas;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

public class FeatureComputer {

    private Context mContext;

    private String mSensorsPath;
    private String mFeaturePath;
    private String mBaseDirPath;

    private String mSurfacePictureNoFlashPath;
    private String mSurfacePictureFlashPath;

    private boolean mDatabaseMode;
    private String mTextureName;
    private int mTextureNumber;

    private HashMap<String, Boolean> mFeaturePreferences;
    private boolean mComputeImageFeatures;
    private boolean mComputeSoundFeatures;
    private boolean mComputeSoundTapFeatures;
    private boolean mComputeSoundMoveFeatures;
    private boolean mComputeAccelFeatures;
    private boolean mComputeInternAccelFeatures;
    private boolean mComputeExternAccelFeatures;

    private Byte[] mRawSoundDataTap;
    private Byte[] mRawSoundDataMove;
    private List<Double> mSoundDataTap;
    private List<Double> mSoundDataSingleTap;
    private List<Double> mSoundDataMove;
    private List<float[]> mAccelDataTap;
    private List<float[]> mAccelDataMove;
    private List<Long> mAccelTimestampsTap;
    private List<Long> mAccelTimestampsMove;
    private List<List<Float>> mFilteredAccelDataTap;
    private List<List<Float>> mFilteredAccelDataMove;
    private List<List<float[]>> mExternAccelDataListTap;
    private List<List<float[]>> mExternAccelDataListMove;
    private List<Float> mFilteredExternAccelDataTap;
    private List<Float> mFilteredExternAccelDataMove;

    private List<Float> mVelocityDataMove;

    boolean mExternAccelDataTapAvailable;
    boolean mExternAccelDataMoveAvailable;

    private Features mFeatures;
    private Features mMinFeatures;
    private Features mMaxFeatures;
    private FileHandler mFileHandler;
    private MathUtil mMathUtil;
    private BitmapOperations mBmpOps;

    private boolean computeSoundFeatures = true;
    private boolean computeAccelerationSpectralSum = true;
    private boolean computeZeroCrossingRate = true;
    private boolean computeDominantColorAndSaturation = true;
    private boolean mSendToTpad = false;

    public FeatureComputer(Context context, Byte[] rawSoundDataTap, Byte[] rawSoundDataMove, SensorLog accelLogTap, SensorLog accelLogMove,
                           List<SensorLog> externAccelLogListTap, List<SensorLog> externAccelLogListMove) {

        mContext = context;

        mFeatures = new Features();
        mFileHandler = new FileHandler();
        mMathUtil = new MathUtil();
        mBmpOps = new BitmapOperations();

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        mTextureName = sharedPrefs.getString(Constants.PREF_KEY_TEXTURE_NAME, null);
        mTextureNumber = sharedPrefs.getInt(Constants.PREF_KEY_TEXTURE_NUMBER, 0);
        mDatabaseMode = sharedPrefs.getBoolean(Constants.PREF_KEY_MODE_SELECT, false);
        mBaseDirPath = sharedPrefs.getString(Constants.PREF_KEY_LOGGING_DIR, null);
        mSensorsPath = mBaseDirPath + File.separator + Constants.SENSOR_SINGLE_RECORDING_LOGGING_FOLDER_NAME + File.separator;
        mFeaturePath = mBaseDirPath + File.separator + Constants.DATA_TO_SEND_FOLDER_NAME + File.separator;

        File dataToSendDir = new File(mFeaturePath);

        if (!dataToSendDir.exists()) {
            if (!dataToSendDir.mkdirs()) {
                Log.e("FeatureComputer", "not able to create dataToSendDir");
            }
        }

        mSurfacePictureNoFlashPath = mBaseDirPath + Constants.BITMAP_NO_FLASH_FILENAME;
        mSurfacePictureFlashPath = mBaseDirPath + Constants.BITMAP_FLASH_FILENAME;

        mRawSoundDataTap = rawSoundDataTap;
        mRawSoundDataMove = rawSoundDataMove;
        mSoundDataSingleTap = new ArrayList<>();

        mAccelDataTap = accelLogTap.getValues();
        mAccelDataMove = accelLogMove.getValues();
        mAccelTimestampsTap = accelLogTap.getTimestamps();
        mAccelTimestampsMove = accelLogMove.getTimestamps();

        mExternAccelDataListTap = new ArrayList<>();
        mExternAccelDataListMove = new ArrayList<>();

        mFilteredExternAccelDataTap = new ArrayList<>();
        mFilteredExternAccelDataMove = new ArrayList<>();

        //if values isAvailable
        if((externAccelLogListTap != null) && (externAccelLogListTap.get(0).getValues().size() > 0)) {

            for (int i = 0; i < externAccelLogListTap.size(); i++) {
                mExternAccelDataListTap.add(externAccelLogListTap.get(i).getValues());
            }

            mExternAccelDataTapAvailable = true;
        } else {
            mExternAccelDataTapAvailable = false;
        }

        //if values isAvailable
        if((externAccelLogListMove != null) && (externAccelLogListMove.get(0).getValues().size() > 0)) {

            for (int i = 0; i < externAccelLogListMove.size(); i++) {
                mExternAccelDataListMove.add(externAccelLogListMove.get(i).getValues());
            }

            mExternAccelDataMoveAvailable = true;
        } else {
            mExternAccelDataMoveAvailable = false;
        }

        mVelocityDataMove = new ArrayList<>();

    }

    public Features computeFeatures() {

        getPreferences();

        getMinMaxFeatureValues();

        loadWaveFiles();

        // this part is important for rendering the texture with the TPAD; don't remove!
        //if(mSendToTpad) {
            initWeights();
            computeMacroAmplitude();
            computeMicroAmplitude();
            computeGlossinessAmplitude();
            computeSoundNoiseDistribution();
            calculateWeightMicroRoughness();
            macroImage();
            //// TODO: 14.10.2016 check for IndexOutOfBounds and make method more stable before uncommenting the next line
            extractImpactSoundData();
        //}

        /**the following ifs are always true as long as getPreferences is commented*/

        if(mComputeImageFeatures) {
            computeImageFeatures();
        } else {
            mFeatures.setGlossiness(Double.NaN);
            mFeatures.setImageCentroid(Double.NaN);
            mFeatures.setImageContrast(Double.NaN);
            mFeatures.setDominantColor(Double.NaN);
            mFeatures.setDominantSaturation(Double.NaN);
            mFeatures.setImageFrequencyBinCount1(Double.NaN);
            mFeatures.setImageSpectralCentroid(Double.NaN);
        }

        if(mComputeSoundFeatures) {
            computeSoundFeatures();
        } else {
            mFeatures.setFeature(Constants.SOUND_SIGNAL_ENERGY_TAP, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_KURTOSIS_TAP, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_CENTROID_TAP, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_ROLL_OFF_TAP, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_SUM_TAP, Double.NaN);

            mFeatures.setFeature(Constants.SOUND_ZERO_CROSSING_RATE, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_NOISE_DISTRIBUTION, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_SIGNAL_ENERGY, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_SPIKINESS, Double.NaN);
            mFeatures.setFeature(Constants.MFCC1, Double.NaN);
            mFeatures.setFeature(Constants.MFCC2, Double.NaN);
            mFeatures.setFeature(Constants.MFCC5, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_CENTROID, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_ENTROPY, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_ROLL_OFF, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_SUM, Double.NaN);
        }

        if(mComputeAccelFeatures) {
            computeAccelerationFeatures();
        }

        renormalizeWeights();

        //use min and max features from the database to normalize features computed by the Smartphone
        normalizeFeatures();

        printAllFeatures();

        return mFeatures;
    }

    public Features computeFeaturesold() {

        /*initWeights();

        preprocessSensorData();

        getMinMaxFeatureValues();

        extractImpactSoundData();

        computeVelocityFeature();

        computeImageFeatures();

        computeSaturationAndDominantColor();

        computeMacroAmplitude();
        computeMicroAmplitude();
        computeGlossinessAmplitude();

        computeSoundNoiseDistribution();

        computeZeroCrossingRate();

        computeKurtosis();

        //computeMFCCs();

        calculateWeightMacroRoughness();
        calculateWeightMicroRoughness();

        //empty, weight glossiness is set in computeImageFeatures()
        calculateWeightGlossiness();

        //for testing
        computeSignalPowerAccel();
        computeSignalEnergy();

        //todo remove; only needed for TPad
        //not necessary -> renormalize weights is empty now
        if(!mDatabaseMode) {
            renormalizeWeights();
        }

        //use min and max features from the database to normalize features computed by the Smartphone
        normalizeFeatures();

        printAllFeatures();*/

        return mFeatures;
    }

    private void loadWaveFiles() {

        String soundDataTapPath = mBaseDirPath + File.separator + Constants.AUDIO_FILENAME_ORIG + Constants.STEP_STRING_TAP + Constants.AUDIO_FILE_EXTENSION;
        String soundDataMovePath = mBaseDirPath + File.separator + Constants.AUDIO_FILENAME_ORIG + Constants.STEP_STRING_MOVE + Constants.AUDIO_FILE_EXTENSION;

        mSoundDataTap = mFileHandler.loadWaveFile(soundDataTapPath);
        mSoundDataMove = mFileHandler.loadWaveFile(soundDataMovePath);

        Log.i("FeatureComputer", mBaseDirPath + Constants.AUDIO_FILENAME_ORIG);
        Log.i("FeatureComputer", "soundDataMove size: " + mSoundDataMove.size());

    }

    private void initWeights() {

        Log.i("Features", "initWeights()");

        mFeatures.setWeightMacro(0.4);
        mFeatures.setWeightMicro(0.4);
        mFeatures.setWeightGlossiness(0.2);
    }

    private void preprocessSensorData() {

        Log.i("Features", "preprocessSensorData()");

        if(mComputeInternAccelFeatures) {
            mFilteredAccelDataMove = eliminateHandMovement(mAccelDataMove, 10, 5);

            if (mDatabaseMode) {
                save3DimData(mFilteredAccelDataMove, mTextureName + "_" + mTextureNumber + "_" + Constants.FILTERED_ACCEL_FILENAME);
            } else {
                save3DimData(mFilteredAccelDataMove, Constants.FILTERED_ACCEL_FILENAME);
            }
        }

        //eliminate Handmovement for extern accel data and concatenate the separate lists
        if(mExternAccelDataMoveAvailable && mComputeExternAccelFeatures) {

            List<Float> tmpList;

            for(int i = 0; i < mExternAccelDataListMove.size(); i++) {

                //extern accel -> z axis -> last parameter is 2
                tmpList = eliminateHandMovement(mExternAccelDataListMove.get(i), 200, 100, 2);

                for(int j = (tmpList.size()-1); j >= 0; j--) {
                    mFilteredExternAccelDataMove.add(tmpList.get(j));
                }
            }

            if(mDatabaseMode) {
                save1DimData(mFilteredExternAccelDataMove, mTextureName + "_" + mTextureNumber + "_" + Constants.FILTERED_EXTERN_ACCEL_FILENAME);
            } else {
                save1DimData(mFilteredExternAccelDataMove, Constants.FILTERED_EXTERN_ACCEL_FILENAME);
            }
        }
    }

    // // TODO: 12.10.2016 currently not used
    private void getPreferences() {

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);

        Set<String> featureSet = sharedPrefs.getStringSet(Constants.PREF_KEY_CLASSIfICATION_FEATURES, null);
        boolean sendToTpad = sharedPrefs.getBoolean(Constants.PREF_KEY_TPAD, false);

        mFeaturePreferences = new HashMap<>();
        mComputeAccelFeatures = false;
        mComputeInternAccelFeatures = false;
        mComputeExternAccelFeatures = false;
        mComputeImageFeatures = false;
        mComputeSoundFeatures = false;
        mComputeSoundTapFeatures = false;
        mComputeSoundMoveFeatures = false;

        if(featureSet != null) {

            for (String feature : Constants.FEATURE_SET) {
                if (featureSet.contains(feature)) {
                    Log.i("FeatureComputer", feature + " selected");
                    mFeaturePreferences.put(feature, true);

                    if((!mComputeImageFeatures) && feature.contains("Dominant") || feature.contains("Image") || feature.contains("Contrast") || feature.contains("Glossiness")) {
                        Log.i("FeatureComputer", "At least one Image Feature has to be computed");
                        mComputeImageFeatures = true;
                    }

                    if((!mComputeAccelFeatures) && feature.contains("Accel")) {
                        Log.i("FeatureComputer", "At least one Accel Features has to be computed");
                        mComputeAccelFeatures = true;
                    }

                    if((!mComputeInternAccelFeatures) && feature.contains("Accel") && !feature.contains("Extern")) {
                        Log.i("FeatureComputer", "At least one internal Accel Features has to be computed");
                        mComputeInternAccelFeatures = true;
                    }

                    if((!mComputeInternAccelFeatures) && feature.contains("Accel") && feature.contains("Extern")) {
                        Log.i("FeatureComputer", "At least one external Accel Features has to be computed");
                        mComputeExternAccelFeatures = true;
                    }

                    if((!mComputeSoundFeatures) && feature.contains("Sound") || feature.contains("MFCC")) {
                        Log.i("FeatureComputer", "At least one Sound Feature has to be computed");
                        mComputeSoundFeatures = true;
                    }

                    if((!mComputeSoundTapFeatures) && feature.contains("Sound") && feature.contains("Tap")) {
                        Log.i("FeatureComputer", "At least one Sound Tap Feature has to be computed");
                        mComputeSoundTapFeatures = true;
                    }

                    if(!mComputeSoundMoveFeatures && (feature.contains("Sound") || feature.contains("MFCC")) && !feature.contains("Tap")) {
                        Log.i("FeatureComputer", "At least one Sound Move Feature has to be computed");
                        mComputeSoundMoveFeatures = true;
                    }

                } else {
                    mFeaturePreferences.put(feature, false);
                }
            }
        }

        if(sendToTpad) {
            mSendToTpad = true;
        }
    }

    private void getMinMaxFeatureValues() {

        JSONHelper jsonHelper = new JSONHelper();

        Features[] minMaxFeatures = jsonHelper.readMinMaxFeatures(new File(Constants.DATABASE_PATH + Constants.JSON_MIN_MAX_FILENAME));

        mMinFeatures = minMaxFeatures[0];
        mMaxFeatures = minMaxFeatures[1];
        Log.i("FeatureComputer", "MFCC1Min " + mMinFeatures.getMFCC1());
        Log.i("FeatureComputer", "MFCC1Max " + mMaxFeatures.getMFCC1());
        Log.i("FeatureComputer", "MFCC2Min " + mMinFeatures.getMFCC2());
        Log.i("FeatureComputer", "MFCC2Max " + mMaxFeatures.getMFCC2());
        Log.i("FeatureComputer", "MFCC5Min " + mMinFeatures.getMFCC5());
        Log.i("FeatureComputer", "MFCC5Max " + mMaxFeatures.getMFCC5());
        Log.i("FeatureComputer", "DominantColorMin " + mMinFeatures.getDominantColor());
        Log.i("FeatureComputer", "DominantColorMax " + mMaxFeatures.getDominantColor());
        Log.i("FeatureComputer", "DominantSaturationMin " + mMinFeatures.getDominantSaturation());
        Log.i("FeatureComputer", "DominantSaturationMax " + mMaxFeatures.getDominantSaturation());

    }

    private List<List<Float>> eliminateHandMovement(List<float[]> data, int length, int shift) {

        List<float[]> movingAverage = new ArrayList<>();
        List<List<Float>> filteredData = new ArrayList<>();

        float[] sum = new float[]{0,0,0};
        float[] average = new float[3];

        for(int i = (length-1); i < data.size(); i++) {

            sum[0] = 0;
            sum[1] = 0;
            sum[2] = 0;

            for(int j = 0; j < length; j++) {

                sum[0] += data.get(i-j)[0];
                sum[1] += data.get(i-j)[1];
                sum[2] += data.get(i-j)[2];
            }

            average[0] = sum[0]/length;
            average[1] = sum[1]/length;
            average[2] = sum[2]/length;

            //Log.i("FeatureComputer", "average[0]: " + average[0] + ", average[1}: " + average[1] + ", average[2]: " + average[2]);

            movingAverage.add((i - length + 1), new float[]{average[0], average[1], average[2]});
        }

        save1DimData2(movingAverage, "movavg.txt");

        List<Float> tmp_x = new ArrayList<>();
        List<Float> tmp_y = new ArrayList<>();
        List<Float> tmp_z = new ArrayList<>();

        float newValue_X;
        float newValue_Y;
        float newValue_Z;

        for(int i = 0; i < (data.size() - length); i++) {

            newValue_X = data.get(i+shift)[0] - movingAverage.get(i)[0];
            newValue_Y = data.get(i+shift)[1] - movingAverage.get(i)[1];
            newValue_Z = data.get(i+shift)[2] - movingAverage.get(i)[2];

            //Log.i("FeatureComputer", "data[" + (i+shift) + "]: " + newValue_Y);

            tmp_x.add(i, newValue_X);
            tmp_y.add(i, newValue_Y);
            tmp_z.add(i, newValue_Z);
        }

        filteredData.add(0, tmp_x);
        filteredData.add(1, tmp_y);
        filteredData.add(2, tmp_z);

        Log.i("hand", "original length: " + data.size() + ", new size: " + filteredData.get(0).size());

        return filteredData;
    }

    private List<Float> eliminateHandMovement(List<float[]> data, int length, int shift, int axis) {

        List<Float> movingAverage = new ArrayList<>();
        List<Float> filteredData = new ArrayList<>();

        float sum;

        for(int i = (length-1); i < data.size(); i++) {

            sum = 0;

            for(int j = 0; j < length; j++) {

                sum += data.get(i-j)[axis];

            }

            movingAverage.add((i - length + 1), sum/length);
        }

        float newValue;

        for(int k = 0; k < (data.size() - length); k++) {

            newValue = data.get(k + shift)[axis] - movingAverage.get(k);

            filteredData.add(k, newValue);
        }

        Log.i("hand", "original length: " + data.size() + ", new size: " + filteredData.size());

        return filteredData;
    }


    //// TODO: 12.10.2016 simplify with tapping data
    private void extractImpactSoundData() {

        byte[] soundImpactArray = toPrimitives(mRawSoundDataMove);

        int fixedDuration = (int)Math.pow(2,13); // approx. 200ms

        double maxImpact = 0;
        int indexMaxImpact = 1;

        for(int i=(int)(Constants.RECORDER_SAMPLING_RATE * 0.5f); i<mSoundDataTap.size(); i++) {
            if(Math.abs(mSoundDataTap.get(i)) > maxImpact) {
                maxImpact = mSoundDataTap.get(i);
                indexMaxImpact = i;
            }
        }

        Log.i("FeatureComputer", "maxIndexImpact: " + indexMaxImpact);

        if(indexMaxImpact < fixedDuration) {
            indexMaxImpact = fixedDuration;
        } else if(indexMaxImpact > (mSoundDataTap.size()) - fixedDuration) {
            indexMaxImpact = mSoundDataTap.size() - fixedDuration;
        }

        byte[] impactData = new byte[2*fixedDuration+1];
        for(int i=-fixedDuration;i<fixedDuration;i++)
        {
            impactData[i+fixedDuration] = soundImpactArray[indexMaxImpact + i]; // start a little bit to the left of the maximum
            mSoundDataSingleTap.add(mSoundDataTap.get(indexMaxImpact + i));
        }

        String pathToFile = "NULL";
        if(mDatabaseMode) {
            pathToFile = mFeaturePath + File.separator + mTextureName + "_" + mTextureNumber + "_" + Constants.AUDIO_FILENAME_IMPACT + Constants.AUDIO_FILE_EXTENSION;
        } else {
            pathToFile = mFeaturePath + File.separator + Constants.AUDIO_FILENAME_IMPACT + Constants.AUDIO_FILE_EXTENSION;
        }

        AudioRecorderWAV recorder = new AudioRecorderWAV(mContext);
        recorder.writeWaveFile(impactData, pathToFile, 1, impactData.length);
    }

    byte[] toPrimitives(Byte[] oBytes) {

        byte[] bytes = new byte[oBytes.length];
        for(int i = 0; i < oBytes.length; i++){
            bytes[i] = oBytes[i];
        }
        return bytes;

    }

    private void computeVelocityFeature() {

        //// TODO: 26.02.2016 draft; test and improve
        int beginMovementIndex = (int) 0.5 * Constants.SAMPLE_RATE_ACCEL;

        float v_x = 0;
        float v_z = 0;

        float dv_x;
        float dv_z;

        for(int i = beginMovementIndex+1; i < mFilteredAccelDataMove.get(0).size(); i++) {

            dv_x = ((mFilteredAccelDataMove.get(0).get(i) + mFilteredAccelDataMove.get(0).get(i-1)) / 2) * (mAccelTimestampsMove.get(i+6) - mAccelTimestampsMove.get(i+5)) * Constants.NS2S;
            dv_z = ((mFilteredAccelDataMove.get(2).get(i) + mFilteredAccelDataMove.get(2).get(i-1)) / 2) * (mAccelTimestampsMove.get(i+6) - mAccelTimestampsMove.get(i+5)) * Constants.NS2S;

            v_x += dv_x;
            v_z += dv_z;

            mVelocityDataMove.add((float) Math.sqrt((v_x * v_x) + (v_z * v_z)));
        }

        if(mDatabaseMode) {
            save1DimData(mVelocityDataMove, mTextureName + "_" + mTextureNumber + "_" + Constants.VELOCITY_FILENAME);
        } else {
            save1DimData(mVelocityDataMove, Constants.VELOCITY_FILENAME);
        }

        mFeatures.setVelocity(mMathUtil.computeMean(mVelocityDataMove));
    }

    private void macroImage() {

        Bitmap bitmapNoFlash = mFileHandler.loadBitmap(mSurfacePictureNoFlashPath);

        if(mDatabaseMode) {
            mFileHandler.saveBitmapToFile(mFeaturePath, mTextureName + "_" + mTextureNumber + "_" + Constants.DISPLAY_IMAGE_FILENAME, bitmapNoFlash);
        } else {
            mFileHandler.saveBitmapToFile(mFeaturePath, Constants.DISPLAY_IMAGE_FILENAME, bitmapNoFlash);
        }

        Bitmap bitmapNoFlashGrayscale = mBmpOps.toGrayscale(bitmapNoFlash);

        bitmapNoFlash.recycle();

        if(mDatabaseMode) {
            mFileHandler.saveBitmapToFile(mFeaturePath, mTextureName + "_" + mTextureNumber + "_" + Constants.MACRO_IMAGE_FILENAME, bitmapNoFlashGrayscale);
        } else {
            mFileHandler.saveBitmapToFile(mFeaturePath, Constants.MACRO_IMAGE_FILENAME, bitmapNoFlashGrayscale);
        }

        bitmapNoFlashGrayscale.recycle();

    }

    private void computeImageFeatures() {

        ImageFeatures imageFeatures = new ImageFeatures();
        imageFeatures.process(mSurfacePictureNoFlashPath, mFeaturePreferences);
        imageFeatures.processFlash(mSurfacePictureFlashPath, mFeaturePreferences);

        mFeatures.setGlossiness(imageFeatures.getImageGlossiness());
        mFeatures.setImageCentroid(imageFeatures.getImageCentroid());
        mFeatures.setImageContrast(imageFeatures.getImageContrast());
        mFeatures.setDominantColor(imageFeatures.getDominantColor());
        mFeatures.setDominantSaturation(imageFeatures.getDominantSaturation());
        mFeatures.setImageFrequencyBinCount1(imageFeatures.getImageFrequencyBinCount1());
        mFeatures.setImageSpectralCentroid(imageFeatures.getImageSpectralCentroid());

    }

    private void computeSoundFeatures() {

        SoundFeatures soundFeatures = new SoundFeatures();
        double[] mfccVals;
        double[] soundDataSingleTap = new double[mSoundDataSingleTap.size()];
        double[] soundDataMove = new double[mSoundDataMove.size()];

        for(int i=0; i< mSoundDataSingleTap.size(); i++) {
            soundDataSingleTap[i] = mSoundDataSingleTap.get(i);
        }

        for(int i=0; i< mSoundDataMove.size(); i++) {
            soundDataMove[i] = mSoundDataMove.get(i);
        }

        //save1DimDataArray(soundDataSingleTap, "soundTap.txt");
        //save1DimDataArray(soundDataMove, "soundMove.txt");
/*
        StringBuilder sb = new StringBuilder();
        for(int i=0; i < soundDataMove.length; i++) {
            sb.append(soundDataMove[i] + "\t");
        }

        FileHandler fh = new FileHandler();
        fh.writeDataToTextFile(sb.toString(), Constants.ANALYSIS_PATH, "sound_data.txt");
*/

        if(mComputeSoundTapFeatures) {
            soundFeatures.processTap(soundDataSingleTap, mFeaturePreferences);

            Log.i("FeatureComputer","signalEnergyTap" + soundFeatures.getSignalEnergyTap());
            Log.i("FeatureComputer","kurtosis" + soundFeatures.getKurtosisTap());
            Log.i("FeatureComputer","spectralCentroidTap" + soundFeatures.getSpectralCentroidTap());
            Log.i("FeatureComputer","spectralRollOffTap" + soundFeatures.getSpectralRollOffTap());
            Log.i("FeatureComputer", "spectralSumTap" + soundFeatures.getSpectralSumTap());

            mFeatures.setFeature(Constants.SOUND_SIGNAL_ENERGY_TAP, soundFeatures.getSignalEnergyTap());
            mFeatures.setFeature(Constants.SOUND_KURTOSIS_TAP, soundFeatures.getKurtosisTap());
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_CENTROID_TAP, soundFeatures.getSpectralCentroidTap());
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_ROLL_OFF_TAP, soundFeatures.getSpectralRollOffTap());
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_SUM_TAP, soundFeatures.getSpectralSumTap());

        } else {

            Log.i("FeatureComputer", "All Sound Tap Features NaN");

            mFeatures.setFeature(Constants.SOUND_SIGNAL_ENERGY_TAP, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_KURTOSIS_TAP, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_CENTROID_TAP, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_ROLL_OFF_TAP, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_SUM_TAP, Double.NaN);
        }

        if(mComputeSoundMoveFeatures) {
            soundFeatures.processMove(soundDataMove, mFeaturePreferences);

            mfccVals=soundFeatures.getMFCCs();

            Log.i("FeatureComputer", "mfccVals[] length: " + mfccVals.length);

            int i = 0;

            for(double val:mfccVals) {
                Log.i("FeatureComputer", "mfcc " + i + ": " + val);
                i++;
            }

            Log.i("FeatureComputer","zeroCrossingRate" + soundFeatures.getZeroCrossingRate());
            Log.i("FeatureComputer","noiseDistribution" + soundFeatures.getNoiseDistribution());
            Log.i("FeatureComputer","signalEnergy" + soundFeatures.getSignalEnergy());
            Log.i("FeatureComputer","spikiness" + soundFeatures.getSpikiness());
            Log.i("FeatureComputer","spectralCentroid" + soundFeatures.getSpectralCentroid());
            Log.i("FeatureComputer","spectralEntropy" + soundFeatures.getSpectralEntropy());
            Log.i("FeatureComputer","spectralRollOff" + soundFeatures.getSpectralRollOff());
            Log.i("FeatureComputer", "spectralSum" + soundFeatures.getSpectralSum());

            mFeatures.setFeature(Constants.SOUND_ZERO_CROSSING_RATE, soundFeatures.getZeroCrossingRate());
            mFeatures.setFeature(Constants.SOUND_NOISE_DISTRIBUTION, soundFeatures.getNoiseDistribution());
            mFeatures.setFeature(Constants.SOUND_SIGNAL_ENERGY, soundFeatures.getSignalEnergy());
            mFeatures.setFeature(Constants.SOUND_SPIKINESS, soundFeatures.getSpikiness());
            mFeatures.setFeature(Constants.MFCC1, mfccVals[0]);
            mFeatures.setFeature(Constants.MFCC2, mfccVals[1]);
            mFeatures.setFeature(Constants.MFCC5, mfccVals[4]);
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_CENTROID, soundFeatures.getSpectralCentroid());
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_ENTROPY, soundFeatures.getSpectralEntropy());
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_ROLL_OFF, soundFeatures.getSpectralRollOff());
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_SUM, soundFeatures.getSpectralSum());

        } else {

            Log.i("FeatureComputer", "All Sound Move Features NaN");

            mFeatures.setFeature(Constants.SOUND_ZERO_CROSSING_RATE, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_NOISE_DISTRIBUTION, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_SIGNAL_ENERGY, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_SPIKINESS, Double.NaN);
            mFeatures.setFeature(Constants.MFCC1, Double.NaN);
            mFeatures.setFeature(Constants.MFCC2, Double.NaN);
            mFeatures.setFeature(Constants.MFCC5, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_CENTROID, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_ENTROPY, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_ROLL_OFF, Double.NaN);
            mFeatures.setFeature(Constants.SOUND_SPECTRAL_SUM, Double.NaN);
        }

    }

    private void computeAccelerationFeatures() {

        AccelerationFeatures accelFeatures = new AccelerationFeatures();

        preprocessSensorData();
        calculateWeightMacroRoughness();

        if(mExternAccelDataMoveAvailable && mComputeExternAccelFeatures) {

            List<Float> externAccelDataMove = new ArrayList<>();

            for(int i=1; i<mExternAccelDataListMove.size(); i++) {

                for(int j=0; j<mExternAccelDataListMove.get(i).size(); j++) {
                    externAccelDataMove.add(mExternAccelDataListMove.get(i).get(j)[2]);
                }
            }

            accelFeatures.processExternalSensor(externAccelDataMove, mFilteredExternAccelDataMove, mFeaturePreferences);

            mFeatures.setFeature(Constants.EXTERNAL_ACCELERATION_SPECTRAL_SUM, accelFeatures.getExternAccelerationSpectralSum());
            mFeatures.setFeature(Constants.EXTERNAL_ACCELERATION_SIGNAL_ENERGY, accelFeatures.getExternSignalEnergy());
            mFeatures.setFeature(Constants.EXTERNAL_ACCELERATION_ZERO_CROSSING_RATE, accelFeatures.getExternZeroCrossingRate());
        } else {
            mFeatures.setFeature(Constants.EXTERNAL_ACCELERATION_SPECTRAL_SUM, Double.NaN);
            mFeatures.setFeature(Constants.EXTERNAL_ACCELERATION_SIGNAL_ENERGY, Double.NaN);
            mFeatures.setFeature(Constants.EXTERNAL_ACCELERATION_ZERO_CROSSING_RATE, Double.NaN);
        }

        if(mComputeInternAccelFeatures) {
            accelFeatures.processInternalSensor(mFilteredAccelDataMove.get(1), mFeaturePreferences);

            mFeatures.setFeature(Constants.ACCELERATION_SPECTRAL_SUM, accelFeatures.getAccelerationSpectralSum());
            mFeatures.setFeature(Constants.ACCELERATION_SIGNAL_ENERGY, accelFeatures.getSignalEnergy());
            mFeatures.setFeature(Constants.ACCELERATION_ZERO_CROSSING_RATE, accelFeatures.getZeroCrossingRate());
        } else {
            mFeatures.setFeature(Constants.ACCELERATION_SPECTRAL_SUM, Double.NaN);
            mFeatures.setFeature(Constants.ACCELERATION_SIGNAL_ENERGY, Double.NaN);
            mFeatures.setFeature(Constants.ACCELERATION_ZERO_CROSSING_RATE, Double.NaN);
        }

        Log.i("FeatureComputer", "ExternalAccelerationSpectralSum" + mFeatures.getExternalAccelerationSpectralSum());
        Log.i("FeatureComputer", "ExternalAccelerationSignalEnergy" + mFeatures.getExternalAccelerationSignalEnergy());
        Log.i("FeatureComputer", "ExternalAccelerationZeroCrossingRate" + mFeatures.getExternalAccelerationZeroCrossingRate());

        Log.i("FeatureComputer", "AccelerationSpectralSum" + mFeatures.getAccelerationSpectralSum());
        Log.i("FeatureComputer", "AccelerationSignalEnergy" + mFeatures.getAccelerationSignalEnergy());
        Log.i("FeatureComputer", "AccelerationZeroCrossingRate" + mFeatures.getAccelerationZeroCrossingRate());

    }

    private void computeMacroAmplitude() {
        mFeatures.setMacroAmplitude(1);
    }

    private void computeMicroAmplitude() {
        mFeatures.setMicroAmplitude(1);
    }

    private void computeGlossinessAmplitude() {
        mFeatures.setGlossinessAmplitude(1);
    }

    private void computeSoundNoiseDistribution() {

        ListIterator<Double> itBegin = mSoundDataMove.listIterator((int) (0.3f * Constants.RECORDER_SAMPLING_RATE));
        ListIterator<Double> itEnd = mSoundDataMove.listIterator(mSoundDataMove.size() - 1);

        double noiseDistribution = 10 * mMathUtil.noiseDistributionDouble(itBegin, itEnd);

        if(!mDatabaseMode) {

            //// TODO: 19.02.2016 remove cheat
            if(noiseDistribution < 0.3) {
                noiseDistribution = 0.3;
            }
            if (noiseDistribution > 1.0) {
                noiseDistribution = 1.0;
            }

        }

        Log.i("Features", "noise distribution: " + noiseDistribution);
        mFeatures.setSoundNoiseDistribution(noiseDistribution);
    }

    private void computeZeroCrossingRate() {

        List<Float> accelData;
        ListIterator itBegin;
        ListIterator itEnd;

        if(mExternAccelDataMoveAvailable) {
            accelData = mFilteredExternAccelDataMove;
            itBegin = accelData.listIterator(1500); //// TODO: 01.03.2016 start recording extern acceleration only after 1s and set 1500 to 0
        } else {
            accelData = mFilteredAccelDataMove.get(1); // y axis
            itBegin = accelData.listIterator((int) (0.3f * Constants.SAMPLE_RATE_ACCEL));
        }

        itEnd = accelData.listIterator(accelData.size()-1);


        mFeatures.setAccelerationZeroCrossingRate(mMathUtil.zeroCrossingRateFloat(itBegin, itEnd));
        Log.i("Features", "zero crossing rate: " + mFeatures.getAccelerationZeroCrossingRate());
    }

    private void calculateWeightMacroRoughness() {

        ListIterator<Float> itBegin = mFilteredAccelDataMove.get(1).listIterator(0);
        ListIterator<Float> itEnd = mFilteredAccelDataMove.get(1).listIterator(mFilteredAccelDataMove.get(1).size() - 1);

        double variance = mMathUtil.varianceFloat(itBegin, itEnd);

        mFeatures.setWeightMacro(3.0 * variance);
		
		/*if(!mDatabaseMode && mFeatures.getWeightMacro() > 1.0) {
			mFeatures.setWeightMacro(1.0);
		}*/

        Log.i("Features", "mWeightMacro: " + mFeatures.getWeightMacro());

    }

    private void calculateWeightMicroRoughness() {

        ListIterator<Double> itBegin = mSoundDataMove.listIterator((int) (0.3f * Constants.RECORDER_SAMPLING_RATE));
        ListIterator<Double> itEnd = mSoundDataMove.listIterator(mSoundDataMove.size() - 1);

        double bandpower = mMathUtil.bandPowerDouble(itBegin, itEnd);

        mFeatures.setWeightMicro(1000 * bandpower);
		if(mFeatures.getWeightMicro() > 0.7) {
			mFeatures.setWeightMicro(0.7);
		} else if(mFeatures.getWeightMicro() < 0.3) {
            mFeatures.setWeightMicro(0.3);
        }

        Log.i("Features", "mWeightMicro: " + mFeatures.getWeightMicro());
    }

    private void calculateWeightGlossiness() {

        //// TODO: 01.03.2016 remove cheat
		/*if(mFeatures.getWeightGlossiness() > 0.33) {
			mFeatures.setWeightGlossiness(0.33);
		}*/

		/*if(mFeatures.getWeightGlossiness() < 0) {
			mFeatures.setWeightGlossiness(0);
		}*/
    }

    private void renormalizeWeights() {
		
		double sumOfWeights = mFeatures.getWeightMacro() + mFeatures.getWeightMicro() + mFeatures.getWeightGlossiness();
		
		if(Math.abs(sumOfWeights - 1) > 0.001) {
			mFeatures.setWeightMacro(mFeatures.getWeightMacro() / sumOfWeights);
			mFeatures.setWeightMicro(mFeatures.getWeightMicro() / sumOfWeights);
			mFeatures.setWeightGlossiness(mFeatures.getWeightGlossiness() / sumOfWeights);
		}
				
		Log.i("Features", "final weights: " + mFeatures.getWeightMacro() + ", " + mFeatures.getWeightMicro() + ", " + mFeatures.getWeightGlossiness());
    }

    private void normalizeFeatures() {

        double oldValue;
        double minValue;
        double maxValue;

        double normalizedValue = 0;

        for(String featureName:Constants.FEATURE_SET) {

            oldValue = mFeatures.getFeature(featureName);
            minValue = mMinFeatures.getFeature(featureName);
            maxValue = mMaxFeatures.getFeature(featureName);

            if(oldValue != Double.NaN) {
                if ((maxValue - minValue) != 0) {
                    normalizedValue = (oldValue - minValue) / (maxValue - minValue);
                } else {
                    normalizedValue = 0;
                }

                if (normalizedValue < 0) {
                    normalizedValue = 0;
                } else if (normalizedValue > 1) {
                    normalizedValue = 1;
                }

                mFeatures.setFeature(featureName, normalizedValue);
            }

            Log.i("FeatureComputer", featureName + ": oldValue: " + oldValue + ", normalizedValue: " + normalizedValue);
        }

    }

    private void printAllFeatures() {

        //only round print in Analysis Mode
        if(!mDatabaseMode) {

            double roundedMacroAmpliude = mMathUtil.roundDigits(mFeatures.getMacroAmplitude(), Constants.PRECISION_PARAMETERS);
            double roundedMicroAmpliude = mMathUtil.roundDigits(mFeatures.getMicroAmplitude(), Constants.PRECISION_PARAMETERS);
            double roundedGlossinessAmpliude = mMathUtil.roundDigits(mFeatures.getGlossinessAmplitude(), Constants.PRECISION_PARAMETERS);
            double roundedNoiseDistribution = mMathUtil.roundDigits(mFeatures.getSoundNoiseDistribution(), Constants.PRECISION_PARAMETERS);
            double roundedWeightMacro = mMathUtil.roundDigits(mFeatures.getWeightMacro(), Constants.PRECISION_WEIGHTS);
            double roundedWeightMicro = mMathUtil.roundDigits(mFeatures.getWeightMicro(), Constants.PRECISION_WEIGHTS);
            double roundedWeightGlossiness = mMathUtil.roundDigits(mFeatures.getWeightGlossiness(), Constants.PRECISION_WEIGHTS);
            double roundedHardness = mMathUtil.roundDigits(mFeatures.getSoundSpectralSumTap(), Constants.PRECISION_PARAMETERS);
            roundedHardness = 0.3;
            roundedWeightMacro = 0.4;
            roundedWeightMicro = 0.4;
            roundedWeightGlossiness = 0.2;
            roundedNoiseDistribution = 0.5;
            //impact duration does not have to be rounded

            String delimiter = "#";

            StringBuilder sb = new StringBuilder();

            sb.append(Double.toString(roundedMacroAmpliude));
            sb.append(delimiter);
            sb.append(Double.toString(roundedMicroAmpliude));
            sb.append(delimiter);
            sb.append(Double.toString(roundedGlossinessAmpliude));
            sb.append(delimiter);
            sb.append(Double.toString(roundedNoiseDistribution));
            sb.append(delimiter);
            sb.append(Double.toString(roundedWeightMacro));
            sb.append(delimiter);
            sb.append(Double.toString(roundedWeightMicro));
            sb.append(delimiter);
            sb.append(Double.toString(roundedWeightGlossiness));
            sb.append(delimiter);
            sb.append(Double.toString(roundedHardness));
            sb.append(delimiter);
            //// TODO: 12.10.2016 don't use a set value
            sb.append(Long.toString(300));

            String featureString = sb.toString();

            mFileHandler.writeDataToTextFile(featureString, mFeaturePath, Constants.PARAMETERS_FILENAME);
        }

        JSONHelper jsonHelper = new JSONHelper();
        JSONObject jsonFeatureSet = jsonHelper.buildJSONFeatureSet(mFeatures);
        mFileHandler.writeDataToTextFile(jsonFeatureSet.toString(), mFeaturePath, Constants.JSON_FEATURE_LOG_FILENAME);

        if(mDatabaseMode) {
            //jsonHelper.updateDatabase(mFeaturePath, "Test", Integer.toString(1), jsonFeatureSet);
            //jsonHelper.updateDatabase(mFeaturePath, mTextureName, Integer.toString(mTextureNumber), jsonFeatureSet);
        }

    }


    //// TODO: 08.12.2016 move to FileHandler
    private void save1DimData(List<Float> data, String filename) {

        String logString = buildLogString1Dim(data);

        mFileHandler.writeDataToTextFile(logString, mSensorsPath, filename);
    }

    private void save1DimData2(List<float[]> data, String filename) {

        String logString = buildLogString1Dim2(data);

        mFileHandler.writeDataToTextFile(logString, mSensorsPath, filename);
    }

    private void save1DimDataArray(double[] data, String filename) {

        String logString = buildLogString1DimArray(data);

        mFileHandler.writeDataToTextFile(logString, mSensorsPath, filename);
    }

    private void save3DimData(List<List<Float>> data, String filename) {

        String logString = buildLogString3Dim(data);

        mFileHandler.writeDataToTextFile(logString, mSensorsPath, filename);
    }

    private String buildLogString1Dim(List<Float> values) {

        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < values.size(); i++) {
            sb.append(values.get(i));
            sb.append('\n');
        }

        return sb.toString();
    }

    private String buildLogString1Dim2(List<float[]> values) {

        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < values.size(); i++) {
            sb.append(values.get(i)[1]);
            sb.append('\n');
        }

        return sb.toString();
    }

    private String buildLogString1DimArray(double[] values) {

        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < values.length; i++) {
            sb.append(values[i]);
            sb.append('\n');
        }

        return sb.toString();
    }

    private String buildLogString3Dim(List<List<Float>> values) {

        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < values.get(0).size(); i++) {
            sb.append(values.get(0).get(i));
            sb.append('\t');
            sb.append(values.get(1).get(i));
            sb.append('\t');
            sb.append(values.get(2).get(i));
            sb.append('\n');
        }

        return sb.toString();
    }
}
