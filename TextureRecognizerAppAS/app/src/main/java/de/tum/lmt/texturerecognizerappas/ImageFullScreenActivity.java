package de.tum.lmt.texturerecognizerappas;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Kev94 on 18.03.2016.
 */
public class ImageFullScreenActivity extends Activity {

    ImageView mImageView;
    ImageView mPauseOverlay;
    ImageView mFilterOverlay;
    TextView mTitleView;
    Button mButtonShare;
    Button mButtonClassification;
    Button mButtonSound;

    View mButtonRow;

    AudioHelper mAudioHelper;

    String mTitle;
    String mPath;

    boolean mIsSoundPlaying;

    private boolean mButtonsVisible = true;

    private FileHandler mFileHandler;

    private AlertDialog mClassificationProgressDialog;
    private ClassificationTask.iOnClassificationTaskFinishedListener mOnClassificationTaskFinishedListener;
    private ServerTask.iOnResultReceivedListener mOnOnlineResultListener;
    private ServerTask.iOnDataSentListener mOnDataSentListener;
    private AudioHelper.iOnSoundFinishedListener mOnSoundFinishedListener;

    AlertDialog mOnlineProcessingDialog;

    private long mClassificationTimeStop;
    private long mClassificationTimeStart;

    private Animation mAnimationButtonIn;
    private Animation mAnimationButtonOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_fullscreen);

        Log.i("fullscreen", "onCreate()");

        mFileHandler = new FileHandler();

        final SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        mButtonShare = (Button) findViewById(R.id.button_share_fullscreen);
        mButtonShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSendDialog();
            }
        });

        mButtonClassification = (Button) findViewById(R.id.button_classification_fullscreen);
        mButtonClassification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mClassificationProgressDialog = null;

                String classificationMethod = sharedPrefs.getString(Constants.PREF_KEY_CLASSIFICATION_METHOD, null);
                Set<String> classificationFeatures = sharedPrefs.getStringSet(Constants.PREF_KEY_CLASSIfICATION_FEATURES, null);
                //String distance = sharedPrefs.getString(Constants.PREF_KEY_DISTANCE, null);
                String neighborhoodSize = sharedPrefs.getString(Constants.PREF_KEY_NEIGHBORHOOD, null);
                String featurePath = mPath + Constants.DATA_TO_SEND_FOLDER_NAME + File.separator;
                Log.i("Fullscreen", "featurePath: " + featurePath);

                boolean onlineProcessing = sharedPrefs.getBoolean(Constants.PREF_KEY_ONLINE, false);

                if(onlineProcessing) {

                    showOnlineProcessingDialog();

                    mClassificationTimeStart = System.currentTimeMillis();

                    String IP = sharedPrefs.getString(Constants.PREF_KEY_IP, "192.168.2.1");

                    JSONHelper jsonHelper = new JSONHelper();

                    for(String string:classificationFeatures) {
                        Log.i("Test", string);
                    }

                    String jsonSettingsString =jsonHelper.buildJSONSettingsString(classificationMethod, neighborhoodSize, classificationFeatures);

                    Log.i("Test", jsonSettingsString);

                    mFileHandler.writeDataToTextFile(jsonSettingsString, mPath, "jsonClassificationSettings.txt");

                    ServerTask serverTask = new ServerTask(getApplicationContext(), false, mOnOnlineResultListener, mOnDataSentListener);
                    serverTask.execute(mPath, IP);

                } else {

                    if (classificationFeatures == null) {
                        Log.i("Classification", "Set null");
                    }

                    JSONHelper jsonHelper = new JSONHelper();

                    File JSONFile = new File(featurePath + Constants.JSON_FEATURE_LOG_FILENAME + Constants.TEXT_FILE_EXTENSION);

                    if(JSONFile.exists()) {
                        Features features = jsonHelper.readFeaturesFromJSONFile(JSONFile, classificationFeatures);

                        if (classificationMethod == null) {
                            Log.e("Classification", "classification parameters not set");
                            Toast.makeText(getApplicationContext(), getString(R.string.parameters_missing), Toast.LENGTH_LONG).show();
                        } else {
                            ClassificationTask classifierTask = new ClassificationTask(featurePath, features, classificationMethod, Integer.parseInt(neighborhoodSize), classificationFeatures, mOnClassificationTaskFinishedListener);
                            classifierTask.execute();
                            showProgressDialog();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Features not found!", Toast.LENGTH_LONG).show();
                    }

                    jsonHelper = null;
                }
            }
        });

        mOnClassificationTaskFinishedListener = new ClassificationTask.iOnClassificationTaskFinishedListener() {
            @Override
            public void onClassifierTaskFinished(List<Texture.Sample> result) {

                Log.i("FullScreen", "Classiicaion finished");

                if(mClassificationProgressDialog != null) {
                    Log.i("FullScreen", "Dialog dismissed");
                    mClassificationProgressDialog.dismiss();
                }

                String classificationMethod = sharedPrefs.getString(Constants.PREF_KEY_CLASSIFICATION_METHOD, null);

                if(result == null) {
                    Log.e("Classification", "Classfication not successful");
                    Toast.makeText(getApplicationContext(), getString(R.string.classification_null), Toast.LENGTH_LONG).show();
                } else {
                    showClassificationListDialog(result, classificationMethod);
                }
            }
        };

        mOnDataSentListener = new ServerTask.iOnDataSentListener() {
            @Override
            public void onDataSent() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "data sent", Toast.LENGTH_LONG).show();
                    }
                });
            }
        };

        mOnOnlineResultListener = new ServerTask.iOnResultReceivedListener() {
            @Override
            public void onResultReceived(String jsonResultString) {

                Log.i("result", "result received");

                String classificationMethod = sharedPrefs.getString(Constants.PREF_KEY_CLASSIFICATION_METHOD, "k-nearest-neighbor");
                String neighborhoodSize = sharedPrefs.getString(Constants.PREF_KEY_NEIGHBORHOOD, "10");
                String distToMean = Constants.CLASSIFICATION_DIST_TO_MEAN;
                String Mahal = Constants.CLASSIFICATION_MAHALANOBIS;
                String kNearestNeighbor = Constants.CLASSIFICATION_KNN;

                List<Texture.Sample> result = new ArrayList<>();

                JSONObject jsonResult = null;
                try {
                    jsonResult = new JSONObject(jsonResultString);

                    if (classificationMethod.equals(distToMean)) {

                        if (jsonResult != null) {

                            for (int i = 1; i <= Constants.NUMBER_OF_TEXTURES; i++) {

                                JSONObject jsonSample;

                                try {
                                    jsonSample = jsonResult.getJSONObject(Integer.toString(i));
                                } catch(JSONException e) {
                                    break;
                                }

                                String name = jsonSample.getString("name");
                                double distance = jsonSample.getDouble("value");

                                Texture texture = new Texture(name);
                                texture.addSampleWithDistance(distance);

                                result.add(texture.getSamples().get(0));

                            }
                        }
                    } else if (classificationMethod.equals(Mahal)) {

                        if (jsonResult != null) {

                            for (int i = 1; i <= Constants.NUMBER_OF_TEXTURES; i++) {

                                JSONObject jsonSample;

                                try {
                                    jsonSample = jsonResult.getJSONObject(Integer.toString(i));
                                } catch(JSONException e) {
                                    break;
                                }

                                String name = jsonSample.getString("name");
                                double distance = jsonSample.getDouble("value");

                                Texture texture = new Texture(name);
                                texture.addSampleWithDistance(distance);

                                result.add(texture.getSamples().get(0));

                            }
                        }
                    } else if (classificationMethod.equals(kNearestNeighbor)) {

                        for (int i = 1; i <= Integer.getInteger(neighborhoodSize, 10); i++) {

                            JSONObject jsonSample;

                            try {
                                jsonSample = jsonResult.getJSONObject(Integer.toString(i));
                            } catch(JSONException e) {
                                break;
                            }

                            String name = jsonSample.getString("name");
                            int occurence = jsonSample.getInt("value");

                            Texture texture = new Texture(name);
                            texture.addSampleWithOccurence(occurence);

                            result.add(texture.getSamples().get(0));

                            /*for(Texture.Sample sample:result) {

                                if(name.equals(sample.getName())) {
                                    sample.setOccurence(sample.getOccurence()+1);
                                    break;
                                } else {
                                    Texture texture = new Texture(name);
                                    texture.addSampleWithOccurence(1);

                                    result.add(texture.getSamples().get(0));
                                }
                            }*/

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mOnlineProcessingDialog.dismiss();

                mClassificationTimeStop = System.currentTimeMillis();

                Toast.makeText(getApplicationContext(), "Classification took " + (mClassificationTimeStop - mClassificationTimeStart) + " ms.", Toast.LENGTH_LONG).show();

                if(jsonResult == null) {
                    Log.e("Classification", "Classfication not successful");
                    Toast.makeText(getApplicationContext(), getString(R.string.classification_null), Toast.LENGTH_LONG).show();
                } else {
                    showClassificationListDialog(result, classificationMethod);
                }
            }
        };

        mButtonSound = (Button) findViewById(R.id.button_sound_fullscreen);
        mButtonSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mOnSoundFinishedListener = new AudioHelper.iOnSoundFinishedListener() {
                    @Override
                    public void onSoundFinished() {

                        Log.i("Fullscreen", "Sound complete");

                        mIsSoundPlaying = false;
                        mAudioHelper = null;

                        removePauseOverlayAndStopMusic();
                    }
                };

                mAudioHelper = new AudioHelper(getApplicationContext(), mOnSoundFinishedListener);
                //// TODO: 21.03.2016 handle errors, but it's working so not urgent
                mAudioHelper.play(mPath + File.separator + Constants.AUDIO_FILENAME_ORIG + Constants.AUDIO_FILE_EXTENSION);
                mIsSoundPlaying = true;

                mButtonShare.setVisibility(View.GONE);
                mButtonClassification.setVisibility(View.GONE);
                mButtonSound.setVisibility(View.GONE);
                mButtonsVisible = false;

                mPauseOverlay.setVisibility(View.VISIBLE);
                mFilterOverlay.setVisibility(View.VISIBLE);
                mPauseOverlay.bringToFront();
                mFilterOverlay.bringToFront();

                mPauseOverlay.setClickable(true);
                mFilterOverlay.setClickable(true);
            }
        });

        mButtonRow = findViewById(R.id.button_row);

        mImageView = (ImageView) findViewById(R.id.image_view_fullscreen);
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mButtonsVisible) {
                    mButtonRow.startAnimation(mAnimationButtonOut);
                } else {

                    mButtonShare.setVisibility(View.VISIBLE);
                    mButtonClassification.setVisibility(View.VISIBLE);
                    mButtonSound.setVisibility(View.VISIBLE);
                    mButtonsVisible = true;

                    mButtonRow.startAnimation(mAnimationButtonIn);
                }
            }
        });

        mFilterOverlay = (ImageView) findViewById(R.id.filter_overlay);
        mFilterOverlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePauseOverlayAndStopMusic();
            }
        });

        mFilterOverlay.setClickable(false);

        mPauseOverlay = (ImageView) findViewById(R.id.pause_overlay);
        mPauseOverlay.setClickable(false);

        mTitleView = (TextView) findViewById(R.id.title_fullscreen);

        mTitle = getIntent().getStringExtra("title");
        mPath = getIntent().getStringExtra("dir");

        BitmapOperations bmpOps = new BitmapOperations();
        mImageView.setImageBitmap(bmpOps.decodeSampledBitmapFromFile((mPath + File.separator + Constants.CAMERA_NO_FLASH_FILENAME + Constants.JPG_FILE_EXTENSION), 640, 480));
        mTitleView.setText(mTitle);

        mAnimationButtonIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.button_row_in);
        mAnimationButtonIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        mAnimationButtonOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.button_row_out);
        mAnimationButtonOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mButtonShare.setVisibility(View.GONE);
                mButtonClassification.setVisibility(View.GONE);
                mButtonSound.setVisibility(View.GONE);
                mButtonsVisible = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        mButtonRow.startAnimation(mAnimationButtonIn);
    }

    public void removePauseOverlayAndStopMusic() {
        if(mFilterOverlay.getVisibility() == View.VISIBLE) {
            mPauseOverlay.setVisibility(View.GONE);
            mFilterOverlay.setVisibility(View.GONE);

            mPauseOverlay.setClickable(false);
            mFilterOverlay.setClickable(false);

            if (mIsSoundPlaying) {
                mAudioHelper.stop();
                mAudioHelper = null;
                mIsSoundPlaying = false;

                mButtonRow.startAnimation(mAnimationButtonIn);
            }

            if(!mButtonsVisible) {
                mButtonShare.setVisibility(View.VISIBLE);
                mButtonClassification.setVisibility(View.VISIBLE);
                mButtonSound.setVisibility(View.VISIBLE);
                mButtonsVisible = true;
            }

            mButtonRow.startAnimation(mAnimationButtonIn);

            Log.i("Fullscreen", "Gone");
        }
    }

    private void showProgressDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        RelativeLayout rl = new RelativeLayout(this);

        ProgressBar progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);
        RelativeLayout.LayoutParams progressBarParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView text = new TextView(this);
        text.setText(R.string.classification_progress);
        RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        textParams.addRule(RelativeLayout.CENTER_VERTICAL);

        rl.addView(progressBar, progressBarParams);
        rl.addView(text, textParams);

        builder.setView(rl);

        mClassificationProgressDialog = builder.create();
        mClassificationProgressDialog = builder.show();

        mClassificationProgressDialog.setCanceledOnTouchOutside(false);
    }

    protected void showOnlineProcessingDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        RelativeLayout rl = new RelativeLayout(this);

        ProgressBar progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);
        RelativeLayout.LayoutParams progressBarParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView text = new TextView(this);
        text.setText(R.string.processing);
        RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        textParams.addRule(RelativeLayout.CENTER_VERTICAL);

        rl.addView(progressBar, progressBarParams);
        rl.addView(text, textParams);

        builder.setView(rl);

        mOnlineProcessingDialog = builder.create();
        mOnlineProcessingDialog = builder.show();

        mOnlineProcessingDialog.setCanceledOnTouchOutside(false);
    }

    private void showSendDialog() {
        DialogSendFragment dialogSend = DialogSendFragment.newInstance(mPath);
        dialogSend.show(getFragmentManager(), "dialogSend");
    }

    private void showClassificationListDialog(List<Texture.Sample> textures, String classificationMethod) {
        DialogClassificationListFragment dialogClassificationList = new DialogClassificationListFragment(textures, classificationMethod);
        dialogClassificationList.show(getFragmentManager(), "dialogClassificationList");
    }
}
