package de.tum.lmt.texturerecognizerappas;

import android.util.Log;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Kev94 on 09.12.2016.
 */
public class AccelerationFeatures {

    private static final String CLASS_NAME = "AccelerationFeatures";

    private double externAccelerationSpectralSum;
    private double externSignalEnergy;
    private double externZeroCrossingRate;

    private double accelerationSpectralSum;
    private double signalEnergy;
    private double zeroCrossingRate;

    public AccelerationFeatures() {

    }

    //external sensor
    public void processExternalSensor(List<Float> accelDataMove, List<Float> filteredAccelDataMove, HashMap featurePreferences) {

        if((boolean)featurePreferences.get(Constants.EXTERNAL_ACCELERATION_SPECTRAL_SUM)) {
            this.externAccelerationSpectralSum = computeAccelerationSpectralSum(accelDataMove, Constants.EXTERNAL_ACCELERATION_SENSOR);
        } else {
            this.externAccelerationSpectralSum = Double.NaN;
        }

        if((boolean)featurePreferences.get(Constants.EXTERNAL_ACCELERATION_SIGNAL_ENERGY)) {
            this.externSignalEnergy = computeSignalEnergy(filteredAccelDataMove);
        } else {
            this.externSignalEnergy = Double.NaN;
        }

        if((boolean)featurePreferences.get(Constants.EXTERNAL_ACCELERATION_ZERO_CROSSING_RATE)) {
            this.externZeroCrossingRate = computeZeroCrossignRate(filteredAccelDataMove);
        } else {
            this.externZeroCrossingRate = Double.NaN;
        }

        Log.i(CLASS_NAME, "externAccelerationSpectralSum: " + this.externAccelerationSpectralSum);
        Log.i(CLASS_NAME, "externSignalEnergy: " + this.externSignalEnergy);
        Log.i(CLASS_NAME, "externZeroCorssingRate: " + this.externZeroCrossingRate);

    }

    //internal sensor
    public void processInternalSensor(List<Float> filteredAccelDataMove, HashMap featurePreferences) {

        if((boolean)featurePreferences.get(Constants.ACCELERATION_SPECTRAL_SUM)) {
            this.accelerationSpectralSum = Math.log(1+computeAccelerationSpectralSum(filteredAccelDataMove, Constants.INTERNAL_ACCELERATION_SENSOR));
        } else {
            this.accelerationSpectralSum = Double.NaN;
        }

        if((boolean)featurePreferences.get(Constants.ACCELERATION_SIGNAL_ENERGY)) {
            this.signalEnergy = Math.log(1+computeSignalEnergy(filteredAccelDataMove));
        } else {
            this.signalEnergy = Double.NaN;
        }

        if((boolean)featurePreferences.get(Constants.ACCELERATION_ZERO_CROSSING_RATE)) {
            this.zeroCrossingRate = computeZeroCrossignRate(filteredAccelDataMove);
        } else {
            this.zeroCrossingRate = Double.NaN;
        }

        Log.i(CLASS_NAME, "accelerationSpectralSum: " + this.accelerationSpectralSum);
        Log.i(CLASS_NAME, "signalEnergy: " + this.signalEnergy);
        Log.i(CLASS_NAME, "zeroCrossingRate: " + this.zeroCrossingRate);

    }

    public double getExternAccelerationSpectralSum() {
        return this.externAccelerationSpectralSum;
    }

    public double getExternSignalEnergy() {
        return this.externSignalEnergy;
    }

    public double getExternZeroCrossingRate() {
        return this.externZeroCrossingRate;
    }

    public double getAccelerationSpectralSum() {
        return this.accelerationSpectralSum;
    }

    public double getSignalEnergy() {
        return this.signalEnergy;
    }

    public double getZeroCrossingRate() {
        return this.zeroCrossingRate;
    }

    private double computeAccelerationSpectralSum(List<Float> data, int internalOrExternal) {

        int length = data.size();

        while(!((length & (length - 1)) == 0)) {
            length--;
        }

        if(length >= 128 && internalOrExternal == Constants.INTERNAL_ACCELERATION_SENSOR) {
            length = 128;
        }

        int maxFrequency;

        if(internalOrExternal == Constants.INTERNAL_ACCELERATION_SENSOR) {
            if(length >= 100) {
                maxFrequency = 100;
            } else {
                maxFrequency = length;
            }
        } else if(internalOrExternal == Constants.EXTERNAL_ACCELERATION_SENSOR) {
            if(length >= 800) {
                maxFrequency = 800;
            } else {
                maxFrequency = length;
            }
        } else {
            throw new IllegalArgumentException("unknown sensor type - use Constants.INTERNAL_ACCELERATION_SENSOR or use Constants.ExTERNAL_ACCELERATION_SENSOR");
        }

        Log.i("FeatureComputer", "accel data length: " + data.size() + ", new length: " + length);

        double[] signal = new double[length];

        int start = data.size() - length - 1;

        Log.i("FeatureComputer", "size: " + data.size());
        Log.i("FeatureComputer", "diff: " + (data.size() - 1 - start));
        Log.i("FeatureComputer", "start: " + start + ", end: " + (data.size()-2));

        for(int i = start; i < data.size()-1; i++) {
            //Log.i("FeatureComputer","i: " + (i-start));
            signal[i-start] = data.get(i);
        }

        FFT magnitudeFFT = new FFT(FFT.FFT_MAGNITUDE, length);

        magnitudeFFT.transform(signal, null);

        double sum = 0;

        for (int i = 9; i < maxFrequency; i++) {
            sum += signal[i];
        }

        if(internalOrExternal == Constants.INTERNAL_ACCELERATION_SENSOR) {
            Log.i("FeatureComputer", "AccelerationSpectralSum: " + sum);
        } else if(internalOrExternal == Constants.EXTERNAL_ACCELERATION_SENSOR) {
            Log.i("FeatureComputer", "ExternalAccelerationSpectralSum: " + sum);
        } else {
            Log.i("FeatureComputer", "else");
        }

        return sum/(maxFrequency-9.0);

    }

    private double computeSignalEnergy(List<Float> data) {

        double signalEnergy = 0;

        for(int i=0; i<data.size(); i++) {
            signalEnergy = signalEnergy + Math.pow(data.get(i), 2);
        }

        Log.i("FeatureComputer", "signalEnergy: " + signalEnergy / data.size());

        return signalEnergy/data.size();

    }

    private double computeZeroCrossignRate(List<Float> data) {

        int zeroCrossings = 0;

        for (int i = 1; i < data.size(); i++) {
            if ((data.get(i - 1) * data.get(i)) < 0) {
                zeroCrossings++;
            }
        }

        Log.i("FeatureComputer", "zeroCorssingRate: " + (double) zeroCrossings / data.size());

        return ((double) zeroCrossings / data.size());

    }
}
