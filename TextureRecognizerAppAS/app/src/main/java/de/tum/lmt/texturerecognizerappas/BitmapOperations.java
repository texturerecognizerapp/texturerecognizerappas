package de.tum.lmt.texturerecognizerappas;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.util.Log;

/**
 * Created by Kev94 on 26.02.2016.
 */
public class BitmapOperations {

    public BitmapOperations() {

    }

    //from leparlon on http://stackoverflow.com/questions/3373860/convert-a-bitmap-to-grayscale-in-android
    public Bitmap toGrayscale(Bitmap bmpOriginal) {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }

    public double getNumberOfBrightPixelsRel(Bitmap bitmap) {

        int height = bitmap.getHeight();
        int width = bitmap.getWidth();

        int numberOfBrightPixels = 0;
        short[][] pixels = this.getPixelsGrayscale(bitmap);

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {

                if(pixels[i][j] > Constants.BRIGHTNESS_THRESHOLD) {
                    numberOfBrightPixels = numberOfBrightPixels + 1;
                }
            }
        }

        return ((double)numberOfBrightPixels / (double)(height*width));
    }

    public double[] computeGradient(Bitmap bmp, int lengthOfWindow) {

        int height = bmp.getHeight();
        int width = bmp.getWidth();

        //by default initialized with zeros
        short[][] grad_x = new short[height][width];
        short[][] grad_xy = new short[height][width];
        short[][] grad_y = new short[height][width];
        double[] gradient = new double[] {0,0,0}; // x, xy, y

        short[][] pixels = getPixelsGrayscale(bmp);

        for(int i = 0; i < (height - lengthOfWindow); i++) {
            for (int j = 0; j < (width - lengthOfWindow); j++) {
                for (int k = 1; k < lengthOfWindow; k++) {
                    grad_x[i][j] = (short)(grad_x[i][j] + Math.abs(pixels[i][j] - pixels[i+k][j]));
                    grad_xy[i][j] = (short)(grad_xy[i][j] + Math.abs(pixels[i][j] - pixels[i+k][j+k]));
                    grad_y[i][j] = (short)(grad_y[i][j] + Math.abs(pixels[i][j] - pixels[i][j + k]));
                }
            }
        }

        double sum_x = 0;
        double sum_xy = 0;
        double sum_y = 0;

        for(int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                sum_x = sum_x + grad_x[i][j];
                sum_xy = sum_xy + grad_xy[i][j];
                sum_y = sum_y + grad_y[i][j];
            }
        }

        Log.i("Bitmaps", "sum_x: " + sum_x + ", sum_xy: " + sum_xy + ", sum_y: " + sum_y);

        double numberOfPixels = height * width;

        gradient[0] = sum_x / numberOfPixels;
        gradient[1] = sum_xy / numberOfPixels;
        gradient[2] = sum_y / numberOfPixels;

        return gradient;
    }

    public double[] computeGradientOverWindow(Bitmap bmp, int windowSize, int reach) {

        int height = bmp.getHeight();
        int width = bmp.getWidth();

        int relHeight = height / windowSize;
        int relWidth = width / windowSize;

        //by default initialized with zeros
        short[][] meanImg = new short[relHeight][relWidth];
        short[][] grad_x = new short[relHeight][relWidth];
        short[][] grad_xy = new short[relHeight][relWidth];
        short[][] grad_y = new short[relHeight][relWidth];
        double[] gradient = new double[] {0,0,0}; // x, xy, y

        short[][] pixels = getPixelsGrayscale(bmp);

        int sum;

        for (int i = 0; i < (height - windowSize); i += windowSize) {
            for (int j = 0; j < (width - windowSize); j += windowSize) {

                sum = 0;

                for(int p = 0; p < windowSize; p++) {
                    for(int q = 0; q < windowSize; q++) {
                        sum = sum + pixels[i+p][j+q];
                    }
                }

                int mean = (sum / (windowSize * windowSize)); //round

                meanImg[i/windowSize][j/windowSize] = (short)mean;
            }
        }

        Log.i("Bitmaps", "meanImg 0 0: " + meanImg[0][0]);
        Log.i("Bitmaps", "meanImg 10 25: " + meanImg[10][25]);
        Log.i("Bitmaps", "meanImg 12 30 : " + meanImg[12][30]);

        for(int i = 0; i < (relHeight - (reach+1)); i++) {
            for(int j = 0; j < (relWidth - (reach+1)); j++) {
                for(int k = 1; k < (reach+1); k++) {
                    grad_x[i][j] = (short)(grad_x[i][j] + Math.abs(meanImg[i][j] - meanImg[i+k][j]));
                    grad_y[i][j] = (short)(grad_y[i][j] + Math.abs(meanImg[i][j] - meanImg[i][j+k]));
                    grad_xy[i][j] = (short)(grad_xy[i][j] + Math.abs(meanImg[i][j] - meanImg[i+k][j+k]));
                }
            }
        }

        double sum_x = 0;
        double sum_y = 0;
        double sum_xy = 0;

        for(int i = 0; i < relHeight; i++) {
            for(int j = 0; j < relWidth; j++) {
                sum_x = sum_x + grad_x[i][j];
                sum_y = sum_y + grad_y[i][j];
                sum_xy = sum_xy + grad_xy[i][j];
            }
        }

        double relNumberOfPixels = (relHeight * relWidth);

        gradient[0] = sum_x / relNumberOfPixels;
        gradient[1] = sum_xy / relNumberOfPixels;
        gradient[2] = sum_y / relNumberOfPixels;

        return gradient;
    }

    //get pixels in RGB
    public short[][][] getPixelsRGB(Bitmap bitmap, int[] cornerTopLeft, int[] cornerBottomRight) {

        //cornerTopLeft: (x,y) where x is the coordinate along the horizontal of the image and y is the coordinate along the vertical

        //pixels[y][x][]
        short[][][] pixels = new short[cornerTopLeft[1]-cornerBottomRight[1]][cornerTopLeft[0]-cornerBottomRight[0]][3];

        // i along y axis; j along x axis
        for(int i=cornerTopLeft[1]; i<cornerBottomRight[1]; i++) {
            for(int j=cornerTopLeft[0]; j<cornerBottomRight[0]; j++){
                int pixel = bitmap.getPixel(j,i);
                pixels[i][j][0] = (short)Color.red(pixel);
                pixels[i][j][1] = (short)Color.green(pixel);
                pixels[i][j][1] = (short)Color.green(pixel);
            }
        }

        return pixels;
    }

    //get pixels in HSV
    public float[][][] getPixelsHSV(Bitmap bitmap, int[] cornerTopLeft, int[] cornerBottomRight) {

        //cornerTopLeft: (x,y) where x is the coordinate along the horizontal of the image and y is the coordinate along the vertical

        //pixels[y][x][]
        float[][][] pixels = new float[cornerBottomRight[1]-cornerTopLeft[1]][cornerBottomRight[0]-cornerTopLeft[0]][3];

        // i along y axis; j along x axis
        for(int i=cornerTopLeft[1]; i<cornerBottomRight[1]; i++) {
            for(int j=cornerTopLeft[0]; j<cornerBottomRight[0]; j++){
                int pixel = bitmap.getPixel(j,i);

                float hsv[] = new float[3];

                Color.RGBToHSV(Color.red(pixel), Color.green(pixel), Color.blue(pixel), hsv);

                pixels[i-cornerTopLeft[1]][j-cornerTopLeft[0]] = hsv;

            }
        }

        return pixels;

    }

    public short[][] getPixelsGrayscale(Bitmap bitmap) {

        int height = bitmap.getHeight();
        int width = bitmap.getWidth();

        short[][] pixels = new short[height][width];

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int pixel = bitmap.getPixel(j,i);
                pixels[i][j] = (short)Color.red(pixel);
            }
        }

        return pixels;
    }

    public short[][] getPixelsGrayscale(Bitmap bitmap, int[] cornerTopLeft, int[] cornerBottomRight) {

        //cornerTopLeft: (x,y) where x is the coordinate along the horizontal of the image and y is the coordinate along the vertical

        //pixels[y][x][]
        short[][] pixels = new short[cornerBottomRight[1]-cornerTopLeft[1]][cornerBottomRight[0]-cornerTopLeft[0]];

        Log.i("Bitmaps", "Size Y: " + (cornerBottomRight[1]-cornerTopLeft[1]) + ", Size X: " + (cornerBottomRight[0]-cornerTopLeft[0]));
        Log.i("Bitmaps", "Max Y: " + cornerBottomRight[1] + ", Max X: " + cornerBottomRight[0]);

        for(int i=cornerTopLeft[1]; i<cornerBottomRight[1]; i++) {
            for(int j=cornerTopLeft[0]; j<cornerBottomRight[0]; j++){
                int pixel = bitmap.getPixel(j,i);
                pixels[i-cornerTopLeft[1]][j-cornerTopLeft[0]] = (short)Color.red(pixel);
            }
        }

        return pixels;
    }

    public Pixels getGrayscalePixelsAndFeatures(Bitmap bitmap, int[] cornerTopLeft, int[] cornerBottomRight, boolean computeHistogram) {

        Pixels p = new Pixels();
        int max = -1;
        double maxNorm = -1;
        double meanNorm = -1;
        int[] histogram = new int[256];

        //cornerTopLeft: (x,y) where x is the coordinate along the horizontal of the image and y is the coordinate along the vertical

        //pixels[y][x][]
        short[][] pixels = new short[cornerBottomRight[1]-cornerTopLeft[1]][cornerBottomRight[0]-cornerTopLeft[0]];

        Log.i("Bitmaps", "Size Y: " + (cornerBottomRight[1]-cornerTopLeft[1]) + ", Size X: " + (cornerBottomRight[0]-cornerTopLeft[0]));
        Log.i("Bitmaps", "Max Y: " + cornerBottomRight[1] + ", Max X: " + cornerBottomRight[0]);

        for(int i=cornerTopLeft[1]; i<cornerBottomRight[1]; i++) {
            for(int j=cornerTopLeft[0]; j<cornerBottomRight[0]; j++){
                int pixel = bitmap.getPixel(j,i);
                pixels[i-cornerTopLeft[1]][j-cornerTopLeft[0]] = (short)Color.red(pixel);

                if((Color.red(pixel) / 255.0) > maxNorm) {
                    max = Color.red(pixel);
                    maxNorm = (Color.red(pixel) / 255.0);
                }

                meanNorm = meanNorm + (Color.red(pixel) / 255.0);
                histogram[Color.red(pixel)] = histogram[Color.red(pixel)] + 1;
            }
        }

        meanNorm = meanNorm / (pixels.length * pixels[0].length);

        p.setPixels(pixels);
        p.setMax(max);
        p.setMaxNorm(maxNorm);
        p.setMeanNorm(meanNorm);
        p.setHistogram(histogram);

        return p;
    }

    //http://developer.android.com/training/displaying-bitmaps/load-bitmap.html
    public Bitmap decodeSampledBitmapFromFile(String file, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(file, options);
    }

    //http://developer.android.com/training/displaying-bitmaps/load-bitmap.html
    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
