package de.tum.lmt.texturerecognizerappas;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Created by Kev94 on 18.07.2016.
 */
public class ServerTask extends AsyncTask<String, Integer , Void>
{
    private static final String TAG = "ServerTask";

    public byte[] dataToServer;

    //Task state
    private final int UPLOADING_PHOTO_STATE  = 0;
    private final int SERVER_PROC_STATE  = 1;

    private Context mContext;

    private ProgressDialog dialog;

    private FileHandler mFileHandler;

    private boolean mUploadOnly;
    private String mResult;

    private File mParentLoggingDir;

    private iOnResultReceivedListener mOnResultReceivedListener;

    private iOnDataSentListener mOnDataSentListener;

    public ServerTask(Context context, iOnDataSentListener onDataSentListener) {
        mFileHandler = new FileHandler();
        mUploadOnly = true;
        mContext = context;
        mOnDataSentListener = onDataSentListener;
    }

    public ServerTask(Context context, boolean uploadOnly, iOnResultReceivedListener onResultReceivedListener, iOnDataSentListener onDataSentListener) {
        mContext = context;
        mFileHandler = new FileHandler();
        mUploadOnly = uploadOnly;
        mOnResultReceivedListener = onResultReceivedListener;
        mOnDataSentListener = onDataSentListener;
    }

    public interface iOnResultReceivedListener {
        void onResultReceived(String result);
    }

    public interface iOnDataSentListener {
        void onDataSent();
    }

    //upload photo to server
    HttpURLConnection uploadFile(String filename, FileInputStream fileInputStream, String ipAdress, boolean startMatlab)
    {

        final String lineEnd = "\r\n";
        final String twoHyphens = "--";
        final String boundary = "*****";

        try
        {
            String urlStringUplaod = Constants.URL_PREFIX + ipAdress + "/" + Constants.PHP_UPLOAD_FILE_NAME;
            String urlStringUplaodAndMatlab = Constants.URL_PREFIX + ipAdress + "/" + Constants.PHP_UPLOAD_AND_MATLAB_FILE_NAME;
            URL url;
            if(!startMatlab) {
                url = new URL(urlStringUplaod);
            } else {
                url = new URL(urlStringUplaodAndMatlab);
            }
            // Open a HTTP connection to the URL
            final HttpURLConnection conn;


            // Open a HTTP  connection to  the URL
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true); // Allow Inputs
            conn.setDoOutput(true); // Allow Outputs
            conn.setUseCaches(false); // Don't use a Cached Copy
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("ENCTYPE", "multipart/form-data");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            conn.setRequestProperty("uploadedfile", filename);

            Log.i(TAG, "get DOS");
            DataOutputStream dos = new DataOutputStream(conn.getOutputStream());

            if(dos != null) {
                Log.i(TAG, "dos ok");
            } else {
                Log.i(TAG, "no dos");
            }

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + filename +"\"" + lineEnd);
            dos.writeBytes(lineEnd);

            // create a buffer of maximum size
            int bytesAvailable = fileInputStream.available();
            int maxBufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);
            byte[] buffer = new byte[bufferSize];

            // read file and write it into form...
            int bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0)
            {
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            // send multipart form data after file data...
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
            publishProgress(SERVER_PROC_STATE);

            int serverResponseCode = conn.getResponseCode();
            String serverResponseMessage = conn.getResponseMessage();

            Log.i("uploadFile", "HTTP Response is : " + serverResponseMessage + ": " + serverResponseCode);

            // close streams
            fileInputStream.close();
            dos.flush();

            return conn;
        }
        catch (MalformedURLException ex){
            Log.e(TAG, "error: " + ex.getMessage(), ex);
            return null;
        }
        catch (IOException ioe){
            Log.e(TAG, "error: " + ioe.getMessage(), ioe);
            return null;
        }
    }

    //get result from server
    void getResult(HttpURLConnection conn){
        // retrieve the response from server
        InputStream is;
        StringBuilder sb = new StringBuilder();

        Log.i(TAG, "getResult()");

        try {
            is = conn.getInputStream();

            //Bitmap bmp = BitmapFactory.decodeStream(is);
            //mFileHandler.saveBitmapToFile(, "bmp", bmp);

            if(is != null)
            {
                BufferedReader br = null;

                String line;
                try {

                    br = new BufferedReader(new InputStreamReader(is));
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (br != null) {
                        try {
                            br.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            else
            {
                Log.e(TAG, "getResult: no result");
            }


            is.close();
        } catch (IOException e) {
            Log.e(TAG, "getResult(): " + e.toString());
            e.printStackTrace();
        }

        if(sb != null && !sb.toString().isEmpty()) {
            mResult = sb.toString();
            Log.i(TAG, "getResult() result: " + mResult);

            /*FileHandler fileHandler = new FileHandler();
            fileHandler.writeDataToTextFile(mResult, , "mResult");*/
        } else {
            Log.i(TAG, "getRsult(): sb empty");
        }
    }

    //Main code for processing image algorithm on the server

    void processFile(File inputFile, String ipAddress, boolean startMatlab) {
        publishProgress(UPLOADING_PHOTO_STATE);

        Log.i(TAG, "process File");

        try {

            //create file stream for captured image file
            FileInputStream fileInputStream  = new FileInputStream(inputFile);

            //upload photo
            final HttpURLConnection conn = uploadFile(inputFile.getName(), fileInputStream, ipAddress, startMatlab);

            fileInputStream.close();
        }
        catch (FileNotFoundException ex){
            Log.e(TAG, ex.toString());
        }
        catch (IOException ex){
            Log.e(TAG, ex.toString());
        }
    }

    void streamFile(String ipAddress) {



        Log.i(TAG, "streamFile");

        final String lineEnd = "\r\n";
        final String twoHyphens = "--";
        final String boundary = "*****";

        String streamURL = Constants.URL_PREFIX + ipAddress + "/" + Constants.PHP_STREAM_FILE_NAME;

        Log.i(TAG, "streamURL: " + streamURL);

        URL url = null;
        try {
            url = new URL(streamURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        // Open a HTTP connection to the URL
        final HttpURLConnection conn;

        // Open a HTTP  connection to  the URL
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true); // Allow Inputs
            conn.setDoOutput(true); // Allow Outputs
            conn.setUseCaches(false); // Don't use a Cached Copy
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("ENCTYPE", "multipart/form-data");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

            getResult(conn);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected Void doInBackground(String... params) {			//background operation
        Log.i(TAG, "Background Operations");

        String uploadFilePath = params[0];
        //String sensorsFilePath = params[1];
        String ipAddress = params[1];

        File uploadFileDir = new File(uploadFilePath);
        //File sensorFileDir = new File(sensorsFilePath);

        List<File> uploadFiles = mFileHandler.getFileListTwoLevels(uploadFileDir);
        //File[] sensorFiles = sensorFileDir.listFiles();

        int numberOfFiles = uploadFiles.size();
        int i = 0;

        for(File file : uploadFiles) {
            if(i < (numberOfFiles - 1)) {
                processFile(file, ipAddress, false);
            } else {
                if(mUploadOnly) {
                    processFile(file, ipAddress, false);
                    mOnDataSentListener.onDataSent();
                } else {
                    processFile(file, ipAddress, true);
                    mOnDataSentListener.onDataSent();
                    streamFile(ipAddress);
                }
            }
            i++;
        }

        return null;
    }
    //progress update, display dialogs
		/*@Override
		protected void onProgressUpdate(Integer... progress) {
			if(progress[0] == UPLOADING_PHOTO_STATE){
				dialog.setMessage("Uploading");
				dialog.show();
			}
			else if (progress[0] == SERVER_PROC_STATE){
				if (dialog.isShowing()) {
					dialog.dismiss();
				}
				dialog.setMessage("Processing");
				dialog.show();
			}
		}*/
    @Override
    protected void onPostExecute(Void param) {
        Log.i(TAG, "finished");
        //if (dialog.isShowing()) {
        //dialog.dismiss();
        //}

        if(mOnResultReceivedListener != null) {
            mOnResultReceivedListener.onResultReceived(mResult);
        }
    }
}
