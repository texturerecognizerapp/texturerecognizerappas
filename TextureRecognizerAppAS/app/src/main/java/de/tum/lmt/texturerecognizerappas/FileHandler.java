package de.tum.lmt.texturerecognizerappas;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kev94 on 18.02.2016.
 * used for writing files etc.
 */
public class FileHandler {

    public FileHandler() {

    }

    public boolean writeDataToTextFile(String data, String path, String filename) {

        boolean successful = false;

        String fileString = path + File.separator + filename;

        if(!fileString.contains(Constants.TEXT_FILE_EXTENSION)) {
            fileString = fileString.concat(Constants.TEXT_FILE_EXTENSION);
        }

        File file = new File(fileString);

        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        FileOutputStream fOut = null;

        try {
            fOut = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
        try {
            myOutWriter.append(data);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            myOutWriter.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            successful = true;
            fOut.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            successful = false;
            e.printStackTrace();
        }

        return successful;
    }

    public Bitmap loadBitmap(String pathToBitmap) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeFile(pathToBitmap, options);
    }

    public void saveBitmapToFile(String pathToFile, String filename, Bitmap picture) {

        File pictureFile = new File(pathToFile + filename + Constants.JPG_FILE_EXTENSION);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();

        picture.compress(Bitmap.CompressFormat.JPEG, Constants.JPG_COMPRESSION_LEVEL, bytes);

        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(pictureFile);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            fos.write(bytes.toByteArray());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            fos.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public String readTextfile(File file) {

        StringBuilder sb = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));

            String line;

            try {
                while((line = br.readLine()) != null) {
                    Log.i("FileHandler", "line added");
                    sb.append(line);
                    sb.append('\n');
                }
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return sb.toString();

    }

    public List<Double> loadWaveFile(String path) {

        WaveReader waveReader;
        InputStream soundStream = getSoundStream(path);

        if(soundStream != null) {

            try {
                waveReader = new WaveReader(soundStream);

                double[] soundDataArray =  waveReader.getData();

                List<Double> soundDataList = new ArrayList<Double>();

                for(double sound : soundDataArray) {
                    soundDataList.add(sound);
                }

                return soundDataList;

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        else {
            Log.e("FileHandler", "Could not read .wav file");
            return null;
        }
    }

    private InputStream getSoundStream(String path) {

        Log.i("FileHandler", "audio path: " + path);

        File soundData = new File(path);
        InputStream soundStream = null;

        try {
            soundStream = new FileInputStream(soundData);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return soundStream;
    }

    public List<File> getFileListOneLevel(File dir) {

        ArrayList<File> fileList = new ArrayList<>();

        for (File file : dir.listFiles()) {
            if (!file.isDirectory()) {
                fileList.add(file);
            }
        }

        return fileList;
    }

    public List<File> getFileListTwoLevels(File dir) {

        ArrayList<File> fileList = new ArrayList<>();

        Log.i("FileHandler", dir.getAbsolutePath());
        Log.i("FileHandler", "" + dir.length());

        for (File file : dir.listFiles()) {
            if (!file.isDirectory()) {
                fileList.add(file);
            } else {
                for (File file2 : file.listFiles()) {
                    if (!file2.isDirectory()) {
                        fileList.add(file2);
                    }
                }
            }
        }

        return fileList;
    }

    public void deleteFolder(File dir) {

        File[] files = dir.listFiles();

        if(files != null) {
            for(File file : files) {
                if(!file.isDirectory()) {
                    file.delete();
                } else {
                    for(File child : file.listFiles()) {
                        //Log.i("FileHandler", "name: " + child.getName());
                        child.delete();
                    }
                    //Log.i("FileHandler", "name: " + file.getName());
                    file.delete();
                }
            }
        }

        dir.delete();

    }
}
