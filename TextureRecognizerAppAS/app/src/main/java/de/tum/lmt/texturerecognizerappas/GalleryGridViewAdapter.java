package de.tum.lmt.texturerecognizerappas;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by Kev94 on 18.03.2016.
 * http://javatechig.com/android/android-gridview-example-building-image-gallery-in-android
 * code for caching from http://stackoverflow.com/questions/34681466/android-image-gridview-gallery-with-lrucache-images-keep-changing
 */
public class GalleryGridViewAdapter extends ArrayAdapter {
    private Context mContext;
    private int mLayoutResourceId;
    private ArrayList<ImageItem> mData = new ArrayList();

    private MyLruCache mMemoryCache;

    public GalleryGridViewAdapter(Context context, int layoutResourceId, ArrayList<ImageItem> data) {
        super(context, layoutResourceId, data);
        mContext = context;
        mLayoutResourceId = layoutResourceId;
        mData = data;

        final int memClass = ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = 1024 * 1024 * memClass / 4;

        mMemoryCache = new MyLruCache(cacheSize) {

            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in bytes rather than number of items.
                return bitmap.getByteCount();
            }

        };
    }


    @Override
    public int getCount() {
        if(mData != null) {
            return mData.size();
        } else {
            return 0;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;

        if (row == null) {
            //Log.i("Adapter", "row null");
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(mLayoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.imageTitle = (TextView) row.findViewById(R.id.grid_item_text);
            holder.image = (ImageView) row.findViewById(R.id.grid_item_image);
            row.setTag(holder);
        } else {
            //Log.i("Adapter", "row not null");
            holder = (ViewHolder) row.getTag();
        }

        ImageItem item = mData.get(position);
        if (item == null) {
            Log.i("Adapter", "item null");
        } else {
            if (holder == null) {
                Log.i("Adapter", "holder null");
            } else {
                holder.imageTitle.setText(item.getTitle());

                final Bitmap bm = mMemoryCache.getBitmapFromCache(mData.get(position).getTitle());

                if (bm == null) {
                    mMemoryCache.getNewBitmap(holder.image, position);
                }

                if (bm != null && !bm.isRecycled()) {
                    holder.image.setImageBitmap(bm);
                } else {
                    holder.image.setImageResource(R.drawable.white_rectangle);
                }
            }

            if (item.isSelected()){
                row.setBackgroundColor(ContextCompat.getColor(mContext, R.color.blue));
            } else {
                row.setBackgroundColor(ContextCompat.getColor(mContext, R.color.black));
            }

        }
        return row;
    }

    static class ViewHolder {
        TextView imageTitle;
        ImageView image;
    }

    public void getData() {
        Log.i("Adapter", "size: " + mData.size());
    }

    public void setData(ArrayList<ImageItem> data) {
        mData = data;
        notifyDataSetChanged();
    }

    public void removeByTitle(Object object) {
        ImageItem item = (ImageItem) object;
        String title = item.getTitle();

        for(int i=0; i<mData.size(); ) {
            if(mData.get(i).getTitle().equals(item.getTitle())) {
                mData.remove(i);
                break;
            } else {
                i++;
            }
        }
    }

    public void removeByTitle(String title) {

        for(int i=0; i<mData.size(); ) {
            if(mData.get(i).getTitle().equals(title)) {
                mData.remove(i);
                break;
            } else {
                i++;
            }
        }
    }

    public void removeBitmap(String key) {
        mMemoryCache.removeBitmapFromCache(key);
    }

    class MyLruCache extends LruCache<String, Bitmap> {

        public MyLruCache(int maxSize) {
            super(maxSize);
        }

        public void addBitmapToCache(String key, Bitmap bitmap) {
            if (getBitmapFromCache(key) == null) {
                if(bitmap != null)
                    put(key, bitmap);
            }
        }

        public void removeBitmapFromCache(String key) {
            if(getBitmapFromCache(key) != null) {
                getBitmapFromCache(key).recycle();
                remove(key);
            }
        }

        public Bitmap getBitmapFromCache(String key) {
            return (Bitmap) get(key);
        }


        public void getNewBitmap(ImageView imageview, int position) {
            BitmapWorkerTask task = new BitmapWorkerTask(imageview);
            task.execute(mData.get(position));
        }

        class BitmapWorkerTask extends AsyncTask<ImageItem, Void, Bitmap> {

            private final WeakReference<ImageView> imageViewReference;
            ImageItem item;

            public BitmapWorkerTask(ImageView imageView) {
                // Use a WeakReference to ensure the ImageView can be garbage collected
                imageViewReference = new WeakReference<ImageView>(imageView);
            }

            @Override
            protected Bitmap doInBackground(ImageItem... params) {
                ImageItem item = params[0];
                this.item = item;
                final Bitmap bitmap = getBitmapForImageItem(item);
                mMemoryCache.addBitmapToCache(item.getTitle(), bitmap);
                return bitmap;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                if (imageViewReference != null && bitmap != null) {
                    final ImageView imageView = (ImageView) imageViewReference.get();
                    if (imageView != null) {
                        imageView.setImageBitmap(bitmap);
                    }
                }
            }
        }
    }

    public Bitmap getBitmapForImageItem(ImageItem item){

        Bitmap bm = null;

        File dir = item.getDir();

        String picturePath = dir.getAbsolutePath() + File.separator + Constants.CAMERA_NO_FLASH_FILENAME + Constants.JPG_FILE_EXTENSION;

        if (!dir.exists()) {
            Log.i("Gallery", "Picture " + dir + " does not exist");
        } else {
            BitmapOperations bmpOps = new BitmapOperations();
            bm = bmpOps.decodeSampledBitmapFromFile(picturePath, 240, 160);
            //Log.i("Gallery", "File name: " + dir.getName());
        }

        return bm;
    }
}