package de.tum.lmt.texturerecognizerappas;

import android.util.Log;

import java.util.Arrays;

import de.tum.lmt.texturerecognizerappas.math.Matrix;

/**
 * Created by ga38huh on 30.08.2016.
 * This class is a temporary alternative to the MFCC.java class
 * uses the same implementation as the matlab functions "features_mfcc_init" and "features_mfcc"
 * Differences being: MFCCs will be calculated for individual frames of the sound signal
 *                      their median value is then returned as MFCC feature.
 *                    A hamming filter is applied to those frames before the FFT.
 *                    The filter banks used in this iteration have the form of overlapping triangles
 *                      which decrease in height and increase in width for larger frequencies.
 */
public class MFCCalternative {
    private static final double fs = 44100;
    private static final double frame=0.5;
    private static final double step =0.1;
    private static final double lowestFrequency = 133.333333;
    private static final double linearSpacing = 66.666666;
    private static final int numberOfLinearFilters = 13;
    private static final int numberOfCoefficients = 13;
    private static final int numberOfLogFilters = 27;
    private static final double logSpacing = 1.0711703;
    private static final double eps= 2.2204*Math.pow(10,-16);
    private double fftSize;
    private double [] fftFrequency;
    private double [] triangleHeight;
    private int stepSize;
    private int totalNumberOfFilters;
    private int windowLength;
    private int numberOfFrames;
    private double [] lowerFrequency;
    private double [] centerFrequency;
    private double [] upperFrequency;
    private Matrix mfccFilterWeights;
    private Matrix DCTMatrix;
    private Matrix hammingFilter;
    private FFT magnitudeFFT;


    public MFCCalternative(){

        initialSteps();
        hammingFilter=computeHammingFilter();
        DCTMatrix=computeDCTMatrix();
        magnitudeFFT = new FFT(FFT.FFT_MAGNITUDE,windowLength);
        mfccFilterWeights=computeMfccFilterWeights();


    }
    private void initialSteps(){
        //defining the variables
        //windowLength ist temporarily adjusted to 2^14, since the FFT requires a windowLength in form of 2^x
        Log.i("MFCCs", "initialStepsStarted");
        windowLength = (int)Math.pow(2,14);//(int)Math.round(frame*fs);
        stepSize = (int)(step*fs);
        fftSize = Math.round(windowLength/2);
        totalNumberOfFilters = numberOfLogFilters+numberOfLinearFilters;

        double [] fftFrequencyBuffer=new double[(int)fftSize];
        double [] frequency=new double[totalNumberOfFilters+2];
        double [] lowerFrequencyBuffer = new double[totalNumberOfFilters];
        double [] centerFrequencyBuffer = new double[totalNumberOfFilters];
        double [] upperFrequencyBuffer = new double[totalNumberOfFilters];


        for(int i=0;i<fftSize;i++) {
            fftFrequencyBuffer[i] = i/fftSize * fs;
        }

        for (int i=0;i<numberOfLinearFilters;i++){
            frequency[i]=lowestFrequency+i*linearSpacing;
        }

        for(int i=numberOfLinearFilters;i<totalNumberOfFilters+2;i++){
            frequency[i]=frequency[numberOfLinearFilters-1]*Math.pow(logSpacing,(i-12d));
        }

        for(int i=0;i<totalNumberOfFilters;i++) {
            lowerFrequencyBuffer[i] = frequency[i];
            centerFrequencyBuffer[i]= frequency[1+i];
            upperFrequencyBuffer[i] = frequency[2+i];
        }
        double [] triangleHeightBuffer = new double[upperFrequencyBuffer.length];
        for(int i=0;i<upperFrequencyBuffer.length;i++){
            triangleHeightBuffer[i]=2/(upperFrequencyBuffer[i]-lowerFrequencyBuffer[i]);
        }

        this.fftFrequency=fftFrequencyBuffer;
        this.triangleHeight=triangleHeightBuffer;
        this.lowerFrequency=lowerFrequencyBuffer;
        this.centerFrequency=centerFrequencyBuffer;
        this.upperFrequency=upperFrequencyBuffer;

    }
    private Matrix computeDCTMatrix() {
        //computes the DCT Matrix which will be used to transform the cepstrum into
        //the MFCC Coefficients
        Log.i("MFCCs", "computeDCTMatrixStarted ");

        double dctConstant= Math.sqrt(2.0d/(double)totalNumberOfFilters);
        double cosConstant= Math.PI/(double)totalNumberOfFilters;
        Matrix matrix = new Matrix(numberOfCoefficients, totalNumberOfFilters);

        for (int i=0;i<numberOfCoefficients;i++){
            for(int j=0;j<totalNumberOfFilters;j++) {
                if(i==0) {
                matrix.set(i, j, 1/Math.sqrt(2)*dctConstant* Math.cos(cosConstant * i * (j + 0.5d)));
                }
                else
                matrix.set(i, j,dctConstant* Math.cos(cosConstant * i * (j + 0.5d)));
            }
        }
        safeMatrixAsTextFile(matrix,"DCT.txt");
        return matrix;

    }
    private Matrix computeHammingFilter(){
        //computes the hamming filter which will be applied to the signal frame
        //in process() just before the signal is transformed via FFT
        Log.i("MFCCs", "computeHammingFilterStarted " );

        Matrix hamming =new Matrix (1,windowLength);
        for (int i=0;i<windowLength;i++){
            hamming.set(0,i, 0.54-0.46*Math.cos(2*Math.PI*i/(windowLength-1)));
        }
        safeMatrixAsTextFile(hamming,"hamming.txt");

        return hamming;
    }
    private Matrix computeMfccFilterWeights(){
        //computes the filter banks which will be applied to the FFT transformed signal frame
        Log.i("MFCCs", "computeMfccFilterWeightsStarted " );

        Matrix filters=new Matrix(totalNumberOfFilters,(int)fftSize);
        double lowerValue;
        double centerValue;
        double upperValue;
        double triangleValue;
        int triangleWidth;
        int filterStart=(int)Math.floor(lowestFrequency/(1/fftSize*fs));
        int iterationBegin= filterStart;
        int[] iterationEnd=new int[totalNumberOfFilters];
        int counter;

    for(int i=0;i<totalNumberOfFilters;i++){
        counter=0;
        lowerValue=lowerFrequency[i];
        centerValue=centerFrequency[i];
        upperValue=upperFrequency[i];
        triangleValue=triangleHeight[i];
        triangleWidth=(int)Math.ceil((upperValue-lowerValue)/(1/fftSize*fs));

        if(i>1){
            iterationBegin=iterationEnd[i-2];
        }
        if(i==1){
            iterationBegin=iterationBegin+(int)Math.ceil(triangleWidth/2);
        }


        for(int j=iterationBegin;j<(iterationBegin+triangleWidth+10);j++) {
            if ((fftFrequency[j] > lowerValue) && (fftFrequency[j] <= centerValue)) {
                filters.set(i, j, (fftFrequency[j] - lowerValue) / (centerValue - lowerValue) * triangleValue);
                counter++;
            }
            else if ((fftFrequency[j] > centerValue) && (fftFrequency[j] < upperValue)) {
                filters.set(i, j, (upperValue - fftFrequency[j]) / (upperValue - centerValue) * triangleValue);
                counter++;
            }
            else if (counter>0){

                iterationEnd[i]=j;

                break;
            }
        }


    }

        safeMatrixAsTextFile(filters,"filterBanks.txt");

    return filters;
    }
    public double[] process(double[] sound){
        //processing the sound signal by dividing it into frames
        //calculates all MFCCs for each signal
        //returns the median Value of the MFCCs from all the frames
        Log.i("MFCCs", "MFCCProcessingStarted" );

        int currPos = 0;
        int dataLength = sound.length;
        numberOfFrames = (int) Math.floor((dataLength-windowLength)/stepSize)+1;
        Matrix cepstrumConstant= new Matrix(totalNumberOfFilters,1,eps);
        Matrix frameSignal = new Matrix (1,windowLength);
        Matrix frameHammingSignal = new Matrix (1,windowLength);
        Matrix mfccs = new Matrix (numberOfCoefficients,numberOfFrames);
        Matrix frameFFT = new Matrix ((int)fftSize,1);
        double [] features = new double [numberOfCoefficients];
        double [] sortedMfccs = new double [numberOfCoefficients];
        Matrix cepstrum = new Matrix (totalNumberOfFilters,1);
        Matrix filteredFFT = new Matrix (totalNumberOfFilters,1);
        Matrix frameFFTBuffer = new Matrix(1,windowLength);


        for(int i=0;i<numberOfFrames;i++){
            // cutting out a single frame from the signal
            for(int j=0;j<windowLength;j++){
                frameSignal.set(0,j,sound[j+currPos]);
            }
            //applying the hamming filter
            frameHammingSignal=frameSignal.arrayTimes(hammingFilter);
            safeMatrixAsTextFile(frameHammingSignal,"frameHammingBeforeFFT.txt");
            //applying the FFT
            magnitudeFFT.transform(frameHammingSignal.getArray()[0], null);
            safeMatrixAsTextFile(frameHammingSignal,"frameHammingSignalAfterFFT.txt");

            //dividing the transformed signal by its own length and halve it
            frameFFTBuffer=frameHammingSignal.times(1/(double)frameHammingSignal.getColumnDimension());
            for(int j=0;j< (int)fftSize;j++){
                frameFFT.set(j,0,frameFFTBuffer.getArray()[0][j]);
            }
            safeMatrixAsTextFile(frameFFT,"frameFFT.txt");

            //filter banks applied and constant added
            filteredFFT=mfccFilterWeights.times(frameFFT);
            filteredFFT=filteredFFT.plus(cepstrumConstant);
            safeMatrixAsTextFile(filteredFFT,"filteredFFT.txt");

            for(int j=0;j<totalNumberOfFilters;j++) {
                cepstrum.set(j,0,Math.log10(filteredFFT.getArray()[j][0]));
            }
            safeMatrixAsTextFile(cepstrum,"cepstrum.txt");

            //transforming from cepstrum into coefficients via the DCT matrix
            for(int j=0;j<numberOfCoefficients;j++){
                mfccs.set(j,i,DCTMatrix.times(cepstrum).getArray()[j][0]);
            }

            currPos=currPos+stepSize;
        }

        safeMatrixAsTextFile(mfccs,"MFCCs.txt");

        //calculate the median value of all the mfccs
        for (int i=0;i<numberOfCoefficients;i++){
            sortedMfccs=mfccs.getArray()[i];
            Arrays.sort(sortedMfccs);
            features[i]= median(sortedMfccs);
        }


    return features;
    }


    private static double median(double[] m) {
        //calculate median values. will be used for the mfcc processing
        int middle = m.length/2;
        if (m.length%2 == 1) {
            return m[middle];
        } else {
            return (m[middle-1] + m[middle]) / 2.0;
        }
    }
    private void safeMatrixAsTextFile(Matrix inputMatrix, String filename){
        StringBuilder sb=new StringBuilder();
        FileHandler fh=new FileHandler();
        for(int i=0; i<inputMatrix.getRowDimension();i++){
            for(int j=0; j<inputMatrix.getColumnDimension();j++){
                sb.append(inputMatrix.get(i,j) + "\t");
            }
        }
        fh.writeDataToTextFile(sb.toString(), Constants.ANALYSIS_PATH,filename);
    }
    private void safeArrayAsTextFile(double [] inputArray, String filename){
        StringBuilder sb=new StringBuilder();
        FileHandler fh=new FileHandler();
        for(int i=0; i<inputArray.length;i++){
                sb.append(inputArray[i] + "\t");
            }
        fh.writeDataToTextFile(sb.toString(), Constants.ANALYSIS_PATH,filename);
    }
}
