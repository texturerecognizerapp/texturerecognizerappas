package de.tum.lmt.texturerecognizerappas;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;

/**
 * Created by Kev94 on 18.03.2016.
 * http://javatechig.com/android/android-gridview-example-building-image-gallery-in-android
 */
public class ImageItem implements Parcelable {

    private String mTitle;
    private File mDir;
    private boolean mSelected = false;

    public ImageItem(String title, File dir) {
        super();
        mTitle = title;
        mDir = dir;
    }

    public ImageItem(Parcel in){
        this.mTitle = in.readString();
        this.mDir = new File(in.readString());

        boolean[] booleanArray = new boolean[1];
        in.readBooleanArray(booleanArray);
        mSelected = booleanArray[0];
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public File getDir() {
        return mDir;
    }

    public void setSelected(boolean selected) {
        mSelected = selected;
    }

    public boolean isSelected() {
        return mSelected;
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mTitle);
        dest.writeString(mDir.getAbsolutePath());
        dest.writeBooleanArray(new boolean[]{mSelected});
    }

    public static final Parcelable.Creator<ImageItem> CREATOR = new Parcelable.Creator<ImageItem>() {

        @Override
        public ImageItem createFromParcel(Parcel source) {
            return new ImageItem(source);
        }

        @Override
        public ImageItem[] newArray(int size) {
            return new ImageItem[size];
        }
    };
}
