package de.tum.lmt.texturerecognizerappas;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.List;
import java.util.Set;

/**
 * Created by Kev94 on 14.03.2016.
 */
public class Features implements Parcelable {

    //unused features with potential
    private double mVelocity;

    //Features mostly for TPAD
    private double mWeightMacro;
    private double mWeightMicro;
    private double mWeightGlossiness;

    private double mMacroAmplitude;
    private double mMicroAmplitude;
    private double mGlossinessAmplitude;

    //Image Features
    private double mGlossiness;
    private double mImageCentroid;
    private double mImageContrast;
    private double mDominantColor;
    private double mDominantSaturation;
    private double mImageFrequencyBinCount1;
    private double mImageSpectralCentroid;
    private double mImageRegularity;

    //Sound Features
    private double mSoundZeroCrossingRate;
    private double mSoundSignalEnergy;
    private double mSoundNoiseDistribution;
    private double mSoundSpikiness;
    private double mSoundSpectralCentroid;
    private double mSoundSpecralEntropy;
    private double mSoundSpectralRollOff;
    private double mMFCC1;
    private double mMFCC2;
    private double mMFCC5;
    private double mSoundSpectralSum;
    private double mSoundSignalEnergyTap;
    private double mSoundKurtosisTap;
    private double mSoundSpectralCentroidTap;
    private double mSoundSpectralRollOffTap;
    private double mSoundSpectralSumTap;

    private double mExternalAccelerationSpectralSum;
    private double mExternalAccelerationSignalEnergy;
    private double mExternalAccelerationZeroCrossingRate;
    private double mAccelerationSpectralSum;
    private double mAccelerationSignalEnergy;
    private double mAccelerationZeroCrossingRate;

    public Features() {
        super();
    }

    public Features(Parcel in){

        //important!!!! keep order as in writeToParcel(Parcel dest, int flags) below
        this.mVelocity = in.readDouble();

        this.mWeightMacro = in.readDouble();
        this.mWeightMicro = in.readDouble();
        this.mWeightGlossiness = in.readDouble();

        this.mMacroAmplitude = in.readDouble();
        this.mMicroAmplitude = in.readDouble();
        this.mGlossinessAmplitude = in.readDouble();

        this.mGlossiness = in.readDouble();
        this.mImageCentroid = in.readDouble();
        this.mImageContrast = in.readDouble();
        this.mDominantColor = in.readDouble();
        this.mDominantSaturation = in.readDouble();
        this.mImageFrequencyBinCount1 = in.readDouble();
        this.mImageSpectralCentroid = in.readDouble();
        this.mImageRegularity = in.readDouble();

        this.mSoundZeroCrossingRate = in.readDouble();
        this.mSoundSignalEnergy = in.readDouble();
        this.mSoundNoiseDistribution = in.readDouble();
        this.mSoundSpikiness = in.readDouble();
        this.mSoundSpectralCentroid = in.readDouble();
        this.mSoundSpecralEntropy = in.readDouble();
        this.mSoundSpectralRollOff = in.readDouble();
        this.mMFCC1 = in.readDouble();
        this.mMFCC2 = in.readDouble();
        this.mMFCC5 = in.readDouble();
        this.mSoundSpectralSum = in.readDouble();
        this.mSoundSignalEnergyTap = in.readDouble();
        this.mSoundKurtosisTap = in.readDouble();
        this.mSoundSpectralCentroidTap = in.readDouble();
        this.mSoundSpectralRollOffTap = in.readDouble();
        this.mSoundSpectralSumTap = in.readDouble();

        this.mExternalAccelerationSpectralSum = in.readDouble();
        this.mExternalAccelerationSignalEnergy = in.readDouble();
        this.mExternalAccelerationZeroCrossingRate = in.readDouble();

        this.mAccelerationSpectralSum = in.readDouble();
        this.mAccelerationSignalEnergy = in.readDouble();
        this.mAccelerationZeroCrossingRate = in.readDouble();
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        //important!!!! keep order as in Features(Parcel in) constructor above
        dest.writeDouble(mVelocity);

        dest.writeDouble(mWeightMacro);
        dest.writeDouble(mWeightMicro);
        dest.writeDouble(mWeightGlossiness);

        dest.writeDouble(mMacroAmplitude);
        dest.writeDouble(mMicroAmplitude);
        dest.writeDouble(mGlossinessAmplitude);

        dest.writeDouble(mGlossiness);
        dest.writeDouble(mImageCentroid);
        dest.writeDouble(mImageContrast);
        dest.writeDouble(mDominantColor);
        dest.writeDouble(mDominantSaturation);
        dest.writeDouble(mImageFrequencyBinCount1);
        dest.writeDouble(mImageSpectralCentroid);
        dest.writeDouble(mImageRegularity);

        dest.writeDouble(mSoundZeroCrossingRate);
        dest.writeDouble(mSoundSignalEnergy);
        dest.writeDouble(mSoundNoiseDistribution);
        dest.writeDouble(mSoundSpikiness);
        dest.writeDouble(mSoundSpectralCentroid);
        dest.writeDouble(mSoundSpecralEntropy);
        dest.writeDouble(mSoundSpectralRollOff);
        dest.writeDouble(mMFCC1);
        dest.writeDouble(mMFCC2);
        dest.writeDouble(mMFCC5);
        dest.writeDouble(mSoundSpectralSum);
        dest.writeDouble(mSoundSignalEnergyTap);
        dest.writeDouble(mSoundKurtosisTap);
        dest.writeDouble(mSoundSpectralCentroidTap);
        dest.writeDouble(mSoundSpectralRollOffTap);
        dest.writeDouble(mSoundSpectralSumTap);

        dest.writeDouble(mExternalAccelerationSpectralSum);
        dest.writeDouble(mExternalAccelerationSignalEnergy);
        dest.writeDouble(mExternalAccelerationZeroCrossingRate);
        dest.writeDouble(mAccelerationSpectralSum);
        dest.writeDouble(mAccelerationSignalEnergy);
        dest.writeDouble(mAccelerationZeroCrossingRate);


    }

    public static final Parcelable.Creator<Features> CREATOR = new Parcelable.Creator<Features>() {

        @Override
        public Features createFromParcel(Parcel source) {
            return new Features(source);
        }

        @Override
        public Features[] newArray(int size) {
            return new Features[size];
        }
    };

    public double[] getFeaturesArray(Set<String> featuresStringSet) {

        double[] features = new double[featuresStringSet.size()];
        int i = 0;

        for(String string:featuresStringSet) {
            features[i] = getFeature(string);
            i++;
        }

        return features;
    }

    public double[] getFeaturesArray(String[] featuresStringArray) {

        double[] features = new double[featuresStringArray.length];
        int i = 0;

        for(String string:featuresStringArray) {
            features[i] = getFeature(string);
            i++;
        }

        return features;
    }

    public double[] getFeaturesArray(List<String> featuresStringList) {

        double[] features = new double[featuresStringList.size()];

        for(int i=0; i<featuresStringList.size(); i++) {
            features[i] = getFeature(featuresStringList.get(i));
        }

        return features;
    }

    public void setFeature(String string, double value) {

        switch(string) {
            case "Velocity":
                setVelocity(value);
                break;
            case "WeightMacro":
                setWeightMacro(value);
                break;
            case "WeightMicro":
                setWeightMicro(value);
                break;
            case "WeightGlossiness":
                setWeightGlossiness(value);
                break;
            case "MacroAmplitude":
                setMacroAmplitude(value);
                break;
            case "MicroAmplitude":
                setMicroAmplitude(value);
                break;
            case "GlossinessAmplitude":
                setGlossinessAmplitude(value);
                break;
            case Constants.GLOSSINESS:
                setGlossiness(value);
                break;
            case Constants.IMAGE_CENTROID:
                setImageCentroid(value);
                break;
            case Constants.IMAGE_CONTRAST:
                setImageContrast(value);
                break;
            case Constants.DOMINANT_COLOR:
                setDominantColor(value);
                break;
            case Constants.DOMINANT_SATURATION:
                setDominantSaturation(value);
                break;
            case Constants.IMAGE_FREQUENCY_BIN_COUNT_1:
                setImageFrequencyBinCount1(value);
                break;
            case Constants.IMAGE_SPECTRAL_CENTROID:
                setImageSpectralCentroid(value);
                break;
            case Constants.IMAGE_REGULARITY:
                setImageRegularity(value);
                break;
            case Constants.SOUND_ZERO_CROSSING_RATE:
                setSoundZeroCrossingRate(value);
                break;
            case Constants.SOUND_SIGNAL_ENERGY:
                setSoundSignalEnergy(value);
                break;
            case Constants.SOUND_NOISE_DISTRIBUTION:
                setSoundNoiseDistribution(value);
                break;
            case Constants.SOUND_SPIKINESS:
                setSoundSpikiness(value);
                break;
            case Constants.SOUND_SPECTRAL_CENTROID:
                setSoundSpectralCentroid(value);
                break;
            case Constants.SOUND_SPECTRAL_ENTROPY:
                setSoundSpectralEntropy(value);
                break;
            case Constants.SOUND_SPECTRAL_ROLL_OFF:
                setSoundSpectralRollOff(value);
                break;
            case Constants.MFCC1:
                setMFCC1(value);
                break;
            case Constants.MFCC2:
                setMFCC2(value);
                break;
            case Constants.MFCC5:
                setMFCC5(value);
                break;
            case Constants.SOUND_SPECTRAL_SUM:
                setSoundSpectralSum(value);
                break;
            case Constants.SOUND_SIGNAL_ENERGY_TAP:
                setSoundSignalEnergyTap(value);
                break;
            case Constants.SOUND_KURTOSIS_TAP:
                setSoundKurtosisTap(value);
                break;
            case Constants.SOUND_SPECTRAL_CENTROID_TAP:
                setSoundSpectralCentroidTap(value);
                break;
            case Constants.SOUND_SPECTRAL_ROLL_OFF_TAP:
                setSoundSpectralRollOffTap(value);
                break;
            case Constants.SOUND_SPECTRAL_SUM_TAP:
                setSoundSpectralSumTap(value);
                break;
            case Constants.EXTERNAL_ACCELERATION_SPECTRAL_SUM:
                setExternalAccelerationSpectralSum(value);
                break;
            case Constants.EXTERNAL_ACCELERATION_SIGNAL_ENERGY:
                setExternalAccelerationSignalEnergy(value);
                break;
            case Constants.EXTERNAL_ACCELERATION_ZERO_CROSSING_RATE:
                setExternalAccelerationZeroCrossingRate(value);
                break;
            case Constants.ACCELERATION_SPECTRAL_SUM:
                setAccelerationSpectralSum(value);
                break;
            case Constants.ACCELERATION_SIGNAL_ENERGY:
                setAccelerationSignalEnergy(value);
                break;
            case Constants.ACCELERATION_ZERO_CROSSING_RATE:
                setAccelerationZeroCrossingRate(value);
                break;
            default:
                //// TODO: 07.04.2016 return value for success or failure
        }
    }

    public double getFeature(String string) {

        switch(string) {
            case "Velocity":
                return mVelocity;
            case "WeightMacro":
                return mWeightMacro;
            case "WeightMicro":
                return mWeightMicro;
            case "WeightGlossiness":
                return mWeightGlossiness;
            case "MacroAmplitude":
                return mMacroAmplitude;
            case "MicroAmplitude":
                return mMicroAmplitude;
            case "GlossinessAmplitude":
                return mGlossinessAmplitude;
            case Constants.GLOSSINESS:
                return mGlossiness;
            case Constants.IMAGE_CENTROID:
                return mImageCentroid;
            case Constants.IMAGE_CONTRAST:
                return mImageContrast;
            case Constants.DOMINANT_COLOR:
                return mDominantColor;
            case Constants.DOMINANT_SATURATION:
                return mDominantSaturation;
            case Constants.IMAGE_FREQUENCY_BIN_COUNT_1:
                return mImageFrequencyBinCount1;
            case Constants.IMAGE_SPECTRAL_CENTROID:
                return mImageSpectralCentroid;
            case Constants.IMAGE_REGULARITY:
                return mImageRegularity;
            case Constants.SOUND_ZERO_CROSSING_RATE:
                return mSoundZeroCrossingRate;
            case Constants.SOUND_SIGNAL_ENERGY:
                return mSoundSignalEnergy;
            case Constants.SOUND_NOISE_DISTRIBUTION:
                return mSoundNoiseDistribution;
            case Constants.SOUND_SPIKINESS:
                return mSoundSpikiness;
            case Constants.SOUND_SPECTRAL_CENTROID:
                return mSoundSpectralCentroid;
            case Constants.SOUND_SPECTRAL_ENTROPY:
                return mSoundSpecralEntropy;
            case Constants.SOUND_SPECTRAL_ROLL_OFF:
                return mSoundSpectralRollOff;
            case Constants.MFCC1:
                return mMFCC1;
            case Constants.MFCC2:
                return mMFCC2;
            case Constants.MFCC5:
                return mMFCC5;
            case Constants.SOUND_SPECTRAL_SUM:
                return mSoundSpectralSum;
            case Constants.SOUND_SIGNAL_ENERGY_TAP:
                return mSoundSignalEnergyTap;
            case Constants.SOUND_KURTOSIS_TAP:
                return mSoundKurtosisTap;
            case Constants.SOUND_SPECTRAL_CENTROID_TAP:
                return mSoundSpectralCentroidTap;
            case Constants.SOUND_SPECTRAL_ROLL_OFF_TAP:
                return mSoundSpectralRollOffTap;
            case Constants.SOUND_SPECTRAL_SUM_TAP:
                return mSoundSpectralSumTap;
            case Constants.EXTERNAL_ACCELERATION_SPECTRAL_SUM:
                return mExternalAccelerationSpectralSum;
            case Constants.EXTERNAL_ACCELERATION_SIGNAL_ENERGY:
                return mExternalAccelerationSignalEnergy;
            case Constants.EXTERNAL_ACCELERATION_ZERO_CROSSING_RATE:
                return mExternalAccelerationZeroCrossingRate;
            case Constants.ACCELERATION_SPECTRAL_SUM:
                return mAccelerationSpectralSum;
            case Constants.ACCELERATION_SIGNAL_ENERGY:
                return mAccelerationSignalEnergy;
            case Constants.ACCELERATION_ZERO_CROSSING_RATE:
                return mAccelerationZeroCrossingRate;
            default:
                return Double.NaN;
        }
    }

    public double getVelocity() {
        return mVelocity;
    }

    public  void setVelocity(double velocity) {
        mVelocity = velocity;
    }

    public double getWeightMacro() {
        return mWeightMacro;
    }

    public void setWeightMacro(double weightMacro) {
        mWeightMacro = weightMacro;
    }

    public double getWeightMicro() {
        return mWeightMicro;
    }

    public void setWeightMicro(double weightMicro) {
        mWeightMicro = weightMicro;
    }

    public double getWeightGlossiness() {
        return mWeightGlossiness;
    }

    public void setWeightGlossiness(double weightGlossiness) {
        mWeightGlossiness = weightGlossiness;
    }

    public double getMacroAmplitude() {
        Log.i("Features", "macro: " + mMacroAmplitude);
        return mMacroAmplitude;
    }

    public void setMacroAmplitude(double macroAmplitude) {
        mMacroAmplitude = macroAmplitude;
        Log.i("Features", "macro: " + mMacroAmplitude);
    }

    public double getMicroAmplitude() {
        return mMicroAmplitude;
    }

    public void setMicroAmplitude(double microAmplitude) {
        mMicroAmplitude = microAmplitude;
    }

    public double getGlossinessAmplitude() {
        return mGlossinessAmplitude;
    }

    public void setGlossinessAmplitude(double glossinessAmplitude) {
        mGlossinessAmplitude = glossinessAmplitude;
    }

    public double getGlossiness() {
        return mGlossiness;
    }

    public void setGlossiness(double glossiness) {
        mGlossiness = glossiness;
    }

    public double getImageCentroid() {
        return mImageCentroid;
    }

    public void setImageCentroid(double imageCentroid) {
        mImageCentroid = imageCentroid;
    }

    public double getImageContrast() {
        return mImageContrast;
    }

    public void setImageContrast(double imageContrast) {
        mImageContrast = imageContrast;
    }

    public double getDominantColor() {
        return mDominantColor;
    }

    public void setDominantColor(double dominantColor) {
        mDominantColor = dominantColor;
    }

    public double getDominantSaturation() {
        return mDominantSaturation;
    }

    public void setDominantSaturation(double dominantSaturation) {
        mDominantSaturation = dominantSaturation;
    }

    public double getImageFrequencyBinCount1() {
        return mImageFrequencyBinCount1;
    }

    public void setImageFrequencyBinCount1(double imageFrequencyBinCount1) {
        mImageFrequencyBinCount1 = imageFrequencyBinCount1;
    }

    public double getImageSpectralCentroid() {
        return mImageSpectralCentroid;
    }

    public void setImageSpectralCentroid(double imageSpectralCentroid) {
        mImageSpectralCentroid = imageSpectralCentroid;
    }

    public double getImageRegularity() {
        return mImageRegularity;
    }

    public void setImageRegularity(double imageRegularity) {
        mImageRegularity = imageRegularity;
    }

    public double getSoundZeroCrossingRate() {
        return mSoundZeroCrossingRate;
    }

    public void setSoundZeroCrossingRate(double soundZeroCrossingRate) {
        mSoundZeroCrossingRate = soundZeroCrossingRate;
    }

    public double getSoundSignalEnergy() {
        return mSoundSignalEnergy;
    }

    public void setSoundSignalEnergy(double soundSignalEnergy) {
        mSoundSignalEnergy = soundSignalEnergy;
    }

    public double getSoundNoiseDistribution() {
        return 0.5f;//mSoundNoiseDistribution;
    }

    public void setSoundNoiseDistribution(double soundNoiseDistribution) {
        mSoundNoiseDistribution = soundNoiseDistribution;
    }

    public double getSoundSpikiness() {
        return mSoundSpikiness;
    }

    public void setSoundSpikiness(double soundSpikiness) {
        mSoundSpikiness = soundSpikiness;
    }

    public double getSoundSpectralCentroid() {
        return mSoundSpectralCentroid;
    }

    public void setSoundSpectralCentroid(double soundSpectralCentroid) {
        this.mSoundSpectralCentroid = soundSpectralCentroid;
    }

    public double getSoundSpectralEntropy() {
        return mSoundSpecralEntropy;
    }

    public void setSoundSpectralEntropy(double soundSpectralEntropy) {
        this.mSoundSpecralEntropy = soundSpectralEntropy;
    }

    public double getSoundSpectralRollOff() {
        return mSoundSpectralRollOff;
    }

    public void setSoundSpectralRollOff(double soundSpectralRollOff) {
        mSoundSpectralRollOff = soundSpectralRollOff;
    }

    public double getMFCC1() {
        return mMFCC1;
    }

    public void setMFCC1(double MFCC1) {
        mMFCC1 = MFCC1;
    }

    public double getMFCC2() {
        return mMFCC2;
    }

    public void setMFCC2(double MFCC2) {
        mMFCC2 = MFCC2;
    }

    public double getMFCC5() {
        return mMFCC5;
    }

    public void setMFCC5(double MFCC5) {
        mMFCC5 = MFCC5;
    }

    public double getSoundSpectralSum() {
        return mSoundSpectralSum;
    }

    public void setSoundSpectralSum(double soundSpectralSum) {
        mSoundSpectralSum = soundSpectralSum;
    }

    public double getSoundSignalEnergyTap() {
        return mSoundSignalEnergyTap;
    }

    public void setSoundSignalEnergyTap(double soundSignalEnergyTap) {
        mSoundSignalEnergyTap = soundSignalEnergyTap;
    }

    public double getSoundKurtosisTap() {
        return mSoundKurtosisTap;
    }

    public void setSoundKurtosisTap(double soundKurtosisTap) {
        mSoundKurtosisTap = soundKurtosisTap;
    }

    public double getSoundSpectralCentroidTap() {
        return mSoundSpectralCentroidTap;
    }

    public void setSoundSpectralCentroidTap(double soundSpectralCentroidTap) {
        this.mSoundSpectralCentroidTap = soundSpectralCentroidTap;
    }

    public double getSoundSpectralRollOffTap() {
        return mSoundSpectralRollOffTap;
    }

    public void setSoundSpectralRollOffTap(double soundSpectralRollOffTap) {
        mSoundSpectralRollOffTap = soundSpectralRollOffTap;
    }

    public double getSoundSpectralSumTap() {
        return mSoundSpectralSumTap;
    }

    public void setSoundSpectralSumTap(double soundSpectralSumTap) {
        mSoundSpectralSumTap = soundSpectralSumTap;
    }

    public double getExternalAccelerationSpectralSum() {
        return mExternalAccelerationSpectralSum;
    }

    public void setExternalAccelerationSpectralSum(double externalAccelerationSpectralSum) {
        mExternalAccelerationSpectralSum = externalAccelerationSpectralSum;
    }

    public double getExternalAccelerationSignalEnergy() {
        return mExternalAccelerationSignalEnergy;
    }

    public void setExternalAccelerationSignalEnergy(double externalAccelerationSignalEnergy) {
        mExternalAccelerationSignalEnergy = externalAccelerationSignalEnergy;
    }

    public double getExternalAccelerationZeroCrossingRate() {
        return mExternalAccelerationZeroCrossingRate;
    }

    public void setExternalAccelerationZeroCrossingRate(double externalAccelerationZeroCrossingRate) {
        mExternalAccelerationZeroCrossingRate = externalAccelerationZeroCrossingRate;
    }

    public double getAccelerationSpectralSum() {
        return mAccelerationSpectralSum;
    }

    public void setAccelerationSpectralSum(double accelerationSpectralSum) {
        mAccelerationSpectralSum = accelerationSpectralSum;
    }

    public double getAccelerationSignalEnergy() {
        return mAccelerationSignalEnergy;
    }

    public void setAccelerationSignalEnergy(double accelerationSignalEnergy) {
        mAccelerationSignalEnergy = accelerationSignalEnergy;
    }

    public double getAccelerationZeroCrossingRate() {
        return mAccelerationZeroCrossingRate;
    }

    public void setAccelerationZeroCrossingRate(double accelerationZeroCrossingRate) {
        mAccelerationZeroCrossingRate = accelerationZeroCrossingRate;
    }

}
