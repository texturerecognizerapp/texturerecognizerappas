package de.tum.lmt.texturerecognizerappas;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

public class LoggingSensorListener implements SensorEventListener {

	private SensorManager mSensorManager = null;
	private Sensor mAccel = null;
	private Sensor mLinAccel = null;
	private Sensor mGrav = null;
	private Sensor mGyro = null;
	private Sensor mMagnet = null;
	private Sensor mRotVec = null;
	
	private SensorLog mAccelLog;
	private SensorLog mLinAccelLog;
	private SensorLog mGravLog;
	private SensorLog mGyroLog;
	private SensorLog mMagnetLog;
	private SensorLog mRotVecLog;

	private SensorLog mCalibrationLog;

	//tap or move
	int mStep;

	boolean mFirstValue;
	long mPrevTimestamp;
	float[] mPreviousValues;
	double mVelocityX;
	double mVelocityY;
	double mVelocityZ;
	double mVelocity;

	int mCounter;

	boolean mCalibrate;

	float meanX = 0;
	float meanY = 0;
	float meanZ = 0;

	float threshold = 0;

	int thresholdCount = 0;
	
	public LoggingSensorListener(Context context, SensorManager manager, boolean useAccel, boolean useLinAccel, boolean useGrav, boolean useGyro, boolean useMagnet, boolean useRotVec, boolean useExternAccel, int step) {

		mSensorManager = manager;

		mStep = step;
		
		if(useAccel) {
            if (mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
                mAccel = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            } else {
                Toast.makeText(context, context.getString(R.string.sensor_missing), Toast.LENGTH_LONG).show();
            }
        }

		if(useLinAccel) {
			if(mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION) != null) {
				mLinAccel = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
			} else {
				Toast.makeText(context, context.getString(R.string.sensor_missing), Toast.LENGTH_LONG).show();
			}
		}
		
		if(useGrav) {
            if (mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY) != null) {
                mGrav = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
            } else {
                Toast.makeText(context, context.getString(R.string.sensor_missing), Toast.LENGTH_LONG).show();
            }
        }
		
		if(useGyro) {
            if (mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null) {
                mGyro = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
            } else {
                Toast.makeText(context, context.getString(R.string.sensor_missing), Toast.LENGTH_LONG).show();
            }
        }
		
		if(useMagnet) {
            if (mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null) {
                mMagnet = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            } else {
                Toast.makeText(context, context.getString(R.string.sensor_missing), Toast.LENGTH_LONG).show();
            }
        }
		
		if(useRotVec) {
            if (mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR) != null) {
                mRotVec = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
            } else {
                Toast.makeText(context, context.getString(R.string.sensor_missing), Toast.LENGTH_LONG).show();
            }
        }

	}
	
	public void registerListener(int step) {
		
		Log.i("Logging", "register Sensor Listener");

		mFirstValue = true;
		mPrevTimestamp = 0;
		mPreviousValues = new float[3];
		mVelocityX = 0;
		mVelocityY = 0;
		mVelocity = 0;
		mCounter = 0;

		if(step == Constants.STEP_RECORD) {

			mCalibrate = false;

			if (mAccel != null) {
				mSensorManager.registerListener(this, mAccel, SensorManager.SENSOR_DELAY_FASTEST);
				mAccelLog = new SensorLog(Sensor.TYPE_ACCELEROMETER, mStep);
			}
			if(mLinAccel != null) {
				mSensorManager.registerListener(this, mLinAccel, SensorManager.SENSOR_DELAY_FASTEST);
				mLinAccelLog = new SensorLog(Sensor.TYPE_LINEAR_ACCELERATION, mStep);
			}
			if (mGrav != null) {
				mSensorManager.registerListener(this, mGrav, SensorManager.SENSOR_DELAY_GAME);
				mGravLog = new SensorLog(Sensor.TYPE_GRAVITY, mStep);
			}
			if (mGyro != null) {
				mSensorManager.registerListener(this, mGyro, SensorManager.SENSOR_DELAY_GAME);
				mGyroLog = new SensorLog(Sensor.TYPE_GYROSCOPE, mStep);
			}
			if (mMagnet != null) {
				mSensorManager.registerListener(this, mMagnet, SensorManager.SENSOR_DELAY_GAME);
				mMagnetLog = new SensorLog(Sensor.TYPE_MAGNETIC_FIELD, mStep);
			}
			if (mRotVec != null) {
				mSensorManager.registerListener(this, mRotVec, SensorManager.SENSOR_DELAY_GAME);
				mRotVecLog = new SensorLog(Sensor.TYPE_ROTATION_VECTOR, mStep);
			}
		} else {
			mCalibrate = true;

			if(mLinAccel != null) {
				mSensorManager.registerListener(this, mLinAccel, SensorManager.SENSOR_DELAY_FASTEST);
				mCalibrationLog = new SensorLog(Sensor.TYPE_LINEAR_ACCELERATION, mStep);
			}
		}
	}
	
	public void unregisterListener(int step) {
		
		Log.i("Logging", "unregister Sensor Listener");

		if(mAccel != null) {
			mSensorManager.unregisterListener(this, mAccel);
		}
		if(mLinAccel != null) {
			mSensorManager.unregisterListener(this, mLinAccel);
		}
		if(mGrav != null) {
			mSensorManager.unregisterListener(this, mGrav);
		}
		if(mGyro != null) {
			mSensorManager.unregisterListener(this, mGyro);
		}
		if(mMagnet != null) {
			mSensorManager.unregisterListener(this, mMagnet);
		}
		if(mRotVec != null) {
			mSensorManager.unregisterListener(this, mRotVec);
		}

		if(mCalibrate) {

			List<float[]> calibrationValues = null;
			int size = 1;

			if(mCalibrationLog != null) {
				calibrationValues = mCalibrationLog.getValues();
			}

			if(calibrationValues != null) {

				size = calibrationValues.size();

				for (int i = Constants.SAMPLE_RATE_ACCEL; i < size; i++) {
					meanX = meanX + calibrationValues.get(i)[0];
					meanY = meanY + calibrationValues.get(i)[1];
					meanZ = meanZ + calibrationValues.get(i)[2];
				}
			}

			Log.i("Listener", "meanX: " + meanX + ", meanY: " + meanY);

			meanX = meanX / (size-Constants.SAMPLE_RATE_ACCEL);
			meanY = meanY / (size-Constants.SAMPLE_RATE_ACCEL);
			meanZ = meanZ / (size-Constants.SAMPLE_RATE_ACCEL);

			Log.i("Listener", "meanX: " + meanX + ", meanY: " + meanY);

			for (int i = Constants.SAMPLE_RATE_ACCEL; i < size; i++) {

				float temp = (float)Math.sqrt(Math.pow(calibrationValues.get(i)[0],2) + Math.pow(calibrationValues.get(i)[1],2) + Math.pow(calibrationValues.get(i)[2],2));

				if(temp > threshold) {
					threshold = temp;
				}
			}
		}
	}
	
	@Override
	public void onSensorChanged(SensorEvent event) {

		switch(event.sensor.getType()) {
		
		case Sensor.TYPE_ACCELEROMETER:
			
			mAccelLog.addTimestamp(event.timestamp);
			mAccelLog.addValues(event.values);
						
			break;
			case Sensor.TYPE_LINEAR_ACCELERATION:

				if(!mCalibrate) {
					mLinAccelLog.addTimestamp(event.timestamp);
					mLinAccelLog.addValues(event.values);

					float sumX = 0;
					float sumY = 0;
					float sumZ = 0;

					int C = 50;
					int start = Constants.SAMPLE_RATE_ACCEL;

					if(mCounter < C) {
						for(int i = 0; i < mCounter; i++) {
							sumX = sumX + mLinAccelLog.getValues().get(mCounter-i-1)[0];
							sumY = sumY + mLinAccelLog.getValues().get(mCounter-i-1)[1];
							sumZ = sumZ + mLinAccelLog.getValues().get(mCounter-i-1)[2];
						}

						sumZ = sumZ / mCounter;
						sumY = sumY / mCounter;
						sumX = sumX / mCounter;

					} else {
						for(int i = 0; i < C; i++) {
							sumX = sumX + mLinAccelLog.getValues().get(mCounter-i-1)[0];
							sumY = sumY + mLinAccelLog.getValues().get(mCounter-i-1)[1];
							sumZ = sumZ + mLinAccelLog.getValues().get(mCounter-i-1)[2];
						}

						sumZ = sumZ / C;
						sumY = sumY / C;
						sumX = sumX / C;
					}

					if (!mFirstValue && (mCounter > start)){

						meanX = ((Constants.SAMPLE_RATE_ACCEL + mCounter - start + 1) * meanX + sumX) / (Constants.SAMPLE_RATE_ACCEL + mCounter - start + 2);
						meanY = ((Constants.SAMPLE_RATE_ACCEL + mCounter - start + 1) * meanY + sumY) / (Constants.SAMPLE_RATE_ACCEL + mCounter -  start + 2);
						meanZ = ((Constants.SAMPLE_RATE_ACCEL + mCounter - start + 1) * meanZ + sumZ) / (Constants.SAMPLE_RATE_ACCEL + mCounter - start + 2);


						if(Math.sqrt(Math.pow(sumX,2) + Math.pow(sumY,2) + Math.pow(sumZ,2)) < (2 * threshold)) {
							thresholdCount++;
						} else {
							thresholdCount = 0;
						}

						//Log.i("Listener", "thresholdCount: " + thresholdCount);

						if(thresholdCount > 40) {
							//mVelocityX = 0;
							//mVelocityZ = 0;
							sumX = meanX;
							sumY = meanY;
							sumZ = meanZ;
							thresholdCount = 0;
						} else {
							if (meanX != 0 && meanY != 0 && meanZ != 0) {
								//mVelocityX = mVelocityX + (mPreviousValues[0] + (((sumX - meanX) - mPreviousValues[0])/2)) * (event.timestamp - mPrevTimestamp) * Constants.NS2S;
								//mVelocityY = mVelocityY + (mPreviousValues[1] + (((sumY - meanY) - mPreviousValues[1])/2)) * (event.timestamp - mPrevTimestamp) * Constants.NS2S;
								//mVelocityZ = mVelocityZ + (mPreviousValues[2] + (((sumZ - meanZ) - mPreviousValues[2])/2)) * (event.timestamp - mPrevTimestamp) * Constants.NS2S;
								mVelocityX = mVelocityX + (sumX -meanX) * (event.timestamp - mPrevTimestamp) * Constants.NS2S;
								//mVelocityY = mVelocityY + (sumY-meanY) * (event.timestamp - mPrevTimestamp) * NS2S;
								mVelocityZ = mVelocityZ + (sumZ-meanZ) * (event.timestamp - mPrevTimestamp) * Constants.NS2S;
							} else {
								mVelocityX = mVelocityX + event.values[0] * (event.timestamp - mPrevTimestamp) * Constants.NS2S;
								//mVelocityY = mVelocityY + event.values[1] * (event.timestamp - mPrevTimestamp) * NS2S;
								mVelocityZ = mVelocityZ + event.values[2] * (event.timestamp - mPrevTimestamp) * Constants.NS2S;
							}
						}
						mVelocity = Math.sqrt(Math.pow(mVelocityX, 2) + Math.pow(mVelocityZ, 2));
						//Log.i("Listener", "VelocityX: " + mVelocityX + ", VelocityZ: " + mVelocityZ + ", Velocity: " + mVelocity);
						mPrevTimestamp = event.timestamp;
					}

					mPrevTimestamp = event.timestamp;
					mPreviousValues = new float[]{sumX-meanX, sumY-meanY, sumZ-meanZ};
					mCounter++;
					mFirstValue = false;

				} else {

					float[] values = new float[]{event.values[0], event.values[1], event.values[2]};

					mCalibrationLog.addTimestamp(event.timestamp);
					mCalibrationLog.addValues(values);
				}

				break;
		case Sensor.TYPE_GRAVITY:
						
			mGravLog.addTimestamp(event.timestamp);
			mGravLog.addValues(event.values);
			
			break;
		case Sensor.TYPE_GYROSCOPE:
			
			mGyroLog.addTimestamp(event.timestamp);
			mGyroLog.addValues(event.values);
			
			break;
		case Sensor.TYPE_MAGNETIC_FIELD:
			
			mMagnetLog.addTimestamp(event.timestamp);
			mMagnetLog.addValues(event.values);
			
			break;
		case Sensor.TYPE_ROTATION_VECTOR:
			
			mRotVecLog.addTimestamp(event.timestamp);
			mRotVecLog.addValues(event.values);
			
			break;
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	public SensorLog getAccelLog() {
		return mAccelLog;
	}

	public SensorLog getLinAccelLog() {
		return mLinAccelLog;
	}
	
	public SensorLog getGravLog() {
		return mGravLog;
	}
	
	public SensorLog getGyroLog() {
		return mGyroLog;
	}
	
	public SensorLog getMagnetLog() {
		return mMagnetLog;
	}
	
	public SensorLog getRotVecLog() {
		return mRotVecLog;
	}

	public double getCurrentVelocity() {
		return mVelocity;
	}

}
