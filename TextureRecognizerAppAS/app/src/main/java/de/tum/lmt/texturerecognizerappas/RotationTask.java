package de.tum.lmt.texturerecognizerappas;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * Created by Kev94 on 22.09.2016.
 */
public class RotationTask extends AsyncTask<String, String, Exception> {

    private iOnRotatedListener mOnRotatedListener;

    public RotationTask(iOnRotatedListener listener) {
        mOnRotatedListener = listener;
    }

    public interface iOnRotatedListener {
        void onRotated();
    }

    @Override
    protected Exception doInBackground(String... params) {

        Log.i("RotationTask", "RotationTask started");

        String pathToPicture = params[0];
        String pathToPictureFlash = params[1];

        rotatePicture(pathToPicture);
        rotatePicture(pathToPictureFlash);

        return null;
    }

    @Override
    protected void onPostExecute(Exception e) {
        if(mOnRotatedListener != null) {
            mOnRotatedListener.onRotated();
        }
    }

    public void rotatePicture(String Path) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(Path, options);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        final Matrix matrix = new Matrix();
        matrix.setRotate(90);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(),matrix,false);

        bitmap.compress(Bitmap.CompressFormat.JPEG, Constants.JPG_COMPRESSION_LEVEL, bytes);
        bitmap.recycle();

        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(Path);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            fos.write(bytes.toByteArray());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            fos.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
