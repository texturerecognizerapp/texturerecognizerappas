package de.tum.lmt.texturerecognizerappas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by Kev94 on 21.07.2016.
 */
public class SplashScreenActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

}
