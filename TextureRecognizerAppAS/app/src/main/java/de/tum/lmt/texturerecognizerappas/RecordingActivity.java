package de.tum.lmt.texturerecognizerappas;

/**
 * Created by Kev94 on 05.10.2016.
 */

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class RecordingActivity extends Activity {

    private static final String TAG = RecordingActivity.class.getSimpleName();

    private static final float ALPHA_ENABLED = 0.6f;
    private static final float ALPHA_DISABLED = 0.4f;

    //tap or move
    int mStep;

    //settings
    boolean mUseAccel;
    boolean mUseLinAccel;
    boolean mUseExternAccel;
    boolean mUseGrav;
    boolean mUseGyro;
    boolean mUseMagnet;
    boolean mUseRotVec;
    boolean mUseFSR;
    boolean mUseVelocity;
    String mDurationToLog;

    //Bluetooth
    private static final int REQUEST_ENABLE_BT = 0;

    //File Output
    private FileHandler mFileHandler;
    private File mParentLoggingDir;
    private String mSensorSingleRecordingLoggingPath;
    private String mSensorTapLoggingPath;
    private String mSensorMoveLoggingPath;
    private File mSensorSingleRecordingLoggingDir;
    private File mSensorTapLoggingDir;
    private File mSensorMoveLoggingDir;

    //Sensors
    LoggingSensorListener mListener;

    //tap (database); not used for single recording
    SensorLog mAccelLogTap;
    SensorLog mAccelMinusOffsetLogTap;
    SensorLog mLinAccelLogTap;
    SensorLog mGravLogTap;
    SensorLog mGyroLogTap;
    SensorLog mMagnetLogTap;
    SensorLog mRotVecLogTap;
    List<SensorLog> mExternAccelLogListTap;
    List<Integer> mFSRValuesTap;

    //move (database) and for single recording
    SensorLog mAccelLogMove;
    SensorLog mAccelMinusOffsetLogMove;
    SensorLog mLinAccelLogMove;
    SensorLog mGravLogMove;
    SensorLog mGyroLogMove;
    SensorLog mMagnetLogMove;
    SensorLog mRotVecLogMove;
    List<SensorLog> mExternAccelLogListMove;
    List<Integer> mFSRValuesMove;

    //Audio
    private AudioRecorderWAV mRecorder;

    //tap
    private Byte[] mRawSoundDataTap;

    //move
    private Byte[] mRawSoundDataMove;

    //UI Elements
    private ImageView mGlow;
    private ImageView mImageViewInstructions;
    private Button mButtonCalibrate;
    private ImageButton mButtonStartLogging;
    private ImageButton mButtonCancelLogging;
    private TextView mText;
    private TextView mTextViewButtonStart;
    private ProgressBar mTimerBar;
    private ProgressBar mVelocityBar;

    private AnimatorSet mInstructionAnimationsTap;
    private AnimatorSet mInstructionAnimationsMove;

    private AlertDialog mProcessingDialog;

    private boolean mDatabaseMode;

    private boolean mIsLogging = false;
    private boolean mLoggingTapFinished = false;
    private boolean mLoggingMoveFinished = false;

    private ServerTask.iOnResultReceivedListener mOnOnlineResultListener;
    private ServerTask.iOnDataSentListener mOnDataSentListener;

    private FeatureTask.iOnFeatureTaskFinishedListener mFeatureListener;

    private long mFeaturesTimeStart;
    private long mFeaturesTimeStop;

    //Bluetooth
    Bluetooth mBT;

    //Timers
    private Handler mTimerHandler;

    private Vibrator mVibrator;

    private String mGapFiller;

    private long mClassificationTimeStart;
    private long mClassificationTimeStop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recording);

        final SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        mUseAccel = sharedPrefs.getBoolean(Constants.PREF_KEY_ACCEL_SELECT, false);
        mUseLinAccel = sharedPrefs.getBoolean(Constants.PREF_KEY_LIN_ACCEL_SELECT, false);
        mUseGrav = sharedPrefs.getBoolean(Constants.PREF_KEY_GRAV_SELECT, false);
        mUseGyro = sharedPrefs.getBoolean(Constants.PREF_KEY_GYRO_SELECT, false);
        mUseMagnet = sharedPrefs.getBoolean(Constants.PREF_KEY_MAGNET_SELECT, false);
        mUseRotVec = sharedPrefs.getBoolean(Constants.PREF_KEY_ROTVEC_SELECT, false);
        mUseExternAccel = sharedPrefs.getBoolean(Constants.PREF_KEY_EXTERN_ACCEL, false);
        mUseFSR = sharedPrefs.getBoolean(Constants.PREF_KEY_FSR, false);
        mDatabaseMode = sharedPrefs.getBoolean(Constants.PREF_KEY_MODE_SELECT, false);

        String loggingDir = sharedPrefs.getString(Constants.PREF_KEY_LOGGING_DIR, null);

        if(loggingDir != null) {
            mParentLoggingDir = new File(loggingDir);
        } else {
            Log.e(TAG, "loggingDir null");
        }

        if(mDatabaseMode) {
            mDurationToLog = sharedPrefs.getString(Constants.PREF_KEY_DURATION_DB, "35000");
            Log.i("duration", "duration: " + mDurationToLog + Long.valueOf(mDurationToLog));

            mStep = Constants.STEP_TAP;

            mSensorTapLoggingPath = mParentLoggingDir.getAbsolutePath() + File.separator + Constants.SENSOR_TAP_LOGGING_FOLDER_NAME;
            mSensorMoveLoggingPath = mParentLoggingDir.getAbsolutePath() + File.separator + Constants.SENSOR_MOVE_LOGGING_FOLDER_NAME;

            mSensorTapLoggingDir = new File(mSensorTapLoggingPath);
            mSensorMoveLoggingDir = new File(mSensorMoveLoggingPath);

        } else {
            mDurationToLog = sharedPrefs.getString(Constants.PREF_KEY_DURATION, "5000");
            Log.i("duration", "duration: " + mDurationToLog + Long.valueOf(mDurationToLog));

            //mStep = Constants.STEP_SINGLE_RECORDING;
            mStep = Constants.STEP_TAP;

            mSensorSingleRecordingLoggingPath = mParentLoggingDir.getAbsolutePath() + File.separator + Constants.SENSOR_SINGLE_RECORDING_LOGGING_FOLDER_NAME;

            mSensorSingleRecordingLoggingDir = new File(mSensorSingleRecordingLoggingPath);
        }

        mFileHandler = new FileHandler();

        if(mUseExternAccel || mUseFSR) {
            mBT = new Bluetooth(getApplicationContext(), false);
        }

        mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        mTimerHandler = new Handler();

        mText = (TextView) findViewById(R.id.textview_tap_move_recording);
        mText.setText(getString(R.string.tap));

        mButtonCalibrate = (Button) findViewById(R.id.button_calibrate_recording);
        mButtonCalibrate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                final SensorManager manager = (SensorManager) getSystemService(SENSOR_SERVICE);

                mListener = new LoggingSensorListener(getApplicationContext(), manager, mUseAccel, mUseLinAccel, mUseGrav, mUseGyro, mUseMagnet, mUseRotVec, mUseExternAccel, mStep);

                mListener.registerListener(Constants.STEP_CALIBRATE);

                mTimerHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mListener.unregisterListener(Constants.STEP_CALIBRATE);
                    }
                }, 2000);
            }
        });

        mImageViewInstructions = (ImageView) findViewById(R.id.image_view_instructions);
        mImageViewInstructions.setVisibility(View.VISIBLE);

        final ObjectAnimator tabAnim1 = new ObjectAnimator();
        tabAnim1.setPropertyName("translationY");
        tabAnim1.setFloatValues(70);
        tabAnim1.setTarget(mImageViewInstructions);
        tabAnim1.setInterpolator(new AccelerateInterpolator(2.0f));
        tabAnim1.setDuration(400);

        final ObjectAnimator tabAnim2 = new ObjectAnimator();
        tabAnim2.setPropertyName("translationY");
        tabAnim2.setFloatValues(0);
        tabAnim2.setTarget(mImageViewInstructions);
        tabAnim2.setInterpolator(new LinearInterpolator());
        tabAnim2.setDuration(400);

        mInstructionAnimationsTap = new AnimatorSet();
        mInstructionAnimationsTap.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mInstructionAnimationsTap.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                /*tabAnim2.start();*/
            }
        });

        mInstructionAnimationsTap.play(tabAnim1).after(300);
        mInstructionAnimationsTap.play(tabAnim2).after(1000).after(tabAnim1);

        mInstructionAnimationsTap.start();

        final ObjectAnimator moveAnim1 = new ObjectAnimator();
        moveAnim1.setPropertyName("translationX");
        moveAnim1.setFloatValues(-70);
        moveAnim1.setTarget(mImageViewInstructions);
        moveAnim1.setInterpolator(new LinearInterpolator());
        moveAnim1.setDuration(250);

        final ObjectAnimator moveAnim2 = new ObjectAnimator();
        moveAnim2.setPropertyName("rotationY");
        moveAnim2.setFloatValues(0, -30);
        moveAnim2.setTarget(mImageViewInstructions);
        moveAnim2.setInterpolator(new LinearInterpolator());
        moveAnim2.setDuration(250);

        final ObjectAnimator moveAnim3 = new ObjectAnimator();
        moveAnim3.setPropertyName("translationX");
        moveAnim3.setFloatValues(70);
        moveAnim3.setTarget(mImageViewInstructions);
        moveAnim3.setInterpolator(new LinearInterpolator());
        moveAnim3.setDuration(500);

        final ObjectAnimator moveAnim4 = new ObjectAnimator();
        moveAnim4.setPropertyName("rotationY");
        moveAnim4.setFloatValues(-30, 30);
        moveAnim4.setTarget(mImageViewInstructions);
        moveAnim4.setInterpolator(new LinearInterpolator());
        moveAnim4.setDuration(500);

        final ObjectAnimator moveAnim5 = new ObjectAnimator();
        moveAnim5.setPropertyName("translationX");
        moveAnim5.setFloatValues(0);
        moveAnim5.setTarget(mImageViewInstructions);
        moveAnim5.setInterpolator(new LinearInterpolator());
        moveAnim5.setDuration(250);

        final ObjectAnimator moveAnim6 = new ObjectAnimator();
        moveAnim6.setPropertyName("rotationY");
        moveAnim6.setFloatValues(30, 0);
        moveAnim6.setTarget(mImageViewInstructions);
        moveAnim6.setInterpolator(new LinearInterpolator());
        moveAnim6.setDuration(250);

        mInstructionAnimationsMove = new AnimatorSet();
        mInstructionAnimationsMove.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                /*moveAnim5.start();
                moveAnim6.start();*/
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mInstructionAnimationsMove.start();
            }
        });

        mInstructionAnimationsMove.play(moveAnim1).with(moveAnim2);
        mInstructionAnimationsMove.play(moveAnim3).with(moveAnim4).after(500).after(moveAnim1);
        mInstructionAnimationsMove.play(moveAnim5).with(moveAnim6).after(1250).after(moveAnim3);


        mGlow = (ImageView) findViewById(R.id.button_glow_circle);
        mGlow.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Animation pulseAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.pulse);
                mGlow.startAnimation(pulseAnimation);

                mGlow.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        mButtonStartLogging = (ImageButton) findViewById(R.id.button_start_recording);
        mButtonStartLogging.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                if (!mLoggingMoveFinished) {

                    mTimerHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            if (mBT != null) {
                                mBT.findBluetooth();
                                try {
                                    mBT.openBT();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                            startLogging();
                        }
                    }, Constants.DURATION_WAIT);
                } else {

                    if(mInstructionAnimationsMove.isRunning()) {
                        mInstructionAnimationsMove.cancel();
                    }

                    if (mDatabaseMode) {
                        finish();
                    } else {

                        boolean onlineProcessing = sharedPrefs.getBoolean(Constants.PREF_KEY_ONLINE, false);

                        if (onlineProcessing) {
                            showProcessingDialog(getString(R.string.processing));

                            mClassificationTimeStart = System.currentTimeMillis();

                            String IP = sharedPrefs.getString(Constants.PREF_KEY_IP, "192.168.2.1");
                            String classificationMethod = sharedPrefs.getString(Constants.PREF_KEY_CLASSIFICATION_METHOD, "k-nearest-neighbor");
                            Set<String> classificationFeatures = sharedPrefs.getStringSet(Constants.PREF_KEY_CLASSIfICATION_FEATURES, null);
                            String neighborhoodSize = sharedPrefs.getString(Constants.PREF_KEY_NEIGHBORHOOD, "10");

                            //construct settings JSON
                            JSONObject jsonSettings = new JSONObject();
                            try {
                                jsonSettings.put(Constants.PREF_TITLE_CLASSIFICATION_METHOD, classificationMethod);

                                if (classificationMethod.equals(Constants.CLASSIFICATION_KNN)) {
                                    jsonSettings.put(Constants.PREF_TITLE_NEIGHBORHOOD_SIZE, neighborhoodSize);
                                }

                                JSONObject jsonClassificationFeatures = new JSONObject();

                                int i = 0;

                                if (classificationFeatures != null) {
                                    for (String element : classificationFeatures) {
                                        if (element != null) {
                                            jsonClassificationFeatures.put("Feature" + i, element);
                                            i++;
                                        }
                                    }
                                } else {
                                    Log.e(TAG, "classificationFeatures null");
                                }

                                jsonSettings.put(Constants.PREF_TITLE_CLASSIFICATION_FEATURES, jsonClassificationFeatures);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            String jsonSettingsString = jsonSettings.toString();

                            mFileHandler.writeDataToTextFile(jsonSettingsString, mParentLoggingDir.getAbsolutePath(), "jsonClassificationSettings.txt");

                            ServerTask serverTask = new ServerTask(getApplicationContext(), false, mOnOnlineResultListener, mOnDataSentListener);
                            serverTask.execute(mParentLoggingDir.getAbsolutePath(), IP);

                        } else {

                            showProcessingDialog(getString(R.string.textview_features));

                            mFeatureListener = new FeatureTask.iOnFeatureTaskFinishedListener() {
                                @Override
                                public void onFeatureTaskFinished(Features features) {

                                    if (features == null) {
                                        Log.e(TAG, "features on finished null");
                                    }
                                    mFeaturesTimeStop = System.currentTimeMillis();

                                    Toast.makeText(getApplicationContext(), "Computing Features took " + (mFeaturesTimeStop - mFeaturesTimeStart) + " ms.", Toast.LENGTH_LONG).show();

                                    if (mProcessingDialog != null) {
                                        mProcessingDialog.dismiss();
                                    }

                                    if (!mDatabaseMode) {
                                        showFinalStepDialog(features);
                                    } else {
                                        finish();
                                    }
                                }
                            };

                            FeatureTask featureTask = new FeatureTask(getBaseContext(), mFeatureListener, mRawSoundDataTap, mRawSoundDataMove,
                                    mAccelMinusOffsetLogTap, mAccelMinusOffsetLogMove, mExternAccelLogListTap, mExternAccelLogListMove);

                            featureTask.execute();

                            mFeaturesTimeStart = System.currentTimeMillis();

                        }
                    }
                }
            }
        });

        mTextViewButtonStart = (TextView) findViewById(R.id.button_start_description);

        /*mButtonCancelLogging = (ImageButton) findViewById(R.id.button_cancel_recording);
        mButtonCancelLogging.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                if (mLoggingMoveFinished) {
                    mGapFiller = getString(R.string.gapFiller_after) + " " + getString(R.string.logging);
                    showCancelDialog(mGapFiller);
                } else {
                    mGapFiller = getString(R.string.gapFiller_before) + " " + getString(R.string.logging);
                    showCancelDialog(mGapFiller);
                }
            }
        });*/

        mOnDataSentListener = new ServerTask.iOnDataSentListener() {
            @Override
            public void onDataSent() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "data sent", Toast.LENGTH_LONG).show();
                    }
                });
            }
        };

        mOnOnlineResultListener = new ServerTask.iOnResultReceivedListener() {
            @Override
            public void onResultReceived(String jsonResultString) {

                String classificationMethod = sharedPrefs.getString(Constants.PREF_KEY_CLASSIFICATION_METHOD, "k-nearest-neighbor");
                String neighborhoodSize = sharedPrefs.getString(Constants.PREF_KEY_NEIGHBORHOOD, "10");
                String distToMean = Constants.CLASSIFICATION_DIST_TO_MEAN;
                String Mahal = Constants.CLASSIFICATION_MAHALANOBIS;
                String kNearestNeighbor = Constants.CLASSIFICATION_KNN;

                List<Texture.Sample> result = new ArrayList<>();

                JSONObject jsonResult = null;
                try {
                    jsonResult = new JSONObject(jsonResultString);

                    if (classificationMethod.equals(distToMean)) {

                        for (int i = 1; i <= Constants.NUMBER_OF_TEXTURES; i++) {

                            JSONObject jsonSample;

                            try {
                                jsonSample = jsonResult.getJSONObject(Integer.toString(i));
                            } catch(JSONException e) {
                                break;
                            }

                            String name = jsonSample.getString("name");
                            double distance = jsonSample.getDouble("value");

                            Texture texture = new Texture(name);
                            texture.addSampleWithDistance(distance);

                            result.add(texture.getSamples().get(0));

                        }
                    } else if (classificationMethod.equals(Mahal)) {

                        for (int i = 1; i <= Constants.NUMBER_OF_TEXTURES; i++) {

                            JSONObject jsonSample;

                            try {
                                jsonSample = jsonResult.getJSONObject(Integer.toString(i));
                            } catch(JSONException e) {
                                break;
                            }

                            String name = jsonSample.getString("name");
                            double distance = jsonSample.getDouble("value");

                            Texture texture = new Texture(name);
                            texture.addSampleWithDistance(distance);

                            result.add(texture.getSamples().get(0));

                        }

                    } else if (classificationMethod.equals(kNearestNeighbor)) {

                        for (int i = 1; i <= Integer.getInteger(neighborhoodSize, 10); i++) {

                            JSONObject jsonSample;

                            try {
                                jsonSample = jsonResult.getJSONObject(Integer.toString(i));
                            } catch(JSONException e) {
                                break;
                            }

                            String name = jsonSample.getString("name");
                            int occurrence = jsonSample.getInt("value");

                            Texture texture = new Texture(name);
                            texture.addSampleWithOccurence(occurrence);

                            result.add(texture.getSamples().get(0));

                            /*for(Texture.Sample sample:result) {

                                if(name.equals(sample.getName())) {
                                    sample.setOccurence(sample.getOccurence()+1);
                                    break;
                                } else {
                                    Texture texture = new Texture(name);
                                    texture.addSampleWithOccurence(1);

                                    result.add(texture.getSamples().get(0));
                                }
                            }*/

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mProcessingDialog.dismiss();

                mClassificationTimeStop = System.currentTimeMillis();

                Toast.makeText(getApplicationContext(), "Classification took " + (mClassificationTimeStop - mClassificationTimeStart) + " ms.", Toast.LENGTH_LONG).show();

                if(jsonResult == null) {
                    Log.e("Classification", "Classfication not successful");
                    Toast.makeText(getApplicationContext(), getString(R.string.classification_null), Toast.LENGTH_LONG).show();
                } else {
                    showClassificationListDialog(result, classificationMethod);
                }

            }
        };

        mTimerBar = (ProgressBar) findViewById(R.id.timer_bar_recording);
        mTimerBar.setMax(Integer.valueOf(mDurationToLog) + (int) Constants.DURATION_EXTRA);
        mTimerBar.setProgress(Integer.valueOf(mDurationToLog) + (int) Constants.DURATION_EXTRA);

        mVelocityBar = (ProgressBar) findViewById(R.id.velocity_bar_recording);
        mVelocityBar.setMax(Constants.MAX_VELOCITY_BAR_PROGRESS);
        mVelocityBar.setProgress(0);

        mButtonCalibrate.setEnabled(false);
        mButtonCalibrate.setClickable(false);
        mButtonCalibrate.setVisibility(View.GONE);
    }

    protected void showProcessingDialog(String string) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        RelativeLayout rl = new RelativeLayout(this);

        ProgressBar progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);
        RelativeLayout.LayoutParams progressBarParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView text = new TextView(this);
        text.setText(string);
        RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        textParams.addRule(RelativeLayout.CENTER_VERTICAL);

        rl.addView(progressBar, progressBarParams);
        rl.addView(text, textParams);

        builder.setView(rl);

        mProcessingDialog = builder.create();
        mProcessingDialog = builder.show();

    }

    private void showClassificationListDialog(List<Texture.Sample> textures, String classificationMethod) {
        DialogClassificationListFragment dialogClassificationList = new DialogClassificationListFragment(textures, classificationMethod);
        dialogClassificationList.show(getFragmentManager(), "dialogClassificationList");
    }

    protected void showFeaturesDialog() {

        //// TODO: 19.05.2016 perhaps create list of SensorLogs and pass it to dialog?

        //feature dialog only opened in single recording mode
        //data with suffix move is also used for single recording mode -> pass this data to the features dialog
        DialogFragment featuresDialog = new DialogFeaturesFragment(getBaseContext(), mRawSoundDataMove, mAccelMinusOffsetLogMove, mExternAccelLogListMove, mGravLogMove, mGyroLogMove, mMagnetLogMove, mRotVecLogMove);
        featuresDialog.show(getFragmentManager(), "DialogFeaturesFragment");
    }

    private void showFinalStepDialog(Features features) {

        if(features == null) {
            Log.e(TAG, "features null");
        }

        Log.i("FinalStep", "MFCC1: " + features.getMFCC1());

        DialogFinalStepFragment finalStepFragment = DialogFinalStepFragment.newInstance(features);
        finalStepFragment.show(getFragmentManager(), "DialogFinalStepFragment");
    }

    protected void showCancelDialog(String gapFiller) {
        DialogCancelFragment cancelDialog = DialogCancelFragment.newInstanceOf(gapFiller, mLoggingMoveFinished);
        cancelDialog.show(getFragmentManager(), "DialogSensorCheckFragment");
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mIsLogging) {
            stopLogging();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_ENABLE_BT) {
            if(resultCode == Activity.RESULT_OK) {
                Toast toast = Toast.makeText(getApplicationContext(), R.string.toast_enabled_bt, Toast.LENGTH_SHORT);
                toast.show();
            }
            else {
                Toast toast = Toast.makeText(getApplicationContext(), R.string.toast_error_bt, Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

    private void startLogging() {

        if (!mDatabaseMode && !mSensorSingleRecordingLoggingDir.exists()) {
            if (!mSensorSingleRecordingLoggingDir.mkdirs()) {
                Log.e(TAG, "not able to create SensorSingleRecordingLoggingDir");
            }
        } else if (mDatabaseMode && (mStep == Constants.STEP_TAP) && !mSensorTapLoggingDir.exists()) {
            if (!mSensorTapLoggingDir.mkdirs()) {
                Log.e(TAG, "not able to create mSensorTapLoggingDir");
            }
        } else if (mDatabaseMode && (mStep == Constants.STEP_MOVE) && !mSensorMoveLoggingDir.exists()) {
            if (!mSensorMoveLoggingDir.mkdirs()) {
                Log.e(TAG, "not able to create mSensorMoveLoggingDir");
            }
        }

        if (!mIsLogging && !mLoggingMoveFinished) {

            mVibrator.vibrate(Constants.DURARTION_VIBRATE);

            if(mStep == Constants.STEP_TAP) {
                if(mInstructionAnimationsTap.isRunning()) {
                    mInstructionAnimationsTap.cancel();
                }
            } else if(mStep == Constants.STEP_MOVE) {
                if(mInstructionAnimationsMove.isRunning()) {
                    mInstructionAnimationsMove.cancel();
                }
            }

            mButtonStartLogging.setEnabled(false);
            mButtonStartLogging.setClickable(false);
            mButtonStartLogging.setAlpha(ALPHA_DISABLED);
            mGlow.clearAnimation();

            mRecorder = new AudioRecorderWAV(getBaseContext(), mStep);
            mRecorder.startRecording();

            if (mListener == null) {
                final SensorManager manager = (SensorManager) getSystemService(SENSOR_SERVICE);
                mListener = new LoggingSensorListener(getApplicationContext(), manager, mUseAccel, mUseLinAccel, mUseGrav, mUseGyro, mUseMagnet, mUseRotVec, mUseExternAccel, mStep);
            }

            mListener.registerListener(Constants.STEP_RECORD);

            //tell microcontroller to start logging acceleration data
            if (mBT != null) {
                try {
                    mBT.sendData("b");
                    //Toast.makeText(this, "b", Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            mIsLogging = true;

            CountDownTimer countDownTimer = new CountDownTimer(Long.valueOf(mDurationToLog) + Constants.DURATION_EXTRA, 100) {
                @Override
                public void onTick(long millisUntilFinished) {
                    double currentVelocity = mListener.getCurrentVelocity();

                    int progress = (int) (currentVelocity * Constants.MAX_VELOCITY_BAR_PROGRESS);

                    //Log.i(TAG, "progress: " + progress);

                    if (progress > Constants.MAX_VELOCITY_BAR_PROGRESS) {
                        mVelocityBar.setProgress(Constants.MAX_VELOCITY_BAR_PROGRESS);
                    } else if (progress < 0) {
                        mVelocityBar.setProgress(0);
                    } else {
                        mVelocityBar.setProgress(progress);
                    }
                }

                @Override
                public void onFinish() {
                    stopLogging();
                }
            };

            ObjectAnimator animation = ObjectAnimator.ofInt(mTimerBar, "progress", mTimerBar.getMax(), 0);
            animation.setDuration(Long.valueOf(mDurationToLog) + Constants.DURATION_EXTRA);
            animation.setInterpolator(new LinearInterpolator());
            animation.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                }

                @Override
                public void onAnimationEnd(Animator animator) {

                }

                @Override
                public void onAnimationCancel(Animator animator) {
                }

                @Override
                public void onAnimationRepeat(Animator animator) {
                }
            });

            countDownTimer.start();
            animation.start();
        }
    }

    private void stopLogging() {

        if(mIsLogging) {

            //mTimedUpdateHandler.removeCallbacks(mTimedUpdateRunnable);

            mRecorder.stopRecording();

            mListener.unregisterListener(Constants.STEP_RECORD);

            if (mBT != null) {
                try {
                    mBT.sendData("e");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if(mInstructionAnimationsMove.isRunning()) {
                mInstructionAnimationsMove.cancel();
            }
            if(mInstructionAnimationsTap.isRunning()) {
                mInstructionAnimationsTap.cancel();
            }

            mIsLogging = false;

            //delay, because it could be that extern values have not always been received until now
            mTimerHandler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    mVibrator.vibrate(Constants.DURARTION_VIBRATE);

                    if (mBT != null) {
                        try {
                            mBT.closeBT();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    if (mStep == Constants.STEP_TAP) {

                        mLoggingTapFinished = true;

                        mText.setText(getString(R.string.scratch));
                        mButtonStartLogging.setEnabled(true);
                        mButtonStartLogging.setClickable(true);
                        mButtonStartLogging.setAlpha(ALPHA_ENABLED);
                        Animation pulseAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.pulse);
                        mGlow.startAnimation(pulseAnimation);
                        mInstructionAnimationsMove.start();

                        getLoggingData();

                        mAccelMinusOffsetLogTap = subtractOffsetFromAccelData(mAccelLogTap);

                        if (mDatabaseMode) {
                            writeLoggingDataToFile(mStep, mSensorTapLoggingDir, mAccelMinusOffsetLogTap, mLinAccelLogTap, mGravLogTap, mGyroLogTap, mMagnetLogTap, mRotVecLogTap, mExternAccelLogListTap, mFSRValuesTap);
                            freeMemory();
                        } else {
                            writeLoggingDataToFile(mStep, mSensorSingleRecordingLoggingDir, mAccelMinusOffsetLogTap, mLinAccelLogTap, mGravLogTap, mGyroLogTap, mMagnetLogTap, mRotVecLogTap, mExternAccelLogListTap, mFSRValuesTap);
                        }

                        if(mBT != null) {
                            mBT.resetData();
                        }

                        mStep = Constants.STEP_MOVE;
                    } else if ((mStep == Constants.STEP_MOVE) || (mStep == Constants.STEP_SINGLE_RECORDING)) {

                        mLoggingMoveFinished = true;

                        mButtonStartLogging.setEnabled(true);
                        mButtonStartLogging.setClickable(true);
                        mButtonStartLogging.setAlpha(1.0f);
                        mButtonStartLogging.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.start));
                        mTextViewButtonStart.setText(getString(R.string.empty));
                        mGlow.setVisibility(View.GONE);

                        getLoggingData();

                        mAccelMinusOffsetLogMove = subtractOffsetFromAccelData(mAccelLogMove);

                        File loggingDir = null;

                        /*if (mStep == Constants.STEP_MOVE) {
                            loggingDir = mSensorMoveLoggingDir;
                        } else if (mStep == Constants.STEP_SINGLE_RECORDING) {
                            loggingDir = mSensorSingleRecordingLoggingDir;
                        }*/

                        if(mDatabaseMode) {
                            loggingDir = mSensorMoveLoggingDir;
                        } else {
                            loggingDir = mSensorSingleRecordingLoggingDir;
                        }

                        writeLoggingDataToFile(mStep, loggingDir, mAccelMinusOffsetLogMove, mLinAccelLogMove, mGravLogMove, mGyroLogMove, mMagnetLogMove, mRotVecLogMove, mExternAccelLogListMove, mFSRValuesMove);

                        if (mDatabaseMode) {
                            freeMemory();
                        }

                        mVibrator.vibrate(Constants.DURARTION_VIBRATE);
                    }

                }
            }, 2000);
        }
    }

    private void getLoggingData() {

        if(mStep == Constants.STEP_TAP) {
            if (mRecorder != null) {
                mRawSoundDataTap = mRecorder.getRawSoundData();
            }

            if (mListener.getAccelLog() != null) {
                mAccelLogTap = mListener.getAccelLog();
            }

            if(mListener.getLinAccelLog() != null) {
                mLinAccelLogTap = mListener.getLinAccelLog();
            }

            if (mListener.getGravLog() != null) {
                mGravLogTap = mListener.getGravLog();
            }

            if (mListener.getGyroLog() != null) {
                mGyroLogTap = mListener.getGyroLog();
            }

            if (mListener.getMagnetLog() != null) {
                mMagnetLogTap = mListener.getMagnetLog();
            }

            if (mListener.getRotVecLog() != null) {
                mRotVecLogTap = mListener.getRotVecLog();
            }

            if (mUseExternAccel && (mBT != null)) {
                mExternAccelLogListTap = mBT.getExternAccelLog(mStep);
            }

            if (mUseFSR && (mBT != null)) {
                mFSRValuesTap = mBT.getFSRValues();
            }
        } else if ((mStep == Constants.STEP_MOVE) || (mStep == Constants.STEP_SINGLE_RECORDING)) {
            if (mRecorder != null) {
                mRawSoundDataMove = mRecorder.getRawSoundData();
            }

            if (mListener.getAccelLog() != null) {
                mAccelLogMove = mListener.getAccelLog();
            }

            if (mListener.getLinAccelLog() != null) {
                mLinAccelLogMove = mListener.getLinAccelLog();
            }

            if (mListener.getGravLog() != null) {
                mGravLogMove = mListener.getGravLog();
            }

            if (mListener.getGyroLog() != null) {
                mGyroLogMove = mListener.getGyroLog();
            }

            if (mListener.getMagnetLog() != null) {
                mMagnetLogMove = mListener.getMagnetLog();
            }

            if (mListener.getRotVecLog() != null) {
                mRotVecLogMove = mListener.getRotVecLog();
            }

            if (mUseExternAccel && (mBT != null)) {
                mExternAccelLogListMove = mBT.getExternAccelLog(mStep);
            }

            if (mUseFSR && (mBT != null)) {
                mFSRValuesMove = mBT.getFSRValues();
            }
        }
    }

    private SensorLog subtractOffsetFromAccelData(SensorLog accelLog) {

        SensorLog accelMinusOffsetLog = null;

        //// TODO: 02.07.2016 either completely remove or uncomment and use again
        /*if((accelLog != null) && (accelLog.getType() != Constants.INVALID_SENSOR_LOG_TYPE)) {

            accelMinusOffsetLog = new SensorLog(accelLog.getType(), mStep);

            for(long timestamp : accelLog.getTimestamps()) {

                accelMinusOffsetLog.addTimestamp(timestamp);
            }

            float[] valuesMinusOffset = new float[3];

            for(float[] values : accelLog.getValues()) {
                valuesMinusOffset[0] = values[0] - (float) SensorCalibrationActivity.offsetValues[0];
                valuesMinusOffset[1] = values[1] - (float) SensorCalibrationActivity.offsetValues[1];
                valuesMinusOffset[2] = values[2] - (float) SensorCalibrationActivity.offsetValues[2];

                accelMinusOffsetLog.addValues(valuesMinusOffset);
            }

            return accelMinusOffsetLog;
        }*/

        accelMinusOffsetLog = accelLog;


        return accelMinusOffsetLog;
    }

    private void writeLoggingDataToFile(int step, File sensorloggingDir, SensorLog accelMinusOffsetLog, SensorLog linAccelLog, SensorLog gravLog, SensorLog gyroLog, SensorLog magnetLog, SensorLog rotVecLog, List<SensorLog> externAccelLogList, List<Integer> fsrValues) {

        String stepString = null;
        String textureName = null;
        int textureNumber = 0;

        if(step == Constants.STEP_SINGLE_RECORDING) {
            stepString = Constants.STEP_STRING_SINGLE_RECORDING;
        } else if(step == Constants.STEP_TAP) {
            stepString = Constants.STEP_STRING_TAP;
        } else if (step == Constants.STEP_MOVE) {
            stepString = Constants.STEP_STRING_MOVE;
        }

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean databaseMode = sharedPrefs.getBoolean(Constants.PREF_KEY_MODE_SELECT, false);

        if(databaseMode) {
            textureName = sharedPrefs.getString(Constants.PREF_KEY_TEXTURE_NAME, null);
            textureNumber = sharedPrefs.getInt(Constants.PREF_KEY_TEXTURE_NUMBER, 0);
        }

        if((accelMinusOffsetLog != null) && !(accelMinusOffsetLog.getType() == Constants.INVALID_SENSOR_LOG_TYPE) && (accelMinusOffsetLog.getValues().size() > 0)) {

            String accelLogString = buildLogString(accelMinusOffsetLog);

            if(databaseMode) {
                //mFileHandler.writeDataToTextFile(accelLogString, sensorloggingDir.getAbsolutePath(), textureName + "_" + Integer.toString(textureNumber) + "_" + Constants.ACCEL_FILENAME + stepString);
                mFileHandler.writeDataToTextFile(accelLogString, sensorloggingDir.getAbsolutePath(), textureName + "_" + Constants.ACCEL_FILENAME + stepString);
            } else {
                mFileHandler.writeDataToTextFile(accelLogString, sensorloggingDir.getAbsolutePath(), Constants.ACCEL_FILENAME + stepString);
            }
        } else if((accelMinusOffsetLog != null) && (accelMinusOffsetLog.getValues().size() == 0)) {
            Toast.makeText(this, "No acceleration data recorded", Toast.LENGTH_LONG).show();
            mVibrator.vibrate(Constants.DURARTION_VIBRATE_ERROR);
        }

        if((linAccelLog != null) && !(linAccelLog.getType() == Constants.INVALID_SENSOR_LOG_TYPE) && (linAccelLog.getValues().size() > 0)) {

            String linAccelLogString = buildLogString(linAccelLog);

            if(databaseMode) {
                //mFileHandler.writeDataToTextFile(linAccelLogString, sensorloggingDir.getAbsolutePath(), textureName + "_" + Integer.toString(textureNumber) + "_" + Constants.LIN_ACCEL_FILENAME + stepString);
                mFileHandler.writeDataToTextFile(linAccelLogString, sensorloggingDir.getAbsolutePath(), textureName + "_" + Constants.LIN_ACCEL_FILENAME + stepString);
            } else {
                mFileHandler.writeDataToTextFile(linAccelLogString, sensorloggingDir.getAbsolutePath(), Constants.LIN_ACCEL_FILENAME + stepString);
            }
        } else if((linAccelLog != null) && (linAccelLog.getValues().size() == 0)) {
            Toast.makeText(this, "No linear acceleration data recorded", Toast.LENGTH_LONG).show();
            mVibrator.vibrate(Constants.DURARTION_VIBRATE_ERROR);
        }

        if((gravLog != null) && !(gravLog.getType() == Constants.INVALID_SENSOR_LOG_TYPE) && (gravLog.getValues().size() > 0)) {

            String gravLogString = buildLogString(gravLog);

            if(databaseMode) {
                mFileHandler.writeDataToTextFile(gravLogString, sensorloggingDir.getAbsolutePath(), textureName + "_" + Constants.GRAV_FILENAME + stepString);
            } else {
                mFileHandler.writeDataToTextFile(gravLogString, sensorloggingDir.getAbsolutePath(), Constants.GRAV_FILENAME + stepString);
            }
        } else if((gravLog != null) && (gravLog.getValues().size() == 0)) {
            Toast.makeText(this, "No gravity data recorded", Toast.LENGTH_LONG).show();
            mVibrator.vibrate(Constants.DURARTION_VIBRATE_ERROR);
        }

        if((gyroLog != null) && !(gyroLog.getType() == Constants.INVALID_SENSOR_LOG_TYPE) && (gyroLog.getValues().size() > 0)) {

            String gyroLogString = buildLogString(gyroLog);

            if(databaseMode) {
                mFileHandler.writeDataToTextFile(gyroLogString, sensorloggingDir.getAbsolutePath(), textureName + "_" + Constants.GYRO_FILENAME + stepString);
            } else {
                mFileHandler.writeDataToTextFile(gyroLogString, sensorloggingDir.getAbsolutePath(), Constants.GYRO_FILENAME + stepString);
            }
        } else if((gyroLog != null) && (gyroLog.getValues().size() == 0)) {
            Toast.makeText(this, "No gyroscope data recorded", Toast.LENGTH_LONG).show();
            mVibrator.vibrate(Constants.DURARTION_VIBRATE_ERROR);
        }

        if((magnetLog != null) && !(magnetLog.getType() == Constants.INVALID_SENSOR_LOG_TYPE) && (magnetLog.getValues().size() > 0)) {

            String magnetLogString = buildLogString(magnetLog);

            if(databaseMode) {
                mFileHandler.writeDataToTextFile(magnetLogString, sensorloggingDir.getAbsolutePath(), textureName + "_" + Constants.MAGNET_FILENAME + stepString);
            } else {
                mFileHandler.writeDataToTextFile(magnetLogString, sensorloggingDir.getAbsolutePath(), Constants.MAGNET_FILENAME + stepString);
            }
        } else if((magnetLog != null) && (magnetLog.getValues().size() == 0)) {
            Toast.makeText(this, "No magnetic sensor data recorded", Toast.LENGTH_LONG).show();
            mVibrator.vibrate(Constants.DURARTION_VIBRATE_ERROR);
        }

        if((rotVecLog != null) && !(rotVecLog.getType() == Constants.INVALID_SENSOR_LOG_TYPE) && (rotVecLog.getValues().size() > 0)) {

            String rotVecLogString = buildLogString(rotVecLog);

            if(databaseMode) {
                mFileHandler.writeDataToTextFile(rotVecLogString, sensorloggingDir.getAbsolutePath(), textureName + "_" + Constants.ROTVEC_FILENAME + stepString);
            } else {
                mFileHandler.writeDataToTextFile(rotVecLogString, sensorloggingDir.getAbsolutePath(), Constants.ROTVEC_FILENAME + stepString);
            }
        } else if((rotVecLog != null) && (rotVecLog.getValues().size() == 0)) {
            Toast.makeText(this, "No rotation vector data recorded", Toast.LENGTH_LONG).show();
            mVibrator.vibrate(Constants.DURARTION_VIBRATE_ERROR);
        }

        if((externAccelLogList != null) && (externAccelLogList.get(0).getValues().size() > 0)) {

            Log.i("Log", "extern size: " + externAccelLogList.get(0).getValues().size());

            String externAccelLogString = buildLogStringList(externAccelLogList);

            if(databaseMode) {
                mFileHandler.writeDataToTextFile(externAccelLogString, sensorloggingDir.getAbsolutePath(), textureName + "_" + Constants.EXTERN_ACCEL_FILENAME + stepString);
            } else {
                mFileHandler.writeDataToTextFile(externAccelLogString, sensorloggingDir.getAbsolutePath(), Constants.EXTERN_ACCEL_FILENAME + stepString);
            }
        } else if((externAccelLogList != null) && (externAccelLogList.get(0).getValues().size() == 0)) {
            Toast.makeText(this, "No extern accel data recorded", Toast.LENGTH_LONG).show();
            mVibrator.vibrate(Constants.DURARTION_VIBRATE_ERROR);
        }

        if((fsrValues != null) && (fsrValues.size() > 0)) {

            Log.i("Log", "fsr size: " + fsrValues.size());

            if(fsrValues.size() < 590) {
                Toast.makeText(this, "not enough fsr values", Toast.LENGTH_LONG);
            }

            StringBuilder sb = new StringBuilder();

            for(int value : fsrValues) {
                sb.append(Float.toString(value));
                sb.append("\n");
            }

            String FSRLogString = sb.toString();

            if(databaseMode) {
                mFileHandler.writeDataToTextFile(FSRLogString, sensorloggingDir.getAbsolutePath(), textureName + "_" + Constants.FSR_FILENAME + stepString);
            } else {
                mFileHandler.writeDataToTextFile(FSRLogString, sensorloggingDir.getAbsolutePath(), Constants.FSR_FILENAME + stepString);
            }
        } else if ((fsrValues != null) && (fsrValues.size() == 0)){
            Toast.makeText(this, "No fsr data recorded", Toast.LENGTH_LONG).show();
            mVibrator.vibrate(Constants.DURARTION_VIBRATE_ERROR);
        }
    }

    private String buildLogString(SensorLog log) {

        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < log.getTimestamps().size(); i++) {

            sb.append(Long.toString(log.getTimestamps().get(i)));
            sb.append("\t");
            sb.append(Float.toString(log.getValues().get(i)[0]));
            sb.append("\t");
            sb.append(Float.toString(log.getValues().get(i)[1]));
            sb.append("\t");
            sb.append(Float.toString(log.getValues().get(i)[2]));
            sb.append("\n");
        }

        return sb.toString();
    }

    private String buildLogStringList(List<SensorLog> logList) {

        StringBuilder sb = new StringBuilder();

        String tmpLogString;

        for(int i = 0; i < logList.size(); i++) {

            tmpLogString = buildLogString(logList.get(i));

            sb.append(tmpLogString);
        }

        return sb.toString();
    }

    private void freeMemory() {
        mAccelLogTap = null;
        mAccelMinusOffsetLogTap = null;
        mLinAccelLogTap = null;
        mGravLogTap = null;
        mGyroLogTap = null;
        mMagnetLogTap = null;
        mRotVecLogTap = null;
        mExternAccelLogListTap = null;
        mFSRValuesTap = null;

        mRawSoundDataTap = null;
        mRecorder = null;

        mAccelLogMove = null;
        mAccelMinusOffsetLogMove = null;
        mLinAccelLogMove = null;
        mGravLogMove = null;
        mGyroLogMove = null;
        mMagnetLogMove = null;
        mRotVecLogMove = null;
        mExternAccelLogListMove = null;
        mFSRValuesMove = null;

        mRawSoundDataMove = null;
        mRecorder = null;
    }

}
