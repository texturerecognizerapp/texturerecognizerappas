package de.tum.lmt.texturerecognizerappas;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Kev94 on 29.09.2016.
 */
public class FullScreenViewActivity extends Activity {

    private static final String TAG = FullScreenViewActivity.class.getSimpleName();
    private static final int FULLSCREEN = 0;

    private ViewPager mViewPager;

    private FullScreenImageAdapter mImageAdapter;

    private ArrayList<ImageItem> mImageItems;

    private ImageView mPauseOverlay;
    private ImageView mFilterOverlay;
    private ImageView mBackImage;
    private TextView mTitleView;
    private ImageButton mButtonClose;
    private ImageButton mButtonDelete;
    private Button mButtonShare;
    private Button mButtonClassification;
    private Button mButtonSound;

    private View mButtonRow;

    private AudioHelper mAudioHelper;

    boolean mIsSoundPlaying;

    private boolean mButtonsVisible = true;

    private FileHandler mFileHandler;

    private String mPath;
    private String mTitle;

    private int mCurrentPosition;

    ArrayList<String> mTitlesOfDeletedItems;

    private AlertDialog mClassificationProgressDialog;
    private ClassificationTask.iOnClassificationTaskFinishedListener mOnClassificationTaskFinishedListener;

    private AlertDialog mOnlineProcessingDialog;
    private ServerTask.iOnResultReceivedListener mOnOnlineResultListener;
    private ServerTask.iOnDataSentListener mOnDataSentListener;
    private AudioHelper.iOnSoundFinishedListener mOnSoundFinishedListener;

    private AlertDialog mDeleteDialog;

    BitmapOperations mBmpOps;

    private long mClassificationTimeStop;
    private long mClassificationTimeStart;

    private Animation mAnimationButtonIn;
    private Animation mAnimationButtonOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_view);

        Intent intent = getIntent();
        mImageItems = intent.getParcelableArrayListExtra("ImageItems");
        final int currentPosition = intent.getIntExtra("currentPosition", 0);

        mTitle = mImageItems.get(currentPosition).getTitle();
        mPath = mImageItems.get(currentPosition).getDir().getAbsolutePath();

        mBmpOps = new BitmapOperations();

        mBackImage = (ImageView) findViewById(R.id.back_image_fullscreen_view);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.i(TAG, "selected page: " + position);

                mTitle = mImageItems.get(position).getTitle();
                mPath = mImageItems.get(position).getDir().getAbsolutePath();

                mCurrentPosition = position;

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //remove view from background before scrolling to next page
                mBackImage.setVisibility(View.GONE);
                return false;
            }
        });

        FullScreenImageAdapter.iOnImageClicked onImageClickedListener = new FullScreenImageAdapter.iOnImageClicked() {
            @Override
            public void onImageClicked() {
                if (mButtonsVisible) {
                    mButtonRow.startAnimation(mAnimationButtonOut);
                } else {

                    mButtonShare.setVisibility(View.VISIBLE);
                    mButtonClassification.setVisibility(View.VISIBLE);
                    mButtonSound.setVisibility(View.VISIBLE);
                    mButtonsVisible = true;

                    mButtonRow.startAnimation(mAnimationButtonIn);
                }
            }
        };

        mImageAdapter = new FullScreenImageAdapter(this, mImageItems, onImageClickedListener);

        mViewPager.setPageTransformer(true, new DepthPageTransformer());
        mViewPager.setAdapter(mImageAdapter);
        mViewPager.setCurrentItem(currentPosition);

        // close button click event
        mButtonClose = (ImageButton) findViewById(R.id.button_back_fullscreen_view);
        mButtonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent result = new Intent();
                result.putStringArrayListExtra("deletedItems", mTitlesOfDeletedItems);

                setResult(FULLSCREEN, result);

                finish();
            }
        });

        mButtonDelete = (ImageButton) findViewById(R.id.button_delete_fullscreen_view);
        mButtonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDeleteDialog();

            }
        });

        mFileHandler = new FileHandler();

        final SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        mButtonShare = (Button) findViewById(R.id.button_share_fullscreen_view);
        if(mButtonShare == null) {
            Log.e(TAG, "mButtonShare null");
        }
        mButtonShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSendDialog();
            }
        });

        mButtonClassification = (Button) findViewById(R.id.button_classification_fullscreen_view);
        mButtonClassification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mClassificationProgressDialog = null;

                String classificationMethod = sharedPrefs.getString(Constants.PREF_KEY_CLASSIFICATION_METHOD, null);
                Set<String> classificationFeatures = sharedPrefs.getStringSet(Constants.PREF_KEY_CLASSIfICATION_FEATURES, null);
                //String distance = sharedPrefs.getString(Constants.PREF_KEY_DISTANCE, null);
                String neighborhoodSize = sharedPrefs.getString(Constants.PREF_KEY_NEIGHBORHOOD, null);
                String featurePath = mPath + Constants.DATA_TO_SEND_FOLDER_NAME + File.separator;
                Log.i("Fullscreen", "featurePath: " + featurePath);

                boolean onlineProcessing = sharedPrefs.getBoolean(Constants.PREF_KEY_ONLINE, false);

                if(onlineProcessing) {

                    showOnlineProcessingDialog();

                    mClassificationTimeStart = System.currentTimeMillis();

                    String IP = sharedPrefs.getString(Constants.PREF_KEY_IP, "192.168.2.1");

                    JSONHelper jsonHelper = new JSONHelper();

                    for(String string:classificationFeatures) {
                        Log.i("Test", string);
                    }

                    String jsonSettingsString =jsonHelper.buildJSONSettingsString(classificationMethod, neighborhoodSize, classificationFeatures);

                    Log.i("Test", jsonSettingsString);

                    mFileHandler.writeDataToTextFile(jsonSettingsString, mPath, "jsonClassificationSettings.txt");

                    ServerTask serverTask = new ServerTask(getApplicationContext(), false, mOnOnlineResultListener, mOnDataSentListener);
                    serverTask.execute(mPath, IP);

                } else {

                    if (classificationFeatures == null) {
                        Log.i("Classification", "Set null");
                    }

                    JSONHelper jsonHelper = new JSONHelper();

                    File JSONFile = new File(featurePath + Constants.JSON_FEATURE_LOG_FILENAME + Constants.TEXT_FILE_EXTENSION);

                    if(JSONFile.exists()) {
                        Features features = jsonHelper.readFeaturesFromJSONFile(JSONFile, classificationFeatures);

                        if (classificationMethod == null) {
                            Log.e("Classification", "classification parameters not set");
                            Toast.makeText(getApplicationContext(), getString(R.string.parameters_missing), Toast.LENGTH_LONG).show();
                        } else {
                            ClassificationTask classifierTask = new ClassificationTask(featurePath, features, classificationMethod, Integer.parseInt(neighborhoodSize), classificationFeatures, mOnClassificationTaskFinishedListener);
                            classifierTask.execute();
                            showProgressDialog();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Features not found!", Toast.LENGTH_LONG).show();
                    }

                    jsonHelper = null;
                }
            }
        });

        mOnClassificationTaskFinishedListener = new ClassificationTask.iOnClassificationTaskFinishedListener() {
            @Override
            public void onClassifierTaskFinished(List<Texture.Sample> result) {

                Log.i("FullScreen", "Classiicaion finished");

                if(mClassificationProgressDialog != null) {
                    Log.i("FullScreen", "Dialog dismissed");
                    mClassificationProgressDialog.dismiss();
                }

                String classificationMethod = sharedPrefs.getString(Constants.PREF_KEY_CLASSIFICATION_METHOD, null);

                if(result == null) {
                    Log.e("Classification", "Classfication not successful");
                    Toast.makeText(getApplicationContext(), getString(R.string.classification_null), Toast.LENGTH_LONG).show();
                } else {
                    showClassificationListDialog(result, classificationMethod);
                }
            }
        };

        mOnDataSentListener = new ServerTask.iOnDataSentListener() {
            @Override
            public void onDataSent() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "data sent", Toast.LENGTH_LONG).show();
                    }
                });
            }
        };

        mOnOnlineResultListener = new ServerTask.iOnResultReceivedListener() {
            @Override
            public void onResultReceived(String jsonResultString) {

                Log.i("result", "result received");

                String classificationMethod = sharedPrefs.getString(Constants.PREF_KEY_CLASSIFICATION_METHOD, "k-nearest-neighbor");
                String neighborhoodSize = sharedPrefs.getString(Constants.PREF_KEY_NEIGHBORHOOD, "10");
                String distToMean = Constants.CLASSIFICATION_DIST_TO_MEAN;
                String Mahal = Constants.CLASSIFICATION_MAHALANOBIS;
                String kNearestNeighbor = Constants.CLASSIFICATION_KNN;

                List<Texture.Sample> result = new ArrayList<>();

                JSONObject jsonResult = null;
                try {
                    jsonResult = new JSONObject(jsonResultString);

                    if (classificationMethod.equals(distToMean)) {

                        if (jsonResult != null) {

                            for (int i = 1; i <= Constants.NUMBER_OF_TEXTURES; i++) {

                                JSONObject jsonSample;

                                try {
                                    jsonSample = jsonResult.getJSONObject(Integer.toString(i));
                                } catch(JSONException e) {
                                    break;
                                }

                                String name = jsonSample.getString("name");
                                double distance = jsonSample.getDouble("value");

                                Texture texture = new Texture(name);
                                texture.addSampleWithDistance(distance);

                                result.add(texture.getSamples().get(0));

                            }
                        }
                    } else if (classificationMethod.equals(Mahal)) {

                        if (jsonResult != null) {

                            for (int i = 1; i <= Constants.NUMBER_OF_TEXTURES; i++) {

                                JSONObject jsonSample;

                                try {
                                    jsonSample = jsonResult.getJSONObject(Integer.toString(i));
                                } catch(JSONException e) {
                                    break;
                                }

                                String name = jsonSample.getString("name");
                                double distance = jsonSample.getDouble("value");

                                Texture texture = new Texture(name);
                                texture.addSampleWithDistance(distance);

                                result.add(texture.getSamples().get(0));

                            }
                        }
                    } else if (classificationMethod.equals(kNearestNeighbor)) {

                        for (int i = 1; i <= Integer.getInteger(neighborhoodSize, 10); i++) {

                            JSONObject jsonSample;

                            try {
                                jsonSample = jsonResult.getJSONObject(Integer.toString(i));
                            } catch(JSONException e) {
                                break;
                            }

                            String name = jsonSample.getString("name");
                            int occurence = jsonSample.getInt("value");

                            Texture texture = new Texture(name);
                            texture.addSampleWithOccurence(occurence);

                            result.add(texture.getSamples().get(0));

                            /*for(Texture.Sample sample:result) {

                                if(name.equals(sample.getName())) {
                                    sample.setOccurence(sample.getOccurence()+1);
                                    break;
                                } else {
                                    Texture texture = new Texture(name);
                                    texture.addSampleWithOccurence(1);

                                    result.add(texture.getSamples().get(0));
                                }
                            }*/

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mOnlineProcessingDialog.dismiss();

                mClassificationTimeStop = System.currentTimeMillis();

                Toast.makeText(getApplicationContext(), "Classification took " + (mClassificationTimeStop - mClassificationTimeStart) + " ms.", Toast.LENGTH_LONG).show();

                if(jsonResult == null) {
                    Log.e("Classification", "Classfication not successful");
                    Toast.makeText(getApplicationContext(), getString(R.string.classification_null), Toast.LENGTH_LONG).show();
                } else {
                    showClassificationListDialog(result, classificationMethod);
                }
            }
        };

        mButtonSound = (Button) findViewById(R.id.button_sound_fullscreen_view);
        mButtonSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mOnSoundFinishedListener = new AudioHelper.iOnSoundFinishedListener() {
                    @Override
                    public void onSoundFinished() {

                        Log.i("Fullscreen", "Sound complete");

                        mIsSoundPlaying = false;
                        mAudioHelper = null;

                        removePauseOverlayAndStopMusic();
                    }
                };

                mAudioHelper = new AudioHelper(getApplicationContext(), mOnSoundFinishedListener);
                //// TODO: 21.03.2016 handle errors, but it's working so not urgent
                mAudioHelper.play(mPath + File.separator + Constants.AUDIO_FILENAME_ORIG + Constants.AUDIO_FILE_EXTENSION);
                mIsSoundPlaying = true;

                mButtonShare.setVisibility(View.GONE);
                mButtonClassification.setVisibility(View.GONE);
                mButtonSound.setVisibility(View.GONE);
                mButtonsVisible = false;

                mPauseOverlay.setVisibility(View.VISIBLE);
                mFilterOverlay.setVisibility(View.VISIBLE);
                mPauseOverlay.bringToFront();
                mFilterOverlay.bringToFront();

                mPauseOverlay.setClickable(true);
                mFilterOverlay.setClickable(true);
            }
        });

        mButtonRow = findViewById(R.id.button_row_fullscreen_view);

        mFilterOverlay = (ImageView) findViewById(R.id.filter_overlay_fullscreen_view);
        mFilterOverlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePauseOverlayAndStopMusic();
            }
        });

        mFilterOverlay.setClickable(false);

        mPauseOverlay = (ImageView) findViewById(R.id.pause_overlay_fullscreen_view);
        mPauseOverlay.setClickable(false);

        mTitleView = (TextView) findViewById(R.id.title_fullscreen_view);

        mAnimationButtonIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.button_row_in);
        mAnimationButtonIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        mAnimationButtonOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.button_row_out);
        mAnimationButtonOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mButtonShare.setVisibility(View.GONE);
                mButtonClassification.setVisibility(View.GONE);
                mButtonSound.setVisibility(View.GONE);
                mButtonsVisible = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        mTitlesOfDeletedItems = new ArrayList<>();

        mButtonRow.startAnimation(mAnimationButtonIn);

    }

    @Override
    public void onBackPressed() {
        Intent result = new Intent();
        result.putStringArrayListExtra("deletedItems", mTitlesOfDeletedItems);

        setResult(FULLSCREEN, result);

        finish();
    }

    public void removePauseOverlayAndStopMusic() {
        if(mFilterOverlay.getVisibility() == View.VISIBLE) {
            mPauseOverlay.setVisibility(View.GONE);
            mFilterOverlay.setVisibility(View.GONE);

            mPauseOverlay.setClickable(false);
            mFilterOverlay.setClickable(false);

            if (mIsSoundPlaying) {
                mAudioHelper.stop();
                mAudioHelper = null;
                mIsSoundPlaying = false;

                mButtonRow.startAnimation(mAnimationButtonIn);
            }

            if(!mButtonsVisible) {
                mButtonShare.setVisibility(View.VISIBLE);
                mButtonClassification.setVisibility(View.VISIBLE);
                mButtonSound.setVisibility(View.VISIBLE);
                mButtonsVisible = true;
            }

            mButtonRow.startAnimation(mAnimationButtonIn);

            Log.i("Fullscreen", "Gone");
        }

    }

    private void showProgressDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        RelativeLayout rl = new RelativeLayout(this);

        ProgressBar progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);
        RelativeLayout.LayoutParams progressBarParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView text = new TextView(this);
        text.setText(R.string.classification_progress);
        RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        textParams.addRule(RelativeLayout.CENTER_VERTICAL);

        rl.addView(progressBar, progressBarParams);
        rl.addView(text, textParams);

        builder.setView(rl);

        mClassificationProgressDialog = builder.create();
        mClassificationProgressDialog = builder.show();

        mClassificationProgressDialog.setCanceledOnTouchOutside(false);
    }

    protected void showOnlineProcessingDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        RelativeLayout rl = new RelativeLayout(this);

        ProgressBar progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);
        RelativeLayout.LayoutParams progressBarParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView text = new TextView(this);
        text.setText(R.string.processing);
        RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        textParams.addRule(RelativeLayout.CENTER_VERTICAL);

        rl.addView(progressBar, progressBarParams);
        rl.addView(text, textParams);

        builder.setView(rl);

        mOnlineProcessingDialog = builder.create();
        mOnlineProcessingDialog = builder.show();

        mOnlineProcessingDialog.setCanceledOnTouchOutside(false);
    }

    private void showSendDialog() {
        DialogSendFragment dialogSend = DialogSendFragment.newInstance(mPath);
        dialogSend.show(getFragmentManager(), "dialogSend");
    }

    private void showClassificationListDialog(List<Texture.Sample> textures, String classificationMethod) {
        DialogClassificationListFragment dialogClassificationList = new DialogClassificationListFragment(textures, classificationMethod);
        dialogClassificationList.show(getFragmentManager(), "dialogClassificationList");
    }

    protected void showDeleteDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = getLayoutInflater();
        View content = inflater.inflate(R.layout.dialog_delete, null);

        final Button buttonCancel = (Button) content.findViewById(R.id.button_cancel_delete_dialog);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation buttonClickAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.click_on_item);
                buttonClickAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mDeleteDialog.dismiss();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                buttonCancel.startAnimation(buttonClickAnimation);
            }
        });

        final Button buttonDelete = (Button) content.findViewById(R.id.button_delete_delete_dialog);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation buttonClickAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.click_on_item);
                buttonClickAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        String bmpFile;

                        Log.i(TAG, "position: " + mCurrentPosition);

                        if (mCurrentPosition == (mImageItems.size() - 1)) {
                            bmpFile = mImageItems.get(mCurrentPosition - 1).getDir().getAbsolutePath() + File.separator + Constants.CAMERA_NO_FLASH_FILENAME + Constants.JPG_FILE_EXTENSION;
                            Log.i(TAG, "show position: " + (mCurrentPosition - 1));
                        } else {
                            bmpFile = mImageItems.get(mCurrentPosition + 1).getDir().getAbsolutePath() + File.separator + Constants.CAMERA_NO_FLASH_FILENAME + Constants.JPG_FILE_EXTENSION;
                            Log.i(TAG, "show position: " + (mCurrentPosition + 1));
                        }

                        mBackImage.setVisibility(View.VISIBLE);
                        mBackImage.setImageBitmap(mBmpOps.decodeSampledBitmapFromFile(bmpFile, 240, 160));

                        Animation animationDelete = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.delete_picture_fullscreen);
                        animationDelete.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {

                                mTitlesOfDeletedItems.add(mImageItems.get(mCurrentPosition).getTitle());

                                mFileHandler.deleteFolder(mImageItems.get(mCurrentPosition).getDir());
                                mImageAdapter.removeItem(mCurrentPosition);
                                mImageAdapter.notifyDataSetChanged();

                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });

                        mViewPager.startAnimation(animationDelete);

                        mDeleteDialog.dismiss();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                buttonDelete.startAnimation(buttonClickAnimation);
            }
        });

        builder.setView(content);

        mDeleteDialog = builder.create();
        mDeleteDialog = builder.show();

        mDeleteDialog.setCanceledOnTouchOutside(false);

    }

}
