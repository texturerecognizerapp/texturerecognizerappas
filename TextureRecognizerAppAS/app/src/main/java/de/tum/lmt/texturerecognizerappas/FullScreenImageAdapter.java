package de.tum.lmt.texturerecognizerappas;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Kev94 on 29.09.2016.
 * from http://www.androidhive.info/2013/09/android-fullscreen-image-slider-with-swipe-and-pinch-zoom-gestures/
 */
public class FullScreenImageAdapter extends PagerAdapter {

    private static final String TAG = "FullScreenImageAdapter";

    private Activity mActivity;
    private int mLayoutResourceId;
    private ArrayList<ImageItem> mImageItems;
    private LayoutInflater mInflater;

    private String mTitle;
    private String mPath;

    private iOnImageClicked mOnImageClickedListener;

    public interface iOnImageClicked {
        public void onImageClicked();
    }

    // constructor
    public FullScreenImageAdapter(Activity activity, ArrayList<ImageItem> imageItems, iOnImageClicked listener) {
        this.mActivity = activity;
        this.mImageItems = imageItems;
        this.mOnImageClickedListener = listener;
    }

    public FullScreenImageAdapter(Activity activity, int layoutResourceId, ArrayList<ImageItem> imageItems) {
        this.mActivity = activity;
        this.mLayoutResourceId = layoutResourceId;
        this.mImageItems = imageItems;
    }

    @Override
    public int getCount() {
        return this.mImageItems.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        TextView titleView;
        ImageView imgDisplay;
        ImageButton btnClose;

        mInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = mInflater.inflate(R.layout.layout_fullscreen_image, container, false);

        titleView = (TextView) viewLayout.findViewById(R.id.title_fullscreen_view);
        imgDisplay = (ImageView) viewLayout.findViewById(R.id.imgDisplay);

        imgDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnImageClickedListener.onImageClicked();
            }
        });

        BitmapOperations bmpOps = new BitmapOperations();
        mPath = mImageItems.get(position).getDir().getAbsolutePath();
        mTitle = mImageItems.get(position).getTitle();

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = bmpOps.decodeSampledBitmapFromFile((mPath + File.separator + Constants.CAMERA_NO_FLASH_FILENAME + Constants.JPG_FILE_EXTENSION), 640, 480);
        imgDisplay.setImageBitmap(bitmap);
        titleView.setText(mTitle);

        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    public void removeItem(int position) {
        if(mImageItems.size() >= (position-1)) {
            mImageItems.remove(position);
        }
    }

    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        /*View view = (View) object;
        ImageView imgView = (ImageView) view.findViewById(R.id.grid_item_image);
        BitmapDrawable bmpDrawable = (BitmapDrawable) imgView.getDrawable();
        if(bmpDrawable != null && bmpDrawable.getBitmap() != null) {
            bmpDrawable.getBitmap().recycle();
        }*/
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
}
