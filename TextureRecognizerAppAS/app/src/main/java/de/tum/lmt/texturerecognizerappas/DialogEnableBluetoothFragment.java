package de.tum.lmt.texturerecognizerappas;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import java.io.IOException;
import java.util.HashMap;

public class DialogEnableBluetoothFragment extends DialogFragment {

    public interface iOnEnableBluetoothDialogButtonPressed {
        public void onEnableBluetoothDialogButtonPressed(String which);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View content = inflater.inflate(R.layout.dialog_enable_bluetooth, null);

        builder.setView(content);
        builder.setMessage(getString(R.string.message_enable_bluetooth))
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        iOnEnableBluetoothDialogButtonPressed activity = (iOnEnableBluetoothDialogButtonPressed) getActivity();
                        activity.onEnableBluetoothDialogButtonPressed(getString(R.string.yes));
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        iOnEnableBluetoothDialogButtonPressed activity = (iOnEnableBluetoothDialogButtonPressed) getActivity();
                        activity.onEnableBluetoothDialogButtonPressed(getString(R.string.no));
                    }
                });

        return builder.create();
    }
}