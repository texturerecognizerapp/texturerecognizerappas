package de.tum.lmt.texturerecognizerappas;

import android.util.Log;

import java.util.Arrays;
import java.util.HashMap;

import de.tum.lmt.texturerecognizerappas.math.Matrix;

/**
 * Created by ga38huh on 30.08.2016.
 * This class calculates the MFCCs, spectral Centroid and spectral Entropy
 */
public class SoundFeatures {

    private static final double fs = 44100;
    private static final double frame=0.5;
    private static final double step =0.1;
    private static final double lowestFrequency = 133.333333;
    private static final double linearSpacing = 66.666666;
    private static final double logSpacing = 1.0711703;
    private static final double eps= 2.2204*Math.pow(10,-16);
    private static final int numberOfLinearFilters = 13;
    private static final int numberOfCoefficients = 13;
    private static final int numberOfLogFilters = 27;
    private static final int numberOfMovementFeatures = 21;
    private static final int numberOfTappingFeatures = 5;
    private double [] lowerFrequency;
    private double [] centerFrequency;
    private double [] upperFrequency;
    private double [] fftFrequencyTap;
    private double [] fftFrequencyMove;
    private double [] triangleHeight;

    //MFCCS
    private double [] MFCCs;

    //time-domain movement features
    private double zeroCrossingRate;
    private double noiseDistribution;
    private double signalEnergy;
    private double spikiness;

    //frequency-domain movement features
    private double spectralEntropy;
    private double spectralCentroid;
    private double spectralRollOff;
    private double spectralSum;

    //tapping time-domain features
    private double signalEnergyTap;
    private double kurtosisTap;

    //tapping frequency-domain features
    private double spectralCentroidTap;
    private double spectralRollOffTap;
    private double spectralSumTap;

    private double fftSizeTap;
    private double fftSizeMove;
    private int stepSizeMove;
    private int stepSizeTap;

    private int totalNumberOfFilters;
    private int windowLengthMove;
    private int windowLengthTap;
    private int numberOfFramesMoves;
    private int numberOfFramesTap;

    private Matrix mfccFilterWeights;
    private Matrix DCTMatrix;
    private Matrix hammingFilterMove;
    private Matrix hammingFilterTap;
    private FFT magnitudeFFTMove;
    private FFT magnitudeFFTTap;


    public SoundFeatures(){

        initialSteps();
        hammingFilterMove = computeHammingFilter(Constants.STEP_MOVE);
        hammingFilterTap = computeHammingFilter(Constants.STEP_TAP);
        DCTMatrix = computeDCTMatrix();
        magnitudeFFTMove = new FFT(FFT.FFT_MAGNITUDE, windowLengthMove);
        magnitudeFFTTap = new FFT(FFT.FFT_MAGNITUDE, windowLengthTap);
        mfccFilterWeights=computeMfccFilterWeights();


    }
    private void initialSteps(){
        //defining the variables
        //windowLengthMove ist temporarily adjusted to 2^14, since the FFT requires a windowLengthMove in form of 2^x
        windowLengthMove = (int)Math.pow(2,14);//(int)Math.round(frame*fs);
        windowLengthTap = (int)Math.pow(2,14);
        stepSizeTap = (int)Math.pow(2,14);//(int)(step*fs/2);
        stepSizeMove = (int)(step*fs);
        fftSizeTap = Math.round(windowLengthTap/2);
        fftSizeMove = Math.round(windowLengthMove/2);
        totalNumberOfFilters = numberOfLogFilters+numberOfLinearFilters;

        double [] fftFrequencyBufferTap=new double[(int) fftSizeTap];
        double [] fftFrequencyBufferMove=new double[(int) fftSizeMove];
        double [] frequency=new double[totalNumberOfFilters+2];
        double [] lowerFrequencyBuffer = new double[totalNumberOfFilters];
        double [] centerFrequencyBuffer = new double[totalNumberOfFilters];
        double [] upperFrequencyBuffer = new double[totalNumberOfFilters];

        for(int i=0;i< fftSizeTap;i++) {
            fftFrequencyBufferTap[i] = i/ fftSizeTap * fs;
        }

        for(int i=0;i< fftSizeMove;i++) {
            fftFrequencyBufferMove[i] = i/ fftSizeMove * fs;
        }

        for (int i=0;i<numberOfLinearFilters;i++){
            frequency[i]=lowestFrequency+i*linearSpacing;
        }

        for(int i=numberOfLinearFilters;i<totalNumberOfFilters+2;i++){
            frequency[i]=frequency[numberOfLinearFilters-1]*Math.pow(logSpacing,(i-12d));
        }

        for(int i=0;i<totalNumberOfFilters;i++) {
            lowerFrequencyBuffer[i] = frequency[i];
            centerFrequencyBuffer[i]= frequency[1+i];
            upperFrequencyBuffer[i] = frequency[2+i];
        }
        double [] triangleHeightBuffer = new double[upperFrequencyBuffer.length];
        for(int i=0;i<upperFrequencyBuffer.length;i++){
            triangleHeightBuffer[i]=2/(upperFrequencyBuffer[i]-lowerFrequencyBuffer[i]);
        }

        this.fftFrequencyTap = fftFrequencyBufferTap;
        this.fftFrequencyMove =fftFrequencyBufferMove;
        this.triangleHeight=triangleHeightBuffer;
        this.lowerFrequency=lowerFrequencyBuffer;
        this.centerFrequency=centerFrequencyBuffer;
        this.upperFrequency=upperFrequencyBuffer;

    }
    private Matrix computeDCTMatrix() {
        //computes the DCT Matrix which will be used to transform the cepstrum into
        //the MFCC Coefficients

        double dctConstant= Math.sqrt(2.0d/(double)totalNumberOfFilters);
        double cosConstant= Math.PI/(double)totalNumberOfFilters;
        Matrix matrix = new Matrix(numberOfCoefficients, totalNumberOfFilters);

        for (int i=0;i<numberOfCoefficients;i++){
            for(int j=0;j<totalNumberOfFilters;j++) {
                if(i==0) {
                    matrix.set(i, j, 1/Math.sqrt(2)*dctConstant* Math.cos(cosConstant * i * (j + 0.5d)));
                }
                else
                    matrix.set(i, j,dctConstant* Math.cos(cosConstant * i * (j + 0.5d)));
            }
        }
        //safeMatrixAsTextFile(matrix,"DCT.txt");
        return matrix;

    }
    private Matrix computeHammingFilter(int moveOrTap){
        //computes the hamming filter which will be applied to the signal frame
        //in processMove() just before the signal is transformed via FFT

        int windowLength;

        if(moveOrTap == Constants.STEP_MOVE) {
            windowLength = windowLengthMove;
        } else if(moveOrTap == Constants.STEP_TAP) {
            windowLength = windowLengthTap;
        } else {
            throw new IllegalArgumentException("unknown step - use STEP_MOVE or STEP_TAP");
        }

        Matrix hamming =new Matrix (1, windowLength);
        for (int i=0;i< windowLength;i++){
            hamming.set(0,i, 0.54-0.46*Math.cos(2*Math.PI*i/(windowLength -1)));
        }
        //safeMatrixAsTextFile(hamming,"hamming.txt");

        return hamming;
    }
    private Matrix computeMfccFilterWeights(){
        //computes the filter banks which will be applied to the FFT transformed signal frame

        Matrix filters=new Matrix(totalNumberOfFilters,(int) fftSizeMove);
        double lowerValue;
        double centerValue;
        double upperValue;
        double triangleValue;
        int triangleWidth;
        int filterStart=(int)Math.floor(lowestFrequency/(1/ fftSizeMove *fs));
        int iterationBegin= filterStart;
        int[] iterationEnd=new int[totalNumberOfFilters];
        int counter;

        for(int i=0;i<totalNumberOfFilters;i++){
            counter=0;
            lowerValue=lowerFrequency[i];
            centerValue=centerFrequency[i];
            upperValue=upperFrequency[i];
            triangleValue=triangleHeight[i];
            triangleWidth=(int)Math.ceil((upperValue-lowerValue)/(1/ fftSizeMove *fs));

            if(i>1){
                iterationBegin=iterationEnd[i-2];
            }
            if(i==1){
                iterationBegin=iterationBegin+(int)Math.ceil(triangleWidth/2);
            }


            for(int j=iterationBegin;j<(iterationBegin+triangleWidth+10);j++) {
                if ((fftFrequencyMove[j] > lowerValue) && (fftFrequencyMove[j] <= centerValue)) {
                    filters.set(i, j, (fftFrequencyMove[j] - lowerValue) / (centerValue - lowerValue) * triangleValue);
                    counter++;
                }
                else if ((fftFrequencyMove[j] > centerValue) && (fftFrequencyMove[j] < upperValue)) {
                    filters.set(i, j, (upperValue - fftFrequencyMove[j]) / (upperValue - centerValue) * triangleValue);
                    counter++;
                }
                else if (counter>0){

                    iterationEnd[i]=j;

                    break;
                }
            }


        }

        //safeMatrixAsTextFile(filters,"filterBanks.txt");

        return filters;
    }
    public void processMove(double[] sound, HashMap featurePreferences){
        //processing the sound signal by dividing it into frames
        //calculates all features for each signal
        //returns the median value of the features from all the frames
        Log.i("SoundFeatures", "ProcessingStarted" );

        int currPos = 0;
        int dataLength = sound.length;
        numberOfFramesMoves = (int) Math.floor((dataLength- windowLengthMove)/ stepSizeMove)+1;
        Matrix cepstrumConstant= new Matrix(totalNumberOfFilters,1,eps);
        Matrix frameSignal = new Matrix (1, windowLengthMove);
        Matrix frameHammingSignal = new Matrix (1, windowLengthMove);
        double [] mfccs = new double [numberOfCoefficients];
        Matrix frameFFT = new Matrix ((int) fftSizeMove,1);
        Matrix features = new Matrix(numberOfMovementFeatures, numberOfFramesMoves);
        double [] medianMovementFeatures = new double [numberOfMovementFeatures];
        double [] sortedFeatures = new double [numberOfFramesMoves];
        Matrix cepstrum = new Matrix (totalNumberOfFilters,1);
        Matrix filteredFFT = new Matrix (totalNumberOfFilters,1);
        Matrix frameFFTBuffer = new Matrix(1, windowLengthMove);


        for(int i=0;i< numberOfFramesMoves;i++) {
            // cutting out a single frame from the signal
            for (int j = 0; j < windowLengthMove; j++) {
                frameSignal.set(0, j, sound[j + currPos]);
            }
            //applying the hamming filter
            frameHammingSignal = frameSignal.arrayTimes(hammingFilterMove);
            //safeMatrixAsTextFile(frameHammingSignal,"frameHammingBeforeFFT.txt");
            //applying the FFT

            //time-domain features
            if ((boolean) featurePreferences.get(Constants.SOUND_ZERO_CROSSING_RATE)) {
                features.set(13, i, computeZeroCrossingRate(frameHammingSignal.getArray()[0]));
            } else {
                features.set(13, i, Double.NaN);
            }
            if ((boolean) featurePreferences.get(Constants.SOUND_NOISE_DISTRIBUTION)) {
                features.set(14, i, Math.log(1 + computeNoiseDistribution(frameHammingSignal.getArray()[0])));
            } else {
                features.set(14, i, Double.NaN);
            }
            if ((boolean) featurePreferences.get(Constants.SOUND_SIGNAL_ENERGY)) {
                features.set(15, i, Math.log(1 + computeSignalEnergy(frameHammingSignal.getArray()[0])));
            } else {
                features.set(15, i, Double.NaN);
            }
            if ((boolean) featurePreferences.get(Constants.SOUND_SPIKINESS)) {
                features.set(16, i, Math.log(1 + computeSpikiness(frameHammingSignal.getArray()[0])));
            } else {
                features.set(16, i, Double.NaN);
            }

            //if spectral features selected
            if ((boolean)featurePreferences.get(Constants.SOUND_SPECTRAL_CENTROID) || (boolean)featurePreferences.get(Constants.SOUND_SPECTRAL_ENTROPY) || (boolean)featurePreferences.get(Constants.SOUND_SPECTRAL_SUM) ||
                    (boolean)featurePreferences.get(Constants.SOUND_SPECTRAL_ROLL_OFF) || (boolean)featurePreferences.get(Constants.MFCC1) || (boolean)featurePreferences.get(Constants.MFCC2) ||
                    (boolean)featurePreferences.get(Constants.MFCC5)) {

                magnitudeFFTMove.transform(frameHammingSignal.getArray()[0], null);
                //safeMatrixAsTextFile(frameHammingSignal,"frameHammingSignalAfterFFT.txt");

                //dividing the transformed signal by its own length and halve it
                frameFFTBuffer = frameHammingSignal.times(1 / (double) frameHammingSignal.getColumnDimension());
                for (int j = 0; j < (int) fftSizeMove; j++) {
                    frameFFT.set(j, 0, frameFFTBuffer.getArray()[0][j]);
                }
                //safeMatrixAsTextFile(frameFFT,"frameFFT.txt");

                // frequency-domain features
                if ((boolean) featurePreferences.get(Constants.SOUND_SPECTRAL_ENTROPY)) {
                    features.set(17, i, computeSpectralEntropy(frameFFT, Constants.STEP_MOVE));
                } else {
                    features.set(17, i, Double.NaN);
                }
                if ((boolean) featurePreferences.get(Constants.SOUND_SPECTRAL_CENTROID)) {
                    features.set(18, i, computeSpectralCentroid(frameFFT, Constants.STEP_MOVE));
                } else {
                    features.set(18, i, Double.NaN);
                }
                if ((boolean) featurePreferences.get(Constants.SOUND_SPECTRAL_ROLL_OFF)) {
                    features.set(19, i, computeSpectralRollOff(frameFFT, 0.9));
                } else {
                    features.set(19, i, Double.NaN);
                }
                if ((boolean) featurePreferences.get(Constants.SOUND_SPECTRAL_SUM)) {
                    features.set(20, i, computeSpectralSum(frameFFT, Constants.STEP_MOVE));
                } else {
                    features.set(20, i, Double.NaN);
                }

                //MFCCs
                if ((boolean) featurePreferences.get(Constants.MFCC1) || (boolean) featurePreferences.get(Constants.MFCC2) || (boolean) featurePreferences.get(Constants.MFCC5)) {
                    //filter banks applied and constant added
                    filteredFFT = mfccFilterWeights.times(frameFFT);
                    filteredFFT = filteredFFT.plus(cepstrumConstant);
                    //safeMatrixAsTextFile(filteredFFT,"filteredFFT.txt");

                    for (int j = 0; j < totalNumberOfFilters; j++) {
                        cepstrum.set(j, 0, Math.log10(filteredFFT.getArray()[j][0]));
                    }
                    //safeMatrixAsTextFile(cepstrum,"cepstrum.txt");

                    //transforming from cepstrum into coefficients via the DCT matrix
                    for (int j = 0; j < numberOfCoefficients; j++) {
                        features.set(j, i, DCTMatrix.times(cepstrum).getArray()[j][0]);
                    }
                } else {
                    for (int j = 0; j < numberOfCoefficients; j++) {
                        features.set(j, i, Double.NaN);
                    }
                }
            } else {
                features.set(17, i, Double.NaN);
                features.set(18, i, Double.NaN);
                features.set(19, i, Double.NaN);
                features.set(20, i, Double.NaN);

                for (int j = 0; j < numberOfCoefficients; j++) {
                    features.set(j, i, Double.NaN);
                }
            }

            currPos=currPos+ stepSizeMove;
        }
        //safeMatrixAsTextFile(features,"FeaturesPerFrame.txt");
        //calculate the median value of all the features (mfccs,spectral centroid, spectral Entropy)
        for (int i=0;i<numberOfMovementFeatures;i++){
            sortedFeatures=features.getArray()[i];
            Arrays.sort(sortedFeatures);
            medianMovementFeatures[i]= median(sortedFeatures);
            if (i<numberOfCoefficients){
                mfccs[i]=medianMovementFeatures[i];
            }
        }

        //safeArrayAsTextFile(medianMovementFeatures,"Features.txt");
        this.MFCCs=mfccs;
        this.zeroCrossingRate=medianMovementFeatures[13];
        this.noiseDistribution=medianMovementFeatures[14];
        this.signalEnergy=medianMovementFeatures[15];
        this.spikiness=medianMovementFeatures[16];
        this.spectralEntropy=medianMovementFeatures[17];
        this.spectralCentroid=medianMovementFeatures[18];
        this.spectralRollOff=medianMovementFeatures[19];
        this.spectralSum=medianMovementFeatures[20];

    }

    public void processTap(double[] sound, HashMap featurePreferences){
        //processing the sound signal by dividing it into frames
        //calculates all featuresTap for each signal
        //returns the median value of the featuresTap from all the frames
        Log.i("SoundFeatures", "ProcessingStarted" );

        int currPos = 0;
        int dataLength = sound.length;
        Log.i("SoundFeatures", "dataLength: " + dataLength + ", windowLengthTap: " + windowLengthTap + ", stepSizeTap: " + stepSizeTap);
        numberOfFramesTap = (int) Math.floor((dataLength- windowLengthTap)/ stepSizeTap)+1;
        Matrix frameSignalTap = new Matrix (1, windowLengthTap);
        Matrix frameHammingSignalTap = new Matrix (1, windowLengthTap);
        Matrix frameFFTTap = new Matrix ((int) fftSizeTap,1);
        Matrix featuresTap = new Matrix(numberOfTappingFeatures, numberOfFramesTap);
        double [] medianTappingFeatures = new double [numberOfTappingFeatures];
        double [] sortedTappingFeatures = new double [numberOfFramesTap];
        Matrix filteredFFTTap = new Matrix (totalNumberOfFilters,1);
        Matrix frameFFTBufferTap = new Matrix(1, windowLengthTap);

        Log.i("SoundFeatures", "numberOfFramesTap: " + numberOfFramesTap);

        for(int i=0;i< numberOfFramesTap;i++){
            // cutting out a single frame from the signal
            for(int j=0;j< windowLengthTap;j++){
                try{
                    frameSignalTap.set(0, j, sound[j + currPos]);
                } catch(ArrayIndexOutOfBoundsException e) {

                }
            }
            //applying the hamming filter
            frameHammingSignalTap=frameSignalTap.arrayTimes(hammingFilterTap);
            //safeMatrixAsTextFile(frameHammingSignalTap,"frameHammingBeforeFFT.txt");
            //applying the FFT

            //time-domain featuresTap
            if((boolean)featurePreferences.get(Constants.SOUND_SIGNAL_ENERGY_TAP)) {
                featuresTap.set(0, i, Math.log(1 + computeSignalEnergy(frameHammingSignalTap.getArray()[0])));
            } else {
                featuresTap.set(0, i, Double.NaN);
            }
            if((boolean)featurePreferences.get(Constants.SOUND_KURTOSIS_TAP)) {
                featuresTap.set(1, i, Math.log(1 + computeKurtosis(frameHammingSignalTap.getArray()[0])));
            } else {
                featuresTap.set(1, i, Double.NaN);
            }

            if((boolean)featurePreferences.get(Constants.SOUND_SPECTRAL_CENTROID_TAP) || (boolean)featurePreferences.get(Constants.SOUND_SPECTRAL_ROLL_OFF_TAP) ||
                    (boolean)featurePreferences.get(Constants.SOUND_SPECTRAL_SUM_TAP)) {

                magnitudeFFTTap.transform(frameHammingSignalTap.getArray()[0], null);
                //safeMatrixAsTextFile(frameHammingSignalTap,"frameHammingSignalAfterFFT.txt");

                //dividing the transformed signal by its own length and halve it
                frameFFTBufferTap = frameHammingSignalTap.times(1 / (double) frameHammingSignalTap.getColumnDimension());
                for (int j = 0; j < (int) fftSizeTap; j++) {
                    frameFFTTap.set(j, 0, frameFFTBufferTap.getArray()[0][j]);
                }
                //safeMatrixAsTextFile(frameFFTTap,"frameFFTTap.txt");

                // frequency-domain featuresTap
                if((boolean)featurePreferences.get(Constants.SOUND_SPECTRAL_CENTROID_TAP)) {
                    featuresTap.set(2, i, computeSpectralCentroid(frameFFTTap, Constants.STEP_TAP));
                    Log.i("Sound", "specCentroid: " + computeSpectralCentroid(frameFFTTap, Constants.STEP_TAP));
                } else {
                    featuresTap.set(2, i, Double.NaN);
                    Log.i("Sound", "no specCentroid");
                }
                if((boolean)featurePreferences.get(Constants.SOUND_SPECTRAL_ROLL_OFF_TAP)) {
                    featuresTap.set(3, i, computeSpectralRollOff(frameFFTTap, 0.9));
                } else {
                    featuresTap.set(3, i, Double.NaN);
                }
                if((boolean)featurePreferences.get(Constants.SOUND_SPECTRAL_SUM_TAP)) {
                    featuresTap.set(4, i, computeSpectralSum(frameFFTTap, Constants.STEP_TAP));
                } else {
                    featuresTap.set(4, i, Double.NaN);
                }
            } else {
                featuresTap.set(2, i, Double.NaN);
                featuresTap.set(3, i, Double.NaN);
                featuresTap.set(4, i, Double.NaN);
            }

            currPos = currPos + stepSizeTap;
        }
        //safeMatrixAsTextFile(featuresTap,"FeaturesPerFrame.txt");
        //calculate the median value of all the featuresTap (mfccs,spectral centroid, spectral Entropy)
        for (int i=0;i<numberOfTappingFeatures;i++){
            sortedTappingFeatures=featuresTap.getArray()[i];
            Arrays.sort(sortedTappingFeatures);
            for(int z=0; z<sortedTappingFeatures.length; z++) {
                Log.i("sound", "Feature: " + z + " : " + sortedTappingFeatures[z]);
            }
            medianTappingFeatures[i]= median(sortedTappingFeatures);
        }

        //safeArrayAsTextFile(medianTappingFeatures,"Features.txt");
        this.signalEnergyTap = medianTappingFeatures[0];
        this.kurtosisTap = medianTappingFeatures[1];
        this.spectralCentroidTap = medianTappingFeatures[2];
        Log.i("Sound", "specCentroid: " + medianTappingFeatures[2]);
        this.spectralRollOffTap = medianTappingFeatures[3];
        this.spectralSumTap = medianTappingFeatures[4];

    }

    private double computeZeroCrossingRate(double window[]) {

        double zeroCrossings = 0;

        for(int i=1; i<window.length; i++) {
            if((window[i]*window[i-1]) < 0) {
                zeroCrossings++;
            }
        }

        return zeroCrossings/window.length;
    }

    private double computeNoiseDistribution(double window[]) {

        int numberOfOnes = 0;
        int numberOfZeros = 0;
        double threshold = 0;

        //get max for threshold
        for(int i=0; i<window.length; i++) {
            if(Math.abs(window[i]) > threshold) {
                threshold = Math.abs(window[i]);
            }
        }

        threshold = 0.3 * threshold;

        for(int i=0; i<window.length; i++) {
            if(Math.abs(window[i]) >= threshold) {
                numberOfOnes++;
            } else {
                numberOfZeros++;
            }
        }

        return (double)numberOfOnes/(double)numberOfZeros;

    }

    private double computeSignalEnergy(double[] window) {

        double signalEnergy = 0;

        for(int i = 0; i<window.length; i++) {
            signalEnergy = signalEnergy + Math.pow(window[i], 2);
        }

        signalEnergy = signalEnergy / (window.length);

        return signalEnergy;

    }

    private double computeSpikiness(double[] window) {

        int MALength = (int)(fs/10.0);
        double mean = 0;
        double std = 0;
        double[] threshold = new double[window.length-MALength];
        double spikiness = 0;
        double numberOfSpikyIndices = 0;

        for(int i=0; i<window.length; i++) {
            mean = mean + Math.abs(window[i]);
        }
        mean = mean/window.length;

        for(int i=0; i<window.length; i++) {
            std = std + Math.pow((Math.abs(window[i]) - mean), 2);
        }

        std = Math.sqrt((1.0/(window.length-1)) * std);

        Log.i("SoundFeatures", "std: " + std);

        for(int i=0; i<threshold.length; i++) {

            for(int j=0; j<MALength; j++) {
                threshold[i] = threshold[i] + Math.abs(window[i+MALength-j-1]);
            }

            threshold[i] = 2 * std * (threshold[i] / MALength);

            if(i<100) {
                Log.i("SoundF#eatures", "threshold " + i + ": " + threshold[i]);
            }

            if(Math.abs(window[i+MALength/2]) > threshold[i]) {
                spikiness = spikiness + Math.abs(window[i+MALength/2]);
                numberOfSpikyIndices++;
            }
        }

        Log.i("SoundFeatures", "spikiness: " + spikiness + "; number: " + numberOfSpikyIndices);

        //safeArrayAsTextFile(threshold, "threshold.txt");

        ///// TODO: 13.10.2016 check whether to use numberOfSpikyIndices or threshold.length
        spikiness = spikiness / numberOfSpikyIndices;

        return spikiness;
    }

    private double computeKurtosis(double[] window) {

        double mean = 0;
        double nominator = 0;
        double denominator = 0;

        for(int i=0; i<window.length; i++) {
            mean = mean + window[i];
        }

        mean = mean/window.length;

        for(int i=0; i<window.length; i++) {
            nominator = nominator + Math.pow((window[i] - mean),4);
        }
        nominator = nominator/window.length;

        for(int i=0; i<window.length; i++) {
            denominator = denominator + Math.pow((window[i] - mean),2);
        }
        denominator = Math.pow((denominator/window.length),2);

        if(denominator != 0) {
            return nominator/denominator;
        } else {
            throw new ArithmeticException("divide by zero");
        }
    }

    private double computeSpectralEntropy(Matrix windowFFT, int moveOrTap){

        int numberOfShortBlocks=10;
        double spectralEntropy;
        int subWindowLength;
        double [] subFrameEnergies=new double [numberOfShortBlocks];
        double [] subFrameEnergiesBuffer= new double [numberOfShortBlocks];

        if(moveOrTap == Constants.STEP_TAP) {
            subWindowLength = (int) Math.floor(fftSizeTap / numberOfShortBlocks);
        } else if(moveOrTap == Constants.STEP_MOVE) {
            subWindowLength = (int) Math.floor(fftSizeMove / numberOfShortBlocks);
        } else {
            throw new IllegalArgumentException("unknown step - choose STEP_TAP or STEP_MOVE");
        }

        Matrix buffer= new Matrix((int)subWindowLength,1);
        for (int i=0;i<numberOfShortBlocks;i++){
            buffer=windowFFT.getMatrix(i*subWindowLength,(i+1)*subWindowLength-1,0,0);
            subFrameEnergies[i]=(buffer.arrayTimes(buffer).sum())/(windowFFT.arrayTimes(windowFFT).sum()+eps);
        }

        for(int i =0;i<numberOfShortBlocks;i++){
            subFrameEnergiesBuffer[i]=subFrameEnergies[i]*(Math.log(subFrameEnergies[i]+eps)/Math.log(2));

        }
        spectralEntropy = -sum(subFrameEnergiesBuffer);

    return spectralEntropy;
    }

    private double computeSpectralCentroid(Matrix windowFFT, int moveOrTap){

        double fftSize;

        if(moveOrTap == Constants.STEP_TAP) {
            fftSize = fftSizeTap;
        } else if(moveOrTap == Constants.STEP_MOVE) {
            fftSize = fftSizeMove;
        } else {
            throw new IllegalArgumentException("unknown step - choose STEP_TAP or STEP_MOVE");
        }

        Matrix normalisedWindowFFT=new Matrix((int) fftSize,1);
        Matrix sampleRange=new Matrix ((int) fftSize,1);
        double spectralCentroid;
        double [] sortedWindowFFT=new double[(int) fftSize];
        double windowFFTMaxValue;
        for(int i=0;i< fftSize;i++){
            sampleRange.set(i,0,(fs/(2* fftSize))*(i+1));
        }
        sortedWindowFFT=windowFFT.getRowPackedCopy();
        Arrays.sort(sortedWindowFFT);
        windowFFTMaxValue=sortedWindowFFT[(int) fftSize -1];
        normalisedWindowFFT=windowFFT.times(1/windowFFTMaxValue);
        spectralCentroid=(sampleRange.arrayTimes(normalisedWindowFFT).sum())/(normalisedWindowFFT.sum()+eps);

        return spectralCentroid;
    }

    private double computeSpectralRollOff(Matrix windowFFT, double c) {
        double totalEnergy = 0;

        for(int i=0; i <windowFFT.getRowDimension(); i++) {
            totalEnergy = totalEnergy + Math.pow(windowFFT.get(i, 0), 2);
        }

        //Log.i("SoundFeatures", "totalEnergy: " + totalEnergy);
        double currentEnergy = 0;
        int countFFT = 0;

        while((currentEnergy <= c*totalEnergy) && countFFT < windowFFT.getRowDimension()) {
            currentEnergy = currentEnergy + Math.pow(windowFFT.get(countFFT,0), 2);
            countFFT++;
        }

        return ((double)(countFFT-1) / (double) windowFFT.getRowDimension());

    }

    private double computeSpectralSum(Matrix windowFFT, int moveOrTap) {

        int maxFrequency;
        int minFrequency = 49;
        double sum = 0;

        //// TODO: 14.10.2016 check whether it is necessary to introduce two cases (length of move of them is too short) 
        if(moveOrTap == Constants.STEP_MOVE) {
            maxFrequency = 8000;
        } else if(moveOrTap == Constants.STEP_TAP) {
            maxFrequency = 4000;
        } else {
            throw new IllegalArgumentException("unknown step - choose STEP_TAP or STEP_MOVE");
        }

        for(int i=minFrequency; i<maxFrequency; i++) {
            sum = sum + windowFFT.get(i,0);
        }

        return sum / (maxFrequency-minFrequency);

    }

    private double sum(double [] Array){
        double result = 0;
        for(double value:Array) {
            result += value;
        }
        return result;
    }
    private double median(double[] m) {
        //calculate median values. will be used for the mfcc processing
        int middle = (int)Math.round(m.length/2.0);
        if(m.length == 1) {
            return m[0];
        } else if (m.length%2 == 1) {
            return m[middle];
        } else {
            if(Double.isNaN(m[middle-1])) {
                return Double.NaN;
            } else {
                return (m[middle - 1] + m[middle]) / 2.0;
            }
        }
    }
    private void safeMatrixAsTextFile(Matrix inputMatrix, String filename){
        StringBuilder sb=new StringBuilder();
        FileHandler fh=new FileHandler();
        for(int i=0; i<inputMatrix.getRowDimension();i++){
            for(int j=0; j<inputMatrix.getColumnDimension();j++){
                sb.append(inputMatrix.get(i,j) + "\t");
            }
        }
        fh.writeDataToTextFile(sb.toString(), Constants.ANALYSIS_PATH,filename);
    }
    private void safeArrayAsTextFile(double [] inputArray, String filename){
        StringBuilder sb=new StringBuilder();
        FileHandler fh=new FileHandler();
        for(int i=0; i<inputArray.length;i++){
            sb.append(inputArray[i] + "\t");
        }
        fh.writeDataToTextFile(sb.toString(), Constants.ANALYSIS_PATH,filename);
    }
    public double getZeroCrossingRate() {
        return zeroCrossingRate;
    }
    public double getNoiseDistribution() {
        return noiseDistribution;
    }
    public double getSignalEnergy() {
        return signalEnergy;
    }
    public double getSpikiness() {
        return spikiness;
    }
    public double[] getMFCCs() {
        return MFCCs;
    }
    public double getSpectralEntropy() {
        return spectralEntropy;
    }
    public double getSpectralCentroid() {
        return spectralCentroid;
    }
    public double getSpectralRollOff() {
        return spectralRollOff;
    }
    public double getSpectralSum() {
        return spectralSum;
    }
    public double getSignalEnergyTap() {
        return signalEnergyTap;
    }
    public double getKurtosisTap() {
        return kurtosisTap;
    }
    public double getSpectralCentroidTap() {
        return spectralCentroidTap;
    }
    public double getSpectralRollOffTap() {
        return spectralRollOffTap;
    }
    public double getSpectralSumTap() {
        return spectralSumTap;
    }
}
