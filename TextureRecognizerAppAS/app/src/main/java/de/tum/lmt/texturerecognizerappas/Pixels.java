package de.tum.lmt.texturerecognizerappas;

/**
 * Created by Kev94 on 08.12.2016.
 */
public class Pixels {

    private short[][] pixels;
    private int max;
    private double maxNorm;
    private double meanNorm;
    private int[] histogram;

    public Pixels() {

        max = -1;
        maxNorm = -1;
        meanNorm = -1;
        histogram = new int[256];

    }

    public Pixels(int[] cornerTopLeft, int[] cornerBottomRight) {

        pixels = new short[cornerBottomRight[1]-cornerTopLeft[1]][cornerBottomRight[0]-cornerTopLeft[0]];
        max = -1;
        maxNorm = -1;
        meanNorm = -1;
        histogram = new int[256];

    }

    public void setPixels(short[][] pixels) {
        this.pixels = pixels;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public void setMaxNorm(double maxNorm) {
        this.maxNorm = maxNorm;
    }

    public void setMeanNorm(double meanNorm) {
        this.meanNorm = meanNorm;
    }

    public void setHistogram(int[] histogram) {
        this.histogram = histogram;
    }

    public short[][] getPixels() {
        return this.pixels;
    }

    //// TODO: 08.12.2016 If value does not exist for the following getters (value equals -1) --> compute value from pixels
    public int getMax() {
        return this.max;
    }

    public double getMaxNorm() {
        return this.maxNorm;
    }

    public double getMeanNorm() {
        return this.meanNorm;
    }

    public int[] getHistogram() {
        return this.histogram;
    }

}
