package de.tum.lmt.texturerecognizerappas;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.PreferenceFragment;
import android.util.Log;

import java.util.Set;

public class SettingsActivity extends Activity {

    private static final String TAG = SettingsActivity.class.getSimpleName();
    
    private static boolean mAccelAvailable = false;
    private static boolean mLinAccelAvailable = false;
    private static boolean mGravAvailable = false;
	private static boolean mGyroAvailable = false;
	private static boolean mMagnetAvailable = false;
	private static boolean mRotVecAvailable = false;
    private static boolean mExternAccelAvailable = true;
    private static boolean mFSRAvailable = true;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        
        mAccelAvailable = intent.getBooleanExtra("accelAvailable", false);
        mLinAccelAvailable = intent.getBooleanExtra("linAccelAvailable", false);
        mGravAvailable = intent.getBooleanExtra("gravAvailable", false);
        mGyroAvailable = intent.getBooleanExtra("gyroAvailable", false);
        mMagnetAvailable = intent.getBooleanExtra("magnetAvailable", false);
        mRotVecAvailable = intent.getBooleanExtra("rotVecAvailable", false);
        mExternAccelAvailable = intent.getBooleanExtra("externAccelAvailable", false);
        mFSRAvailable = intent.getBooleanExtra("fsrAvailable", false);
        
        // Display the fragment as the gallery_menu content.
        getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();
    }

    //this Fragment contains the Settings
    public static class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    	private CheckBoxPreference mAccelPref;
        private CheckBoxPreference mLinAccelPref;
        private CheckBoxPreference mGravPref;
        private CheckBoxPreference mGyroPref;
        private CheckBoxPreference mMagnetPref;
        private CheckBoxPreference mRotVecPref;
        private CheckBoxPreference mExternAccelPref;
        private CheckBoxPreference mFSRPref;
        private CheckBoxPreference mVelocityPref;
        private ListPreference mClassificationMethodPref;
        private EditTextPreference mNeighborhoodPref;
        private MultiSelectListPreference mFeaturesPref;
        private CheckBoxPreference mOnlinePref;
        private EditTextPreference mIPPref;
        private CheckBoxPreference mModeSelectPref;
        private EditTextPreference mDurationPref;
        private EditTextPreference mDurationPrefDB;
        private CheckBoxPreference mTPADPref;

        //always set the entries and the entry values when this fragment is being created (maybe the
        // user downloaded new languages and voices)
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.preferences);
            
            mAccelPref = (CheckBoxPreference) findPreference(Constants.PREF_KEY_ACCEL_SELECT);
            mLinAccelPref = (CheckBoxPreference) findPreference(Constants.PREF_KEY_LIN_ACCEL_SELECT);
            mGravPref = (CheckBoxPreference) findPreference(Constants.PREF_KEY_GRAV_SELECT);
            mGyroPref = (CheckBoxPreference) findPreference(Constants.PREF_KEY_GYRO_SELECT);
            mMagnetPref = (CheckBoxPreference) findPreference(Constants.PREF_KEY_MAGNET_SELECT);
            mRotVecPref = (CheckBoxPreference) findPreference(Constants.PREF_KEY_ROTVEC_SELECT);
            mExternAccelPref = (CheckBoxPreference) findPreference(Constants.PREF_KEY_EXTERN_ACCEL);
            mFSRPref = (CheckBoxPreference) findPreference(Constants.PREF_KEY_FSR);
            mVelocityPref = (CheckBoxPreference) findPreference(Constants.PREF_KEY_VELOCITY);
            mOnlinePref = (CheckBoxPreference) findPreference(Constants.PREF_KEY_ONLINE);
            mIPPref = (EditTextPreference) findPreference(Constants.PREF_KEY_IP);
            mTPADPref = (CheckBoxPreference) findPreference(Constants.PREF_KEY_TPAD);
            mModeSelectPref = (CheckBoxPreference) findPreference(Constants.PREF_KEY_MODE_SELECT);
            mDurationPref = (EditTextPreference) findPreference(Constants.PREF_KEY_DURATION);
            mDurationPrefDB = (EditTextPreference) findPreference(Constants.PREF_KEY_DURATION_DB);
            mClassificationMethodPref = (ListPreference) findPreference(Constants.PREF_KEY_CLASSIFICATION_METHOD);
            //mDistancePref = (ListPreference) findPreference(Constants.PREF_KEY_DISTANCE);
            mNeighborhoodPref = (EditTextPreference) findPreference(Constants.PREF_KEY_NEIGHBORHOOD);
            mFeaturesPref = (MultiSelectListPreference) findPreference(Constants.PREF_KEY_CLASSIfICATION_FEATURES);
            
            //Accel always checked
            mAccelPref.setEnabled(false);

            if(mModeSelectPref.isChecked()) {
                mDurationPref.setEnabled(false);
                mDurationPrefDB.setEnabled(true);
            } else {
                mDurationPrefDB.setEnabled(false);
                mDurationPref.setEnabled(true);

            }
            
            checkIfSensorAvailable();
            
            setSummaries();
        }
        
        private void checkIfSensorAvailable() {
        	if(!mAccelAvailable) {
                mAccelPref.setEnabled(false);
                mAccelPref.setSummary(getString(R.string.not_available));
                mAccelPref.setChecked(false);
            }

            if(!mLinAccelAvailable) {
                mLinAccelPref.setEnabled(false);
                mLinAccelPref.setSummary(R.string.not_available);
                mLinAccelPref.setChecked(false);
            }
            
            if(!mGravAvailable) {
                mGravPref.setEnabled(false);
                mGravPref.setSummary(getString(R.string.not_available));
                mGravPref.setChecked(false);
            }
            
            if(!mGyroAvailable) {
                mGyroPref.setEnabled(false);
                mGyroPref.setSummary(getString(R.string.not_available));
                mGyroPref.setChecked(false);
            }
            
            if(!mMagnetAvailable) {
                mMagnetPref.setEnabled(false);
                mMagnetPref.setSummary(getString(R.string.not_available));
                mMagnetPref.setChecked(false);
            }
            
            if(!mRotVecAvailable) {
                mRotVecPref.setEnabled(false);
                mRotVecPref.setSummary(getString(R.string.not_available));
                mRotVecPref.setChecked(false);
            }

            if(!mExternAccelAvailable) {
                mExternAccelPref.setEnabled(false);
                mExternAccelPref.setSummary(getString(R.string.not_available));
                mExternAccelPref.setChecked(false);
            }

            if(!mFSRAvailable) {
                mFSRPref.setEnabled(false);
                mFSRPref.setSummary(getString(R.string.not_available));
                mFSRPref.setChecked(false);
            }

            //// TODO: 12.03.2016 Check if internet connection isAvailable
        }
        
        private void setSummaries() {
        	
        	if(mAccelPref.isChecked()) {
                mAccelPref.setSummary(getString(R.string.active));
            } else {
            	mAccelPref.setSummary(getString(R.string.inactive));
            }

            if(mLinAccelPref.isChecked()) {
                mLinAccelPref.setSummary(R.string.active);
            } else {
                mLinAccelPref.setSummary(R.string.inactive);
            }
            
            if(mGravPref.isChecked()) {
            	mGravPref.setSummary(getString(R.string.active));
            } else {
            	mGravPref.setSummary(getString(R.string.inactive));
            }
            
            if(mGyroPref.isChecked()) {
            	mGyroPref.setSummary(getString(R.string.active));
            } else {
            	mGyroPref.setSummary(getString(R.string.inactive));
            }
            
            if(mMagnetPref.isChecked()) {
            	mMagnetPref.setSummary(getString(R.string.active));
            } else {
            	mMagnetPref.setSummary(getString(R.string.inactive));
            }
            
            if(mRotVecPref.isChecked()) {
            	mRotVecPref.setSummary(getString(R.string.active));
            } else {
                mRotVecPref.setSummary(getString(R.string.inactive));
            }

            if(mExternAccelPref.isChecked()) {
                mExternAccelPref.setSummary(getString(R.string.active));
            } else {
                mExternAccelPref.setSummary(getString(R.string.inactive));
            }

            if(mFSRPref.isChecked()) {
                mFSRPref.setSummary(getString(R.string.active));
            } else {
                mFSRPref.setSummary(getString(R.string.inactive));
            }

            if(mVelocityPref.isChecked()) {
                mVelocityPref.setSummary(getString(R.string.used));
            } else {
                mVelocityPref.setSummary(getString(R.string.not_used));
            }

            if(mOnlinePref.isChecked()) {
                mOnlinePref.setSummary(getString(R.string.used));
            } else {
                mOnlinePref.setSummary(getString(R.string.not_used));
            }

            mIPPref.setSummary(mIPPref.getText().toString());

            if(mTPADPref.isChecked()) {
                mTPADPref.setSummary(getString(R.string.used));
            } else {
                mTPADPref.setSummary(getString(R.string.not_used));
            }

            mClassificationMethodPref.setSummary(mClassificationMethodPref.getValue());
           // mDistancePref.setSummary(mDistancePref.getValue());
            if(mClassificationMethodPref.getValue().equals(Constants.CLASSIFICATION_DIST_TO_MEAN) || mClassificationMethodPref.getValue().equals(Constants.CLASSIFICATION_MAHALANOBIS)) {
                mNeighborhoodPref.setSummary(getString(R.string.neighborhood_not_relevant));
                mNeighborhoodPref.setEnabled(false);
            } else {
                mNeighborhoodPref.setSummary(mNeighborhoodPref.getText().toString());
            }

            Set<String> features = mFeaturesPref.getValues();
            if(features.size() == 1) {
                mFeaturesPref.setSummary(getString(R.string.feature_selection_summary_single));
            } else {
                mFeaturesPref.setSummary(getString(R.string.feature_selection_summary_multiple, features.size()));
            }

            if(mModeSelectPref.isChecked()) {
                mModeSelectPref.setSummary(getString(R.string.used));
            } else {
                mModeSelectPref.setSummary(getString(R.string.not_used));
            }

            mDurationPref.setSummary(mDurationPref.getText().toString());
            mDurationPrefDB.setSummary(mDurationPrefDB.getText().toString());
        }
        
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

            switch(key) {

                case Constants.PREF_KEY_ACCEL_SELECT:

                    if (sharedPreferences.getBoolean(key, false)) {
                        mAccelPref.setSummary(getString(R.string.active));
                    } else {
                        mAccelPref.setSummary(getString(R.string.inactive));
                    }

                    break;
                case Constants.PREF_KEY_LIN_ACCEL_SELECT:

                    if (sharedPreferences.getBoolean(key, false)) {
                        mLinAccelPref.setSummary(getString(R.string.active));
                    } else {
                        mLinAccelPref.setSummary(getString(R.string.inactive));
                    }

                    break;
                case Constants.PREF_KEY_GRAV_SELECT:

                    if (sharedPreferences.getBoolean(key, false)) {
                        mGravPref.setSummary(getString(R.string.active));
                    } else {
                        mGravPref.setSummary(getString(R.string.inactive));
                    }

                    break;
                case Constants.PREF_KEY_GYRO_SELECT:

                    if (sharedPreferences.getBoolean(key, false)) {
                        mGyroPref.setSummary(getString(R.string.active));
                    } else {
                        mGyroPref.setSummary(getString(R.string.inactive));
                    }

                    break;
                case Constants.PREF_KEY_MAGNET_SELECT:

                    if (sharedPreferences.getBoolean(key, false)) {
                        mMagnetPref.setSummary(getString(R.string.active));
                    } else {
                        mMagnetPref.setSummary(getString(R.string.inactive));
                    }

                    break;
                case Constants.PREF_KEY_ROTVEC_SELECT:

                    if (sharedPreferences.getBoolean(key, false)) {
                        mRotVecPref.setSummary(getString(R.string.active));
                    } else {
                        mRotVecPref.setSummary(getString(R.string.inactive));
                    }

                    break;
                case Constants.PREF_KEY_EXTERN_ACCEL:

                    if (sharedPreferences.getBoolean(key, false)) {
                        mExternAccelPref.setSummary(getString(R.string.yes));
                    } else {
                        mExternAccelPref.setSummary(getString(R.string.no));
                    }

                    break;
                case Constants.PREF_KEY_FSR:
                    if (sharedPreferences.getBoolean(key, false)) {
                        mFSRPref.setSummary(getString(R.string.yes));
                    } else {
                        mFSRPref.setSummary(getString(R.string.no));
                    }

                    break;
                case Constants.PREF_KEY_VELOCITY:

                    if (sharedPreferences.getBoolean(key, false)) {
                        mVelocityPref.setSummary(getString(R.string.used));
                    } else {
                        mVelocityPref.setSummary(getString(R.string.not_used));
                    }

                    break;
                case Constants.PREF_KEY_ONLINE:

                    if (sharedPreferences.getBoolean(key, false)) {
                        mOnlinePref.setSummary(getString(R.string.used));
                    } else {
                        mOnlinePref.setSummary(getString(R.string.not_used));
                    }
                    break;
                case Constants.PREF_KEY_IP:
                    mIPPref.setSummary(mIPPref.getText().toString());
                    break;
                case Constants.PREF_KEY_TPAD:

                    if (sharedPreferences.getBoolean(key, false)) {
                        mTPADPref.setSummary(getString(R.string.used));
                    } else {
                        mTPADPref.setSummary(getString(R.string.not_used));
                    }
                    break;
                case Constants.PREF_KEY_CLASSIFICATION_METHOD:

                    mClassificationMethodPref.setSummary(mClassificationMethodPref.getValue());

                    if(mClassificationMethodPref.getValue().equals(Constants.CLASSIFICATION_DIST_TO_MEAN) || mClassificationMethodPref.getValue().equals(Constants.CLASSIFICATION_MAHALANOBIS)) {
                        mNeighborhoodPref.setSummary(getString(R.string.neighborhood_not_relevant));
                        mNeighborhoodPref.setEnabled(false);
                    } else {
                        mNeighborhoodPref.setSummary(mNeighborhoodPref.getText().toString());
                        mNeighborhoodPref.setEnabled(true);
                    }
                    break;
                /*case Constants.PREF_KEY_DISTANCE:
                    mDistancePref.setSummary(mDistancePref.getValue());
                    break;*/
                case Constants.PREF_KEY_NEIGHBORHOOD:
                    mNeighborhoodPref.setSummary(mNeighborhoodPref.getText().toString());
                    break;
                case Constants.PREF_KEY_CLASSIfICATION_FEATURES:

                    Set<String> features = mFeaturesPref.getValues();

                    if(features.size() == 1) {
                        mFeaturesPref.setSummary(getString(R.string.feature_selection_summary_single));
                    } else {
                        mFeaturesPref.setSummary(getString(R.string.feature_selection_summary_multiple, features.size()));
                    }

                    break;
                case Constants.PREF_KEY_MODE_SELECT:

                    CheckBoxPreference modeSelectPref = (CheckBoxPreference) findPreference(key);

                    if (sharedPreferences.getBoolean(key, false)) {
                        modeSelectPref.setSummary(getString(R.string.yes));
                        mDurationPref.setEnabled(false);
                        mDurationPrefDB.setEnabled(true);
                    } else {
                        modeSelectPref.setSummary(getString(R.string.no));
                        mDurationPref.setEnabled(true);
                        mDurationPrefDB.setEnabled(false);
                    }

                    break;
                case Constants.PREF_KEY_DURATION:
                    mDurationPref.setSummary(mDurationPref.getText().toString());
                    break;
                case Constants.PREF_KEY_DURATION_DB:
                    mDurationPrefDB.setSummary(mDurationPrefDB.getText().toString());
                    break;
            }

        }
    
        @Override
        public void onResume() {
            super.onResume();
            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        }
    }
}
