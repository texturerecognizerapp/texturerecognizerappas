package de.tum.lmt.texturerecognizerappas;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Kev94 on 14.03.2016.
 */
public class Texture {

    private String mName;
    private List<Sample> mSamples;
    private double mDistance;

    public Texture(String name) {
        mName = name;
        mSamples = new ArrayList<>();
    }

    public Texture(String name, String number, Features features) {
        mName = name;
        mSamples = new ArrayList<>();
    }

    public String getName() {
        return mName;
    }

    public void addSample(String number, Features features) {
        Sample newSample = new Sample(number, features);
        mSamples.add(newSample);
    }

    public void addSampleWithDistance(double distance) {
        Sample newSample = new Sample();
        newSample.setDistance(distance);
        mSamples.add(newSample);
    }

    public void addSampleWithOccurence(int occurence) {
        Sample newSample = new Sample();
        newSample.setOccurence(occurence);
        mSamples.add(newSample);
    }

    public List<Sample> getSamples() {
        return mSamples;
    }

    public void setDistance(double distance) {
        mDistance = distance;
    }

    public double getDistance() {
        return mDistance;
    }

    public class Sample {

        private String mNumber;
        private Features mFeatures;
        private double mDistance;
        private int mOccurence; // needed for nearest neighbor only

        public Sample() {

        }

        public Sample(String number) {
            mNumber = number;
        }

        public Sample(String number, Features features) {
            mNumber = number;
            mFeatures = features;
        }

        public String getName() {
            return mName;
        }

        public String getNumber() {
            return mNumber;
        }

        public void setNumber(String number) {
            mNumber = number;
        }

        public Features getFeatures() {
            return mFeatures;
        }

        public void setFeatures(Features features) {
            mFeatures = features;
        }

        public void setDistance(double distance) {
            mDistance = distance;
        }

        public double getDistance() {
            return mDistance;
        }

        public void setOccurence(int occurence) {
            mOccurence = occurence;
        }

        public int getOccurence() {
            return mOccurence;
        }
    }
}
