package de.tum.lmt.texturerecognizerappas;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kev94 on 16.07.2016.
 */
public class DialogEnableWifiFragment extends DialogFragment {

    private Button mButtonYes;
    private Button mButtonNo;

    private ListView mWifiList;

    private WifiManager mWifi;
    private WifiReceiver mReceiverWifi;

    private String mBaseDirPath;

    private ServerTask.iOnDataSentListener mOnDataSentListener;

    //http://developer.android.com/reference/android/app/DialogFragment.html
    static DialogEnableWifiFragment newInstance(String dir) {
        DialogEnableWifiFragment f = new DialogEnableWifiFragment();

        Bundle args = new Bundle();
        args.putString("dir", dir);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        mBaseDirPath = getArguments().getString("dir");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View content = inflater.inflate(R.layout.dialog_enable_wifi, null);

        builder.setView(content);

        mButtonYes = (Button) content.findViewById(R.id.button_wifi_yes);
        mButtonNo = (Button) content.findViewById(R.id.button_wifi_no);
        mWifiList = (ListView) content.findViewById(R.id.wifi_list);

        mButtonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWifi = (WifiManager) getActivity().getSystemService(getActivity().getApplicationContext().WIFI_SERVICE);

                mWifi.setWifiEnabled(true);

                mReceiverWifi = new WifiReceiver();
                getActivity().registerReceiver(mReceiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

            }
        });

        mButtonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mReceiverWifi != null) {
                    getActivity().unregisterReceiver(mReceiverWifi);
                }

                Toast.makeText(getActivity().getApplicationContext(), getString(R.string.do_not_send), Toast.LENGTH_LONG).show();
                dismiss();

            }
        });

        mOnDataSentListener = new ServerTask.iOnDataSentListener() {
            @Override
            public void onDataSent() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "data sent", Toast.LENGTH_LONG).show();
                    }
                });
            }
        };

        mWifiList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final ScanResult scanResult = (ScanResult) parent.getItemAtPosition(position);

                if(mWifi.getConnectionInfo().getSSID() == scanResult.SSID) {

                    Toast.makeText(getActivity(), R.string.wifi_already_connected, Toast.LENGTH_LONG).show();

                    SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    String IP = sharedPrefs.getString(Constants.PREF_KEY_IP, "192.168.2.1");

                    ServerTask serverTask = new ServerTask(getActivity(), mOnDataSentListener);
                    serverTask.execute(mBaseDirPath, IP);

                } else {

                    int security = Constants.SECURITY_NONE;
                    boolean performNextStep = true;

                    if (scanResult.capabilities.contains("WEP")) {
                        security = Constants.SECURITY_WEP;
                    } else if (scanResult.capabilities.contains("PSK")) {
                        security = Constants.SECURITY_PSK;
                    } else if (scanResult.capabilities.contains("EAP")) {
                        security = Constants.SECURITY_EAP;
                    } else {
                        Toast.makeText(getActivity(), R.string.wifi_no_within_app_log_in, Toast.LENGTH_LONG).show();
                        performNextStep = false;
                    }

                    if (performNextStep) {
                        WifiConfiguration conf = new WifiConfiguration();
                        AlertDialog.Builder builder;

                        //from http://stackoverflow.com/questions/6517314/android-wifi-connection-programmatically
                        switch (security) {
                            case Constants.SECURITY_NONE:

                                conf.SSID = "\"" + scanResult.SSID;
                                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);

                                int res = mWifi.addNetwork(conf);
                                mWifi.enableNetwork(res, true);
                                mWifi.setWifiEnabled(true);

                                boolean changeHappened = mWifi.saveConfiguration();

                                if (res != -1 && changeHappened) {
                                    Toast.makeText(getActivity(), getString(R.string.wifi_connected, scanResult.SSID), Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getActivity(), getString(R.string.wifi_not_connected, scanResult.SSID), Toast.LENGTH_LONG).show();
                                }

                                break;

                            case Constants.SECURITY_WEP:

                                builder = getNetworkPassDialog(Constants.SECURITY_WEP, scanResult.SSID);
                                builder.show();
                                break;

                            case Constants.SECURITY_EAP:

                                builder = getNetworkPassDialog(Constants.SECURITY_EAP, scanResult.SSID);
                                builder.show();
                                break;

                            case Constants.SECURITY_PSK:

                                builder = getNetworkPassDialog(Constants.SECURITY_PSK, scanResult.SSID);
                                builder.show();
                                break;
                        }
                    }
                }
            }
        });


        return builder.create();
    }

    @Override
    public void onDismiss(DialogInterface dialogInterface) {
        getActivity().unregisterReceiver(mReceiverWifi);
    }

    //function to return an Alert Dialog
    AlertDialog.Builder getNetworkPassDialog(final int securityType, final String SSID) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Set up the input
        final EditText input = new EditText(getActivity());
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        builder.setView(input);

        switch(securityType) {
            case Constants.SECURITY_WEP:
                builder.setMessage(getString(R.string.wifi_enter_network_key, "WEP"));
                break;
            case Constants.SECURITY_EAP:
                builder.setMessage(getString(R.string.wifi_enter_network_key, "WPA_EAP"));
                break;
            case Constants.SECURITY_PSK:
                builder.setMessage(getString(R.string.wifi_enter_network_key, "WAP_PSK"));
                break;
        }

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                connectToWifi(securityType, SSID, input.getText().toString());
                dialog.cancel();
                dismiss();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                dismiss();
            }
        });

        return builder;

    }

    void connectToWifi(int securityType, String SSID, String networkKey) {

        WifiConfiguration conf = new WifiConfiguration();
        int res = -1;

        switch(securityType) {
            case Constants.SECURITY_WEP:

                conf.SSID = "\"" + SSID + "\"";
                conf.wepKeys[0] = "\"" + networkKey + "\"";
                conf.wepTxKeyIndex = 0;
                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);

                res = mWifi.addNetwork(conf);
                mWifi.enableNetwork(res, true);
                mWifi.setWifiEnabled(true);

                break;
            case Constants.SECURITY_EAP:

                conf.SSID = "\"" + SSID + "\"";
                conf.preSharedKey = "\"" + networkKey + "\"";
                conf.hiddenSSID = true;
                conf.status = WifiConfiguration.Status.ENABLED;
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_EAP);
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);

                res = mWifi.addNetwork(conf);
                mWifi.enableNetwork(res, true);
                mWifi.setWifiEnabled(true);

                break;

            case Constants.SECURITY_PSK:

                conf.SSID = "\"" + SSID + "\"";
                conf.preSharedKey = "\"" + networkKey + "\"";
                conf.hiddenSSID = true;
                conf.status = WifiConfiguration.Status.ENABLED;
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);

                res = mWifi.addNetwork(conf);
                mWifi.enableNetwork(res, true);
                mWifi.setWifiEnabled(true);

                break;
        }

        boolean changeHappened = mWifi.saveConfiguration();

        if (res != -1 && changeHappened) {
            Toast.makeText(getActivity(), getString(R.string.wifi_connected, SSID), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getActivity(), getString(R.string.wifi_not_connected, SSID), Toast.LENGTH_LONG).show();
        }
    }

    //from: http://stackoverflow.com/questions/17167084/android-scanning-wifi-network-selectable-list
    class WifiReceiver extends BroadcastReceiver
    {
        public void onReceive(Context c, Intent intent) {

            List<ScanResult> wifiList;
            wifiList = mWifi.getScanResults();

            WifiListAdapter adapter = new WifiListAdapter(getActivity().getApplicationContext(), R.layout.wifi_item_layout, wifiList);
            mWifiList.setAdapter(adapter);
        }
    }

    class WifiListAdapter extends ArrayAdapter {

        private int mLayoutResourceId;
        private List<ScanResult> mData;

        public WifiListAdapter(Context context, int layoutResourceId, List<ScanResult> data) {
            super(context, layoutResourceId, data);
            mLayoutResourceId = layoutResourceId;
            mData = data;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            ViewHolder holder;

            if (row == null) {
                Log.i("Adapter", "row null");
                LayoutInflater inflater = (getActivity()).getLayoutInflater();
                row = inflater.inflate(mLayoutResourceId, parent, false);
                holder = new ViewHolder();
                holder.SSID = (TextView) row.findViewById(R.id.SSID);
                row.setTag(holder);
            } else {
                Log.i("Adapter", "row not null");
                holder = (ViewHolder) row.getTag();
            }

            ScanResult item = mData.get(position);

            if(item == null) {
                Log.i("Adapter", "item null");
            } else {
                if(holder == null) {
                    Log.i("Adapter", "holder null");
                } else {
                    holder.SSID.setText(item.SSID);
                }
            }
            return row;
        }

        class ViewHolder {
            TextView SSID;
        }
    }

}


