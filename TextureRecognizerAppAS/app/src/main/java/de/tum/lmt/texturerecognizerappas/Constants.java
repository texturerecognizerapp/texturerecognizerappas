package de.tum.lmt.texturerecognizerappas;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class Constants {

	//Preferences
	public static final String PREF_KEY_LOGGING_DIR = "pref_key_logging_dir";
	public static final String PREF_KEY_ACCEL_SELECT = "pref_key_accel_select";
	public static final String PREF_KEY_LIN_ACCEL_SELECT = "pref_key_lin_accel_select";
	public static final String PREF_KEY_GRAV_SELECT = "pref_key_grav_select";
	public static final String PREF_KEY_GYRO_SELECT = "pref_key_gyro_select";
	public static final String PREF_KEY_MAGNET_SELECT = "pref_key_magnet_select";
	public static final String PREF_KEY_ROTVEC_SELECT = "pref_key_rotvec_select";
	public static final String PREF_KEY_EXTERN_ACCEL = "pref_key_extern_accel";
	public static final String PREF_KEY_FSR = "pref_key_fsr";
	public static final String PREF_KEY_MODE_SELECT = "pref_key_mode_select";
	public static final String PREF_KEY_DURATION = "pref_key_duration";
	public static final String PREF_KEY_DURATION_DB = "pref_key_duration_db";
	public static final String PREF_KEY_VELOCITY = "pref_key_velocity";
	public static final String PREF_KEY_ONLINE = "pref_key_online";
	public static final String PREF_KEY_IP = "pref_key_ip";
	public static final String PREF_KEY_TPAD = "pref_key_tpad";
	public static final String PREF_KEY_TEXTURE_NAME = "pref_key_texture_name";
	public static final String PREF_KEY_TEXTURE_NUMBER = "pref_key_texture_number";
	public static final String PREF_KEY_CLASSIFICATION_METHOD = "pref_key_classification_method";
	public static final String PREF_KEY_DISTANCE = "pref_key_distance";
	public static final String PREF_KEY_NEIGHBORHOOD = "pref_key_neighborhood";
	public static final String PREF_KEY_CLASSIfICATION_FEATURES = "pref_key_classification_features";

	public static final int DEFAULT_NEIGHBORHOOD = 20;

	public static final String CLASSIFICATION_DIST_TO_MEAN = "DistanceToMean";
	public static final String CLASSIFICATION_KNN = "k-nearest-neighbors";
	public static final String CLASSIFICATION_MAHALANOBIS = "Mahalanobis";
	public static final String DISTANCE_EUCLIDEAN = "Euclidean";

	public static final String PREF_TITLE_CLASSIFICATION_METHOD = "ClassificationMethod";
	public static final String PREF_TITLE_CLASSIFICATION_FEATURES = "ClassificationFeatures";
	public static final String PREF_TITLE_NEIGHBORHOOD_SIZE = "NeighborhoodSize";

	//Files and Paths
	public static final String DATABASE_PATH = "/storage/emulated/0/Log/Database/";
	public static final String ANALYSIS_PATH = "/storage/emulated/0/Log/Analysis/";
	public static final String PATH_TO_STORAGE = "Log/";
	public static final String ANALYSIS_FOLDER_NAME = "/Analysis/";
	public static final String DATABASE_FOLDER_NAME = "/Database";
	public static final String DATA_TO_SEND_FOLDER_NAME = "/DataToSend";
	public final static String SENSOR_SINGLE_RECORDING_LOGGING_FOLDER_NAME = "Sensors";
	public final static String SENSOR_TAP_LOGGING_FOLDER_NAME = "Sensors_TAP";
	public final static String SENSOR_MOVE_LOGGING_FOLDER_NAME = "Sensors_Move";
	public static final String TEXT_FILE_EXTENSION = ".txt";
	public static final String JPG_FILE_EXTENSION = ".jpg";
	public static final String STEP_STRING_SINGLE_RECORDING = "";
	public static final String STEP_STRING_TAP = "_tap";
	public static final String STEP_STRING_MOVE = "_move";

	public static String TEXTURE_DESCRIPTION_FILENAME = "texture_description" + TEXT_FILE_EXTENSION;

	public static final String ACCEL_FILENAME = "accel";
	public static final String LIN_ACCEL_FILENAME = "linAccel";
	public static final String EXTERN_ACCEL_FILENAME = "externAccel";
	public static final String FILTERED_ACCEL_FILENAME = "filteredAccel";
	public static final String FILTERED_EXTERN_ACCEL_FILENAME = "filteredExternAccel";
	public static final String GRAV_FILENAME = "grav";
	public static final String GYRO_FILENAME = "gyro";
	public static final String MAGNET_FILENAME = "magnet";
	public static final String ROTVEC_FILENAME = "rotvec";
	public static final String FSR_FILENAME = "fsr";
	public static final String PARAMETERS_FILENAME = "parameters";
	public static final String JSON_FEATURE_LOG_FILENAME = "jsonFeatureLog";
	public static final String MACRO_IMAGE_FILENAME = "macro";
	public static final String DISPLAY_IMAGE_FILENAME = "display";
	public static final String VELOCITY_FILENAME = "velocity";

	public static final String JSON_DATABASE_FILENAME = "JSONdatabase.txt";
	public static final String JSON_MIN_MAX_FILENAME = "JSONMinMax.txt";

	//Gallery Activity
	public static final int NUM_OF_COLUMNS = 3;

	//Calibration Activity
	public static boolean GRAVITY_NOT_PRESENT;
	public static final float CALIB_PROXIMITY_THRESHOLD = 4.5f;
	public static final float CALIB_FILTER_FORGETTING_FACTOR = 1.0f/100.0f;
	public static final float CALIB_THRESH = 0.015f;
	public static final long CALIB_TIME_DIFF_SINCE_LAST_GYRO_EVENT = 500;
	
	//Camera Activity
	public static final int DURATION_BREAK_PICTURE = 1500;
	public static final int DESIRED_CAMERA_IMAGE_WIDTH = 480;
	public static final int DESIRED_IMAGE_CAMERA_HEIGHT = 640;
	public static final String CAMERA_NO_FLASH_FILENAME = "picture";
	public static final String CAMERA_FLASH_FILENAME = "picture_flash";
	public static final int JPG_COMPRESSION_LEVEL = 100; // the bigger the better
	
	//SensorLoggingActivity
	public final static long DURATION_WAIT = 500;
	public final static long DURATION_EXTRA = 2000;
	public final static long DURARTION_VIBRATE = 200;
	public final static long DURARTION_VIBRATE_ERROR = 500;
	public final static int MAX_VELOCITY_BAR_PROGRESS = 1000;

	//LoggingSensorListener
	public static final int STEP_CALIBRATE = 0;
	public static final int STEP_RECORD = 1;

	//AUDIO
	public static final String AUDIO_FILENAME_ORIG = "audio";
	public static final String AUDIO_FILENAME_MOVEMENT = "sound";
	public static final String AUDIO_FILENAME_IMPACT = "impact";
	public static final String FEATURE_FILE_NAME = "accel";
	public static final String AUDIO_FILE_EXTENSION = ".wav";
	public static final int RECORDER_SAMPLING_RATE = 44100;

	//Continue Dialog
	public static final String CALIBRATION = "calibration";
	public static final String CAMERA = "camera";
	
	//Features Dialog
	public static final String BITMAP_NO_FLASH_FILENAME = "/" + CAMERA_NO_FLASH_FILENAME + JPG_FILE_EXTENSION;
	public static final String BITMAP_FLASH_FILENAME = "/" + CAMERA_FLASH_FILENAME + JPG_FILE_EXTENSION;

	//Feature Extractor
	public static final int SAMPLE_RATE_ACCEL = 100;
	
	//Send Dialog
	public static final List<String> NAMES_OF_BLUETOOTH_FILES_TO_SEND = Arrays.asList(new String[] { "macro", "parameters", "display", "sound", "impact" });
	public static final List<String> NAMES_OF_BLUETOOTH_FILES_TO_SEND_ALL = Arrays.asList(new String[] { "macro", "parameters", "display", "sound", "impact", "linAccel" });
	public static final String URL_PREFIX = "http://";
	public static final String PHP_UPLOAD_FILE_NAME = "uploadFile.php";
	public static final String PHP_UPLOAD_AND_MATLAB_FILE_NAME = "uploadFileAndStartMatlab2.php";
	public static final String PHP_STREAM_FILE_NAME = "streamFile.php";

	//Mail
	public static final String ZIP_FILE_NAME = "data.zip";

	//Wifi
	public static final int SECURITY_NONE = 0;
	public static final int SECURITY_WEP = 1;
	public static final int SECURITY_PSK = 2;
	public static final int SECURITY_EAP = 3;

	//Sensor Log
	public static final int INVALID_SENSOR_LOG_TYPE = -2;
	public static final int TYPE_EXTERN_ACCEL = 30;
	public static final int STEP_SINGLE_RECORDING = 0;
	public static final int STEP_TAP = 1;
	public static final int STEP_MOVE = 2;

	//FeatureComputer
	public static final int BRIGHTNESS_THRESHOLD = 240;
	public static final int PRECISION_PARAMETERS = 2;
	public static final int PRECISION_WEIGHTS = 3;
	public static final int INTERNAL_ACCELERATION_SENSOR = 0;
	public static final int EXTERNAL_ACCELERATION_SENSOR = 1;

	//ClassificationTask
	public static final int SORT_DISTANCE = 0;
	public static final int SORT_OCCURENCE = 1;
	public static final int NUMBER_OF_RELEVANT_FEATURES = 13;

	//unit conversion
	public static final float NS2S = 0.000000001f;

	//Database
	public static final int NUMBER_OF_TEXTURES = 108;

	//Feature Names
	//Image Features
	public static final String GLOSSINESS = "Glossiness";
	public static final String IMAGE_CENTROID = "ImageCentroid";
	public static final String IMAGE_CONTRAST = "Contrast";
	public static final String DOMINANT_COLOR = "DominantColor";
	public static final String DOMINANT_SATURATION = "DominantSaturation";
	public static final String IMAGE_FREQUENCY_BIN_COUNT_1 = "ImageFrequencyBinCount1";
	public static final String IMAGE_SPECTRAL_CENTROID = "ImageSpectralCentroid";
	public static final String IMAGE_REGULARITY = "ImageRegularity";

	//Sound Features
	public static final String SOUND_ZERO_CROSSING_RATE = "SoundZeroCrossingRate";
	public static final String SOUND_SIGNAL_ENERGY = "SoundSignalEnergy";
	public static final String SOUND_NOISE_DISTRIBUTION = "SoundNoiseDistribution";
	public static final String SOUND_SPIKINESS = "SoundSpikiness";
	public static final String SOUND_SPECTRAL_CENTROID = "SoundSpectralCentroid";
	public static final String SOUND_SPECTRAL_ENTROPY = "SoundSpectralEntropy";
	public static final String SOUND_SPECTRAL_ROLL_OFF = "SoundSpecRollOff";
	public static final String MFCC1 ="MFCC1";
	public static final String MFCC2 ="MFCC2";
	public static final String MFCC5 ="MFCC5";
	public static final String SOUND_SPECTRAL_SUM = "SoundSpectralSum";
	public static final String SOUND_SIGNAL_ENERGY_TAP = "SoundSignalEnergyTap";
	public static final String SOUND_KURTOSIS_TAP = "SoundKurtosisTap";
	public static final String SOUND_SPECTRAL_CENTROID_TAP = "SoundSpectralCentroidTap";
	public static final String SOUND_SPECTRAL_ROLL_OFF_TAP = "SoundSpectralRollOffTap";
	public static final String SOUND_SPECTRAL_SUM_TAP = "SoundSpectralSumTap";

	//Acceleration Features
	public static final String EXTERNAL_ACCELERATION_SPECTRAL_SUM = "ExternalAccelerationSpectralSum";
	public static final String EXTERNAL_ACCELERATION_SIGNAL_ENERGY = "ExternalAccelerationSignalEnergy";
	public static final String EXTERNAL_ACCELERATION_ZERO_CROSSING_RATE= "ExternalAccelerationZeroCrossingRate";
	public static final String ACCELERATION_SPECTRAL_SUM = "AccelerationSpectralSum";
	public static final String ACCELERATION_SIGNAL_ENERGY = "AccelerationSignalEnergy";
	public static final String ACCELERATION_ZERO_CROSSING_RATE= "AccelerationZeroCrossingRate";


	//FeatureSet containing all feature names for reading out or setting all features at once
	public static final String[] FEATURE_SET = {GLOSSINESS, IMAGE_CENTROID, IMAGE_CONTRAST, DOMINANT_COLOR, DOMINANT_SATURATION, IMAGE_FREQUENCY_BIN_COUNT_1, IMAGE_SPECTRAL_CENTROID,
			IMAGE_REGULARITY, SOUND_ZERO_CROSSING_RATE, SOUND_SIGNAL_ENERGY, SOUND_NOISE_DISTRIBUTION, SOUND_SPIKINESS, SOUND_SPECTRAL_CENTROID, SOUND_SPECTRAL_ENTROPY, SOUND_SPECTRAL_ROLL_OFF,
			MFCC1, MFCC2, MFCC5, SOUND_SPECTRAL_SUM, SOUND_SIGNAL_ENERGY_TAP, SOUND_KURTOSIS_TAP, SOUND_SPECTRAL_CENTROID_TAP, SOUND_SPECTRAL_ROLL_OFF_TAP, SOUND_SPECTRAL_SUM_TAP,
			EXTERNAL_ACCELERATION_SPECTRAL_SUM, EXTERNAL_ACCELERATION_SIGNAL_ENERGY, EXTERNAL_ACCELERATION_ZERO_CROSSING_RATE, ACCELERATION_SPECTRAL_SUM, ACCELERATION_SIGNAL_ENERGY,
			ACCELERATION_ZERO_CROSSING_RATE};

	public static final String MEAN_SAMPLE_NUMBER = "mean";
}
