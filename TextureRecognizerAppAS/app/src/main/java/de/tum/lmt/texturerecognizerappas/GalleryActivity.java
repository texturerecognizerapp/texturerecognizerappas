package de.tum.lmt.texturerecognizerappas;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ActionMode;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Kev94 on 18.03.2016.
 */
public class GalleryActivity extends AppCompatActivity {

    private static final int FULLSCREEN = 0;

    GridView mGridView;
    GalleryGridViewAdapter mGridAdapter;
    String mLoggingDirPath;
    File mBaseDir;
    private boolean mSelected = false;
    ImageItem mSelectedItem;
    private ImageItem mTouchedItem;
    private View mSelectedView;
    ActionMode mActionMode;
    AudioHelper mAudioHelper;

    ImageView mOverlay;

    AlertDialog mClassificationProgressDialog;
    ClassificationTask.iOnClassificationTaskFinishedListener mOnClassificationTaskFinishedListener;

    AlertDialog mOnlineProcessingDialog;
    ServerTask.iOnResultReceivedListener mOnOnlineResultListener;
    ServerTask.iOnDataSentListener mOnDataSentListener;

    AlertDialog mDeleteDialog;

    int mClickedPosition;
    int mSelectedPosition;
    boolean mIsSoundPlaying;

    FileHandler mFileHandler;

    private long mClassificationTimeStop;
    private long mClassificationTimeStart;

    private Animation mAnimationDelete;
    private Animation mAnimationShowPicture;

    //http://developer.android.com/guide/topics/ui/menus.html#CAB
    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.gallery_menu, menu);
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_share:
                    showSendDialog();
                    mode.finish(); // Action picked, so close the CAB
                    return true;
                case R.id.menu_classification:
                    SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

                    String classificationMethod = sharedPrefs.getString(Constants.PREF_KEY_CLASSIFICATION_METHOD, "k-nearest-neighbor");
                    Set<String> classificationFeatures = sharedPrefs.getStringSet(Constants.PREF_KEY_CLASSIfICATION_FEATURES, null);
                    String neighborhoodSize = sharedPrefs.getString(Constants.PREF_KEY_NEIGHBORHOOD, "10");

                    boolean onlineProcessing = sharedPrefs.getBoolean(Constants.PREF_KEY_ONLINE, false);

                    if(onlineProcessing) {
                        showOnlineProcessingDialog();

                        mClassificationTimeStart = System.currentTimeMillis();

                        String IP = sharedPrefs.getString(Constants.PREF_KEY_IP, "192.168.2.1");

                        JSONHelper jsonHelper = new JSONHelper();

                        String jsonSettingsString =jsonHelper.buildJSONSettingsString(classificationMethod, neighborhoodSize, classificationFeatures);

                        mFileHandler.writeDataToTextFile(jsonSettingsString, mSelectedItem.getDir().getAbsolutePath(), "jsonClassificationSettings.txt");

                        ServerTask serverTask = new ServerTask(getApplicationContext(), false, mOnOnlineResultListener, mOnDataSentListener);
                        serverTask.execute(mSelectedItem.getDir().getAbsolutePath(), IP);

                    } else {

                        //String distance = sharedPrefs.getString(Constants.PREF_KEY_DISTANCE, null);

                        String featurePath = mSelectedItem.getDir().getAbsolutePath() + Constants.DATA_TO_SEND_FOLDER_NAME + File.separator;
                        Log.i("Gallery", "featurePath: " + featurePath);

                        if (classificationFeatures == null) {
                            Log.i("Classification", "Set null");
                        }

                        JSONHelper jsonHelper = new JSONHelper();

                        File JSONFile = new File(featurePath + Constants.JSON_FEATURE_LOG_FILENAME + Constants.TEXT_FILE_EXTENSION);

                        if(JSONFile.exists()) {
                            Features features = jsonHelper.readFeaturesFromJSONFile(JSONFile, classificationFeatures);

                            /*Set<String>

                            for(String featureName:classificationFeatures) {
                                if(features.getFeature(featureName) == -1) {

                                }
                            }*/

                            if (classificationMethod == null) {
                                Log.e("Classification", "classification parameters not set");
                                Toast.makeText(getApplicationContext(), getString(R.string.parameters_missing), Toast.LENGTH_LONG).show();
                            } else {
                                ClassificationTask classifierTask = new ClassificationTask(featurePath, features, classificationMethod, Integer.parseInt(neighborhoodSize), classificationFeatures, mOnClassificationTaskFinishedListener);
                                classifierTask.execute();
                                showProgressDialog();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Features not found!", Toast.LENGTH_LONG).show();
                        }

                        jsonHelper = null;
                    }

                    mode.finish(); // Action picked, so close the CAB
                    return true;
                case R.id.menu_preview:
                    startFullScreenViewActivity(mSelectedPosition);
                    mode.finish(); // Action picked, so close the CAB
                    return true;
                case R.id.menu_sound:
                    mAudioHelper = new AudioHelper(getApplicationContext());

                    //// TODO: 21.03.2016 handle errors, but it's working so not urgent
                    mAudioHelper.play(mSelectedItem.getDir().getAbsolutePath() + File.separator + Constants.AUDIO_FILENAME_ORIG + Constants.AUDIO_FILE_EXTENSION);
                    mIsSoundPlaying = true;

                    mode.finish(); // Action picked, so close the CAB
                    return true;
                case R.id.menu_delete:
                    showDeleteDialog();
                    return true;
                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
            deselect();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        mFileHandler = new FileHandler();

        final SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        mLoggingDirPath = sharedPrefs.getString(Constants.PREF_KEY_LOGGING_DIR, null);
        if(mLoggingDirPath != null) {
            mBaseDir = (new File(mLoggingDirPath)).getParentFile();
        } else {
            mBaseDir = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + Constants.PATH_TO_STORAGE + Constants.ANALYSIS_FOLDER_NAME);
        }

        int columnWidth = (int) (getScreenWidth() / Constants.NUM_OF_COLUMNS);

        mGridView = (GridView) findViewById(R.id.gallery_view);
        mGridView.setColumnWidth(columnWidth);
        mGridAdapter = new GalleryGridViewAdapter(this, R.layout.grid_item_layout, getData());
        mGridView.setAdapter(mGridAdapter);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i("Gallery", "click");

                if (!mSelected) {
                    if (!mIsSoundPlaying) {
                        mTouchedItem = (ImageItem) parent.getItemAtPosition(position);
                        mClickedPosition = position;
                        view.startAnimation(mAnimationShowPicture);
                    } else {
                        mAudioHelper.stop();
                        mAudioHelper = null;
                        mIsSoundPlaying = false;
                        mGridView.setSelector(R.drawable.selector_blue);
                    }
                } else {
                    deselect();
                }
            }
        });

        mAnimationShowPicture = AnimationUtils.loadAnimation(this, R.anim.click_on_item);
        mAnimationShowPicture.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.i("Gallery", "Clicked on position: " + mClickedPosition);
                startFullScreenViewActivity(mClickedPosition);
            }
        });

        mGridView.setLongClickable(true);
        //// TODO: 20.03.2016 do not highlight view when only clicked once
        mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i("Gallery", "long click");

                if (!mIsSoundPlaying) {

                    if(mSelected == true) {
                        deselect();
                    }

                    mSelected = true;
                    mActionMode = startActionMode(mActionModeCallback);
                    view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.blue));

                    mOverlay = (ImageView) view.findViewById(R.id.selector_overlay);
                    mOverlay.setVisibility(View.VISIBLE);
                    mOverlay.setLayoutParams(new RelativeLayout.LayoutParams(view.getWidth(), view.getHeight()));
                    mOverlay.bringToFront();

                    mSelectedPosition = position;
                    ((ImageItem) parent.getItemAtPosition(position)).setSelected(true);
                    mSelectedItem = (ImageItem) parent.getItemAtPosition(position);
                    if(mSelectedItem == null) {
                        Log.e("Gallery", "selected item null");
                    }
                    mSelectedView = view;
                    if (mSelectedItem == null) {
                        Toast.makeText(getApplicationContext(), "Can't select item. Show fullscreen image instead.", Toast.LENGTH_LONG).show();
                        return false;
                    } else {
                        //no further processing, onClick is not fired when returning true
                        return true;
                    }
                } else {
                    return false;
                }
            }
        });

        mAnimationDelete = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        mAnimationDelete.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                deleteItem(mSelectedItem);
                deselect();
            }
        });

        mOnClassificationTaskFinishedListener = new ClassificationTask.iOnClassificationTaskFinishedListener() {
            @Override
            public void onClassifierTaskFinished(List<Texture.Sample> result) {

                Log.i("Gallery", "ClassificationTask finished");

                if(mClassificationProgressDialog != null) {
                    mClassificationProgressDialog.dismiss();
                }

                String classificationMethod = sharedPrefs.getString(Constants.PREF_KEY_CLASSIFICATION_METHOD, null);

                if(result == null) {
                    Log.e("Classification", "Classfication not successful");
                    Toast.makeText(getApplicationContext(), getString(R.string.classification_null), Toast.LENGTH_LONG).show();
                } else {
                    showClassificationListDialog(result, classificationMethod);
                }
            }
        };

        mOnDataSentListener = new ServerTask.iOnDataSentListener() {
            @Override
            public void onDataSent() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "data sent", Toast.LENGTH_LONG).show();
                    }
                });
            }
        };

        mOnOnlineResultListener = new ServerTask.iOnResultReceivedListener() {
            @Override
            public void onResultReceived(String jsonResultString) {

                String classificationMethod = sharedPrefs.getString(Constants.PREF_KEY_CLASSIFICATION_METHOD, "k-nearest-neighbor");
                String neighborhoodSize = sharedPrefs.getString(Constants.PREF_KEY_NEIGHBORHOOD, "10");
                String distToMean = Constants.CLASSIFICATION_DIST_TO_MEAN;
                String Mahal = Constants.CLASSIFICATION_MAHALANOBIS;
                String kNearestNeighbor = Constants.CLASSIFICATION_KNN;

                List<Texture.Sample> result = new ArrayList<>();

                JSONObject jsonResult = null;
                try {
                    jsonResult = new JSONObject(jsonResultString);

                    if (classificationMethod.equals(distToMean)) {

                        if (jsonResult != null) {

                            for (int i = 1; i <= Constants.NUMBER_OF_TEXTURES; i++) {

                                JSONObject jsonSample;

                                try {
                                    jsonSample = jsonResult.getJSONObject(Integer.toString(i));
                                } catch(JSONException e) {
                                    break;
                                }

                                String name = jsonSample.getString("name");
                                double distance = jsonSample.getDouble("value");

                                Texture texture = new Texture(name);
                                texture.addSampleWithDistance(distance);

                                result.add(texture.getSamples().get(0));

                            }
                        }
                    } else if (classificationMethod.equals(Mahal)) {

                        if (jsonResult != null) {

                            for (int i = 1; i <= Constants.NUMBER_OF_TEXTURES; i++) {

                                JSONObject jsonSample;

                                try {
                                    jsonSample = jsonResult.getJSONObject(Integer.toString(i));
                                } catch(JSONException e) {
                                    break;
                                }

                                String name = jsonSample.getString("name");
                                double distance = jsonSample.getDouble("value");

                                Texture texture = new Texture(name);
                                texture.addSampleWithDistance(distance);

                                result.add(texture.getSamples().get(0));

                            }
                        }
                    } else if (classificationMethod.equals(kNearestNeighbor)) {

                        for (int i = 1; i <= Integer.getInteger(neighborhoodSize, 10); i++) {

                            JSONObject jsonSample;

                            try {
                                jsonSample = jsonResult.getJSONObject(Integer.toString(i));
                            } catch(JSONException e) {
                                break;
                            }

                            String name = jsonSample.getString("name");
                            int occurence = jsonSample.getInt("value");

                            Texture texture = new Texture(name);
                            texture.addSampleWithOccurence(occurence);

                            result.add(texture.getSamples().get(0));

                            /*for(Texture.Sample sample:result) {

                                if(name.equals(sample.getName())) {
                                    sample.setOccurence(sample.getOccurence()+1);
                                    break;
                                } else {
                                    Texture texture = new Texture(name);
                                    texture.addSampleWithOccurence(1);

                                    result.add(texture.getSamples().get(0));
                                }
                            }*/

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mOnlineProcessingDialog.dismiss();

                mClassificationTimeStop = System.currentTimeMillis();

                Toast.makeText(getApplicationContext(), "Classification took " + (mClassificationTimeStop - mClassificationTimeStart) + " ms.", Toast.LENGTH_LONG).show();

                if(jsonResult == null) {
                    Log.e("Classification", "Classfication not successful");
                    Toast.makeText(getApplicationContext(), getString(R.string.classification_null), Toast.LENGTH_LONG).show();
                } else {
                    showClassificationListDialog(result, classificationMethod);
                }
            }
        };

    }

    /*@Override
    public void onBackPressed()
    {
        Log.i("Gallery", "onBackPressed");
        if(mSelected) {
            deselect();
        } else {
            finish();
        }
    }*/

    @Override
    protected void onResume() {
        super.onResume();


    }

    private void deselect() {
        Log.i("Gallery", "deselect()");
        if(mSelectedItem != null) {
            mSelectedItem.setSelected(false);
        }
        if(mSelectedView != null) {
            mSelectedView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
            mSelectedView = null;
        }
        if(mActionMode != null) {
            mActionMode.finish();
        }
        if(mOverlay != null) {
            mOverlay.setVisibility(View.GONE);
        }

        mSelected = false;
    }

    private ArrayList<ImageItem> getData() {

        ArrayList<ImageItem> imageItems = new ArrayList<>();
        int numberOfImageItems = 0;

        if(mBaseDir != null) {

            Log.i("Gallery", mBaseDir.getName());
            File[] loggingDirList = mBaseDir.listFiles();

            for(File file:loggingDirList) {
                if(file.isDirectory()) {
                    String picturePath = file.getAbsolutePath() + File.separator + Constants.CAMERA_NO_FLASH_FILENAME + Constants.JPG_FILE_EXTENSION;
                    File picture = new File(picturePath);

                    if (!picture.exists()) {
                        Log.i("Gallery", "Picture " + picturePath + " does not exist");
                    } else {
                        imageItems.add(new ImageItem(file.getName(), file));
                        numberOfImageItems++;
                    }
                }
            }

            String name1, name2;
            String[] parts1, parts2;
            int number1;
            int number2;

            for(int i = 1; i < numberOfImageItems; i++) {

                for(int j = i-1; j >= 0; j--) {

                    Log.i("Gallery", "i: " + i + ", j: " + j);

                    name1 = imageItems.get(i).getTitle();
                    parts1 = name1.split("_");
                    number1 = Integer.parseInt(parts1[0]);

                    name2 = imageItems.get(j).getTitle();
                    parts2 = name2.split("_");
                    number2 = Integer.parseInt(parts2[0]);

                    if(number2 > number1) {
                        Log.i("Gallery", number2 + " " + j + " > " + i + " " + number1);
                        if(j == 0) {
                            Log.i("Gallery", number2 + " < " + number1 + " -> add item " + i + " at position " + j + " and remove item at position " + i);
                            Log.i("Gallery", "Now the item at position " + j + " (" + imageItems.get(j).getTitle() + ") ...");
                            imageItems.add(j, imageItems.get(i));
                            Log.i("Gallery", " ... is at position " + (j + 1) + " (" + imageItems.get(j+1).getTitle() + ") ...");
                            imageItems.remove(i+1);
                            break;
                        } else {
                            continue;
                        }
                    } else if(number2 < number1) {
                        if(i-j > 1) {
                            Log.i("Gallery", number2 + " < " + number1 + " -> add item " + i + " at position " + (j+1) + " and remove item at position " + i);
                            Log.i("Gallery", "Now the item at position " + (j+1) + " (" + imageItems.get(j+1).getTitle() + ") ...");
                            imageItems.add(j+1, imageItems.get(i));
                            Log.i("Gallery", " ... is at position " + (j + 2) + " (" + imageItems.get(j + 2).getTitle() + ") ...");
                            imageItems.remove(i + 1);
                        }
                        break;
                    } else {

                        Log.i("Gallery", number2 + " = " + number1);

                        number1 = Integer.parseInt(parts1[1]);
                        number2 = Integer.parseInt(parts2[1]);

                        Log.i("Gallery", "number2: " + number2 + ", number1: " + number1);

                        if(number2 > number1) {
                            Log.i("Gallery", number2 + " > " + number1);
                            Log.i("Gallery", number2 + " " + j + " > " + i + " " + number1);
                            if(j == 0) {
                                Log.i("Gallery", number2 + " < " + number1 + " -> add item " + i + " at position " + j + " and remove item at position " + i);
                                Log.i("Gallery", "Now the item at position " + j + " (" + imageItems.get(j).getTitle() + ") ...");
                                imageItems.add(j, imageItems.get(i));
                                Log.i("Gallery", " ... is at position " + (j + 1) + " (" + imageItems.get(j+1).getTitle() + ") ...");
                                imageItems.remove(i+1);
                                break;
                            } else {
                                continue;
                            }
                        } else if(number2 < number1) {
                            if(i-j > 1) {
                                Log.i("Gallery", number2 + " < " + number1 + " -> add item " + i + " at position " + (j+1) + " and remove item at position " + i);
                                Log.i("Gallery", "Now the item at position " + (j+1) + " (" + imageItems.get(j+1).getTitle() + ") ...");
                                imageItems.add(j+1, imageItems.get(i));
                                Log.i("Gallery", " ... is at position " + (j + 2) + " (" + imageItems.get(j + 2).getTitle() + ") ...");
                                imageItems.remove(i + 1);
                            }
                            break;
                        }

                    }

                }

            }

            for(int i = 0; i < numberOfImageItems; i++) {
                Log.i("Gallery", "item no " + i + ": " + imageItems.get(i).getTitle());
            }

            return imageItems;

        } else {
            Toast.makeText(this, "path error", Toast.LENGTH_LONG).show();
            Log.i("Gallery", "Path: " + mLoggingDirPath);
            return null;
        }
    }

    private void deleteItem(ImageItem selectedItem) {

        if(mSelectedItem != null) {
            Log.i("Gallery", "delete... " + selectedItem.getTitle());
            mGridAdapter.getData();
            //only works if two items never have the same title, which is the case here, since the time with date, hours, minutes, and seconds is used as a file name
            mGridAdapter.removeByTitle(selectedItem);
            mGridAdapter.getData();
            mFileHandler.deleteFolder(selectedItem.getDir());
            mGridAdapter.removeBitmap(selectedItem.getTitle());
            //mGridView.clearChoices();
            mGridAdapter.notifyDataSetChanged();
        }

        /*if(mSelectedItem != null) {
            Log.i("Gallery", "delete...");
            mGridAdapter.getData();
            mGridAdapter.removeBitmap(selectedItem.getTitle());
            mFileHandler.deleteFolder(selectedItem.getDir());
            mGridAdapter.setData(getData());
            mGridAdapter.notifyDataSetChanged();
            mGridView.clearChoices();
        }*/
    }

    public int getScreenWidth() {
        int columnWidth;
        WindowManager wm = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        final Point point = new Point();
        try {
            display.getSize(point);
        } catch (java.lang.NoSuchMethodError ignore) { // Older device
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        columnWidth = point.x;
        return columnWidth;
    }

    protected void startFullScreenViewActivity(int currentPosition) {
        Intent fullscreenIntent = new Intent(GalleryActivity.this, FullScreenViewActivity.class);
        fullscreenIntent.putParcelableArrayListExtra("ImageItems", getData());
        fullscreenIntent.putExtra("currentPosition", currentPosition);
        startActivityForResult(fullscreenIntent, FULLSCREEN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == FULLSCREEN) {

            ArrayList<String> deletedItems = data.getStringArrayListExtra("deletedItems");

            if(!deletedItems.isEmpty()) {
                for (String item : deletedItems) {
                    Log.i("Gallery", "remove... " + item);
                    mGridAdapter.getData();
                    //only works if two items never have the same title, which is the case here, since the time with date, hours, minutes, and seconds is used as a file name
                    mGridAdapter.removeByTitle(item);
                    mGridAdapter.getData();
                    mGridAdapter.removeBitmap(item);
                }

                mGridAdapter.notifyDataSetChanged();
            }

        }
    }

    private void showProgressDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        RelativeLayout rl = new RelativeLayout(this);

        ProgressBar progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);
        RelativeLayout.LayoutParams progressBarParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView text = new TextView(this);
        text.setText(R.string.classification_progress);
        RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        textParams.addRule(RelativeLayout.CENTER_VERTICAL);

        rl.addView(progressBar, progressBarParams);
        rl.addView(text, textParams);

        builder.setView(rl);

        mClassificationProgressDialog = builder.create();
        mClassificationProgressDialog = builder.show();

        mClassificationProgressDialog.setCanceledOnTouchOutside(false);
    }

    private void showSendDialog() {
        DialogSendFragment dialogSend = DialogSendFragment.newInstance(mSelectedItem.getDir().getAbsolutePath());
        dialogSend.show(getFragmentManager(), "dialogSend");
    }

    private void showClassificationListDialog(List<Texture.Sample> textures, String classificationMethod) {
        DialogClassificationListFragment dialogClassificationList = new DialogClassificationListFragment(textures, classificationMethod);
        dialogClassificationList.show(getFragmentManager(), "dialogClassificationList");
    }

    protected void showOnlineProcessingDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        RelativeLayout rl = new RelativeLayout(this);

        ProgressBar progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);
        RelativeLayout.LayoutParams progressBarParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TextView text = new TextView(this);
        text.setText(R.string.processing);
        RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        textParams.addRule(RelativeLayout.CENTER_VERTICAL);

        rl.addView(progressBar, progressBarParams);
        rl.addView(text, textParams);

        builder.setView(rl);

        mOnlineProcessingDialog = builder.create();
        mOnlineProcessingDialog = builder.show();

        mOnlineProcessingDialog.setCanceledOnTouchOutside(false);
    }

    protected void showDeleteDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = getLayoutInflater();
        View content = inflater.inflate(R.layout.dialog_delete, null);

        final Button buttonCancel = (Button) content.findViewById(R.id.button_cancel_delete_dialog);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation buttonClickAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.click_on_item);
                buttonClickAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mDeleteDialog.dismiss();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                buttonCancel.startAnimation(buttonClickAnimation);
            }
        });

        final Button buttonDelete = (Button) content.findViewById(R.id.button_delete_delete_dialog);
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation buttonClickAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.click_on_item);
                buttonClickAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mSelectedView.startAnimation(mAnimationDelete);
                        mActionMode.finish();
                        mDeleteDialog.dismiss();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                buttonDelete.startAnimation(buttonClickAnimation);
            }
        });

        builder.setView(content);

        mDeleteDialog = builder.create();
        mDeleteDialog = builder.show();

        mDeleteDialog.setCanceledOnTouchOutside(false);

    }
}
