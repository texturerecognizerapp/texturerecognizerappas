package de.tum.lmt.texturerecognizerappas;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import de.tum.lmt.texturerecognizerappas.math.Matrix;


/***********************
 * remove subtraction of mean after implementation of some preprocessing in FeatureComputer
 */

public class MathUtil {

	public double roundDigits(double toRound, int precision) {
		
		double factor = Math.pow(10, precision);
		double value = toRound * factor;
		value = Math.round(value);
		return value /= factor;
	}

	public float computeMean(List<Float> data) {

		int size = data.size();
		float sum = 0;

		for(int i = 0; i < size; i++) {
			sum += data.get(i);
		}

		return sum/size;
	}
	
	public double bandPowerDouble(ListIterator<Double> begin, ListIterator<Double> end) {
		
		double bandPower = 0;
		int beginIndex = begin.nextIndex();
		
		for(ListIterator<Double> lit = begin; (lit.hasNext() && lit != end); ) {
			bandPower += Math.pow(lit.next(), 2.0);
		}
		
		bandPower = bandPower / ((end.previousIndex() + 1) - beginIndex);
		
		return bandPower;
	}
	
	public double bandPowerFloat(ListIterator<Float> begin, ListIterator<Float> end) {
		
		double bandPower = 0;
		int beginIndex = begin.nextIndex();
		
		for(ListIterator<Float> lit = begin; (lit.hasNext() && lit != end); ) {
			bandPower = bandPower + Math.pow(lit.next(), 2.0);
		}
		
		bandPower = bandPower / ((end.previousIndex() + 1) - beginIndex);
		
		return bandPower;
	}
	
	public double varianceDouble(ListIterator<Double> begin, ListIterator<Double> end) {
		
		double mean = 0;
		int beginIndex = begin.nextIndex();
		
		for(ListIterator<Double> lit = begin; (lit.hasNext() && lit != end); ) {
			mean = mean + lit.next();
		}
		
		mean = mean / ((end.previousIndex() + 1) - beginIndex);
		
		while(begin.hasPrevious()) {
			begin.previous();
		}
		
		double variance = 0;
		
		for(ListIterator<Double> lit = begin; (lit.hasNext() && lit != end); ) {
			variance = variance + Math.pow(lit.next() - mean, 2.0);
		}
		
		variance = variance / ((end.previousIndex() + 1) - beginIndex); // NDK version (end - begin + 1) ??
		
		return variance;
	}
	
	public double varianceFloat(ListIterator<Float> begin, ListIterator<Float> end) {
				
		float mean = 0;
		int beginIndex = begin.nextIndex();
		
		for(ListIterator<Float> lit = begin; (lit.hasNext() && lit != end); ) {
			mean = mean + lit.next();
		}
		
		mean = mean / ((end.previousIndex() + 1) - beginIndex);
		
		while(begin.hasPrevious()) {
			begin.previous();
		}
		
		double variance = 0;
		
		for(ListIterator<Float> lit = begin; (lit.hasNext() && lit != end); ) {
			variance = variance + Math.pow(lit.next() - mean, 2.0);
		}
		
		variance = variance / ((end.previousIndex() + 1) - beginIndex); // NDK version (end - begin + 1) ??
		
		return variance;
	}

	public double kurtosisFloat(ListIterator<Float> begin, ListIterator<Float> end) {

		float mean = 0;
		int beginIndex = begin.nextIndex();
		int n = (end.previousIndex() + 1) - beginIndex;

		for(ListIterator<Float> lit = begin; (lit.hasNext() && lit != end); ) {
			mean = mean + lit.next();
		}

		mean = mean / ((end.previousIndex() + 1) - beginIndex);

		while(begin.hasPrevious()) {
			begin.previous();
		}

		double numerator = 0;
		double denominator = 0;

		for(ListIterator<Float> lit = begin; (lit.hasNext() && lit != end); ) {

			float currentValue = lit.next();

			numerator = numerator + Math.pow((currentValue - mean),4);
			denominator = denominator + Math.pow((currentValue - mean),2);
		}

		numerator = numerator / n;
		denominator = Math.pow((denominator / n), 2);

		return (numerator / denominator);
	}

	public double kurtosisDouble(ListIterator<Double> begin, ListIterator<Double> end) {

		double mean = 0;
		int beginIndex = begin.nextIndex();
		int n = (end.previousIndex() + 1) - beginIndex;

		for(ListIterator<Double> lit = begin; (lit.hasNext() && lit != end); ) {
			mean = mean + lit.next();
		}

		mean = mean / ((end.previousIndex() + 1) - beginIndex);

		while(begin.hasPrevious()) {
			begin.previous();
		}

		double numerator = 0;
		double denominator = 0;

		for(ListIterator<Double> lit = begin; (lit.hasNext() && lit != end); ) {

			double currentValue = lit.next();

			numerator = numerator + Math.pow((currentValue - mean),4);
			denominator = denominator + Math.pow((currentValue - mean),2);
		}

		numerator = numerator / n;
		denominator = Math.pow((denominator / n),2);

		return (numerator / denominator);
	}

	public double noiseDistributionFloat(ListIterator<Float> begin, ListIterator<Float> end) {

		double maxAbs = getAbsoluteMaximumFloat(begin, end);

		Log.i("Features", "Max Sound: " + maxAbs);

		double distThresh = 0.1 * maxAbs;

		List<Integer> binarySignal = new ArrayList<>();

		int numberOfOnes = 0;
		int numberOfZeros = 0;

		while(begin.hasPrevious()) {
			begin.previous();
		}

		for(ListIterator<Float> lit = begin; (lit.hasNext() && lit != end); ) {

			double nextValue = lit.next();

			if(nextValue > distThresh) {
				binarySignal.add(1);
				numberOfOnes++;
			} else {
				binarySignal.add(0);
				numberOfZeros++;
			}
		}

		if(numberOfZeros != 0) {
			return ((double)numberOfOnes / (double)numberOfZeros);
		} else {
			Log.i("Features", "Error computing noise distribution! Division by Zero!");
			return 0;
		}
	}

	public double noiseDistributionDouble(ListIterator<Double> begin, ListIterator<Double> end) {

		double maxAbs = getAbsoluteMaximumDouble(begin, end);

		Log.i("Features", "Max Sound: " + maxAbs);

		double distThresh = 0.1 * maxAbs;

		List<Integer> binarySignal = new ArrayList<>();

		int numberOfOnes = 0;
		int numberOfZeros = 0;

		while(begin.hasPrevious()) {
			begin.previous();
		}

		for(ListIterator<Double> lit = begin; (lit.hasNext() && lit != end); ) {

			double nextValue = lit.next();

			if(nextValue > distThresh) {
				binarySignal.add(1);
				numberOfOnes++;
			} else {
				binarySignal.add(0);
				numberOfZeros++;
			}
		}

		if(numberOfZeros != 0) {
			return (double) numberOfOnes / (double) numberOfZeros;
		} else {
			Log.i("Features", "Error computing noise distribution! Division by Zero!");
			return 0;
		}
	}
	
	public double getAbsoluteMaximumFloat(ListIterator<Float> begin, ListIterator<Float> end) {
		
		double maxAbs = 0;
		for(ListIterator<Float> lit = begin; (lit.hasNext() && lit != end); ) {
			
			double currentValue = Math.abs(lit.next());

			if(currentValue > maxAbs) {
				maxAbs = currentValue;
			}
		}
		
		return maxAbs;
	}
	
	public double getAbsoluteMaximumDouble(ListIterator<Double> begin, ListIterator<Double> end) {
		
		double maxAbs = 0;
		for(ListIterator<Double> lit = begin; (lit.hasNext() && lit != end); ) {
			
			double currentValue = Math.abs(lit.next());
			
			if(currentValue > maxAbs) {
				maxAbs = currentValue;
			}
		}
		
		return maxAbs;
	}

	public double zeroCrossingRateDouble(ListIterator<Double> begin, ListIterator<Double> end) {

		boolean firstCall = true;
		int numberOfZeroCrossings = 0;
		int beginIndex = begin.nextIndex();
		double currentValue = 0;
		double nextValue = 0;

		for(ListIterator<Double> lit = begin; (lit.hasNext() && lit != end); ) {

			if(firstCall) {
				currentValue = lit.next();
				nextValue = lit.next();
				firstCall = false;
			} else {
				currentValue = nextValue;
				nextValue = lit.next();
			}

			if((currentValue * nextValue) < 0) {
				numberOfZeroCrossings++;
			}
		}

		double zeroCrossingRate = (double)numberOfZeroCrossings / (double)((end.previousIndex() + 1) - beginIndex);

		return zeroCrossingRate;
	}

	public double zeroCrossingRateFloat(ListIterator<Float> begin, ListIterator<Float> end) {

		boolean firstCall = true;
		int numberOfZeroCrossings = 0;
		int beginIndex = begin.nextIndex();
		double currentValue = 0;
		double nextValue = 0;

		for(ListIterator<Float> lit = begin; (lit.hasNext() && lit != end); ) {

			if(firstCall) {
				currentValue = lit.next();
				nextValue = lit.next();
				firstCall = false;
			} else {
				currentValue = nextValue;
				nextValue = lit.next();
			}

			if((currentValue * nextValue) < 0) {
				numberOfZeroCrossings++;
			}
		}

		Log.i("MathUtil", "nozc: " + numberOfZeroCrossings);
		Log.i("MathUtil", "diff: " + ((end.previousIndex() + 1) - beginIndex));

		double zeroCrossingRate = (double)numberOfZeroCrossings / (double)((end.previousIndex() + 1) - beginIndex);

		Log.i("MathUtil", "zcr: " + zeroCrossingRate);

		return zeroCrossingRate;
	}

	public double[] computeHammingWindow(int windowLength) {

		double[] hamming = new double[windowLength];

		Log.i("MFCCs", "computeHammingFilterStarted ");

		for (int i=0;i<windowLength;i++){
			hamming[i] = 0.54-0.46*Math.cos(2*Math.PI*i/(windowLength-1));
		}

		return hamming;
	}

	public double getVectorDistanceEuclidean(double[] featureVector_1, double[] featureVector_2) {

		if(featureVector_1.length != featureVector_2.length)  {
			return Double.NaN;
		} else {
			double distance = 0;

			for (int i = 0; i < featureVector_1.length; i++) {
				distance += Math.pow(featureVector_1[i] - featureVector_2[i],2);
			}

			distance = Math.sqrt(distance);

			return distance;
		}
	}

	public double[][] computeVectorProductToMatrix(double[] featureVector_1, double[] featureVector_2) {

		if(featureVector_1.length != featureVector_2.length) {
		 return null;
		} else {
			double[][] mat = new double[featureVector_1.length][featureVector_1.length];

			for (int i = 0; i < featureVector_1.length; i++) {
				for (int j = 0; j < featureVector_1.length; j++) {
					mat[i][j] = featureVector_1[i] * featureVector_2[j];
				}
			}

			return mat;
		}
	}

	public double[] subtractVectors(double[] featureVector_1, double[] featureVector_2) {

		if(featureVector_1.length != featureVector_2.length) {
			return null;
		} else {
			for (int i = 0; i < featureVector_1.length; i++) {
				featureVector_1[i] = featureVector_1[i] - featureVector_2[i];
			}

			return featureVector_1;
		}
	}

	public double[][] subtractMatrix(double[][] mat_1, double[][] mat_2) {

		//// TODO: 28.03.2016 do not allocate memory for result
		double[][] resultMat = new double[mat_1.length][mat_1[0].length];

		for(int i = 0; i < mat_1.length; i++) {
			for(int j = 0; j < mat_1[0].length; j++) {
				resultMat[i][j] = mat_1[i][j] - mat_2[i][j];
			}
		}

		return resultMat;
	}

	public double[][] addMatrix(double[][] mat_1, double[][] mat_2) {

		//// TODO: 28.03.2016 do not allocate memory for result
		double[][] resultMat = new double[mat_1.length][mat_1[0].length];

		for(int i = 0; i < mat_1.length; i++) {
			for(int j = 0; j < mat_1[0].length; j++) {
				resultMat[i][j] = mat_1[i][j] + mat_2[i][j];
			}
		}

		return resultMat;
	}

	//// TODO: 28.03.2016 check if it is calles divisor or divident
	public double[][] divideMatrixByDouble(double[][] mat, double divisor) {

		for(int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat[0].length; j++) {
				mat[i][j] = mat[i][j] / divisor;
			}
		}

		return mat;
	}

	public double[] multiplyTransposedVectorByMatrix(double[] transposedVector, double[][] matrix) {

		double[] resultVector = new double[matrix.length];

		if(transposedVector.length != matrix[0].length) {
			Log.i("calculator", "dimensions do not match (trans by mat)");
		} else {
			for (int i = 0; i < matrix.length; i++) {
				for (int j = 0; j < matrix[0].length; j++) {
					resultVector[i] = resultVector[i] + (transposedVector[j] * matrix[i][j]);
				}
			}
		}

		return resultVector;
	}

	public double dotProduct(double[] vec_1, double[] vec_2) {

		double result = 0;

		if(vec_1.length != vec_2.length) {
			Log.i("calculator", "dimensions do not match (dot product)");
		} else {
			for(int i = 0; i < vec_1.length; i++) {
				result = result + (vec_1[i] * vec_2[i]);
			}
		}

		return result;
	}
}
