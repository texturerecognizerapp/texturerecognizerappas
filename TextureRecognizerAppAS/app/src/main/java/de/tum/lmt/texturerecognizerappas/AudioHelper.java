package de.tum.lmt.texturerecognizerappas;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;

import java.io.IOException;

/**
 * Created by Kev94 on 21.03.2016.
 */
public class AudioHelper {

    private Context mContext;
    private MediaPlayer mMP;

    private boolean mStopped;

    private iOnSoundFinishedListener mOnSoundFinishedListener;

    public AudioHelper(Context context) {

        mContext = context;
    }

    public AudioHelper(Context context, iOnSoundFinishedListener listener) {

        mContext = context;
        mOnSoundFinishedListener = listener;
    }

    public interface iOnSoundFinishedListener {
        public void onSoundFinished();
    }

    public void play(String path) {
        Uri uri = Uri.parse(path);
        Log.i("AudioHelper", "path: " + path);

        if(mMP == null) {
            mMP = new MediaPlayer();
        }

        mMP.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mOnSoundFinishedListener.onSoundFinished();
            }
        });

        mMP.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {

                if(mMP == mp) {
                    if (!mStopped) {
                        mMP.start();
                    } else {
                        mMP.release();
                        mMP = null;
                    }
                }
            }
        });

        mMP.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mMP.setDataSource(mContext, uri);
        } catch (IOException e) {
            e.printStackTrace();
        }

        mMP.prepareAsync();
    }

    public void stop() {
        mStopped = true;
        if(mMP.isPlaying()) {
            mMP.stop();
            mMP.release();
            mMP = null;
        }
    }
}
