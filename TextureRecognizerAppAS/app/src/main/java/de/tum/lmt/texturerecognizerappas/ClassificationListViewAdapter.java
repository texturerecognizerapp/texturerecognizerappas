package de.tum.lmt.texturerecognizerappas;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Kev94 on 20.09.2016.
 */
public class ClassificationListViewAdapter extends ArrayAdapter {

    private Context mContext;
    private int mLayoutResourceId;
    private ArrayList<ResultItem> mData = new ArrayList();

    public ClassificationListViewAdapter(Context context, int layoutResourceId, ArrayList<ResultItem> data) {
        super(context, layoutResourceId, data);
        mContext = context;
        mLayoutResourceId = layoutResourceId;
        mData = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;

        if (row == null) {
            //Log.i("Adapter", "row null");
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(mLayoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.name = (TextView) row.findViewById(R.id.texture_name);
            holder.quantifier = (TextView) row.findViewById(R.id.texture_quantifier);
            holder.image = (ImageView) row.findViewById(R.id.image_preview);
            row.setTag(holder);
        } else {
            //Log.i("Adapter", "row not null");
            holder = (ViewHolder) row.getTag();
        }

        ResultItem item = mData.get(position);
        if(item == null) {
            Log.i("Adapter", "item null");
        } else {
            if(holder == null) {
                Log.i("Adapter", "holder null");
            } else {
                holder.name.setText(item.getName());
                holder.quantifier.setText(Double.toString(item.getQuantifier()));

                Resources resources = mContext.getResources();
                final int resourceId = resources.getIdentifier(item.getName().toLowerCase(), "drawable", mContext.getPackageName());

                holder.image.setImageDrawable(mContext.getResources().getDrawable(resourceId));
            }
        }
        return row;
    }

    static class ViewHolder {
        TextView name;
        TextView quantifier;
        ImageView image;
    }
}
