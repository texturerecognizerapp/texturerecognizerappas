package de.tum.lmt.texturerecognizerappas;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class DialogTextureChoiceFragment extends DialogFragment {

    private File mLoggingDir = null;

    /*public DialogTextureChoiceFragment(File loggingDir) {
        mLoggingDir = loggingDir;
    }*/

    public static DialogTextureChoiceFragment newInstance(String loggingDirPath) {

        Bundle args = new Bundle();
        args.putString("loggingDirPath", loggingDirPath);

        DialogTextureChoiceFragment fragment = new DialogTextureChoiceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public interface iOnTextureChosen {
        public void onTextureChosen(File dir, String name, int number);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        mLoggingDir = new File(getArguments().getString("loggingDirPath"));

        File[] textureList;

        textureList = mLoggingDir.listFiles();

        // -1 for json database
        final String[] textureListString = new String[textureList.length-1];
        String[] textureListItems = new String[textureList.length-1];

        int i = 0;

        for(File file : textureList) {
            if(file.isDirectory()) {
                Log.i("file", "filename: " + file.getName());
                textureListItems[i] = file.getName() + " (" + Integer.toString(file.listFiles().length)+ ")";
                textureListString[i] = file.getName();
                i++;
            }
        }

        builder.setTitle(getString(R.string.title_texture_choice))
                .setItems(textureListItems, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        File textureDir = new File(mLoggingDir.getAbsolutePath() + File.separator + textureListString[which]);
                        int numberOfFiles;

                        if(!textureDir.exists()) {
                            numberOfFiles = 0;
                        } else {
                            numberOfFiles = textureDir.listFiles().length;
                        }

                        //no +1 for numberOfFiles because of data_descr.txt file that is created together with the first recording
                        textureDir = new File(textureDir + File.separator + Integer.toString((numberOfFiles)));
                        textureDir.mkdirs();

                        iOnTextureChosen activity = (iOnTextureChosen) getActivity();
                        activity.onTextureChosen(textureDir, textureListString[which], numberOfFiles);

                        dismiss();
                    }
                });

        return builder.create();
    }

}
