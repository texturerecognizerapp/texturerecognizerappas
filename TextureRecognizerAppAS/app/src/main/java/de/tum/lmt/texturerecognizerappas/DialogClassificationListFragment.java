package de.tum.lmt.texturerecognizerappas;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kev94 on 26.03.2016.
 */
public class DialogClassificationListFragment extends DialogFragment {

    private List<Texture.Sample> mClassificationResult;
    private String mClassificationMethod;

    private ListView mListView;
    private ClassificationListViewAdapter mListAdapter;

    public DialogClassificationListFragment(List<Texture.Sample> classificationResult, String classificationMethod)  {
        mClassificationResult = classificationResult;
        mClassificationMethod = classificationMethod;
    }

    /*static DialogClassificationListFragment newInstance(Features features) {
        DialogClassificationListFragment f = new DialogClassificationListFragment();

        Bundle args = new Bundle();
        args.putParcelable("features", features);
        f.setArguments(args);

        return f;
    }*/

    /*@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        String[] items = new String[mClassificationResult.size()];
        int i;

        switch(mClassificationMethod) {
            case Constants.CLASSIFICATION_DIST_TO_MEAN:

                i = 0;

                for(Texture.Sample result:mClassificationResult) {
                    items[i] = result.getName() + " , Distance: " + result.getDistance();
                    i++;
                }
                break;
            case Constants.CLASSIFICATION_MAHALANOBIS:

                i = 0;

                for(Texture.Sample result:mClassificationResult) {
                    items[i] = result.getName() + " , Distance: " + result.getDistance();
                    i++;
                }
                break;
            case Constants.CLASSIFICATION_KNN:

                i = 0;

                for(Texture.Sample result:mClassificationResult) {
                    items[i] = result.getName() + ", Occurence: " + result.getOccurence();
                    i++;
                }
                break;
        }

        builder.setTitle(getString(R.string.classification_dialog_title))
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        return builder.create();
    }*/

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View content = inflater.inflate(R.layout.dialog_classification_list, null);

        builder.setTitle(getString(R.string.classification_dialog_title))
                .setView(content);

        mListView = (ListView) content.findViewById(R.id.classification_list_view);
        mListAdapter = new ClassificationListViewAdapter(getActivity(), R.layout.classification_list_item, getData());
        mListView.setAdapter(mListAdapter);

        return builder.create();
    }

    private ArrayList<ResultItem> getData() {

        ArrayList<ResultItem> data = new ArrayList<>();

        for(Texture.Sample sample:mClassificationResult) {

            ResultItem item = null;

            if((mClassificationMethod.equals(Constants.CLASSIFICATION_DIST_TO_MEAN)) || (mClassificationMethod.equals(Constants.CLASSIFICATION_MAHALANOBIS))) {
                item = new ResultItem(null, sample.getName(), sample.getDistance(), null);
            } else if (mClassificationMethod.equals(Constants.CLASSIFICATION_KNN)) {
                item = new ResultItem(null, sample.getName(), sample.getOccurence(), null);
            }

            if(item != null) {
                data.add(item);
            }

        }

        return data;

    }

}
