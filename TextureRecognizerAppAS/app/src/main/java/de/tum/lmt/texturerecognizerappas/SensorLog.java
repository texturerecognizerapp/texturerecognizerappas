package de.tum.lmt.texturerecognizerappas;

import java.util.ArrayList;
import java.util.List;

public class SensorLog {
	
	private int mType = Constants.INVALID_SENSOR_LOG_TYPE;
	private int mStep;
	private List<Long> mTimestamps;
	private List<float[]> mValues;
	
	public SensorLog(int type, int step) {
		mType = type;
		mStep = step;
		
		mTimestamps = new ArrayList<Long>();
		mValues = new ArrayList<float[]>();
	}
	
	public int getType() {
		return mType;
	}

	public int getStep() {
		return mStep;
	}
	
	public void addTimestamp(long timestamp) {
		mTimestamps.add(timestamp);
	}
	
	public List<Long> getTimestamps() {
		return mTimestamps;
	}
	
	public void addValues(float[] values) {

		mValues.add(new float[]{values[0], values[1], values[2]});
	}
	
	public List<float[]> getValues() {
		return mValues;
	}
}
