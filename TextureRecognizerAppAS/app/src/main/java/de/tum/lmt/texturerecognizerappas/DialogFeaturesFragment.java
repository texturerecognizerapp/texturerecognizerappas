package de.tum.lmt.texturerecognizerappas;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Set;


/** currently not used **/
public class DialogFeaturesFragment extends DialogFragment {

	private static final String TAG = DialogFeaturesFragment.class.getSimpleName();

	private Context mContext;
	private boolean mDatabaseMode;

	private FeatureTask.iOnFeatureTaskFinishedListener mFeatureListener;
	private ClassificationTask.iOnClassificationTaskFinishedListener mClassificationListener;

	private AlertDialog mClassificationProgressDialog;

	ProgressBar mProgressBar;
	TextView mTextView;
	Button mButtonShare;
	Button mButtonClassification;
	Button mButtonRestart;

	Byte[] mRawSoundData;
	SensorLog mAccelLog;
	List<SensorLog> mExternAccelLogList;
	SensorLog mGravLog;
	SensorLog mGyroLog;
	SensorLog mMagnetLog;
	SensorLog mRotVecLog;

	private InputStream mSoundStream;
	private WaveReader mWaveReader;

	private File mLoggingDir;
	private String mLoggingDirPath;
	private String mDataToSendPath;
	private File mDataToSendDir;
	private String mAudioFilename;
	private String mBitmapNoFlashPath = MainActivity.getLoggingDir() + Constants.BITMAP_NO_FLASH_FILENAME;
	private String mBitmapFlashPath = MainActivity.getLoggingDir() + Constants.BITMAP_FLASH_FILENAME;
	private AudioRecorderWAV recorder;

	private FeatureComputer mFeatureComputer;

	private Features mFeatures;

	private long mFeaturesTimeStart;
	private long mFeaturesTimeStop;

	private long mClassificationTimeStart;
	private long mClassificationTimeStop;

	public DialogFeaturesFragment(Context context, Byte[] rawSoundData, SensorLog accelLog, List<SensorLog> externAccelLogList, SensorLog gravLog, SensorLog gyroLog, SensorLog magnetLog, SensorLog rotVecLog) {
		mContext = context;

		mRawSoundData = rawSoundData;
		mAccelLog = accelLog;
		mExternAccelLogList = externAccelLogList;
		mGravLog = gravLog;
		mGyroLog = gyroLog;
		mMagnetLog = magnetLog;
		mRotVecLog = rotVecLog;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		final SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		mDatabaseMode = sharedPrefs.getBoolean(Constants.PREF_KEY_MODE_SELECT, false);
		mLoggingDirPath = sharedPrefs.getString(Constants.PREF_KEY_LOGGING_DIR, null);
		mLoggingDir = new File(mLoggingDirPath);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View content = inflater.inflate(R.layout.dialog_features, null);

		mProgressBar = (ProgressBar) content.findViewById(R.id.progressbar_features);
		mTextView = (TextView) content.findViewById(R.id.textview_features);

		mButtonShare = (Button) content.findViewById(R.id.button_share_features);
		mButtonShare.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!mDatabaseMode) {
					showSendDialog();
					dismiss();
				}
			}
		});

		mButtonClassification = (Button) content.findViewById(R.id.button_classification);
		mButtonClassification.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!mDatabaseMode) {

					String classificationMethod = sharedPrefs.getString(Constants.PREF_KEY_CLASSIFICATION_METHOD, null);
					//String distance = sharedPrefs.getString(Constants.PREF_KEY_DISTANCE, null);
					int neighborhood = Integer.valueOf(sharedPrefs.getString(Constants.PREF_KEY_NEIGHBORHOOD, null));
					Set<String> featureSet = sharedPrefs.getStringSet(Constants.PREF_KEY_CLASSIfICATION_FEATURES, null);
 					String featurePath = MainActivity.getLoggingDir() + Constants.DATA_TO_SEND_FOLDER_NAME + File.separator;

					if(classificationMethod == null) {
						Log.e("Classification", "classification parameters not set");
						Toast.makeText(getActivity(), getString(R.string.parameters_missing), Toast.LENGTH_LONG).show();
					} else {
						ClassificationTask classifierTask = new ClassificationTask(featurePath, mFeatures, classificationMethod, neighborhood, featureSet, mClassificationListener);
						classifierTask.execute();

						mClassificationTimeStart = System.currentTimeMillis();

						showProgressDialog();
					}
				}
			}
		});

		mButtonRestart = (Button) content.findViewById(R.id.button_restart);
		mButtonRestart.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent mStartActivity = new Intent(mContext, MainActivity.class);
				int mPendingIntentId = 123456;
				PendingIntent mPendingIntent = PendingIntent.getActivity(mContext, mPendingIntentId,    mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
				AlarmManager mgr = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
				mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, mPendingIntent);
				System.exit(0);

			}
		});


		mButtonShare.setVisibility(View.GONE);
		mButtonClassification.setVisibility(View.GONE);
        mButtonRestart.setVisibility(View.GONE);

		if (mDatabaseMode) {
			mDataToSendPath = mLoggingDir.getAbsolutePath();
			mAudioFilename = mLoggingDir.getAbsolutePath() + File.separator + Constants.AUDIO_FILENAME_ORIG + ".wav";
		} else {
			mDataToSendPath = MainActivity.getLoggingDir().getAbsolutePath() + Constants.DATA_TO_SEND_FOLDER_NAME;
			mAudioFilename = mDataToSendPath + File.separator + Constants.AUDIO_FILENAME_IMPACT + Constants.AUDIO_FILE_EXTENSION;
			mDataToSendDir = new File(mDataToSendPath);

			if (!mDataToSendDir.exists()) {
				mDataToSendDir.mkdirs();
			}
		}

		builder.setView(content);

		Dialog featureDialog = builder.create();
		featureDialog.setCanceledOnTouchOutside(false);

		return featureDialog;
	}

	@Override
	public void onStart() {
		super.onStart();

		final SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);

		mFeatureListener = new FeatureTask.iOnFeatureTaskFinishedListener() {
			@Override
			public void onFeatureTaskFinished(Features features) {

				mFeaturesTimeStop = System.currentTimeMillis();

				Toast.makeText(getActivity().getApplicationContext(), "Computing Features took " + (mFeaturesTimeStop - mFeaturesTimeStart) + " ms.", Toast.LENGTH_LONG).show();

				if(!mDatabaseMode) {
					mButtonShare.setVisibility(View.VISIBLE);
					mButtonClassification.setVisibility(View.VISIBLE);
					mButtonShare.setEnabled(true);
					mButtonClassification.setEnabled(true);
                    mButtonRestart.setVisibility(View.VISIBLE);
                    mButtonRestart.setEnabled(true);
					mProgressBar.setVisibility(View.GONE);
					mTextView.setVisibility(View.GONE);
					//mTextView.setText(getString(R.string.features_ready));
				} else {
					dismiss();
					getActivity().finish();
				}

				mFeatures = features;
			}
		};

		mClassificationListener = new ClassificationTask.iOnClassificationTaskFinishedListener() {
			@Override
			public void onClassifierTaskFinished(List<Texture.Sample> result) {

				mClassificationTimeStop = System.currentTimeMillis();

				Toast.makeText(getActivity().getApplicationContext(), "Classification took " + (mClassificationTimeStop - mClassificationTimeStart) + " ms.", Toast.LENGTH_LONG).show();

				if(mClassificationProgressDialog != null) {
					mClassificationProgressDialog.dismiss();
				}

				String classificationMethod = sharedPrefs.getString(Constants.PREF_KEY_CLASSIFICATION_METHOD, null);

				if(result == null) {
					Log.e("Classification", "Classfication not successful");
					Toast.makeText(getActivity(), getString(R.string.classification_null), Toast.LENGTH_LONG).show();
				} else {
					showClassificationListDialog(result, classificationMethod);
				}
			}
		};

		final String featurePath;
		if (!mDatabaseMode) {
			featurePath = mLoggingDirPath + Constants.DATA_TO_SEND_FOLDER_NAME + File.separator;
		} else {
			featurePath = mLoggingDirPath + File.separator;
		}

		//FeatureTask featureTask = new FeatureTask(getActivity().getBaseContext(), mFeatureListener, featurePath, mAudioFilename, mBitmapNoFlashPath, mBitmapFlashPath, mRawSoundData, mAccelLog, mExternAccelLogList, mDatabaseMode);
		//featureTask.execute();

		mFeaturesTimeStart = System.currentTimeMillis();
	}

	private void showProgressDialog() {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		RelativeLayout rl = new RelativeLayout(getActivity());

		ProgressBar progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleLarge);
		progressBar.setIndeterminate(true);
		RelativeLayout.LayoutParams progressBarParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

		TextView text = new TextView(getActivity());
		text.setText(R.string.classification_progress);
		RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		textParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		textParams.addRule(RelativeLayout.CENTER_VERTICAL);

		rl.addView(progressBar, progressBarParams);
		rl.addView(text, textParams);

		builder.setView(rl);

		mClassificationProgressDialog = builder.create();
		mClassificationProgressDialog = builder.show();

		mClassificationProgressDialog.setCanceledOnTouchOutside(false);
	}

	private void showSendDialog() {
		DialogSendFragment dialogSend = DialogSendFragment.newInstance(mLoggingDirPath);
		dialogSend.show(getFragmentManager(), "dialogSend");
	}

	private void showClassificationListDialog(List<Texture.Sample> textures, String classificationMethod) {
		DialogClassificationListFragment dialogClassificationList = new DialogClassificationListFragment(textures, classificationMethod);
		dialogClassificationList.show(getFragmentManager(), "dialogClassificationList");
	}


}