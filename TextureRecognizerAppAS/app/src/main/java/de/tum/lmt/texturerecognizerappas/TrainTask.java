package de.tum.lmt.texturerecognizerappas;

import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kev94 on 26.03.2016.
 */
public class TrainTask extends AsyncTask<Void, Void, Exception> {

    private iOnTrainedListener mListener;

    private String mDatabasePath;
    private String mClassificationMethod;

    public TrainTask(String databasePath, String classificationMethod, iOnTrainedListener listener) {
        mDatabasePath = databasePath;
        mClassificationMethod = classificationMethod;
        mListener = listener;
    }

    public interface iOnTrainedListener {
        public void onTrained();
    }

    @Override
    protected Exception doInBackground(Void... params) {

        switch(mClassificationMethod) {
            case Constants.CLASSIFICATION_DIST_TO_MEAN:
                computeMeanSamples();
                break;
            default:
                Log.e("Train", "Cannot Train");
                break;
        }

        return null;
    }

    protected void onPostExecute(Exception e) {
        if(mListener != null) {
            mListener.onTrained();
        }
    }

    private void computeMeanSamples() {

        File databaseDir = new File(mDatabasePath);
        File databaseFile = new File(databaseDir.getAbsolutePath() + File.separator + Constants.JSON_DATABASE_FILENAME);

        if (!databaseFile.exists()) {
            Log.i("Database", "Database file does not exist");
        } else {
            JSONHelper jsonHelper = new JSONHelper();

            List<Texture> textures = jsonHelper.readTexturesFromDatabase(databaseFile, false);

            for(Texture tex:textures) {

                List<Features> featureList = new ArrayList<>();
                boolean meanExists = false;

                for(Texture.Sample sample:tex.getSamples()) {
                    if(!sample.getNumber().equals(Constants.MEAN_SAMPLE_NUMBER)) {
                        featureList.add(sample.getFeatures());
                    } else {
                        meanExists = true;
                    }
                }

                Features meanFeatures = computeMeanFeatures(featureList);

                if(meanExists) {
                    for (Texture.Sample sample : tex.getSamples()) {
                        if (sample.getNumber().equals(Constants.MEAN_SAMPLE_NUMBER)) {
                            sample.setFeatures(meanFeatures);
                        }
                    }
                } else {
                    tex.addSample(Constants.MEAN_SAMPLE_NUMBER, meanFeatures);
                }
            }

            jsonHelper.updateDatabaseFromTextureList(textures, mDatabasePath);
        }
    }

    //// TODO: 12.05.2016 change to current features and implementation with less code lines (attempt see below)
    /*private Features computeMeanFeatures(List<Features> featureList) {

        Features meanFeatures = new Features();
        int numberOfFeatures = featureList.size();

        double[] meanFeaturesArray = new double[numberOfFeatures];

        float meanImpactBegins = 0; //in seconds, can be converted to sound or sensor index with known sampling rate
        float meanImpactEnds = 0;
        float meanMovementBegins = 0;
        double meanWeightMacro = 0;
        double meanWeightMicro = 0;
        double meanWeightGlossiness = 0;
        double meanVelocity = 0;
        //double[] currImageGradient = new double[3];
        //double[] meanImageGradient = {0,0,0};
        //double[] currImageGradientOverWindow = new double[3];
        //double[] meanImageGradientOverWindow = {0,0,0};
        double currImageGradient = 0;
        double meanImageGradient = 0;
        double currImageGradientOverWindow = 0;
        double meanImageGradientOverWindow = 0;
        double meanMacroAmplitude = 0;
        double meanMicroAmplitude = 0;
        double meanGlossinessAmplitude = 0;
        double meanHardness = 0;
        long meanImpactDuration = 0;
        double meanNoiseDistribution = 0;
        double meanZeroCrossingRate = 0;
        double meanKurtosis = 0;

        for(Features features:featureList) {
            double[] currentFeatureArray = features.getFeaturesArray(Constants.FEATURE_SET);

            int i = 0;
            for(double featureValue:currentFeatureArray) {
                meanFeaturesArray[i] += featureValue;
                i++;
            }

            for(int j = 0; j < numberOfFeatures; j++) {
                meanFeaturesArray[j] /= numberOfFeatures;
            }
        }



        for(Features features:featureList) {
            meanImpactBegins += features.getImpactBegins();
            meanImpactEnds += features.getImpactEnds();
            meanMovementBegins += features.getMovementBegins();
            meanWeightMacro += features.getWeightMacro();
            meanWeightMicro += features.getWeightMicro();
            meanWeightGlossiness += features.getWeightGlossiness();
            meanVelocity += features.getVelocity();
            meanImageGradient += features.getImageGradient();
            meanImageGradientOverWindow += features.getImageGradientOverWindow();
            meanMacroAmplitude += features.getMacroAmplitude();
            meanMicroAmplitude += features.getMicroAmplitude();
            meanGlossinessAmplitude += features.getGlossinessAmplitude();
            meanHardness += features.getHardness();
            meanImpactDuration += features.getImpactDuration();
            meanNoiseDistribution += features.getNoiseDistribution();
            meanZeroCrossingRate += features.getZeroCrossingRate();
            meanKurtosis += features.getSoundKurtosisTap();
        }

        meanImpactBegins /= numberOfFeatures;
        meanImpactEnds /= numberOfFeatures;
        meanMovementBegins /= numberOfFeatures;
        meanWeightMacro /= numberOfFeatures;
        meanWeightMicro /= numberOfFeatures;
        meanWeightGlossiness /= numberOfFeatures;
        meanVelocity /= numberOfFeatures;
        meanImageGradient /= numberOfFeatures;
        meanImageGradientOverWindow /= numberOfFeatures;
        meanMacroAmplitude /= numberOfFeatures;
        meanMicroAmplitude /= numberOfFeatures;
        meanGlossinessAmplitude /= numberOfFeatures;
        meanHardness /= numberOfFeatures;
        meanImpactDuration /= numberOfFeatures;
        meanNoiseDistribution /= numberOfFeatures;
        meanZeroCrossingRate /= numberOfFeatures;
        meanKurtosis /= numberOfFeatures;

        meanFeatures.setImpactBegins(meanImpactBegins);
        meanFeatures.setImpactEnds(meanImpactEnds);
        meanFeatures.setMovementBegins(meanMovementBegins);
        meanFeatures.setWeightMacro(meanWeightMacro);
        meanFeatures.setWeightMicro(meanWeightMicro);
        meanFeatures.setWeightGlossiness(meanWeightGlossiness);
        meanFeatures.setVelocity(meanVelocity);
        meanFeatures.setImageGradient(meanImageGradient);
        meanFeatures.setImageGradientOverWindow(meanImageGradientOverWindow);
        meanFeatures.setMacroAmplitude(meanMacroAmplitude);
        meanFeatures.setMicroAmplitude(meanMicroAmplitude);
        meanFeatures.setGlossinessAmplitude(meanGlossinessAmplitude);
        meanFeatures.setHardness(meanHardness);
        meanFeatures.setImpactDuration(meanImpactDuration);
        meanFeatures.setNoiseDistribution(meanNoiseDistribution);
        meanFeatures.setSoundZeroCrossingRate(meanZeroCrossingRate);
        meanFeatures.setKurtosis(meanKurtosis);

        return meanFeatures;
    }*/

    private Features computeMeanFeatures(List<Features> featureList) {

        Features meanFeatures = new Features();
        int numberOfFeatures = featureList.size();

        double[] meanFeaturesArray = new double[Constants.FEATURE_SET.length];

        for(Features features:featureList) {
            double[] currentFeatureArray = features.getFeaturesArray(Constants.FEATURE_SET);

            int i = 0;
            for(double featureValue:currentFeatureArray) {
                Log.i("mean features", "mean value" + featureValue);
                meanFeaturesArray[i] += featureValue;
                i++;
            }
        }

        for(int j = 0; j < numberOfFeatures; j++) {
            meanFeaturesArray[j] /= numberOfFeatures;
        }

        int i = 0;
        for(String featureName:Constants.FEATURE_SET) {
            Log.i("mean features", "name: " + featureName);

            meanFeatures.setFeature(featureName, meanFeaturesArray[i]);
            i++;
        }

        return meanFeatures;
    }
}
