package de.tum.lmt.texturerecognizerappas;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by Kev94 on 16.03.2016.
 */
public class JSONHelper {

    FileHandler mFileHandler;

    public JSONHelper() {
        mFileHandler = new FileHandler();
    }

    public JSONObject createNewFeatureSet() {
        //// TODO: 16.03.2016 implement method
        return null;
    }

    public JSONObject buildJSONFeatureSet(Features features) {

        JSONObject jsonFeatureSet = new JSONObject();

        //add a name value pair for every feature
        for(String featureString:Constants.FEATURE_SET) {
            if(!Double.isNaN(features.getFeature(featureString))) {
                try {
                    jsonFeatureSet.put(featureString, features.getFeature(featureString));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                //Log.i("JSONHelper", featureString + ": NaN");
                try {
                    jsonFeatureSet.put(featureString, -1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return jsonFeatureSet;
    }

    /*public JSONObject buildDBentry(int textureNumber, Features features) {

        JSONObject dbEntry = new JSONObject();
        try {
            dbEntry.put(Integer.toString(textureNumber), jsonFeatureSet);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return dbEntry;
    }*/

    //add a new feature set to a texture
    public void updateDatabase(String featurePath, String textureName, String textureNumber, JSONObject featureSet) {
        File featureDir = new File(featurePath);
        File databaseDir = featureDir.getParentFile().getParentFile();

        File databaseFile = new File(databaseDir.getAbsolutePath() + File.separator + Constants.JSON_DATABASE_FILENAME);

        String databaseText;

        if (!databaseFile.exists()) {
            try {
                databaseFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONObject jsonDB = new JSONObject();
            JSONObject jsonTexture = new JSONObject();
            try {
                jsonTexture.put(textureNumber, featureSet);
                jsonDB.put(textureName, jsonTexture);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            mFileHandler.writeDataToTextFile(jsonDB.toString(), databaseDir.getAbsolutePath(), Constants.JSON_DATABASE_FILENAME);
        } else {
            databaseText = mFileHandler.readTextfile(databaseFile);

            JSONObject jsonDB = null;
            try {
                jsonDB = new JSONObject(databaseText);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JSONObject jsonTexture = jsonDB.optJSONObject(textureName);
            if(jsonTexture != null) {
                Log.i("JSON", "Texture already exists");
                try {
                    jsonTexture.put(textureNumber, featureSet);
                    jsonDB.put(textureName, jsonTexture);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                JSONObject newTexure = new JSONObject();
                try {
                    newTexure.put(textureNumber, featureSet);
                    jsonDB.put(textureName, newTexure);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            mFileHandler.writeDataToTextFile(jsonDB.toString(), databaseDir.getAbsolutePath(), Constants.JSON_DATABASE_FILENAME);
        }
    }

    //for training the k means model; create/update a database out of a list of textures
    public void updateDatabaseFromTextureList(List<Texture> textures, String databasePath) {

        JSONObject jsonDB = new JSONObject();

        for(Texture tex:textures) {

            JSONObject jsonCurrentTexture = new JSONObject();

            for(Texture.Sample sample:tex.getSamples()) {
                try {
                    jsonCurrentTexture.put(sample.getNumber(), buildJSONFeatureSet(sample.getFeatures()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            try {
                jsonDB.put(tex.getName(), jsonCurrentTexture);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.i("Database", "update, path: " + databasePath);
            File databaseDir = new File(databasePath);
            mFileHandler.writeDataToTextFile(jsonDB.toString(), databaseDir.getAbsolutePath(), Constants.JSON_DATABASE_FILENAME);
        }
    }

    //// TODO: 07.04.2016 Set all Features
    //currently unused; see next method
    /*public Features readFeaturesFromJSONFile(File jsonFile) {

        Features features = new Features();
        FileHandler fileHandler = new FileHandler();

        String featureString = fileHandler.readTextfile(jsonFile);

        JSONObject jsonFeatureSet = null;
        try {
            jsonFeatureSet = new JSONObject(featureString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            features.setImpactDuration(jsonFeatureSet.getLong("ImpactDuration"));
            features.setHardness(jsonFeatureSet.getDouble("Hardness"));
            features.setImageGradientOverWindow(jsonFeatureSet.getDouble("ImageGradientOverWindow"));
            features.setImageGradient(jsonFeatureSet.getDouble("ImageGradient"));
            features.setWeightGlossiness(jsonFeatureSet.getDouble("WeightGlossiness"));
            features.setWeightMacro(jsonFeatureSet.getDouble("WeightMacro"));
            features.setWeightMicro(jsonFeatureSet.getDouble("WeightMicro"));
            features.setKurtosis(jsonFeatureSet.getDouble("Kurtosis"));
            features.setSoundZeroCrossingRate(jsonFeatureSet.getDouble("ZeroCrossingRate"));
            features.setNoiseDistribution(jsonFeatureSet.getDouble("NoiseDistribution"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return features;

    }*/

    //read features that are listed in the set into Feature object
    public Features readFeaturesFromJSONFile(File jsonFile, Set<String> featureStringSet) {

        Features features = new Features();
        FileHandler fileHandler = new FileHandler();

        String featureString = fileHandler.readTextfile(jsonFile);

        JSONObject jsonFeatureSet = null;
        try {
            jsonFeatureSet = new JSONObject(featureString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            for(String string:featureStringSet) {

                features.setFeature(string, jsonFeatureSet.getDouble(string));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return features;

    }

    //read whole Textures from the database (if withMean is true, the mean features will also be read (if they were set before))
    public List<Texture> readTexturesFromDatabase(File databaseFile, boolean withMean) {

        List<Texture> textures = new ArrayList<>();

        FileHandler fileHandler = new FileHandler();

        Log.i("JSON", "path: " + databaseFile.getAbsolutePath());

        if(!databaseFile.exists()) {
            Log.i("JSON", "file does not exist");
            return null;
        } else {
            String dbString = fileHandler.readTextfile(databaseFile);

            JSONObject jsonDB = null;
            try {
                jsonDB = new JSONObject(dbString);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Iterator<String>  textureNames = jsonDB.keys();
            while(textureNames.hasNext()) {
                String currentName = textureNames.next();
                Texture currentTexture = new Texture(currentName);

                JSONObject jsonCurrentTexture = new JSONObject();
                try {
                    jsonCurrentTexture = jsonDB.getJSONObject(currentName);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Iterator<String> textureSamples = jsonCurrentTexture.keys();
                while(textureSamples.hasNext()) {
                    String currentNumber = textureSamples.next();
                    //Log.i("JSON", "currentName: " + currentName + ", currentNumber: " + currentNumber);

                    if((!currentNumber.equals(Constants.MEAN_SAMPLE_NUMBER)) || (currentNumber.equals(Constants.MEAN_SAMPLE_NUMBER) && withMean)) {
                        JSONObject jsonCurrentFeatureSet = new JSONObject();
                        try {
                            jsonCurrentFeatureSet = jsonCurrentTexture.getJSONObject(currentNumber);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Features features = new Features();

                        for(String featureString:Constants.FEATURE_SET) {
                            try {

                                features.setFeature(featureString, jsonCurrentFeatureSet.getDouble(featureString));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        currentTexture.addSample(currentNumber, features);
                    }
                }
                textures.add(currentTexture);
            }
        }

        return textures;
    }

    //// TODO: 25.03.2016 almost the same as readTexturesfromDatabase(); think of a way to put them together to one method
    //read only mean Textures from database
    public List<Texture> readMeanTexturesFromDatabase(File databaseFile) {
        List<Texture> textures = new ArrayList<>();

        FileHandler fileHandler = new FileHandler();

        //// TODO: 18.03.2016 predefine the path in constants
        //String databasePath = MainActivity.getLoggingDir().getParentFile().getParentFile().getAbsolutePath() + File.separator + Constants.DATABASE_FOLDER_NAME + File.separator + Constants.JSON_DATABASE_FILENAME;

        //File databaseFile = new File(databasePath);

        Log.i("JSON", "path: " + databaseFile.getAbsolutePath());

        if (!databaseFile.exists()) {
            Log.i("JSON", "file does not exist");
            return null;
        } else {
            Log.i("JSON", "db file path: " + databaseFile.getAbsolutePath());
            String dbString = fileHandler.readTextfile(databaseFile);

            JSONObject jsonDB = null;
            try {
                jsonDB = new JSONObject(dbString);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Iterator<String> textureNames = jsonDB.keys();
            while (textureNames.hasNext()) {
                String currentName = textureNames.next();
                Texture currentTexture = new Texture(currentName);

                JSONObject jsonCurrentTexture = new JSONObject();
                try {
                    jsonCurrentTexture = jsonDB.getJSONObject(currentName);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Iterator<String> textureSamples = jsonCurrentTexture.keys();
                while (textureSamples.hasNext()) {
                    String currentNumber = textureSamples.next();
                    //Log.i("JSON", "currentName: " + currentName + ", currentNumber: " + currentNumber);

                    if (currentNumber.equals(Constants.MEAN_SAMPLE_NUMBER)) {
                        JSONObject jsonMeanFeatureSet = new JSONObject();
                        try {
                            jsonMeanFeatureSet = jsonCurrentTexture.getJSONObject(currentNumber);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Features features = new Features();

                        for(String featureString:Constants.FEATURE_SET) {
                            try {

                                features.setFeature(featureString, (double) jsonMeanFeatureSet.get(featureString));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        /*try {
                            features.setImpactDuration(jsonMeanFeatureSet.getLong("ImpactDuration"));
                            features.setHardness(jsonMeanFeatureSet.getDouble("Hardness"));
                            features.setImageGradientOverWindow(jsonMeanFeatureSet.getDouble("ImageGradientOverWindow"));
                            features.setImageGradient(jsonMeanFeatureSet.getDouble("ImageGradient"));
                            features.setWeightGlossiness(jsonMeanFeatureSet.getDouble("WeightGlossiness"));
                            features.setWeightMacro(jsonMeanFeatureSet.getDouble("WeightMacro"));
                            features.setWeightMicro(jsonMeanFeatureSet.getDouble("WeightMicro"));
                            features.setKurtosis(jsonMeanFeatureSet.getDouble("Kurtosis"));
                            features.setSoundZeroCrossingRate(jsonMeanFeatureSet.getDouble("ZeroCrossingRate"));
                            features.setNoiseDistribution(jsonMeanFeatureSet.getDouble("NoiseDistribution"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }*/

                        currentTexture.addSample(currentNumber, features);
                        textures.add(currentTexture);
                    }
                }
            }
        }

        return textures;
    }

    public String buildJSONSettingsString(String classificationMethod, String neighborhoodSize, Set<String> classificationFeatures) {

        //construct settings JSON
        JSONObject jsonSettings = new JSONObject();
        try {
            jsonSettings.put(Constants.PREF_TITLE_CLASSIFICATION_METHOD, classificationMethod);

            if(classificationMethod.equals(Constants.CLASSIFICATION_KNN)) {
                jsonSettings.put(Constants.PREF_TITLE_NEIGHBORHOOD_SIZE, neighborhoodSize);
            }

            JSONObject jsonClassificationFeatures = new JSONObject();

            int i = 0;

            for(String element:classificationFeatures) {
                jsonClassificationFeatures.put("Feature" + i, element);
                i++;
            }

            jsonSettings.put(Constants.PREF_TITLE_CLASSIFICATION_FEATURES, jsonClassificationFeatures);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonSettings.toString();

    }

    public List<Texture.Sample> getResultFromOnlineClassification(String jsonResultString, String classificationMethod, String neighborhoodSize, String kMeansEucl, String kMeansMahal, String kNearestNeighbor) {

        List<Texture.Sample> result = new ArrayList<>();

        JSONObject jsonResult = null;
        try {
            jsonResult = new JSONObject(jsonResultString);

            if (classificationMethod.equals(kMeansEucl)) {

                if (jsonResult != null) {

                    for (int i = 1; i <= Constants.NUMBER_OF_TEXTURES; i++) {

                        JSONObject jsonSample = jsonResult.getJSONObject(Integer.toString(i));

                        String name = jsonSample.getString("name");
                        double distance = jsonSample.getDouble("value");

                        Texture texture = new Texture(name);
                        texture.addSampleWithDistance(distance);

                        result.add(texture.getSamples().get(0));

                    }
                }
            } else if (classificationMethod.equals(kMeansMahal)) {

                if (jsonResult != null) {

                    for (int i = 1; i <= Constants.NUMBER_OF_TEXTURES; i++) {

                        JSONObject jsonSample = jsonResult.getJSONObject(Integer.toString(i));

                        String name = jsonSample.getString("name");
                        double distance = jsonSample.getDouble("value");

                        Texture texture = new Texture(name);
                        texture.addSampleWithDistance(distance);

                        result.add(texture.getSamples().get(0));

                    }
                }
            } else if (classificationMethod.equals(kNearestNeighbor)) {

                for (int i = 1; i <= Integer.getInteger(neighborhoodSize, 10); i++) {

                    JSONObject jsonSample = jsonResult.getJSONObject(Integer.toString(i));

                    String name = jsonSample.getString("name");
                    int occurence = jsonSample.getInt("value");

                    Texture texture = new Texture(name);
                    texture.addSampleWithOccurence(occurence);

                    result.add(texture.getSamples().get(0));

                            /*for(Texture.Sample sample:result) {

                                if(name.equals(sample.getName())) {
                                    sample.setOccurence(sample.getOccurence()+1);
                                    break;
                                } else {
                                    Texture texture = new Texture(name);
                                    texture.addSampleWithOccurence(1);

                                    result.add(texture.getSamples().get(0));
                                }
                            }*/

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    public Features[] readMinMaxFeatures(File JSONMinMaxFile) {

        Features[] minMaxFeatures = new Features[2];

        minMaxFeatures[0] = new Features();
        minMaxFeatures[1] = new Features();

        FileHandler fileHandler = new FileHandler();

        //// TODO: 18.03.2016 predefine the path in constants
        //String databasePath = MainActivity.getLoggingDir().getParentFile().getParentFile().getAbsolutePath() + File.separator + Constants.DATABASE_FOLDER_NAME + File.separator + Constants.JSON_DATABASE_FILENAME;

        //File databaseFile = new File(databasePath);

        Log.i("JSON", "path: " + JSONMinMaxFile.getAbsolutePath());

        if (!JSONMinMaxFile.exists()) {
            Log.e("JSON", "file does not exist");
            return null;
        } else {
            Log.i("JSON", "db file path: " + JSONMinMaxFile.getAbsolutePath());
            String minMaxString = fileHandler.readTextfile(JSONMinMaxFile);

            JSONObject jsonMinMax = null;

            try {
                jsonMinMax = new JSONObject(minMaxString);

                for(String featureName:Constants.FEATURE_SET) {

                    JSONObject feature = jsonMinMax.getJSONObject(featureName);

                    if(feature != null) {
                        minMaxFeatures[0].setFeature(featureName, feature.getDouble("Minimum"));
                        minMaxFeatures[1].setFeature(featureName, feature.getDouble("Maximum"));
                    }

                    Log.i("JSONHelper", "featureName: " + featureName + ", min" + feature.getDouble("Minimum") + ", max: " + feature.getDouble("Maximum"));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return minMaxFeatures;
    }
}
