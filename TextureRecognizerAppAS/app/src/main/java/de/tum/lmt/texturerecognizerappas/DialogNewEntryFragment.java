package de.tum.lmt.texturerecognizerappas;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class DialogNewEntryFragment extends DialogFragment {

    private static final String TAG = DialogNewEntryFragment.class.getSimpleName();

    private EditText edittextTexture;
    private EditText edittextDatadesc;
    private TextView textviewWarning;

    private static File mLoggingDir;
    private int mError = 0;

    //http://developer.android.com/reference/android/app/DialogFragment.html
    static DialogNewEntryFragment newInstance(String path, int error) {
        DialogNewEntryFragment f = new DialogNewEntryFragment();

        Bundle args = new Bundle();
        args.putString("path", path);
        args.putInt("error", error);
        f.setArguments(args);

        return f;
    }

    public interface iOnNewEntry {
        public void onNewEntry(File dir, String name);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View content = inflater.inflate(R.layout.dialog_new_entry, null);

        mLoggingDir = new File(getArguments().getString("path"));
        mError = getArguments().getInt("error");

        edittextTexture = (EditText) content.findViewById(R.id.edittext_dialog_datadesc_texture_name);
        edittextDatadesc = (EditText) content.findViewById(R.id.edittext_dialog_datadesc_texture_datadesc);
        textviewWarning = (TextView) content.findViewById(R.id.textview_dialog_datadesc_texture_warning);

        switch(mError) {
            case 1:
                textviewWarning.setText(R.string.spaces_used);
                textviewWarning.setTextColor(Color.RED);
                break;
            case 2:
                textviewWarning.setText(R.string.missing_data);
                textviewWarning.setTextColor(Color.RED);
                break;
        }

        builder.setView(content)
                .setPositiveButton(R.string.submit, new DialogInterface.OnClickListener() {

                    String metadata;
                    String datadesc;

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String textureName = edittextTexture.getText().toString();

                        if(textureName.contains(" ")) {
                            DialogNewEntryFragment newEntryDialog = DialogNewEntryFragment.newInstance(mLoggingDir.getAbsolutePath(), 1);
                            newEntryDialog.show(getFragmentManager(), "DialogNewEntryFragment");;
                        }
                        else if ((textureName.length() == 0) || (edittextTexture.getText().toString().length() == 0)) {
                            DialogNewEntryFragment newEntryDialog = DialogNewEntryFragment.newInstance(mLoggingDir.getAbsolutePath(), 2);
                            newEntryDialog.show(getFragmentManager(), "DialogNewEntryFragment");
                        }
                        else {

                            File file = new File(mLoggingDir.getAbsolutePath() + File.separator + textureName);

                            if(!file.exists()) {
                                file.mkdirs();

                                datadesc = edittextDatadesc.getText().toString();
                                writeDatasetDesc(file, datadesc);

                                metadata = getString(R.string.texture) + " " + edittextTexture.getText().toString() + "\n" +
                                        getString(R.string.datadesc_texture) + " " + edittextDatadesc.getText().toString();

                                //// TODO: 14.03.2016 think of adding the dialog to confirm user input, might be important to ensure corectness of databse data
                                iOnNewEntry activity = (iOnNewEntry) getActivity();
                                activity.onNewEntry(file, textureName);

                                dismiss();
                            }
                            else {
                                Toast toast = Toast.makeText(getActivity(), R.string.entry_exists, Toast.LENGTH_LONG);
                                toast.show();
                                textviewWarning.setText(R.string.entry_exists);
                            }
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });

        edittextTexture.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(edittextTexture.getText().toString().contains(" ")) {
                    textviewWarning.setText(R.string.spaces_used);
                }
                else {
                    textviewWarning.setText(R.string.empty);
                }
            }
        });

        return builder.create();
    }

    private void writeDatasetDesc(File file, String str) {

        String path = file.getParentFile().getAbsolutePath();
        try {
            File data_desc = new File(path + "/" + Constants.TEXTURE_DESCRIPTION_FILENAME);
            if (!data_desc.exists()) {
                data_desc.createNewFile();

                BufferedOutputStream temp = new BufferedOutputStream(new FileOutputStream(data_desc));
                temp.write(str.getBytes());
                temp.flush();
                temp.close();
                Log.i(TAG, "writing to file");
            }
        }
        catch (Exception e) {
            Log.e(TAG, "problem while writing to file");
        }
    }
}
