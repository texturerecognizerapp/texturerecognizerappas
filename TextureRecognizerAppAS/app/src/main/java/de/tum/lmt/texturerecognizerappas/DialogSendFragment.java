// Dima: Here the fragment is not fullscreen, it seems not so obvious how to make it fullscreen though
// worth a try to rewrite this fragment according to example in 
// http://www.techrepublic.com/article/pro-tip-unravel-the-mystery-of-androids-full-screen-dialog-fragments/
package de.tum.lmt.texturerecognizerappas;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DialogSendFragment extends DialogFragment {

	private static final String TAG = DialogSendFragment.class.getSimpleName();
	private String mBaseDirPath;
	private File mDataToSendDir;
	private File mSensorDir;

	private BluetoothAdapter mBluetoothAdapter;

	private View mBluetoothButton;
	private View mServerButton;
	private View mMailButton;

	private AlertDialog mProcessingDialog;

	private FileHandler	mFileHandler;

	private ServerTask.iOnDataSentListener mOnDataSentListener;

	private Animation mClickAnimation;

	//http://developer.android.com/reference/android/app/DialogFragment.html
	static DialogSendFragment newInstance(String dir) {
		DialogSendFragment f = new DialogSendFragment();

		Bundle args = new Bundle();
		args.putString("dir", dir);
		f.setArguments(args);

		return f;
	}


	class FileToSendName {
		File file;
		String name;
		FileToSendName(File file_, String name_) {
			file = file_;
			name = name_;
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View content = inflater.inflate(R.layout.dialog_send, null);
		
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		mBaseDirPath = getArguments().getString("dir");
		Log.i(TAG, "Base dir: " + mBaseDirPath);

		mFileHandler = new FileHandler();

		mDataToSendDir = new File(mBaseDirPath + File.separator + Constants.DATA_TO_SEND_FOLDER_NAME);
		mSensorDir = new File(mBaseDirPath  + File.separator + Constants.SENSOR_TAP_LOGGING_FOLDER_NAME);

		if (mBluetoothAdapter == null) {
			Toast.makeText(getActivity(), "Bluetooth is not isAvailable", Toast.LENGTH_LONG).show();
			dismiss();
			return null;
		}

		mClickAnimation = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.click_on_item);

		mBluetoothButton = content.findViewById(R.id.send_bluetooth_button);
		mBluetoothButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				mBluetoothButton.startAnimation(mClickAnimation);

				SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
				boolean sendToTPAD = sharedPrefs.getBoolean(Constants.PREF_KEY_TPAD, false);

				if (sendToTPAD) {
					File[] fileList = mDataToSendDir.listFiles();

					ArrayList<FileToSendName> fileNames = new ArrayList<>();

					for (String name : Constants.NAMES_OF_BLUETOOTH_FILES_TO_SEND) {
						fileNames.add(new FileToSendName(null, name));
					}

					// find which one is there
					for (File file : fileList) {
						for (FileToSendName n : fileNames) {
							if (file.getName().contains(n.name)) {
								n.file = file;
								n.name = file.getName();
							}
						}
					}

					if (!fileNames.isEmpty()) {
						sendViaBluetooth(fileNames);
					} else {
						Toast.makeText(getActivity().getApplicationContext(), "No files to send", Toast.LENGTH_LONG).show();
					}
				}
				else
				{
					File baseDir = new File(mBaseDirPath);

					List<File> fileList = mFileHandler.getFileListTwoLevels(baseDir);

					ArrayList<FileToSendName> fileToSendNameList = new ArrayList<>();

					for (File file : fileList) {
						fileToSendNameList.add(new FileToSendName(file, file.getName()));
					}

					if (!fileToSendNameList.isEmpty()) {
						sendViaBluetooth(fileToSendNameList);
					} else {
						Toast.makeText(getActivity().getApplicationContext(), "No files to send", Toast.LENGTH_LONG).show();
					}

				}
			}
		});

		mServerButton = content.findViewById(R.id.send_server_button);
		mServerButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				mServerButton.startAnimation(mClickAnimation);

				SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
				String IP = sharedPrefs.getString(Constants.PREF_KEY_IP, "192.168.2.1");

				WifiManager wifi = (WifiManager) getActivity().getSystemService(getActivity().getApplicationContext().WIFI_SERVICE);

				if (!wifi.isWifiEnabled()) {

					Log.i("Wifi", "Wifi not enabled");

					DialogEnableWifiFragment wifiDialog = DialogEnableWifiFragment.newInstance(mBaseDirPath);
					wifiDialog.show(getFragmentManager(), "DialogEnableWifiFragment");

				} else {
					if (wifi.getConnectionInfo().getNetworkId() != -1) {

						ServerTask serverTask = new ServerTask(getActivity(), mOnDataSentListener);
						serverTask.execute(mBaseDirPath, IP);

						showProcessingDialog();

					} else {

						Log.i("Wifi", "Wifi not connected");

						DialogFragment wifiDialog = DialogEnableWifiFragment.newInstance(mBaseDirPath);
						wifiDialog.show(getFragmentManager(), "DialogEnableWifiFragment");
					}
				}
			}
		});

		mMailButton = content.findViewById(R.id.send_mail_button);
		mMailButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				mMailButton.startAnimation(mClickAnimation);

				showMailDialog();
			}
		});

		mOnDataSentListener = new ServerTask.iOnDataSentListener() {
			@Override
			public void onDataSent() {
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mProcessingDialog.dismiss();
						Toast.makeText(getActivity(), "data sent", Toast.LENGTH_LONG).show();
					}
				});
			}
		};

		builder.setView(content);
		/*.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {

			//// TODO: 04.03.2016 check if nothing is selected and for example reopen dialog
			@Override
			public void onClick(DialogInterface dialog, int which) {

				Log.i(TAG, "DialongSendFragment called, getting URIS");

				if (checkboxMail.isChecked()) {
					showMailDialog();
				} else if (checkboxBluetooth.isChecked()) {

					SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
					boolean sendToTPAD = sharedPrefs.getBoolean(Constants.PREF_KEY_TPAD, false);

					if (sendToTPAD) {
						File[] fileList = mDataToSendDir.listFiles();

						ArrayList<FileToSendName> fileNames = new ArrayList<>();

						for (String name : Constants.NAMES_OF_BLUETOOTH_FILES_TO_SEND) {
							fileNames.add(new FileToSendName(null, name));
						}

						// find which one is there
						for (File file : fileList) {
							for (FileToSendName n : fileNames) {
								if (file.getName().contains(n.name)) {
									n.file = file;
									n.name = file.getName();
								}
							}
						}

						if (!fileNames.isEmpty()) {
							sendViaBluetooth(fileNames);
						} else {
							Toast.makeText(getActivity().getApplicationContext(), "No files to send", Toast.LENGTH_LONG).show();
						}
					} else {
						File baseDir = new File(mBaseDirPath);

						List<File> fileList = mFileHandler.getFileListTwoLevels(baseDir);

						ArrayList<FileToSendName> fileToSendNameList = new ArrayList<>();

						for (File file : fileList) {
							fileToSendNameList.add(new FileToSendName(file, file.getName()));
						}

						if (!fileToSendNameList.isEmpty()) {
							sendViaBluetooth(fileToSendNameList);
						} else {
							Toast.makeText(getActivity().getApplicationContext(), "No files to send", Toast.LENGTH_LONG).show();
						}
					}

				} else if (checkboxServer.isChecked()) {

					SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
					String IP = sharedPrefs.getString(Constants.PREF_KEY_IP, "192.168.2.1");

					WifiManager wifi = (WifiManager) getActivity().getSystemService(getActivity().getApplicationContext().WIFI_SERVICE);

					if (!wifi.isWifiEnabled()) {

						Log.i("Wifi", "Wifi not enabled");

						DialogEnableWifiFragment wifiDialog = DialogEnableWifiFragment.newInstance(mBaseDirPath);
						wifiDialog.show(getFragmentManager(), "DialogEnableWifiFragment");

					} else {
						if (wifi.getConnectionInfo().getNetworkId() != -1) {

							ServerTask serverTask = new ServerTask(getActivity(), mOnDataSentListener);
							serverTask.execute(mBaseDirPath, IP);

						} else {

							Log.i("Wifi", "Wifi not connected");

							DialogFragment wifiDialog = DialogEnableWifiFragment.newInstance(mBaseDirPath);
							wifiDialog.show(getFragmentManager(), "DialogEnableWifiFragment");
						}
					}

				}
			}
		}).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

			}
		});*/

		AlertDialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);

		return dialog;
	}

	void sendViaBluetooth(ArrayList<FileToSendName> fileNames) {
		if (fileNames.size()>0) {
			Log.i(TAG, "sending multiple files");
			Intent sendBt = new Intent(Intent.ACTION_SEND_MULTIPLE);
			sendBt.setType("*/*"); // previously this was sendBt.setType("image/*"); but it does not work on some devices so "*/*"
			sendBt.setPackage("com.android.bluetooth");
			ArrayList<Uri> uris = new ArrayList<Uri>();
			for (FileToSendName n : fileNames) {
				if (n.file != null) {
					Uri currentUri = Uri.fromFile(n.file);
					if (currentUri != null) {
						uris.add(currentUri);
					}
					else {
						Toast.makeText(getActivity().getApplicationContext(), "One file empty " + n.name, Toast.LENGTH_SHORT).show();
					}
				}
			}
			if (uris.size()>0) {
				sendBt.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
				startActivity(sendBt);
				Log.i(TAG, "Creating bluetooth intent:and now starting stuff");
			}
			else {
				Toast.makeText(getActivity().getApplicationContext(), "Empty files", Toast.LENGTH_LONG).show();
			}
		}
		else {
			Toast.makeText(getActivity().getApplicationContext(), "Empty files", Toast.LENGTH_LONG).show();
		}
	}

	protected void showMailDialog() {

		DialogFragment mailDialog = DialogMailFragment.newInstance(mBaseDirPath);
		mailDialog.show(getFragmentManager(), "DialogMailFragment");
	}

	private void showProcessingDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		RelativeLayout rl = new RelativeLayout(getActivity());

		ProgressBar progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleLarge);
		progressBar.setIndeterminate(true);
		RelativeLayout.LayoutParams progressBarParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

		TextView text = new TextView(getActivity());
		text.setText(R.string.sending);
		RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		textParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		textParams.addRule(RelativeLayout.CENTER_VERTICAL);

		rl.addView(progressBar, progressBarParams);
		rl.addView(text, textParams);

		builder.setView(rl);

		mProcessingDialog = builder.create();
		mProcessingDialog = builder.show();

		mProcessingDialog.setCanceledOnTouchOutside(false);
	}
}
