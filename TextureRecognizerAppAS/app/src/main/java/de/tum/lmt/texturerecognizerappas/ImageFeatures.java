package de.tum.lmt.texturerecognizerappas;

import android.graphics.Bitmap;
import android.util.Log;

import java.util.HashMap;

/**
 * Created by Kev94 on 07.12.2016.
 */
public class ImageFeatures {

    private static final String CLASS_NAME = "ImageFeatures";

    FileHandler mFileHandler;
    BitmapOperations mBmpOps;

    //remember threshold from no flash image to compute glossiness for flash image
    private double mGlossinessThreshold;

    //Features
    private double mImageGlossiness;
    private double mImageCentroid;
    private double mImageContrast;
    private double mDominantColor;
    private double mDominantSaturation;
    private double mImageFrequencyBinCount1;
    private double mImageSpectralCentroid;

    public ImageFeatures() {

        mFileHandler = new FileHandler();
        mBmpOps = new BitmapOperations();

    }

    public void process(String pathToBitmapNoFlash, HashMap featurePreferences) {

        Bitmap bitmapNoFlash = mFileHandler.loadBitmap(pathToBitmapNoFlash);

        Bitmap bitmapNoFlashGrayscale = mBmpOps.toGrayscale(bitmapNoFlash);

        //number of bright pixels relative to total number of pixels
        //double brightPixelsToTotalNumberOfPixelsNoFlash = mBmpOps.getNumberOfBrightPixelsRel(bitmapNoFlashGrayscale);

        int height = bitmapNoFlashGrayscale.getHeight();
        int width = bitmapNoFlashGrayscale.getWidth();

        int topLeftX = width/4;
        int topLeftY = height/4;

        int bottomRightX = 3*(width/4);
        int bottomRightY = 3*(height/4);

        Log.i("FeatureComputer", "Top Left X: " + topLeftX + ", Top Left Y: " + topLeftY + "Bottom Right X: " + topLeftX + ", Bottom Right Y: " + bottomRightY);

        int[] topLeftCorner = new int[]{topLeftX, topLeftY};
        int[] bottomRightCorner = new int[]{bottomRightX, bottomRightY};

        // centroid, max, mean, and histogram already computed here, because pixels have to be read out anyway --> not really slower;
        Pixels pixels = mBmpOps.getGrayscalePixelsAndFeatures(bitmapNoFlashGrayscale, topLeftCorner, bottomRightCorner, (boolean) featurePreferences.get(Constants.IMAGE_CONTRAST));

        height = bitmapNoFlash.getHeight();
        width = bitmapNoFlash.getWidth();

        topLeftX = width/4;
        topLeftY = height/4;

        bottomRightX = 3*(width/4);
        bottomRightY = 3*(height/4);

        Log.i("FeatureComputer", "Top Left X: " + topLeftX + ", Top Left Y: " + topLeftY + "Bottom Right X: " + topLeftX + ", Bottom Right Y: " + bottomRightY);

        topLeftCorner = new int[]{topLeftX, topLeftY};
        bottomRightCorner = new int[]{bottomRightX, bottomRightY};

        float[][][] pixelsHSV = mBmpOps.getPixelsHSV(bitmapNoFlash, topLeftCorner, bottomRightCorner);

        bitmapNoFlash.recycle();
        bitmapNoFlashGrayscale.recycle();

        //DominantColor and DominantSaturation
        if(((boolean)featurePreferences.get(Constants.DOMINANT_COLOR)) || ((boolean)featurePreferences.get(Constants.DOMINANT_SATURATION))) {

            double[] values = computeDominantColorAndDominantSaturation(pixelsHSV);

            mDominantColor = values[0];
            mDominantSaturation = Math.log(1+values[1]);

            Log.i(CLASS_NAME, "DominantColor: " + mDominantColor);
            Log.i(CLASS_NAME, "DominantSaturation: " + mDominantSaturation);
        } else {
            mDominantColor = Double.NaN;
            mDominantSaturation = Double.NaN;
        }

        //imageCentroid
        mImageCentroid = pixels.getMeanNorm();
        Log.i(CLASS_NAME, "ImageCentroid: " + mImageCentroid);

        //ImageContrast
        if((boolean)featurePreferences.get(Constants.IMAGE_CONTRAST)) {
            mImageContrast = computeImageContrast(pixels);
            Log.i(CLASS_NAME, "ImageContrast: " + mImageContrast);
        } else {
            mImageContrast = Double.NaN;
        }

        //Glossiness first step; second step follows in the processFlash function
        if((boolean)featurePreferences.get(Constants.GLOSSINESS)) {
            mImageGlossiness = computeImageGlossinessFlash(pixels) - computeImageGlossiness(pixels);
            Log.i(CLASS_NAME, "ImageGlossiness: " + mImageGlossiness);
        } else {
            mImageGlossiness = Double.NaN;
        }

        //ImageFrequencyBinCount1 and ImageSpectralCentroid
        if((boolean)featurePreferences.get(Constants.IMAGE_FREQUENCY_BIN_COUNT_1) || (boolean)featurePreferences.get(Constants.IMAGE_SPECTRAL_CENTROID)) {

            double[] values = computeImage1DimAndSpectralCentroid(pixels, (boolean) featurePreferences.get(Constants.IMAGE_FREQUENCY_BIN_COUNT_1), (boolean)featurePreferences.get(Constants.IMAGE_SPECTRAL_CENTROID));

            mImageFrequencyBinCount1 = values[0];
            mImageSpectralCentroid = values[1];

            Log.i(CLASS_NAME, "mImageFrequencyBinCount1: " + mImageFrequencyBinCount1);
            Log.i(CLASS_NAME, "mImageSpectralCentroid: " + mImageSpectralCentroid);

        } else {
            mImageFrequencyBinCount1 = Double.NaN;
            mImageSpectralCentroid = Double.NaN;
        }


    }

    public void processFlash(String pathToBitmapFlash, HashMap featurePreferences) {

        Bitmap bitmapFlash = mFileHandler.loadBitmap(pathToBitmapFlash);

        Bitmap bitmapFlashGrayscale = mBmpOps.toGrayscale(bitmapFlash);

        int height = bitmapFlashGrayscale.getHeight();
        int width = bitmapFlashGrayscale.getWidth();

        int topLeftX = width / 4;
        int topLeftY = height / 4;

        int bottomRightX = 3 * (width / 4);
        int bottomRightY = 3 * (height / 4);

        Log.i("FeatureComputer", "Top Left X: " + topLeftX + ", Top Left Y: " + topLeftY + "Bottom Right X: " + topLeftX + ", Bottom Right Y: " + bottomRightY);

        int[] topLeftCorner = new int[]{topLeftX, topLeftY};
        int[] bottomRightCorner = new int[]{bottomRightX, bottomRightY};

        // centroid, max, mean, and histogram already computed here, because pixels have to be read out anyway --> not really slower;
        Pixels pixels = mBmpOps.getGrayscalePixelsAndFeatures(bitmapFlashGrayscale, topLeftCorner, bottomRightCorner, (boolean) featurePreferences.get(Constants.IMAGE_CONTRAST));

        bitmapFlash.recycle();
        bitmapFlashGrayscale.recycle();

        //Glossiness final step
        if((boolean)featurePreferences.get(Constants.GLOSSINESS))
        {
            //mImageGlossiness = Math.log(1 + computeImageGlossinessFlash(pixels));
            mImageGlossiness = computeImageGlossinessFlash(pixels);
            Log.i(CLASS_NAME, "ImageGlossiness: " + mImageGlossiness);
        }
        else
        {
            mImageGlossiness = Double.NaN;
        }
    }

    private double[] computeDominantColorAndDominantSaturation(float[][][] pixelsHSV) {
        // first value: color; second value: saturation

        float dominantColor = 0;
        float dominantSaturation = 0;
        double[] values = new double[2];

        int dimY = pixelsHSV.length;
        int dimX = pixelsHSV[0].length;

        for(int i=0; i<dimY; i++) {
            for(int j=0; j<dimX; j++) {

                dominantColor += pixelsHSV[i][j][0];
                dominantSaturation += pixelsHSV[i][j][1];
            }
        }

        Log.i(CLASS_NAME, "dominantColor: " + dominantColor);
        Log.i(CLASS_NAME, "saturation: " + dominantSaturation);

        dominantColor = dominantColor / (dimX * dimY);
        dominantSaturation = dominantSaturation / (dimX * dimY);

        if(dominantColor > 350) {
            dominantColor = 360 - dominantColor;
        }

        values[0] = dominantColor;
        values[1] = dominantSaturation;

        return values;

    }

    private double computeImageContrast(Pixels pixels) {
        //ImageContrast
        double imageContrast = 0;
        int[] histogram = pixels.getHistogram();

        for(int i=0; i<256; i++) {
            imageContrast = imageContrast + (histogram[i] * Math.pow((i/255.0-(pixels.getMeanNorm())),2));
        }

        imageContrast = (1.0/((pixels.getPixels().length * pixels.getPixels()[0].length) - 1)) * imageContrast;

        return imageContrast;
    }

    private double computeImageGlossiness(Pixels pixels) {

        short[][] p = pixels.getPixels();

        double threshold = 0.6;
        double sumLowPixels = 0;
        double sumHighPixels = 0;

        if(pixels.getMaxNorm() < threshold) {
            Log.i("FeatureComputer", "maxPixelValue: " + pixels.getMaxNorm() + " is smaller than threshold: " + threshold);
            threshold = pixels.getMaxNorm();
        }

        Log.i("FeatureComputer", "maxPixelValue: " + pixels.getMaxNorm());
        Log.i("FeatureComputer", "threshold: " + threshold);

        for(int i=0; i<p.length; i++) {
            for(int j=0; j<p[0].length; j++) {
                if((p[i][j]/255.0) >= threshold) {
                    sumHighPixels++;
                } else {
                    sumLowPixels++;
                }
            }
        }

        Log.i("FeatureComputer", "sumHighPixels: " + sumHighPixels);
        Log.i("FeatureComputer", "sumLowPixels: " + sumLowPixels);

        Log.i("FeatureComputer", "numberOfPixelsTotal: " + (p.length*p[0].length));

        sumHighPixels = sumHighPixels/((p.length*p[0].length));
        sumLowPixels = sumLowPixels/((p.length*p[0].length));

        Log.i("FeatureComputer", "sumHighPixels: " + sumHighPixels);
        Log.i("FeatureComputer", "sumLowPixels: " + sumLowPixels);

        //return (sumHighPixels/sumLowPixels);
        return (sumHighPixels);
    }

    private double computeImageGlossinessFlash(Pixels pixels) {

        short[][] p = pixels.getPixels();

        double threshold = 0.95;//mGlossinessThreshold;
        double sumLowPixels = 0;
        double sumHighPixels = 0;

        Log.i("FeatureComputer", "maxPixelValue: " + pixels.getMaxNorm());
        Log.i("FeatureComputer", "threshold: " + threshold);

        for(int i=0; i<p.length; i++) {
            for(int j=0; j<p[0].length; j++) {
                if((p[i][j]/255.0) >= threshold) {
                    sumHighPixels++;
                } else {
                    sumLowPixels++;
                }
            }
        }

        Log.i("FeatureComputer", "sumHighPixels: " + sumHighPixels);
        Log.i("FeatureComputer", "sumLowPixels: " + sumLowPixels);

        Log.i("FeatureComputer", "numberOfPixelsTotal: " + (p.length*p[0].length));

        sumHighPixels = sumHighPixels/((p.length*p[0].length));
        sumLowPixels = sumLowPixels/((p.length*p[0].length));

        Log.i("FeatureComputer", "sumHighPixels: " + sumHighPixels);
        Log.i("FeatureComputer", "sumLowPixels: " + sumLowPixels);

        //mImageGlossiness = sumNpFlashHigh/sumNoFlashLow; computed in process() function of this class
        //return (sumHighPixels/sumLowPixels / mImageGlossiness);
        return (sumHighPixels);
    }

    private double[] computeImage1DimAndSpectralCentroid(Pixels p, boolean computeImage1Dim, boolean computeSpectralCentroid) {

        short[][] pixels = p.getPixels();

        //FrequencyBinCount1
        int w = pixels[0].length;
        int w_new = w;
        int diff = 0;

        while(!((w_new & (w_new - 1)) == 0)) {
            w_new--;
        }

        w_new = 256;

        diff = w-w_new;

        double[] image1Dim = new double[w_new];
        double[] image2Dim = new double[w_new];
        int start = (int)Math.floor(diff/2);
        int end = start + w_new - 1;

        Log.i("FeatureComputer", "Start: " + start + ", End: " + end);
        Log.i("FeatureComputer", "pixels.length: " + pixels.length);


        for(int i=start; i<=end; i++)  {
            for(int j=0; j<pixels.length; j++) {
                image1Dim[i-start] = image1Dim[i-start] + pixels[j][i] / 255.0;
            }
            image1Dim[i-start] = image1Dim[i-start] / (double)pixels.length;
        }
        for(int j=start; j<end; j++) {
            for(int i=0; i<=pixels.length-121; i++)  {
                //Log.i("FeatureComputer", "i j " + i + " " + j);
                image2Dim[j-start] = image2Dim[j-start] + pixels[j][i] / 255.0;
            }
            image2Dim[j-start] = image2Dim[j-start] / (double)(pixels.length-121);
        }


        FFT magnitudeFFT = new FFT(FFT.FFT_MAGNITUDE, w_new);
        magnitudeFFT.transform(image1Dim, null);
        FFT magnitudeFFT2 = new FFT(FFT.FFT_MAGNITUDE, w_new);
        magnitudeFFT2.transform(image2Dim, null);


        for(int i=0; i<w_new; i++)
        {
            image1Dim[i] = image1Dim[i]+image2Dim[i];
            if( i < 3)  image1Dim[i] = 0;
        }
        //// TODO: 17.10.2016 use normalization also in Matlab
        /*double maxValImageFFT = 0;

        for(int i=0; i<image1Dim.length; i++) {
            if(image1Dim[i] > maxValImageFFT) {
                maxValImageFFT = image1Dim[i];
            }
        }

        for(int i=0; i<image1Dim.length; i++) {
            image1Dim[i] = image1Dim[i] / maxValImageFFT;
        }*/

        double binCount1 = 0;

        if(computeImage1Dim) {

            for (int i = 0; i < 8; i++)
            {
                if (i < Math.ceil(image1Dim.length / 2.0))
                {
                    binCount1 = binCount1 + image1Dim[i];
                }
            }

            binCount1 = binCount1 / 8;
        } else {
            binCount1 = Double.NaN;
        }

        double spectralCentroid = 0;

        if(computeSpectralCentroid) {
            double maxValue = 0;
            double sum = 0;
            for (int i = 0; i < Math.ceil(image1Dim.length / 2.0); i++) {
                //Log.i("FeatureComputer", "Image1Dim: " + (i) + "   " + image1Dim[i]) ;
                if (image1Dim[i] > maxValue) {
                    maxValue = image1Dim[i];
                }
            }

            Log.i("FeatureComputer", "maxValue of image1Dim: " + maxValue);

            for (int i = 0; i < Math.ceil(image1Dim.length / 2.0); i++) {
                image1Dim[i] = image1Dim[i] / maxValue;
                //Log.i("FeatureComputer", "Image1Dim: " + i + "   " + image1Dim[i]) ;
                sum = sum + image1Dim[i];
            }

            Log.i("FeatureComputer", "sum of image1Dim: " + sum);

            for (int i = 0; i < Math.ceil(image1Dim.length / 2.0); i++) {
                spectralCentroid = spectralCentroid + ((i + 1) * image1Dim[i]);
            }

            Log.i("FeatureComputer", "spectralCentroid 1: " + spectralCentroid);

            spectralCentroid = spectralCentroid / sum;
        } else {
            spectralCentroid = Double.NaN;
        }

        double[] values = new double[2];
        values[0] = binCount1;
        values[1] = spectralCentroid;

        return values;
    }

    public double getImageGlossiness() {
        return mImageGlossiness;
    }

    public double getImageCentroid() {
        return mImageCentroid;
    }

    public double getImageContrast() {
        return mImageContrast;
    }

    public double getDominantColor() {
        return mDominantColor;
    }

    public double getDominantSaturation() {
        return mDominantSaturation;
    }

    public double getImageFrequencyBinCount1() {
        return mImageFrequencyBinCount1;
    }

    public double getImageSpectralCentroid() {
        return mImageSpectralCentroid;
    }

}
