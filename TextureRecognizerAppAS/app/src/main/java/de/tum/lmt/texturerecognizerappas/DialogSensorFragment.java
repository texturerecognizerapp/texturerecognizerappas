package de.tum.lmt.texturerecognizerappas;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

public class DialogSensorFragment extends DialogFragment {
	
	private static final String TAG = DialogSensorFragment.class.getSimpleName();

    private static final String ACCEL_AVAILABLE = "accelAvailable";
    private static final String LIN_ACCEL_AVAILABLE = "linAccelAvailable";
    private static final String GRAV_AVAILABLE = "gravAvailable";
    private static final String GYRO_AVAILABLE = "gyroAvailable";
    private static final String MAGNET_AVAILABLE = "magnetAvailable";
    private static final String ROTVEC_AVAILABLE = "rotVecAvailable";
    private static final String EXTERN_ACCEL_AVAILABLE = "externAccelAvailable";
    private static final String FSR_AVAILABLE = "fsrAvailable";

	private CheckBox mAccelBox;
    private CheckBox mLinAccelBox;
	private CheckBox mGravBox;
	private CheckBox mGyroBox;
	private CheckBox mMagnetBox;
	private CheckBox mRotVecBox;
	private CheckBox mExternAccelBox;
    private CheckBox mFSRBox;
    private CheckBox mVelocityBox;
    private CheckBox mOnlineBox;
	
	private boolean mAccelAvailable = false;
    private boolean mLinAccelAvailable = false;
	private boolean mGravAvailable = false;
	private boolean mGyroAvailable = false;
	private boolean mMagnetAvailable = false;
	private boolean mRotVecAvailable = false;
    private boolean mExternAccelAvailable = true;
    private boolean mFSRAvailable = true;

    private boolean mUseAccel = false;
    private boolean mUseLinAccel = false;
	private boolean mUseGrav = false;
	private boolean mUseGyro = false;
	private boolean mUseMagnet = false;
	private boolean mUseRotVec = false;
	private boolean mUseExternAccel = false;
    private boolean mUseFSR = false;
    private boolean mUseVelocity = false;
    private boolean mOnlineComputation = false;
	
	public interface iOnDialogButtonClickListener {
        public void onFinishSensorDialog(String buttonClicked);
    }
	
	/*public DialogSensorFragment(Context context, boolean accelAvailable, boolean linAccelAvailable, boolean gravAvailable, boolean gyroAvailable, boolean magnetAvailable, boolean rotVecAvailable, boolean externAccelAvailable, boolean fSRAvailable) {
		mContext = context;
		mAccelAvailable = accelAvailable;
        mLinAccelAvailable = accelAvailable;
		mGravAvailable = gravAvailable;
		mGyroAvailable = gyroAvailable;
		mMagnetAvailable = magnetAvailable;
		mRotVecAvailable = rotVecAvailable;
        mExternAccelAvailable = externAccelAvailable;
        mFSRAvailable = fSRAvailable;
	}*/

    public static DialogSensorFragment newInstance(boolean accelAvailable, boolean linAccelAvailable, boolean gravAvailable, boolean gyroAvailable, boolean magnetAvailable, boolean rotVecAvailable, boolean externAccelAvailable, boolean fSRAvailable) {

        Bundle args = new Bundle();

        args.putBoolean(ACCEL_AVAILABLE, accelAvailable);
        args.putBoolean(LIN_ACCEL_AVAILABLE, linAccelAvailable);
        args.putBoolean(GRAV_AVAILABLE, gravAvailable);
        args.putBoolean(GYRO_AVAILABLE, gyroAvailable);
        args.putBoolean(MAGNET_AVAILABLE, magnetAvailable);
        args.putBoolean(ROTVEC_AVAILABLE, rotVecAvailable);
        args.putBoolean(EXTERN_ACCEL_AVAILABLE, externAccelAvailable);
        args.putBoolean(FSR_AVAILABLE, fSRAvailable);

        DialogSensorFragment fragment = new DialogSensorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View content = inflater.inflate(R.layout.dialog_sensor, null);

        mAccelAvailable = getArguments().getBoolean(ACCEL_AVAILABLE);
        mLinAccelAvailable = getArguments().getBoolean(LIN_ACCEL_AVAILABLE);
        mGravAvailable = getArguments().getBoolean(GRAV_AVAILABLE);
        mGyroAvailable = getArguments().getBoolean(GYRO_AVAILABLE);
        mMagnetAvailable = getArguments().getBoolean(MAGNET_AVAILABLE);
        mRotVecAvailable = getArguments().getBoolean(ROTVEC_AVAILABLE);
        mExternAccelAvailable = getArguments().getBoolean(EXTERN_ACCEL_AVAILABLE);
        mFSRAvailable = getArguments().getBoolean(FSR_AVAILABLE);

        mAccelBox = (CheckBox) content.findViewById(R.id.accel_box);
        mLinAccelBox = (CheckBox) content.findViewById(R.id.lin_accel_box);
        mGravBox = (CheckBox) content.findViewById(R.id.grav_box);
        mGyroBox = (CheckBox) content.findViewById(R.id.gyro_box);
        mMagnetBox = (CheckBox) content.findViewById(R.id.magnet_box);
        mRotVecBox = (CheckBox) content.findViewById(R.id.rotvec_box);
        mExternAccelBox = (CheckBox) content.findViewById(R.id.extern_accel_box);
        mFSRBox = (CheckBox) content.findViewById(R.id.fsr_box);
        mVelocityBox = (CheckBox) content.findViewById(R.id.velocity_box);
        mOnlineBox = (CheckBox) content.findViewById(R.id.online_box);

        // read shared preferences and check CheckBoxes accordingly so that the CheckBoxes show the settings
        // of the last use of the app when the app gets opened for the next time
        final SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mUseAccel = sharedPrefs.getBoolean(Constants.PREF_KEY_ACCEL_SELECT, true);
        mUseLinAccel = sharedPrefs.getBoolean(Constants.PREF_KEY_LIN_ACCEL_SELECT, true);
        mUseGrav = sharedPrefs.getBoolean(Constants.PREF_KEY_GRAV_SELECT, false);
        mUseGyro = sharedPrefs.getBoolean(Constants.PREF_KEY_GYRO_SELECT, false);
        mUseMagnet = sharedPrefs.getBoolean(Constants.PREF_KEY_MAGNET_SELECT, false);
        mUseRotVec = sharedPrefs.getBoolean(Constants.PREF_KEY_ROTVEC_SELECT, false);
        mUseExternAccel = sharedPrefs.getBoolean(Constants.PREF_KEY_EXTERN_ACCEL, false);
        mUseFSR = sharedPrefs.getBoolean(Constants.PREF_KEY_FSR, false);
        mUseVelocity = sharedPrefs.getBoolean(Constants.PREF_KEY_VELOCITY, false);
        mOnlineComputation = sharedPrefs.getBoolean(Constants.PREF_KEY_ONLINE, false);

        //// TODO: 13.03.2016 if accel not isAvailable close app or disable all functionalities
        //Accel immer verwenden
        if(mAccelAvailable) {
        	mUseAccel = true;
        	mAccelBox.setClickable(false);
        	mAccelBox.setEnabled(false);
        }

        if(mLinAccelAvailable) {
            mUseLinAccel = true;
        }
        
        mAccelBox.setChecked(mUseAccel);
        mLinAccelBox.setChecked(mUseLinAccel);
        mGravBox.setChecked(mUseGrav);
        mGyroBox.setChecked(mUseGyro);
        mMagnetBox.setChecked(mUseMagnet);
        mRotVecBox.setChecked(mUseRotVec);
        mExternAccelBox.setChecked(mUseExternAccel);
        mFSRBox.setChecked(mUseFSR);
        mVelocityBox.setChecked(mUseVelocity);
        mOnlineBox.setChecked(mOnlineComputation);

        if(!mAccelAvailable) {
        	mAccelBox.setEnabled(false);
        	mAccelBox.setClickable(false);
            mAccelBox.setChecked(false);
            mUseAccel = false;
        }

        if(!mLinAccelAvailable) {
            mLinAccelBox.setEnabled(false);
            mLinAccelBox.setClickable(false);
            mLinAccelBox.setChecked(false);
            mUseLinAccel = false;
        }

        if(!mGravAvailable) {
        	mGravBox.setEnabled(false);
        	mGravBox.setClickable(false);
            mGravBox.setChecked(false);
            mUseGrav = false;
        }
        if(!mGyroAvailable) {
        	mGyroBox.setEnabled(false);
        	mGyroBox.setClickable(false);
            mGyroBox.setChecked(false);
            mUseGyro = false;
        }
        if(!mMagnetAvailable) {
        	mMagnetBox.setEnabled(false);
        	mMagnetBox.setClickable(false);
            mMagnetBox.setChecked(false);
            mUseMagnet = false;
        }
        if(!mRotVecAvailable) {
        	mRotVecBox.setEnabled(false);
        	mRotVecBox.setClickable(false);
            mRotVecBox.setChecked(false);
            mUseRotVec = false;
        }
        if(!mExternAccelAvailable) {
            mExternAccelBox.setEnabled(false);
            mExternAccelBox.setClickable(false);
            mExternAccelBox.setChecked(false);
            mUseExternAccel = false;
        }
        if(!mFSRAvailable) {
            mFSRBox.setEnabled(false);
            mFSRBox.setClickable(false);
            mFSRBox.setChecked(false);
            mUseFSR = false;
        }

        
        //onClickListener for CheckBoxes to check/uncheck them and activate/deactivate their features
        mAccelBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = !mAccelBox.isChecked();
                if (checked) {
                    Log.i(TAG, "checked");
                    //can't uncheck Accel
                    //mAccelBox.setChecked(false);
                    //mUseAccel = false;
                    Toast.makeText(getActivity(), getString(R.string.accel_click), Toast.LENGTH_LONG);
                } else {
                    Log.i(TAG, "not checked");
                    mAccelBox.setChecked(true);
                    mUseAccel = true;
                }
            }
        });

        mLinAccelBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = !mLinAccelBox.isChecked();
                if (checked) {
                    Log.i(TAG, "checked");
                    mLinAccelBox.setChecked(false);
                    mUseLinAccel = false;
                    Toast.makeText(getActivity(), getString(R.string.accel_click), Toast.LENGTH_LONG);
                } else {
                    Log.i(TAG, "not checked");
                    mLinAccelBox.setChecked(true);
                    mUseLinAccel = true;
                }
            }
        });
        
        mGravBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = !mGravBox.isChecked();
                if (checked) {
                    Log.i(TAG, "checked");
                    mGravBox.setChecked(false);
                    mUseGrav = false;
                } else {
                    Log.i(TAG, "not checked");
                    mGravBox.setChecked(true);
                    mUseGrav = true;
                }
            }
        });
        
        mGyroBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = !mGyroBox.isChecked();
                if (checked) {
                    Log.i(TAG, "checked");
                    mGyroBox.setChecked(false);
                    mUseGyro = false;
                } else {
                    Log.i(TAG, "not checked");
                    mGyroBox.setChecked(true);
                    mUseGyro = true;
                }
            }
        });
        
        mMagnetBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = !mMagnetBox.isChecked();
                if (checked) {
                    Log.i(TAG, "checked");
                    mMagnetBox.setChecked(false);
                    mUseMagnet = false;
                } else {
                    Log.i(TAG, "not checked");
                    mMagnetBox.setChecked(true);
                    mUseMagnet = true;
                }
            }
        });
        
        mRotVecBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = !mRotVecBox.isChecked();
                if (checked) {
                    Log.i(TAG, "checked");
                    mRotVecBox.setChecked(false);
                    mUseRotVec = false;
                } else {
                    Log.i(TAG, "not checked");
                    mRotVecBox.setChecked(true);
                    mUseRotVec = true;
                }
            }
        });
        
        mExternAccelBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = !mExternAccelBox.isChecked();
                if (checked) {
                    Log.i(TAG, "checked");
                    mExternAccelBox.setChecked(false);
                    mUseExternAccel = false;
                } else {
                    Log.i(TAG, "not checked");
                    mExternAccelBox.setChecked(true);
                    mUseExternAccel = true;
                }
            }
        });

        mFSRBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = !mFSRBox.isChecked();
                if (checked) {
                    Log.i(TAG, "checked");
                    mFSRBox.setChecked(false);
                    mUseFSR = false;
                } else {
                    Log.i(TAG, "not checked");
                    mFSRBox.setChecked(true);
                    mUseFSR = true;
                }
            }
        });

        mVelocityBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = !mVelocityBox.isChecked();
                if (checked) {
                    Log.i(TAG, "checked");
                    mVelocityBox.setChecked(false);
                    mUseVelocity = false;
                } else {
                    Log.i(TAG, "not checked");
                    mVelocityBox.setChecked(true);
                    mUseVelocity = true;
                }
            }
        });

        mOnlineBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = !mOnlineBox.isChecked();
                if (checked) {
                    Log.i(TAG, "checked");
                    mOnlineBox.setChecked(false);
                    mOnlineComputation = false;
                } else {
                    Log.i(TAG, "not checked");
                    mOnlineBox.setChecked(true);
                    mOnlineComputation = true;
                }
            }
        });

        builder.setView(content)

                .setMessage(getString(R.string.select_sensors))

                //save values to Shared Preferences so they can be read again in the CommandsActivity and the selected features can be activated
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        updatePrefs(sharedPrefs);

                        iOnDialogButtonClickListener activity = (iOnDialogButtonClickListener) getActivity();
                        activity.onFinishSensorDialog(getString(R.string.ok));
                    }
                })

                .setNeutralButton(R.string.settings, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        updatePrefs(sharedPrefs);

                        iOnDialogButtonClickListener activity = (iOnDialogButtonClickListener) getActivity();
                        activity.onFinishSensorDialog(getString(R.string.settings));
                    }
                })

                //dialog dismissed automatically, close activity (action taken in implementation of
                // iOnDialogButtonClickListener in CommandsActivity itself)
                .setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        updatePrefs(sharedPrefs);

                        iOnDialogButtonClickListener activity = (iOnDialogButtonClickListener) getActivity();
                        activity.onFinishSensorDialog(getString(R.string.close));
                    }
                });

        builder.setView(content);

        Dialog sensorDialog = builder.create();
        sensorDialog.setCanceledOnTouchOutside(false);

        return sensorDialog;
    }

    private void updatePrefs(SharedPreferences sharedPrefs) {
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putBoolean(Constants.PREF_KEY_ACCEL_SELECT, mUseAccel);
        editor.putBoolean(Constants.PREF_KEY_LIN_ACCEL_SELECT, mUseLinAccel);
        editor.putBoolean(Constants.PREF_KEY_GRAV_SELECT, mUseGrav);
        editor.putBoolean(Constants.PREF_KEY_GYRO_SELECT, mUseGyro);
        editor.putBoolean(Constants.PREF_KEY_MAGNET_SELECT, mUseMagnet);
        editor.putBoolean(Constants.PREF_KEY_ROTVEC_SELECT, mUseRotVec);
        editor.putBoolean(Constants.PREF_KEY_EXTERN_ACCEL, mUseExternAccel);
        editor.putBoolean(Constants.PREF_KEY_FSR, mUseFSR);
        editor.putBoolean(Constants.PREF_KEY_VELOCITY, mUseVelocity);
        editor.putBoolean(Constants.PREF_KEY_ONLINE, mOnlineComputation);
        editor.commit();
    }
}
