package de.tum.lmt.texturerecognizerappas;

import android.graphics.Bitmap;

import java.io.File;

/**
 * Created by Kev94 on 20.09.2016.
 */
public class ResultItem {

    private String mName;
    private Bitmap mPicture;
    private double mQuantifier;
    private File mDir;

    public ResultItem(Bitmap image, String name, double quantifier, File dir) {
        super();
        mPicture = image;
        mName = name;
        mQuantifier = quantifier;
        mDir = dir;
    }

    public Bitmap getImage() {
        return mPicture;
    }

    public void setImage(Bitmap image) {
        mPicture = image;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public double getQuantifier() {
        return mQuantifier;
    }

    public void setQuantifier(double quantifier) {
        mQuantifier = quantifier;
    }

    public File getDir() {
        return mDir;
    }
}
