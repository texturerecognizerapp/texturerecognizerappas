package de.tum.lmt.texturerecognizerappas;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Kev94 on 29.01.2016.
 */
public class Bluetooth {

    private static final String TAG = "Bluetooth";

    private Context mContext;

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothDevice mBluetoothDevice;
    private BluetoothSocket mSocket;

    private Thread mWorkerThread;

    private OutputStream mOutputStream;
    private InputStream mInputStream;
    volatile boolean stopWorker;

    int readBufferPosition = 0;
    byte[] readBuffer = new byte[1024];

    StringBuilder mSB;
    String mData;

    //private SensorLog mAccelLog;
    private List<SensorLog> mAccelLogList;
    private List<Integer> mFSRValues = new ArrayList<>();

    List<Byte> mByteListAccel = new ArrayList<>();
    List<Byte> mByteListFSR = new ArrayList<>();

    int[] mCheckArray = new int[2];

    float[] accelArray = new float[3];

    int mAxisCounter = 0;
    int mRecordingCounter = 0;

    boolean mReceivingAccelValues = true;
    String mAwaitCheckValue;

    boolean mCheckMode;

    public Bluetooth(Context context) {
        mContext = context;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public Bluetooth(Context context, boolean checkMode) {
        mContext = context;
        mSB = new StringBuilder();

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        //mAccelLog = new SensorLog(Constants.TYPE_EXTERN_ACCEL);
        mAccelLogList = new ArrayList<>();

        mCheckMode = checkMode;

        mAwaitCheckValue = "none";
    }

    public boolean isAvailable() {
        if(mBluetoothAdapter == null) {
            return false;
        } else {
            return true;
        }
    }

    public boolean isEnabled() {
        if(mBluetoothAdapter != null) {
            return mBluetoothAdapter.isEnabled();
        } else {
            return false;
        }
    }

    public void findBluetooth() {

        if(mBluetoothAdapter == null)
        {
            Toast.makeText(mContext, "no Bluetooth isAvailable", Toast.LENGTH_LONG).show();
        }

        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        if(pairedDevices.size() > 0)
        {
            for(BluetoothDevice device : pairedDevices)
            {
                Log.i("TAG", "Current found device: " + device.getName());
                if(device.getName().equals("HC-06"))
                {
                    mBluetoothDevice = device;
                    Toast.makeText(mContext, "found HC-06", Toast.LENGTH_LONG).show();
                    break;
                }
                else
                {
                    Log.e("TAG","No BT module found!!!");
                }
            }
        }
    }

    void openBT() throws IOException
    {
        Log.i("TAG","3");
        UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"); //Standard SerialPortService ID
        Log.i("TAG", "4");
        if(mBluetoothDevice != null)
        {
            mSocket = mBluetoothDevice.createRfcommSocketToServiceRecord(uuid);
            Log.i("TAG", "5");
            mSocket.connect();
            Log.i("TAG","6");
            mOutputStream = mSocket.getOutputStream();
            mInputStream = mSocket.getInputStream();
            Log.i("TAG", "7");
            beginListenForData();

            Log.i(TAG, "Bluetooth device HTC-06 opened!");
        }
        else
        {
            Log.i(TAG, "No Bluetooth device could be opened...");
        }
    }

    void beginListenForData()
    {
        final Handler handler = new Handler();
        final byte delimiter = 78; //This is the ASCII code for N

        stopWorker = false;
        readBufferPosition = 0;
        readBuffer = new byte[1024];
        mWorkerThread = new Thread(new Runnable()
        {
            public void run()
            {
                while(!Thread.currentThread().isInterrupted() && !stopWorker)
                {
                    try
                    {
                        int bytesAvailable = mInputStream.available();
                        if(bytesAvailable > 0)
                        {
                            byte[] packetBytes = new byte[bytesAvailable];
                            mInputStream.read(packetBytes);
                            for(int i=0;i<bytesAvailable;i++)
                            {
                                byte b = packetBytes[i];

                                if(!mCheckMode) {
                                    mRecordingCounter++;

                                    //Log.i(TAG, "b: " + (int) b);

                                    if ((int) b == -128) {
                                        //Log.i("Count", "" + mRecordingCounter);
                                        mReceivingAccelValues = false;
                                    } else if ((int) b == 127) {
                                        //Log.i("Count", "" + mRecordingCounter);
                                        mReceivingAccelValues = true;
                                    }

                                    if (mReceivingAccelValues) {
                                        mByteListAccel.add(b);
                                    } else {
                                        mByteListFSR.add(b);
                                    }
                                } else {
                                    if(mAwaitCheckValue.equals("none")) {
                                        if ((char)b == 'a') {
                                            Log.i(TAG, "a received");
                                            mAwaitCheckValue = "accel";
                                        } else if((char)b == 'f') {
                                            mAwaitCheckValue = "fsr";
                                            Log.i(TAG, "f received");
                                        }
                                    } else if(mAwaitCheckValue.equals("accel")) {
                                        mCheckArray[0] = (int)b;
                                        mAwaitCheckValue = "none";
                                        Log.i(TAG, "extern accel isAvailable: " + mCheckArray[0]);
                                    } else if(mAwaitCheckValue.equals("fsr")) {
                                        mCheckArray[1] = (int)b;
                                        mAwaitCheckValue = "none";
                                        Log.i(TAG, "fsr isAvailable: " + mCheckArray[1]);
                                    }
                                }
                            }
                        }
                    }
                    catch (IOException ex)
                    {
                        stopWorker = true;
                    }
                }
            }
        });

        mWorkerThread.start();
    }

    void closeBT() throws IOException
    {
        stopWorker = true;
        if(mOutputStream != null)
            mOutputStream.close();
        if(mInputStream != null)
            mInputStream.close();
        if(mSocket != null)
            mSocket.close();
        Log.i(TAG, "Bluetooth device closed!");
    }

    public List<SensorLog> getExternAccelLog(int step) {

        if(stopWorker) {

            int mListCounter = 0;
            mAccelLogList.add(new SensorLog(Constants.TYPE_EXTERN_ACCEL, step));

            int i = 0;
            int sizeOfList = mByteListAccel.size();

            Log.i(TAG, "size of List accel: " + sizeOfList);

            while(i < sizeOfList) {

                if(mByteListAccel.get(0) == 127) {
                    mByteListAccel.remove(0);
                    mAccelLogList.add(new SensorLog(Constants.TYPE_EXTERN_ACCEL, step));
                    mListCounter++;
                    i++;
                } else {
                    accelArray[0] = 0f;
                    accelArray[1] = 0f;
                    accelArray[2] = (float) (int) mByteListAccel.get(0);
                    mByteListAccel.remove(0);

                    mAccelLogList.get(mListCounter).addTimestamp(0);
                    mAccelLogList.get(mListCounter).addValues(accelArray);

                    i++;
                }
            }

            //Log.i(TAG, "i accel: " + i + ", number of accel Values: " + mAccelLog.getValues().size());

            return mAccelLogList;
        }

        return null;
    }

    /*public SensorLog get3AxisExternAccelLog() {

        if(stopWorker) {

            mAxisCounter = 0;

            int i = 0;
            int sizeOfList = mByteListAccel.size();

            Log.i(TAG, "size of List accel: " + sizeOfList);

            while(i < sizeOfList) {

                if(mAxisCounter < 3) {
                    accelArray[mAxisCounter] = (float)(int) mByteListAccel.get(0);
                    mByteListAccel.remove(0);
                    mAxisCounter++;
                    i++;
                } else if(mAxisCounter == 3) {
                    mAccelLog.addTimestamp(0);
                    mAccelLog.addValues(accelArray);
                    //Log.i(TAG, "v1: " + accelArray[0] + ", v2: " + accelArray[1] + ", v3: " + accelArray[2]);
                    mAxisCounter = 0;
                }
            }

            Log.i(TAG, "i accel: " + i + ", number of accel Values: " + mAccelLog.getValues().size());

            return mAccelLog;
        }

        return null;
    }*/

    List<Integer> getFSRValues() {

        if(stopWorker) {

            int i = 0;
            int sizeOfList = mByteListFSR.size();

            while(i < sizeOfList) {
                if(mByteListFSR.get(0) != -128) {
                    mFSRValues.add((int) mByteListFSR.get(0));
                }
                mByteListFSR.remove(0);
                i++;
            }

            Log.i(TAG, "i FSR: " + i + ", FSRValues size: " + mFSRValues.size());

            return mFSRValues;

        }

        return null;
    }

    int[] getCheckData() {

        if(stopWorker) {
            return mCheckArray;
        }
        return null;
    }

    public void sendData(String stringToSend) throws IOException
    {
        if(mOutputStream != null) {
            mOutputStream.write(stringToSend.getBytes());
        }
    }

    public void resetData() {
        mAccelLogList = new ArrayList<>();
        mFSRValues = new ArrayList<>();
    }
}
