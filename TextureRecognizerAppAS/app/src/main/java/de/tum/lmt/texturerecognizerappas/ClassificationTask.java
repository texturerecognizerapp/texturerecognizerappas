package de.tum.lmt.texturerecognizerappas;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Kev94 on 16.03.2016.
 */
public class ClassificationTask extends AsyncTask<Void, Void, Exception> {

    private iOnClassificationTaskFinishedListener mListener;

    File mDatabaseDir;
    String mClassificationMethod;
    int mNeighborhood;
    Set<String> mFeatureStringSet;
    List<String> mFeatureStringList;
    Features mFeatures;
    List<Texture.Sample> mResult;

    MathUtil mMathUtil;

    public ClassificationTask(String featurePath, Features features, String classificationMethod, Set<String> featureStringSet, iOnClassificationTaskFinishedListener listener) {
        String databasePath = (new File(featurePath)).getParentFile().getParentFile().getParentFile().getAbsolutePath();
        mDatabaseDir = (new File(databasePath + File.separator + Constants.DATABASE_FOLDER_NAME + File.separator + Constants.JSON_DATABASE_FILENAME));
        Log.i("Classification", "database dir: " + mDatabaseDir.getAbsolutePath());
        mClassificationMethod = classificationMethod;
        mFeatureStringSet = featureStringSet;
        mFeatureStringList = new ArrayList<>();

        //use List instead of set to ensure that order is the same
        for(String featureString:mFeatureStringSet) {
            mFeatureStringList.add(featureString);
        }

        mFeatures = features;

        mListener = listener;

        mMathUtil = new MathUtil();
    }

    public ClassificationTask(String featurePath, Features features, String classificationMethod, int neighborhood, Set<String> featureStringSet, iOnClassificationTaskFinishedListener listener) {
        String databasePath = (new File(featurePath)).getParentFile().getParentFile().getParentFile().getAbsolutePath();
        mDatabaseDir = (new File(databasePath + File.separator + Constants.DATABASE_FOLDER_NAME + File.separator + Constants.JSON_DATABASE_FILENAME));
        Log.i("Classification", "database dir: " + mDatabaseDir.getAbsolutePath());
        mClassificationMethod = classificationMethod;
        mNeighborhood = neighborhood;
        mFeatureStringSet = featureStringSet;
        mFeatureStringList = new ArrayList<>();

        //use List instead of set to ensure that order is the same
        for(String featureString:mFeatureStringSet) {
            mFeatureStringList.add(featureString);
        }

        mFeatures = features;

        mListener = listener;

        mMathUtil = new MathUtil();

        Log.i("ClassificationTask", "MFCC1 rec: " + features.getMFCC1());
        Log.i("ClassificationTask", "Roll off rec: " + features.getSoundSpectralRollOffTap());
    }

    public interface iOnClassificationTaskFinishedListener {
        void onClassifierTaskFinished(List<Texture.Sample> result);
    }

    @Override
    protected Exception doInBackground(Void... params) {

        switch(mClassificationMethod) {
            case Constants.CLASSIFICATION_DIST_TO_MEAN:
                mResult = classifyDistToMean(mFeatures);
                break;
            case Constants.CLASSIFICATION_MAHALANOBIS:
                mResult = classifyMahalanobis(mFeatures);
                break;
            case Constants.CLASSIFICATION_KNN:
                mResult = classifyKNN(mFeatures);
                break;
        }

        return null;
    }

    @Override
    protected void onPostExecute(Exception Result) {
        if(mListener != null) {
            mListener.onClassifierTaskFinished(mResult);
        }
    }

    private List<Texture.Sample> classifyDistToMean(Features features) {

        MathUtil mathUtil = new MathUtil();

        JSONHelper jsonHelper = new JSONHelper();
        List<Texture> textures = jsonHelper.readMeanTexturesFromDatabase(mDatabaseDir);
        Log.i("Classification", "size of texture list: " + textures.size());
        List<Texture.Sample> samples = new ArrayList<>();

        if (textures != null) {

            double distance;

            for (Texture texture : textures) {

                for(Texture.Sample sample:texture.getSamples()) {
                    if(features == null) {
                        Log.i("Classification", "features null");
                    }
                    if(mFeatureStringList == null) {
                        Log.i("Classification", "mFeatureStringList null");
                    }

                    //Log.i("ClassificationTask", "feature rec: " + features.getFeaturesArray(mFeatureStringList)[0] + ", " + features.getFeaturesArray(mFeatureStringList)[1] + ", "
                    //            + features.getFeaturesArray(mFeatureStringList)[2] + ", " + features.getFeaturesArray(mFeatureStringList)[3]);/* + ", " + features.getFeaturesArray(mFeatureStringList)[4] + ", "
                    //            + features.getFeaturesArray(mFeatureStringList)[5]);*/

                    /*Log.i("ClassificationTask", "feature db: " + sample.getFeatures().getFeaturesArray(mFeatureStringList)[0] + ", " + sample.getFeatures().getFeaturesArray(mFeatureStringList)[1] + ", "
                            + sample.getFeatures().getFeaturesArray(mFeatureStringList)[2] + ", " + sample.getFeatures().getFeaturesArray(mFeatureStringList)[3] + ", "
                            + sample.getFeatures().getFeaturesArray(mFeatureStringList)[4] + ", " + sample.getFeatures().getFeaturesArray(mFeatureStringList)[5]);*/

                    distance = mathUtil.getVectorDistanceEuclidean(features.getFeaturesArray(mFeatureStringList), sample.getFeatures().getFeaturesArray(mFeatureStringList));
                    //Log.i("ClassificationTask", "distance: " + mMathUtil.roundDigits(distance,3));
                    sample.setDistance(mMathUtil.roundDigits(distance,3));
                    samples.add(sample);
                    /*Log.i("Classification", "Name: " + sample.getName() + ", Number: " + sample.getNumber() + ", feature name: " + mFeatureStringList.get(0) +
                            ", feature value: " +  sample.getFeatures().getFeaturesArray(mFeatureStringList)[0] + ", Distance: " + sample.getDistance());*/
                }
            }

            Log.i("Classification", "sort");

            samples = sortSampleList(samples, Constants.SORT_DISTANCE);

            for (Texture texture : textures) {

                /*for(Texture.Sample sample:texture.getSamples()) {
                    Log.i("Classification", "Name: " + sample.getName() + ", Number: " + sample.getNumber() + ", Distamce: " + sample.getDistance());
                }*/
            }

        } else {
            Log.e("Classiication", "texture null");
        }

        return samples;
    }

    private List<Texture.Sample> classifyMahalanobis(Features features) {

        MathUtil mathUtil = new MathUtil();

        JSONHelper jsonHelper = new JSONHelper();
        List<Texture> textures = jsonHelper.readTexturesFromDatabase(mDatabaseDir, true);
        List<Texture> resultTextures = new ArrayList<>();
        List<Texture.Sample> resultSamples = new ArrayList<>();

        int i = 0;

        if(textures != null) {

            List<Texture.Sample> samples;
            List<Texture.Sample> samplesWithoutMean = new ArrayList<>();
            Texture.Sample meanSample;
            int Mk;

            for(Texture texture:textures) {

                samples = texture.getSamples();
                samplesWithoutMean.clear();

                double[][] Wk = new double[mFeatureStringSet.size()][mFeatureStringSet.size()];
                meanSample = null;
                Mk = 0;

                for(Texture.Sample sample:samples) {
                    if (sample.getNumber().equals(Constants.MEAN_SAMPLE_NUMBER)) {
                        Mk++;
                        meanSample = sample;
                    } else {
                        samplesWithoutMean.add(sample);
                    }
                }

                for(Texture.Sample sample:samplesWithoutMean) {
                    Wk = mathUtil.addMatrix(Wk, mathUtil.computeVectorProductToMatrix(sample.getFeatures().getFeaturesArray(mFeatureStringList), sample.getFeatures().getFeaturesArray(mFeatureStringList)));
                }

                Wk = mathUtil.divideMatrixByDouble(Wk, Mk);

                //Wk = mathUtil.subtractMatrix(Wk, mathUtil.computeVectorProductToMatrix(meanSample.getFeatures().getFeaturesArray(mFeatureStringList), meanSample.getFeatures().getFeaturesArray(mFeatureStringList)));

                StringBuilder sb = new StringBuilder();
                for(double[] dd:Wk) {
                    for(double d:dd) {
                        sb.append(Double.toString(d) + "\t");
                    }
                    sb.append("\n");
                }

                /*FileHandler fh = new FileHandler();
                fh.writeDataToTextFile(sb.toString(), mDatabaseDir.getParentFile().getAbsolutePath(), "matrix" + i + ".txt");
                i++;*/

                RealMatrix Wk_mat = MatrixUtils.createRealMatrix(Wk);
                LUDecomposition Wk_lu = new LUDecomposition(Wk_mat);
                RealMatrix Wk_inv_mat = Wk_lu.getSolver().getInverse();

                double[][] Wk_inv = Wk_inv_mat.getData();

                double[] differenceVector = mathUtil.subtractVectors(features.getFeaturesArray(mFeatureStringList), meanSample.getFeatures().getFeaturesArray(mFeatureStringList));

                Texture newResultTexture = new Texture(texture.getName());
                double distance = Math.sqrt(mathUtil.dotProduct(mathUtil.multiplyTransposedVectorByMatrix(differenceVector, Wk_inv), differenceVector));
                Log.i("Classification", "distance: " + distance);
                newResultTexture.addSampleWithDistance(mMathUtil.roundDigits(distance, 3));

                resultTextures.add(newResultTexture);

            }

        } else {
            Log.e("Classiicaion", "texture null");
        }

        for(Texture texture:resultTextures) {
            if(!texture.getSamples().isEmpty()) {
                resultSamples.add(texture.getSamples().get(0));
            }
        }

        resultSamples = sortSampleList(resultSamples, Constants.SORT_DISTANCE);

        Log.i("Classification", "number of resultSamples: " + resultSamples.size());
        return resultSamples;
    }

    private List<Texture.Sample> classifyKNN(Features features) {

        MathUtil mathUtil = new MathUtil();

        JSONHelper jsonHelper = new JSONHelper();
        List<Texture> textures = jsonHelper.readTexturesFromDatabase(mDatabaseDir, false);
        List<Texture.Sample> samples = new ArrayList<>();
        List<Texture.Sample> hood = new ArrayList<>();

        if (textures != null) {

            double distance;

            for (Texture texture : textures) {

                for(Texture.Sample sample:texture.getSamples()) {
                    distance = mathUtil.getVectorDistanceEuclidean(features.getFeaturesArray(mFeatureStringList), sample.getFeatures().getFeaturesArray(mFeatureStringList));
                    sample.setDistance(distance);
                    samples.add(sample);
                }
            }

            samples = sortSampleList(samples, Constants.SORT_DISTANCE);

            /*for(Texture.Sample sample:samples) {
                Log.i("Classification", "name: " + sample.getName() + ", number: " + sample.getNumber() + ", distance: " + sample.getDistance());
            }*/

            if(samples.size() > mNeighborhood-1) {
                samples = samples.subList(0, mNeighborhood);
            }

            Log.i("Classification", "cut");

            /*for(Texture.Sample sample:samples) {
                Log.i("Classification", "name: " + sample.getName() + ", number: " + sample.getNumber() + ", distance: " + sample.getDistance());
            }*/

            boolean contains;

            for(Texture.Sample sample:samples) {
                contains = false;
                for(Texture.Sample neighbor:hood) {
                    if(sample.getName().equals(neighbor.getName())) {
                        contains = true;
                        neighbor.setOccurence(neighbor.getOccurence()+1);
                        break;
                    }
                }
                if(!contains) {
                    sample.setOccurence(1);
                    hood.add(sample);
                }
            }

            hood = sortSampleList(hood, Constants.SORT_OCCURENCE);


        } else {
            Log.e("Classiicaion", "texture null");
        }

        return hood;
    }

    public List<Texture.Sample> sortSampleList(List<Texture.Sample> samples, int field) {

        switch(field) {
            case Constants.SORT_DISTANCE:
                for(int i = 1; i < samples.size(); i++) {
                    for(int j = 0; j < i; j++) {
                        if(samples.get(i).getDistance() < samples.get(j).getDistance()) {
                            samples.add(j, samples.get(i));
                            samples.remove(i + 1);
                        }
                    }
                }
                break;
            case Constants.SORT_OCCURENCE:
                for(int i = 1; i < samples.size(); i++) {
                    for(int j = 0; j < i; j++) {
                        if(samples.get(i).getOccurence() > samples.get(j).getOccurence()) {
                            samples.add(j, samples.get(i));
                            samples.remove(i+1);
                        }
                    }
                }
                break;
        }


        return samples;
    }
}
