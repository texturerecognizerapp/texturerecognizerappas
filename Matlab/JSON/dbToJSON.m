function [ ] = dbToJSON( db, maxima, minima, textureNames, featureNames )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    %% Databse json file
    
    dbSize = size(db);
    featureSize = numel(featureNames);
    
    featureMean = zeros(1,featureSize);
    
    jsonStringDB = '{';
    
    for i=1:(dbSize(1)/10)
        
        jsonStringDB = [jsonStringDB, '"', textureNames{i,1}, '":{']; 
        
        for j=1:10
            jsonStringDB = [jsonStringDB, '"', num2str(j), '":{'];
            
            for k=1:featureSize
                if k<featureSize 
                    jsonStringDB = [jsonStringDB, '"', featureNames{k}, '": ', num2str(db((i-1)*10+j, k)), ','];
                else
                    jsonStringDB = [jsonStringDB, '"', featureNames{k}, '": ', num2str(db((i-1)*10+j, k)), '}'];
                end
            end
            
            jsonStringDB = [jsonStringDB, ','];
            
            featureMean = featureMean + db((i-1)*10+j, :);
        end
        
        featureMean = featureMean ./ 10;
        
        jsonStringDB = [jsonStringDB, '"mean":{'];
            
            for k=1:featureSize
                if k<featureSize 
                    jsonStringDB = [jsonStringDB, '"', featureNames{k}, '": ', num2str(featureMean(k)), ','];
                else
                    jsonStringDB = [jsonStringDB, '"', featureNames{k}, '": ', num2str(featureMean(k)), '}'];
                end
            end
        
        jsonStringDB = [jsonStringDB, '}'];
        
        if i<(dbSize(1)/10)
            jsonStringDB = [jsonStringDB, ','];
        end
        
    end
    
    fileID = fopen([pwd, '\JSONdatabase.txt'], 'w');
    fprintf(fileID, '%s', jsonStringDB);
    fclose(fileID);
    
    %% maxima and minima json file
    
    jsonStringMinMax = '{';
    
    numel(maxima)
    numel(featureNames)
    
    for i=1:numel(maxima)
        
        if(i==numel(maxima))
            jsonStringMinMax = [jsonStringMinMax, '"', featureNames{i}, '":{', ...
                '"Minimum":', num2str(minima(i)), ',"Maximum":', num2str(maxima(i)), '}'];
        else
            jsonStringMinMax = [jsonStringMinMax, '"', featureNames{i}, '":{', ...
                '"Minimum":', num2str(minima(i)), ',"Maximum":', num2str(maxima(i)), '},'];
        end
        
    end
    
    jsonStringMinMax = [jsonStringMinMax, '}']

    fileID = fopen([pwd, '\JSONMinMax.txt'], 'w');
    fprintf(fileID, '%s', jsonStringMinMax);
    fclose(fileID);
    
end

