
X = featureSpace_Training(1:10, 21:22)
X2 = featureSpace_Training(11:20, 21:22)

scatter(X(:,1), X(:,2), 'r', 'filled');
hold on
scatter(X2(:,1), X2(:,2), 'g', 'filled');
axis([0 0.4 -0.2 0.2]);

C1 = zeros(2,2);
C2 = zeros(2,2);

m1 = mean(X)';
m2 = mean(X2)';

scatter(m1(1), m1(2), 'bx');
scatter(m2(1), m2(2), 'bx');

for i=1:10
    C1 = C1 + X(i,:)' * X(i,:);
    C2 = C2 + X2(i,:)' * X2(i,:);
end

C1 = (1/10) * C1 - (m1 * m1');
C2 = (1/10) * C2 - (m2 * m2');

C1 = eye(2);
%C2 = eye(2);

x = linspace(0,0.4);
y = linspace(-0.2,0.2);
[x,y] = meshgrid(x,y);
Z = zeros(100,100);

for i=1:10000
        d1 = ([x(i);y(i)] - m1)' * inv(C1) * ([x(i);y(i)] - m1); 
        d2 = ([x(i);y(i)] - m2)' * inv(C2) * ([x(i);y(i)] - m2);
        
%         if(d1 > d2)
%             Z(i) =  0;
%         else 
%             Z(i) =  1;
%         end

        Z(i) = d1;
end

surf(x,y,Z);

hold off