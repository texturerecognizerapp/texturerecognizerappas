function [ ] = plotdct4dim( folder, file, index )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    if exist([folder,file], 'file')
        M = importdata(strcat(folder,file));
        d = size(M);
        if index==3
            x = M(1:d(1), index);
        else
            x = M(1500:d(1), index);
        end
        figure; stem(abs(dct(x,5000))); title(['dct', file]); axis([0 1200 0 inf]);
    end
    
end

