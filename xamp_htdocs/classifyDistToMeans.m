function [ minIndices, minDistances ] = classifyDistToMeans( f1, f2, distMeasure )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

    % compute means of feature values
    dbFeaturesSize = size(f2);
    
    means = zeros(dbFeaturesSize(1)/10, dbFeaturesSize(2));
    
    for i=1:dbFeaturesSize(2)
        for j=1:10:dbFeaturesSize(1)
            
            means(ceil(j/10), i) = mean(f2([j:(j+9)], i));
            
        end
    end
    
    d = 1000 .* ones(size(means,1),1);
    
    if(strcmp(distMeasure,'Euclidean'))
        
        for i=1:size(means,1)
            d(i) = sqrt((f1 - means(i,:)) * (f1 - means(i,:))');
        end
        
    end
    
    if(strcmp(distMeasure,'Mahalanobis'))
        
        for j=1:size(means,1)
            
            C = zeros(size(f1,2));
        
            for i=((j-1)*10 + 1):((j-1)*10 + 10)
                C = C + f2(i,:)' * f2(i,:);
            end
            
            C = (1/10) * C - (means(j,:)' * means(j,:));
            
            d(j) = sqrt((f1 - means(j,:))/C * (f1 - means(j,:))');
        end
        
    end
    
    if(~(strcmp(distMeasure,'Euclidean') || strcmp(distMeasure,'Mahalanobis')))
        error('distMeasure has to be either equal to "Euclidean" or to "Mahalanobis"');
    end
    
    [minDistances, minIndices] = sort(d, 'ascend');

end

