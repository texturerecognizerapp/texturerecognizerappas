function [ indices, occurences ] = classifyKNN( f1, f2, k )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    %compute distances
    d = zeros(1,size(f2,1));

    for i=1:size(f2,1)
        d(i) = norm(f1-f2(i,:));
    end

    %sort distances
    [minVal, minIndex] = sort(d, 'ascend');
    
    % get number of occurence of k nearest neighbors and their indices
    minTextureIndex = ceil(minIndex(1:k)/10);
    [occurencesUnsorted,textureIndices] = hist(minTextureIndex,unique(minTextureIndex));
    
    M = [occurencesUnsorted',textureIndices'];
    
    [Y,I]=sort(M(:,1), 'descend');
    
    B=M(I,:);
    
    indices = B(:,2);
    occurences = B(:,1);
end

