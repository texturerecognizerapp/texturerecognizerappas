function [ ] = plotdct3dim( folder, file )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

    if exist([folder,file], 'file')
        M = importdata(strcat(folder,file));
        d = size(M);
        x = M(1:d(1), 2);
        figure; stem(abs(dct(x,5000))); title(['dct', file]); axis([0 1200 0 inf]);
    end

end

