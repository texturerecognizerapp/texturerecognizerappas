function [ tmp ] = Smartphone_SoundTraining( querySound )
%   Description:        
%       This function calculates the training and test data sets for
%       further Machine Learning processing and evaluation
%
%   Input:  acceleration struct 
%   Output: 
%           ...
%
%   Algorithm
%
%
%
%
%   References
%
%           [1] .................
%

%   Author: .....................



    % Ensure correct number of inputs
    if( nargin~=  1), help Smartphone_SoundTraining; 
        return; 
    end; 

    % Check, if CBTR_Constants has already been loaded for global variables
    if ( any(strcmp(who,'numTextures')) == 0)
       Smartphone_Constants 
    end
    
    load CBSR_Training_NormalisationMinima_Android_MFCCS;
    load CBSR_Training_NormalisationMaxima_Android_MFCCS;
    
    tmp = [Smartphone_SoundProcessQuery(querySound,'',0) ];
    %tmp = abs(0.0001 .* randn(numSoundFeatures,1));
        
    %% only add if more than 10 queries per textures a required.
    % add random noise to previous instance line and save it, just makes it
    % harder to classify - random noise is a good test for robustness
    % for the entire algorithm
    tmp = tmp + abs(0.0001 .* randn(numel(tmp),1));
    %%%%%%%CBSR_Training_PlainFile = [CBSR_Training_PlainFile , tmp];
    
    for i=1:numel(minima);
        tmp(i) = (tmp(i) - minima(i)) / (maxima(i) - minima(i));
    end
end

