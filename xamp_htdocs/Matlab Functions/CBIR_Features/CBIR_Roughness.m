function [ output_asm ] = CBIR_Roughness( input_image, ttitle, plotIt )
%
% 
% Description: CBTR_Roughness calculates the roughness of the image by
%              doing the following steps:
%              1) seperate the input grayscaled image into Roughness and
%              Waviness components using a 2D Gaussian low pass
%              filter(sigma = 2.54mm).               Why and how??????????
%              2) calculates the GLCM for four directions
%              3) estimate the roughness using angular second moment (ASM)
% 
% Input: input_image 
%        grayscaled image
%
% Output: output_asm
%         ASM for this input image as roughness parameter
%
% Author: Moyuan Sun 
% 
% First edition: 11,11,2014
% 
% Last adjustment: 13,11,2014
% 
% Reference: Heather Culbertson. 'Modeling and Rendering Realistic Textures from Unconstrained
%            Tool-Surface Interactions.' IEEE Transactioins on Haptics, Vol.7, No.3, 2014
% 
% Notice: (1)The GLCM has been calculated for four directions:
%         0 grad ;
%           ---> 
%         45 grad ^ ; 
%                /           
%               /          
%         90 grad ^ ; 
%                 |                
%                 |                
%         and 135 grad ^   
%                       \
%                        \
%         And sumed up for the normalization.
%         (2)The input is grayscaled            
%

% figure
% imshow(input_image)

%% mit Gaußfilter 

hsize = size(input_image);
sigma = 0.5;                                                               % sigma=0,5 ist default. Vllt braucht man nen geeigneten Wert für jedes Bild?
h = fspecial('gaussian', hsize, sigma);
Image = imfilter(input_image,h,'replicate');

imageGlcm0 = graycomatrix(Image,'Offset',[0 1],'Symmetric',true);    
imageGlcm45 = graycomatrix(Image,'Offset',[-1 1],'Symmetric',true);
imageGlcm90 = graycomatrix(Image,'Offset',[-1 0],'Symmetric',true);
imageGlcm135 = graycomatrix(Image,'Offset',[-1 -1],'Symmetric',true);

%% ohne Gaußfilter

% imageGlcm0 = graycomatrix(input_image,'Offset',[0 1],'Symmetric',true);    
% imageGlcm45 = graycomatrix(input_image,'Offset',[-1 1],'Symmetric',true);
% imageGlcm90 = graycomatrix(input_image,'Offset',[-1 0],'Symmetric',true);
% imageGlcm135 = graycomatrix(input_image,'Offset',[-1 -1],'Symmetric',true);

%% mit Medianfilter (ohne Gauß-)

% Image = medfilt2(input_image);                                           % mit der Property kann man vllt auch bissl herumspielen.
% 
% imageGlcm0 = graycomatrix(Image,'Offset',[0 1],'Symmetric',true);    
% imageGlcm45 = graycomatrix(Image,'Offset',[-1 1],'Symmetric',true);
% imageGlcm90 = graycomatrix(Image,'Offset',[-1 0],'Symmetric',true);
% imageGlcm135 = graycomatrix(Image,'Offset',[-1 -1],'Symmetric',true);
%%
imageGLCM = imageGlcm0 + imageGlcm45 + imageGlcm90 + imageGlcm135;
imageGLCM = imageGLCM./sum(imageGLCM(:));

% figure
% mesh(imageGLCM)

asm = (imageGLCM)^2;
output_asm = sum(asm(:));








if plotIt == 1
    [Ny,Nx] = size(input_image);
    Ng = 255;
    % GLCM computation
    P_H  = graycomatrix(input_image, 'NumLevels',Ng, 'GrayLimits', [], 'offset', [-1 -1], 'Symmetric', true);
    R = 2*Ny*(Nx-1); % neighbroing resolution cell pairs
    p_H = P_H./max(max(P_H))  %R

    size(p_H);
    figure;imshow(p_H);
end




end

