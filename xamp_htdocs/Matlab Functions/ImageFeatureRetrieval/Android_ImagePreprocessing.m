function [ preprocessedImage ] = Smartphone_ImagePreprocessing( I, useHistEqualization, ttitle, plotIt )
%CBIR_PREPROCESSING


preprocessedImage = struct;


%% cutting 
try
    I = I(251:1250,251:1250,:);
catch exc
end
[height, width,numChannels] = size(I);
%% converting 2 double
I       = im2double(I);
colorI  = I;
grayI   = rgb2gray(I);
if useHistEqualization == 1
    grayI   = histeq(grayI);
end

%figure; imshow(grayI)




%% point processing section (global)
pointI              = imsubtract(grayI, 128);
pointI2             = imadd(immultiply(grayI,0.5),128);
pointI3             = imcomplement(grayI);

histogramGrayI      = imhist(grayI);


histogramRedI       = imhist(colorI(:,:,1));
histogramGreenI     = imhist(colorI(:,:,2));
histogramBlueI      = imhist(colorI(:,:,3));



%% filtering section

fLow        = fspecial('gaussian',[5 5],3);
gaussianI   = (filter2(fLow,grayI));

fHigh       = fspecial('sobel');
sobelI      = (filter2(fHigh,grayI));
sobelEdgesI = edge(grayI,'sobel');


fHigh       = fspecial('prewitt');
prewittI    = (filter2(fHigh,grayI));

cannyI2     = edge((grayI),'canny', [],2);
cannyI5     = edge((grayI),'canny', [],5);


fUnsharp    = fspecial('unsharp',0.5);
unsharpI    = (filter2(fUnsharp,grayI) );



% fff2 section
dft2I = fftshift(fft2(grayI));

% stopband = 5:50;
% dft2I(round(width/2) + stopband,:)=0;
% dft2I(round(width/2) - stopband,:)=0;
% dft2I(:,round(height/2) + stopband)=0;
% dft2I(:,round(height/2) - stopband)=0;
% stopBandI = abs(ifft2(dft2I));



%% GLCM section
% strong main diagonal: homogenous texture
% the farther an entry is away from the main diagonal, the higher its pixel
% difference!

directions = [0 1; -1 1;-1 0;-1 -1];
[glcms, ~] = graycomatrix(grayI,'Offset',directions,'Symmetric', true);

statsDir1   = graycoprops(glcms(:,:,1));
statsDir2   = graycoprops(glcms(:,:,2));



preprocessedImage.height            = height;
preprocessedImage.width             = width;
preprocessedImage.numChannels       = numChannels;
preprocessedImage.colorI            = colorI;
preprocessedImage.grayI             = grayI;

preprocessedImage.histogramRedI     = histogramRedI;
preprocessedImage.histogramGreenI   = histogramGreenI;
preprocessedImage.histogramBlueI    = histogramBlueI;
preprocessedImage.histogramGrayI    = histogramGrayI;

preprocessedImage.sobelI            = sobelI;
preprocessedImage.sobelEdgesI       = sobelEdgesI;
preprocessedImage.prewittI          = prewittI;
preprocessedImage.gaussianI         = gaussianI;
preprocessedImage.cannyI2           = cannyI2;
preprocessedImage.cannyI5           = cannyI5;

preprocessedImage.dft2I             = dft2I;

preprocessedImage.glcms             = glcms;
preprocessedImage.statsDir1         = statsDir1;
preprocessedImage.statsDir2         = statsDir2;

%preprocessedImage
%preprocessedImage
%preprocessedImage


if plotIt == 1
    
    figure; imshow(colorI); title(strcat('',ttitle))    
%     figure; imshow(grayI); title(strcat('',ttitle))
% 
%     %figure; imshow(pointI); title(strcat('',ttitle))
%     
%     figure;imhist(colorI(:,:,1)); title(strcat('Histogram...','Red'))
%     figure;imhist(colorI(:,:,2)); title(strcat('Histogram...','Green'))
%     figure;imhist(colorI(:,:,3)); title(strcat('Histogram...','Blue'))
% 
%     
%     
%     figure; imshow(prewittI); title('Prewitt');
%     figure; imshow(sobelI); title('Sobel');
%     figure; imshow(cannyI2); title('Canny 2');
     figure; imshow(cannyI5); title('Canny 5');   
    
%figure;imhist(rgb2gray(I)); title(strcat('Histogram...','Red'))
%figure; fftshow(dft2I,'log'); title(ttitle);
    %figure; imshow(mat2gray(stopBandI))
   
end


end














