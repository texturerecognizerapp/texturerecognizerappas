function [ CBIR_Training_PlainFile,CBIR_Training_TextureCentroids ] = Android_ImageTraining( queryImage )

% CBTR_Image
%
%   Description
%       Calculates and stores image instance data, will be used by
%       CBTR_Training
%
%
%   Inputs
%       queryImage
%           image data struct
%
%
%   Outputs
%
%       imageQueryVector
%           image instance plain file
%
%
%
%   Algorithm
%
%   References
%
%           [1] .................
%

%   Author: Matti Strese 2014

%% PRELIMINARIES %%


% Ensure correct number of inputs
if( nargin~= 1 ), help Android_ImageTraining;
    return;
end;


if ( any(strcmp(who,'numTextures')) == 0)
    Smartphone_Constants
end

% local variables
CBIR_Training_TextureCentroids    = zeros(numTextures,numImageFeatures,2);
CBIR_Training_PlainFile           = [];





% iterate through all pictures
for k=1:(numQueries*numTextures)
    
    disp(strcat(num2str(k/(10*numTextures)*100),' % calculated.'));
    
    tmp = Android_ImageProcessing(queryImage(k).noFlash.dataTraining,queryImage(k).noFlash.nameTraining,0);
    %tmp = abs(0.0001 .* randn(numImageFeatures,1));
    CBIR_Training_PlainFile = [CBIR_Training_PlainFile , tmp];
    
    %% only add if more than 10 queries per textures a required.
    % add random noise to previous instance line and save it, just makes it
    % harder to classify - random noise is a good test for robustness
    % for the entire algorithm
    tmp = tmp + abs(0.0001 .* randn(numel(tmp),1));
    
end

CBIR_Training_PlainFile = CBIR_Training_PlainFile';

save 'MatlabData/CBIR_Training_PlainFileWithoutNormalisation.mat' CBIR_Training_PlainFile


% calc mean for each numQueries following columns
for m=1:numTextures
    
    beginS = m * numQueries - (numQueries-1);
    endS   = m * numQueries;
    
    tmpMatr = CBIR_Training_PlainFile(beginS:endS,:);
    
    CBIR_Training_TextureCentroids(m,:,1) = mean(tmpMatr);
    CBIR_Training_TextureCentroids(m,:,2) = std(tmpMatr);
    %tmpVec = q(beginS:endS,11);
    %tmpVec(tmpVec == 0) = [];
    %CBSR_TextureCentroids(m,11) = mean(tmpVec);
end




% global normalisation
minima = zeros(numImageFeatures,1);
maxima = zeros(numImageFeatures,1);

for k=1:numImageFeatures
    minima(k,1) = min(CBIR_Training_PlainFile(:,k));
    maxima(k,1) = max(CBIR_Training_PlainFile(:,k));
end



for l=1:numImageFeatures
    for m=1:numInstances
        CBIR_Training_PlainFile(m,l) = CBTR_Normalise(CBIR_Training_PlainFile(m,l),minima(l,1),maxima(l,1));
    end
end
save 'MatlabData/CBIR_Training_NormalisationMinima.mat' minima
save 'MatlabData/CBIR_Training_NormalisationMaxima.mat' maxima


save 'MatlabData/CBIR_Training_TextureCentroids.mat' CBIR_Training_TextureCentroids
save 'MatlabData/CBIR_Training_PlainFile.mat' CBIR_Training_PlainFile

end





