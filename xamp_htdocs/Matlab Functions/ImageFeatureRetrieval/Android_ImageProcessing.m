function [ imageFeatureVector ] = Android_ImageProcessing( surfaceImageNoFlash, ttitle,plotIt )




if ( any(strcmp(who,'numTextures')) == 0)
    Smartphone_Constants
end




imageFeatureVector = zeros(numImageFeatures,1);



%%  Preprocess Image
[preprocessedImageNoFlash] = Android_ImagePreprocessing(surfaceImageNoFlash,0,ttitle,0);
%[preprocessedImageFlash]   = Smartphone_ImagePreprocessing(surfaceImageFlash,0,ttitle,0);




%%  Features
%% Glossiness
% there are no flash pictures in the smartphone database
% imageFeatureVector(1,1)                               = CBIR_Glossiness(preprocessedImageNoFlash.histogramGrayI,preprocessedImageFlash.histogramGrayI,ttitle,plotIt);

%% Regularity
% [imageFeatureVector(2,1),imageFeatureVector(3,1)]     = CBIR_Regularity(preprocessedImageNoFlash.dft2I,ttitle,0);

%% Line-Likeness
%  imageFeatureVector(4,1)                                = CBIR_LineLikeness(preprocessedImageNoFlash.grayI,ttitle,0);

%% Coarseness
%  imageFeatureVector(5,1)                                = CBIR_Coarseness(preprocessedImageNoFlash.grayI,ttitle,0);
% 
%% Contrast
[imageFeatureVector(6,1)]                               = CBIR_Contrast(preprocessedImageNoFlash.histogramGrayI,ttitle,0);

%% Dominant Color and Dominant Saturation
[imageFeatureVector(7,1),imageFeatureVector(8,1)]       = CBIR_DominantColor(preprocessedImageNoFlash.colorI,ttitle,0);
% 
%% Roughness
[imageFeatureVector(9,1)]                               = CBIR_Roughness(preprocessedImageNoFlash.grayI,ttitle,0);
% 
%% Softness
% [imageFeatureVector(10,1)]                              = CBIR_Softness(preprocessedImageNoFlash.colorI,ttitle,0);

%% Directionality
% [imageFeatureVector(11,1),~]                            = CBIR_Directionality(preprocessedImageNoFlash.grayI);

%% Wiener Entropy
%[imageFeatureVector(12,1),~,~,~]                       = CBIR_WienerEntropy(preprocessedImageNoFlash.dft2I);


%% display features
if plotIt==1
    disp(' ');
    disp(strcat('___',ttitle,'___'));
    disp(' ');
    disp(strcat('Glossiness_____',num2str(imageFeatureVector(1,1))));
    disp(strcat('Regularity_____',num2str(imageFeatureVector(2,1))));
    disp(strcat('MeshSize_______',num2str(imageFeatureVector(3,1))));
    disp(strcat('Linelikeness___',num2str(imageFeatureVector(4,1))));
    disp(strcat('Coarsness_',num2str(imageFeatureVector(5,1))));
    disp(strcat('Contrast_____',num2str(imageFeatureVector(6,1))));
    disp(strcat('DominantColor_______',num2str(imageFeatureVector(7,1))));
    disp(strcat('DominantSaturation_',num2str(imageFeatureVector(8,1))));
    disp(strcat('Roughness______',num2str(imageFeatureVector(9,1))));
    %disp(strcat('Softness_______',num2str(imageFeatureVector(10,1))));
    
    disp(strcat('Entropy________',num2str(imageFeatureVector(12,1))));
    
    disp(' ');
    disp('________________________________________');
    disp(' ');
end




end

