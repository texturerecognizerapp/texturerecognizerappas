function [ class ] = CBSR_GetHardnessClass( signal )
%   Description:        
%       This function calculates the hardness class of a given signal
%
%   Input:  tapping signal of a texture for which i want to know the
%           hardness
%
%   Output: 
%           class: is a value between 1 and 10, describing the hardness of
%           the texure of which the feature vector ist computed of
%           0 is default, if something goes wrong...;-)
%
%   Algorithm
%
%
%
%
%   References
%
%           [1] .................
%

%   Author: Andrea Schuett 2015

%% Preliminaries

    % Ensure correct number of inputs
    if( nargin~=  1), help CBSR_GetHardnessClass; 
        return; 
    end; 
    % Check, if CBTR_Constants has already been loaded for global variables
    if ( any(strcmp(who,'numTextures')) == 0)
       CBTR_Constants 
    end

    class = 0;
    roughClass = 0;
    notfound = true;
    
    
%%  calculation

% Prefilter:
%           If I can detect a special material afront, I dont need to
%           calculate the feature vector

    [signal,~,~,~,~,intensity,~,~] = CBSR_TapSegmentation(signal,'',0);

    if CBSR_TileDetection(signal,intensity) == 1
        class = 10;
        disp('The Matrial is a Stone, Tile, Glass or Marble');
        return;
    end

    if CBSR_WoodDetection(signal,intensity) == 1
        disp('The Material is Wood');
        class = 9;
        return;
    end
    
    if CBSR_MetalDetection(signal,intensity) == 1
        disp('The Material is a Metal');
        class = 10;
        return;
    end
    
    if CBSR_NoiseDetection(signal) == 1
        disp('No tapping, just Noise detected');
        class = 1;
        return;
    end
    
    if CBSR_SoftDetection(signal, intensity) == 1
        disp('The Material is very soft');
        class = 3;
        return;
    end
    
    

           
        %% If I cant detect a special material I calculate the feature
        %  vector and unse some of the features for the hardness mapping
        
    if notfound == true
        
        mfcc = zeros(13,1);
        [~,~,hardness] = CBSR_Temporal_Analysis(signal);
        [~, slope, ~, rolloff, ~, ~] = CBSR_SpectralShape_Analysis(signal);
        [sharpness, loudness] = CBSR_Perceptual_Analysis(signal);
        [distanceHL, ~, ~, ~ ] = CBSR_Frequency_Analysis(signal);
        [~,mfcc(1:13),~,~] = CBSR_Spectroid(signal,'',0);
        mfcc1 = mfcc(1);
        mfcc4 = mfcc(4);
        
        
    if mfcc4 > 2
        disp('The Material is Marble');
        class = 10;
        return;
    end
    
    if sharpness < 1.6
        disp('rubbery/springy texture, cork or carpet');
        class = 7;
        return;
    end
        
    %% Check Loudness for rough classification?
    
    if loudness > 25
        roughClass = 3;
    
    else if loudness > 10
            roughClass = 2;
        else 
            roughClass = 1;
        end
    end
    
%     %% check Intensity first and get rough classification
%         if intensity>0.02
%             % hard material
%             roughClass = 3;
%             
%         else if 0.001<intensity && intensity<0.02
%             % medium hard material
%             roughClass = 2;
%                 
%             else
%                 %soft material
%                 roughClass = 1;
%             end
%         end
      %%   
        if intensity<0.06 && intensity>0.005 && distanceHL > 11000
            disp('The Material is Styropor');
            class = 8;
            notfound = false;
        end

        %% Within the roughnessclass evaluation of the Hardness
        % I will check four features and add up the classvalues, then I'll
        % divide it by four and roud it to the neares integer. Like this
        % I'll compensate for "wrong" classification
        
        cnt = 0;
    while notfound
        cnt = cnt+1;
        switch roughClass
            case 3
                if hardness > 2
                    if mfcc1> (-15)
                        disp('The Matrial is a Stone, Tile or Glass - roughClass3');
                        class = 10;
                        notfound = false;
                        break;
                    else 
                        disp('roughClass 3, hardness > 2 , mfcc1 < -15');
                        class = 9;
                        notfound = false;
                        break;
                    end
                else
                    if hardness > 0.8
                        disp('roughClass 3, hardness > 0.8');
                        class = 8;
                        notfound = false;
                        break;
                    else
                        roughClass = 2;
                        break;
                    end
                end
                
            case 2
                if hardness > 5 
                    roughClass = 3;
                    break;
                end
                if slope < -2*10^-7 && intensity > 0.001
                    disp('The Material is "Wood-Like" - roughClass 2, slope > -9e16 ');
                    class = 8;
                    notfound = false;
                    break;
                else if slope < -0.5*10^-7 && slope > -2*10^-7
                        disp('The Material is some foil, paper or plasik,slope < -0.5*10^-7 && slope > -2*10^-7');
                        class = 7;
                        if intensity < 0.001
                            disp('Foam Foil-like Material Detected!');
                            class = 3;
                            notfound = false;
                            break;
                        end
                        notfound = false;
                        break;
                    end
                end
                if slope > -0.5*10^-7
                    disp('Irgendwie weich...;-) ');
                    class = 4;
                    notfound = false;
                    break;
                end
                
                if loudness < 7.5
                        disp('The Material is a Textile, Foam or soft Rubber, loudness < 6.5');
                        class = 5;
                        notfound = false;
                        break;
                end
%                 if hardness > 0.4
%                         disp('The Material is a Metal');
%                         class = 10;
%                         notfound = false;
                if hardness < 0.55 && hardness > 0.4
                    disp('roughClass 2, 0.55 > hardness > 0.4');
                    class = 7;
                    notfound = false;
                    break;
                else if hardness < 0.3
                        roughClass = 1;
                        break;
                     end
                end
                disp('Folie/Papier detektiert, roughClass 2, keine sonstige Detektion');
                class = 6; %oder hier 7?
                notfound = false;
                
            case 1
                if rolloff < 300
                    disp('The Material is fine or bumpy Foam, roughClass 1, rolloff < 300');
                    class = 4;
                    notfound = false;
                    break;
                end
                if mfcc1 < (-32.5)
                    disp('The Material is very soft, mfcc1 < -32.5');
                    class = 3;
                    notfound = false;
                    break;
                end
                if loudness < 5.5
                    disp('The Material is a Textile, Foam or soft Rubber, loudness < 5.5');
                    class = 4;
                    notfound = false;
                    break;
                else if loudness < 6.5
                        disp('The Material is a Textile, Foam or soft Rubber, loudness < 6.5');
                        class = 5;
                        notfound = false;
                        break;
                     else if loudness < 8
                            disp('The Material is a Textile, Foam or soft Rubber, loudness < 8');
                            class = 6;
                            notfound = false;
                            break;
                         end
                    end
                 end
                disp('roughClass 1, no Detection');
                class = 6;
                notfound = false;
              
        end
        
        if cnt>3
            notfound=false;
        end
        
    end
    end
    
        
if class == 0
    disp('Keine Klasse zugeordnet!');
end


end



function [ IsTile ] = CBSR_TileDetection( signal, intensity )
% CBSR_TileDetection returns a 1 if a very hard material is detected
% Input is only the signal

    % Ensure correct number of inputs
    if( nargin ~= 2 ), help CBSR_TileDetection; 
        return; 
    end; 

    IsTile = false;
    fs = 44100;

%%      

bins = 10;
signal = abs(dct(signal,fs*0.5));
binsize = floor(numel(signal)/bins);
%binned = zeros(bins,1);
binned = [];

for i = 1:binsize:numel(signal)-binsize;
    binned = [binned sum(signal(i:i+binsize))];
end

% h=figure;plot(signal);title('Spektrum eines Ziegels')
% print(h,'-djpeg','Ziegel_Spektrum');
%figure;plot(binned);

    thresh = mean(binned)*0.5;

for i = 5:bins-1 
    if binned(i)>thresh && intensity > 0.001
        IsTile = true;
    end
end
    
    
end






function [ IsMetal ] = CBSR_MetalDetection( signal, intensity )
% CBSR_MetalDetection returns a 1 if a metall material is detected
% Input is only the signal

    if nargin ~= 2
        help CBSR_MetalDetection;
        return;
    end
    
    fs = 44100;
    IsMetal = false;

    spec = abs(dct(signal,fs*0.5));
    
    bins = 30;
    binsize = floor(numel(spec)/bins);
    %binned = zeros(bins,1);
    binned = [];

    for i = 1:binsize:numel(spec)-binsize;
        binned = [binned sum(spec(i:i+binsize))];
    end
    xb = linspace(1,numel(spec),bins-1);
    x = linspace(1,numel(spec),numel(spec));
%    h=figure;plot(x,spec,xb,binned,'or');title('Spektrum eines Metalls')
%    print(h,'-djpeg','Metall_Spektrum');
    
    m = mean(binned(1:6));
    thresh = m*0.9;
    
    for i = 1:6
        if binned(i) > m+thresh || binned(i) < m-thresh
            IsMetal = false;
            break;
        else if intensity > 0.001
            IsMetal= true;
            end
        end
    end
    

end






function [ IsWood ] = CBSR_WoodDetection( signal ,intensity )
% CBSR_WoodDetection returns a 1 if a wooden material is detected
% Input is only the signal

    % Ensure correct number of inputs
    if( nargin ~= 2 ), help CBSR_WoodDetection; 
        return; 
    end; 

    fs = 44100;
    IsWood = false;
    
    %%
   
spec = abs(dct(signal,fs*0.5));

bins = 30;
binsize = floor(numel(spec)/bins);
%binned = zeros(bins,1);
binned = [];
xb = linspace(1,numel(spec),bins-1);
x = linspace(1,numel(spec),numel(spec));

for i = 1:binsize:numel(spec)-binsize;
    binned = [binned sum(spec(i:i+binsize))];
end

% h=figure;plot(x,spec,xb,binned,'or');title('Spektrum eines Holzes');
% print(h,'-djpeg','Holz_Spektrum');

    thresh = mean(binned)*0.8;
    index = 0;
    
temp = binned(1);
for i = 2:9
    if binned(i)<temp && binned(i)<binned(i+1)
        index = i;
        break;
    end
end
    
if index ~= 0
for i = index+1:index+3
    if binned(i)>thresh && intensity >0.001
        if binned(index+2) > binned(index) && binned(index+2) > binned(index+1)
            IsWood = true;
        end
    end
end
end

for i = 15:bins-1
    if binned(i)>thresh*0.8
        IsWood = false;
    end
end

end





function [ IsNoise ] = CBSR_NoiseDetection( signal )
% CBSR_NoiseDetection returns a 1 if only Noise is detected.
% Input is only the signal

    if nargin ~= 1
        help CBSR_NoiseDetection;
        return;
    end
    
    fs = 44100;
    
IsNoise = false;
thresh = 0.5;

spec = abs(dct(signal,fs*0.5));

Exp = ExpError(spec);

if Exp<thresh
    IsNoise = true;
end

end



function [ IsSoft ] = CBSR_SoftDetection( signal ,intensity )
% CBSR_SoftDetection returns a 1 if a very soft material is detected
% Input is only the signal

    if nargin ~= 2
        help CBSR_SoftDetection;
        return;
    end

    fs = 44100;
    
IsSoft = false;
thresh1 = 0.5;
thresh2 = 1.8;

spec = abs(dct(signal,fs*0.5));

Exp = ExpError(spec);

if Exp>thresh1 && Exp<thresh2 && intensity<0.001
    IsSoft = true;
end


end

