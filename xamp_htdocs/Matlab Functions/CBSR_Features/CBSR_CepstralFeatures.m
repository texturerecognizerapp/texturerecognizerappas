function [spectroid,mfcCoeff,lpcCoeff,energyCoeff ] = CBSR_CepstralFeatures( speechSignal,ttitle,plotIt )
%   Description:        
%       This function calculates ...
%
%   Input:  data trace
%           title
%           flag for plotting
%   Output: 
%           ...
%
%   Algorithm
%
%
%
%
%   References
%
%           [1] .................
%

%   Author: .....................



    % Ensure correct number of inputs
    if( nargin~=  3), help CBSR_Spectroid; 
        return; 
    end; 
    % Check, if CBTR_Constants has already been loaded for global variables
    if ( any(strcmp(who,'numTextures')) == 0)
        CBTR_Constants
    end
    
    list1 = [];

    binSumList = [];
    lpcList = [];
    energyList = [];
    frame = 10000;
    
    
    
    %% calculation section
    
    
    for l = 1:frame:numel(speechSignal)-frame

        %% Spectral Centroid Fineness
        data = speechSignal(l:l+frame);
        spec = abs(dct(data));
        %spec = spec ./max(spec);
        list1 = [list1,SpectralCentroid( abs(spec).^2,44100)];


        binList = spectralGaussianBinning(spec);
        binSum = sum(binList);
        binSumList = [binSumList,binSum];
        %figure; 
        %stem(spec);title(strcat(num2str(l),db(k).acc.name));    axis([0 2000 -inf inf]);




        %% lpc
        tmpLpc = [];
        internalFrame = 500;
        for m=1:internalFrame:frame-internalFrame
            size(lpc(data(m:m+internalFrame),13));
            tmpLpc = [tmpLpc;lpc(data(m:m+internalFrame),13)];
        end

        lpcVal = mean(tmpLpc);
        lpcList = [lpcList,lpcVal'];

        if plotIt == 1
            %lpc visualisation
            
            X =fft(data);
            X = X(1:frame/2+1)';
            X = 10*log10(abs(X).^2);
        
            Z = fft(lpcVal,frame);
            Z = sqrt(frame*e)./Z(1:frame/2+1);
            Z = 10*log10(abs(Z).^2);
        
            figure;title(ttitle);
            plot(X');
            hold on;
            plot(Z,'r');
            grid;
        end

        %% Kuchenbecker Romano 30 energy values
        energyList = [energyList,get30Energies(spec)'];

    end

    if plotIt == 1
        CBTR_Plot(strcat('printed_plot_of_',ttitle,'.pdf'),list1); 
    end
    
    

    spectroid = mean( list1 );

    

%    spectroid = CBTR_Normalise(spectroid,55,660); 
    [mfcCoeff] = getMfcc(speechSignal);
    lpcCoeff = mean(lpcList');
    lpcCoeff = lpcCoeff(2:end);
    energyCoeff = mean(energyList');






end


function[average13Coeff] = getMfcc(speechSignal)
    % Check, if CBTR_Constants has already been loaded for global variables
    if ( any(strcmp(who,'numTextures')) == 0)
        CBTR_Constants
    end
    Tw = 5;%25;              % analysis frame duration (ms)
    Ts = 2;%10;                  % analysis frame shift (ms)
    fs = CBSR_SAMPLERATE;
    alpha = 0.97;             % preemphasis coefficient
    R = [ 10 CBSR_SAMPLERATE ];        % frequency range to consider
    M = 20;                   % number of filterbank channels
    C = 13;                   % number of cepstral coefficients
    L = 22;                   % cepstral sine lifter parameter
    nbits = 10;

    % hamming window (see Eq. (5.2) on p.73 of [1])
    hamming = @(N)(0.54-0.46*cos(2*pi*[0:N-1].'/(N-1)));

   
    % Feature extraction (feature vectors as columns)
    [ MFCCs, FBEs, frames ] = CBSR_mfcc( speechSignal, fs, Tw, Ts, alpha, hamming, R, M, C, L );

    

    average13Coeff = mean(MFCCs');

end







function [energies]=get30Energies(spec)

    %calc energy spectrum
    spec = spec.^2;
    
    minF = 0;
    maxF = 1500;
    step = (maxF-minF) / 30;
    energies = [];
    for k=1:step:maxF-step+1
        e = sum(abs(spec(k:k+step)));
        energies = [energies,e];
    end
    
end




function[binList] = spectralGaussianBinning(X)

    a=0.12;
    b= 10;

    binList = [];
    for b = 5:50:1000

        f = 1:1:1000;
        w_f = 0.1 * exp(-0.01*b) .*  exp( -(  ((f-b).^2) / (2*power(a*b,2)) )  );
        binList = [binList,X(1:1000)'.*w_f];

    end



end
























