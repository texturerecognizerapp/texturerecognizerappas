function [mfcc13 ] = CBSR_CepstralFeaturesNew( speech,ttitle,plotIt )
%   Description:        
%       This function calculates ...
%
%   Input:  data trace
%           title
%           flag for plotting
%   Output: 
%           ...
%
%   Algorithm
%
%
%
%
%   References
%
%           [1] .................
%

%   Author: .....................



    % Ensure correct number of inputs
    if( nargin~=  3), help CBSR_CepstralFeaturesNew; 
        return; 
    end; 
    % Check, if CBTR_Constants has already been loaded for global variables
    if ( any(strcmp(who,'numTextures')) == 0)
        CBTR_Constants
    end
    
    size(speech)
    speech = speech ./ range(speech);
    
    
    
    Tw = 50;                % analysis frame duration (ms)
    Ts = 25;                % analysis frame shift (ms)
    alpha = 0.97;           % preemphasis coefficient
    M = 20;                 % number of filterbank channels 
    C = 13;                 % number of cepstral coefficients
    L = 22;                 % cepstral sine lifter parameter
    R = [ 20 7600 ];  % frequency range to consider
    hamming = @(N)(0.54-0.46*cos(2*pi*[0:N-1].'/(N-1)));
    nbits = 10;
    
    fs = 44100;

    
    
    % Feature extraction (feature vectors as columns)
    [ MFCCs, FBEs, frames ] = mfcc( speech, fs, Tw, Ts, alpha, hamming, R, M, C, L );

    
    mfcc13     = MFCCs(2:14);
    mfcc13(12) = mean(MFCCs(2:14));
    mfcc13(13) = std(MFCCs(2:14));
    
    if plotIt == 1
       figure; stem(mfcc13,'black');title(strcat('MFCC:',ttitle)); axis([0 13 -3 3]) 
    end
    
    
    
    
    
end

