function [ X,f ] = CBSR_SignalSpectrum( x, fs, ttitle, plotIt )
% t = 0:1/10000:5;
% x = cos(2*500*pi*t);
% CBSR_SignalSpectrum(x,10000,'',2);


N = length(x);
X = abs(fft(x)) / N;

if plotIt == 1
    figure;
    subplot(211)
    plot(x,'black'); title('Time domain');
    X = fftshift(X);
    
    if mod(N,2) == 0
        f = -N/2 : N/2 - 1;
    else
        f = -(N-1) / 2 : (N-1) / 2;
    end
    f = (fs/2) * f ./ ceil(N/2);
    subplot(212)
    stem(f,X,'black'); title('Spectral Domain');
elseif plotIt == 2
    figure;
    subplot(211)
    plot(x,'black'); title('Time domain');
    X = fftshift(X);
    
    if mod(N,2) == 0
        f = -N/2 : N/2 - 1;
    else
        f = -(N-1) / 2 : (N-1) / 2;
    end
    f = (fs/2) * f ./ ceil(N/2);
    subplot(212)
    stem(f,X,'black'); title('Spectral Domain'); axis([0 ceil(fs/2) -inf inf])
else
    X = X(1:ceil(N/2));
    f = (fs/2) * ( 1:ceil(N/2)) / ceil(N/2) ;
    
end




end

