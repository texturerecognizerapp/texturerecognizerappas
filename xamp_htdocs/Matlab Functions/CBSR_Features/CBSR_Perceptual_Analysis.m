function [loudness, sharpness ] = CBSR_Perceptual_Analysis( impulseData )

% CBTR_HardnessAndFriction
%
%   Description
%
%
%
%   Inputs
%
%
%
%
%
%   Outputs
%
%
%
%
%
%
%   Algorithm
%
%
%
%
%   References
%
%           [1] .................
%

%   Author: Andrea Sch�tt 2015

    %% PRELIMINARIES %% 

    loudness = 0;
    sharpness = 0;


    % Ensure correct number of inputs
    if( nargin~= 1 ), help CBSR_Perceptual_Analysis; 
        return; 
    end; 

    % Check, if CBTR_Constants has already been loaded for global variables
    if ( any(strcmp(who,'numTextures')) == 0)
        CBTR_Constants
    end

      
    %% _____Barkscale______%%
     
%     [sharpnessA, loudnessA] = BarkAnalysis(dataA);
%     [sharpnessD, loudnessD] = BarkAnalysis(dataD);
      [sharpness, loudness] = BarkAnalysis(impulseData);

end




function [ sharpness, loudness ] = BarkAnalysis( data )
%   Description:        
%       This function calculates the loudness and sharpness of the provided
%       data
%
%   Input:  data trace
%           title
%           flag for plotting
%   Output: 
%          first output: sharpness
%          second output: loudness
%
%   Algorithm
%
%
%
%
%   References
%
%           [1] .................
%

%   Author: Andrea Sch�tt 2015

    fs = 44100;

    % Ensure correct number of inputs
    if( nargin~=  1), help BarkAnalysis; 
        return; 
    end; 


%%  calculate barkscale and specific and overall loudness   

    bark = [];
    index = [1];
    Amplitude = zeros(24,1);
   
    data = abs(dct(data,fs/2));

    f = linspace(1,numel(data),numel(data));
    
    for i = 1:numel(f)
        bark = [bark 13*atan(0.00076*f(i))+3.5*atan((f(i)/7500)^2)];
    end
    for i = 2:numel(bark)
        if floor(bark(i-1)) ~= floor(bark(i))
            index = [index i];
        end
    end
    
    for i = 1:ceil(bark(numel(bark)))-1
        beginVal = index(i);
        endVal = index(i+1);
    Amplitude(i) = (sum(data(beginVal:endVal)))^2;
    end
    
    
    Nspec = zeros(24);
    loudness = 0;
    g = Nspec;
    enum = 0;
    
    for k = 1: numel(Amplitude)
        % specific loudness:
        Nspec(k) = Amplitude(k)^0.23;

        % total loudness:
        loudness = loudness+Nspec(k);
        if k<15
            g(k) = 1;
        else
            g(k) = 0.066 * exp(0.171*k);
        end
        
        enum = enum + k*g(k)*Nspec(k);
        
    end

    %% ___ sharpness___%%

    sharpness = 0.11*enum/loudness;


end

