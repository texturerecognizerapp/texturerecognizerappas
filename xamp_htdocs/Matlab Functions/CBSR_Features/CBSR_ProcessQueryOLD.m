function [ queryFeatureVector ] = CBSR_ProcessQuery( query,ttitle,plotIt ) %
%   Description:
%       This function processes a datatrace
%
%   Input:  data trace
%           title
%           flag for plotting
%   Output:
%           ...
%
%   Algorithm
%
%
%
%
%   References
%
%           [1] .................
%

%   Author: .....................


% Ensure correct number of inputs
if( nargin~=  3), help CBSR_ProcessQuery;
    return;
end;



%%  plotIt
if (plotIt == 1)
    CBTR_Plot(strcat('printed_plot_of_',ttitle,'.pdf'),query);
end


% Check, if CBTR_Constants has already been loaded for global variables
if ( any(strcmp(who,'numTextures')) == 0)
    CBTR_Constants
end


% define output vector
queryFeatureVector = zeros(numSoundFeatures,1);


tic;

disp(' ')
disp('--------------------------------------------------------')
disp(ttitle);
disp('--------------------------------------------------------')

% do segmentation and get special/obvious signal features, if they
% exist
[ dataTap,~,dataMovement,~,~,~,~ ] = CBSR_Prefilter( query,ttitle, 0 );





%% calculate query features %%


%% Preprocessing

[ dataTap, dataAscend, dataDescend, dataAscendS, dataDescendS, intensity , ...
    logAttackTime, softness] = CBSR_TapSegmentation( dataTap, ttitle, plotIt );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% disp('Hardness Features:');
% disp('Query Hardness and Impact Spectrum Centroid and energy frames of impact');
%% Temporal Features

queryFeatureVector(1,1) = intensity;                                  %intensity
queryFeatureVector(2,1) = logAttackTime;                              %LogAttackTime
[duration, zcr, hardness] = CBSR_Temporal_Analysis(dataTap)        %duration, zcr, hardness

queryFeatureVector(3,1) = duration;
queryFeatureVector(4,1) = zcr;
queryFeatureVector(5,1) = hardness;

%% Spectral Features

[distanceHL, f1, f2, sp1] = CBSR_Frequency_Analysis(dataTap);       %distanceHL, f1, f2, sp1
[spread, slope, skewness, rolloff, specCentr, highSpecMean] = CBSR_SpectralShape_Analysis(dataTap) 
%spread, slope, skewness, rolloff, specCentr, highSpecMean

queryFeatureVector(6,1) = distanceHL;
queryFeatureVector(7,1) = f1;
queryFeatureVector(8,1) = f2;
queryFeatureVector(9,1) = sp1;
queryFeatureVector(10,1) = spread;
queryFeatureVector(11,1) = slope;
queryFeatureVector(12,1) = skewness;
queryFeatureVector(13,1) = rolloff;
queryFeatureVector(14,1) = specCentr;
queryFeatureVector(15,1) = highSpecMean;

%% Perceptual Features

[loudness, sharpness] = CBSR_Perceptual_Analysis(dataMovement);    %loudness, sharpness
queryFeatureVector(16,1) = loudness;
queryFeatureVector(17,1) = sharpness;








%% Movement Phase




%% Cepstral Features
mfcc = zeros(13,1);
[~,mfcc(1:13),~,~] = CBSR_Spectroid(dataMovement,ttitle, 0); 
queryFeatureVector(18,1) = mfcc(1);
queryFeatureVector(19,1) = mfcc(2);
queryFeatureVector(20,1) = mfcc(3);
queryFeatureVector(21,1) = mfcc(4);
queryFeatureVector(22,1) = mfcc(5);
queryFeatureVector(23,1) = mfcc(6);
queryFeatureVector(24,1) = mfcc(7);
queryFeatureVector(25,1) = mfcc(8);
queryFeatureVector(26,1) = mfcc(9);
queryFeatureVector(27,1) = mfcc(10);
queryFeatureVector(28,1) = mfcc(11);
queryFeatureVector(29,1) = mfcc(12);
queryFeatureVector(30,1) = mfcc(13);






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



[duration, zcr, hardness] = CBSR_Temporal_Analysis(dataMovement)        %duration, zcr, hardness

queryFeatureVector(31,1) = duration;
queryFeatureVector(32,1) = zcr;


%% Spectral Features

[distanceHL, f1, f2, sp1] = CBSR_Frequency_Analysis(dataMovement);       %distanceHL, f1, f2, sp1
[spread, slope, skewness, rolloff, specCentr, highSpecMean] = CBSR_SpectralShape_Analysis(dataMovement) 
%spread, slope, skewness, rolloff, specCentr, highSpecMean

queryFeatureVector(33,1) = distanceHL;
queryFeatureVector(34,1) = f1;
queryFeatureVector(35,1) = sp1;
queryFeatureVector(36,1) = spread;
queryFeatureVector(37,1) = slope;
queryFeatureVector(38,1) = skewness;
queryFeatureVector(39,1) = rolloff;
queryFeatureVector(40,1) = specCentr;
queryFeatureVector(41,1) = highSpecMean;

%% Perceptual Features

[loudness, sharpness] = CBSR_Perceptual_Analysis(dataMovement);    %loudness, sharpness
queryFeatureVector(42,1) = loudness;
queryFeatureVector(43,1) = sharpness;










timeUsed = toc;
disp(strcat('Calculation Time...',num2str(timeUsed),'sec'));

%% further features here.......

% [queryFeatureVector(x,1)] = Sound_FunctionTemplate(data, titleOfTexture, plotIt);


end

