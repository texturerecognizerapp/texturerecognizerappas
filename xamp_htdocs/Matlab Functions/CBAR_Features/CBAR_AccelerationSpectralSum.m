function [ accelerationSpectralSumFeature ] = CBAR_AccelerationSpectralSum( accelData )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

len = length(accelData.X);

while ~(floor(log2(len)) == log2(len))
    len = len - 1;
end

start = length(accelData.X) - len + 1;

magnitudeSpectrum = struct;

magnitudeSpectrum.X = abs(fft(accelData.X(start:end)));
magnitudeSpectrum.Y = abs(fft(accelData.Y(start:end)));
magnitudeSpectrum.Z = abs(fft(accelData.Z(start:end)));

sumX = sum(magnitudeSpectrum.X(10:100));
sumY = sum(magnitudeSpectrum.Y(10:100));
sumZ = sum(magnitudeSpectrum.Z(10:100));

accelerationSpectralSumFeature = sumX + sumY + sumZ;

end

