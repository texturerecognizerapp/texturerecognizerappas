function [ Features,featureNames, averagedFeatures ] = Android_AudioFeatures(signal, fs, frame, step, plotNr)

% function Features = stFeatureExtraction(signal, fs, win, step)
%
% This function computes basic audio feature sequencies for an audio
% signal, on a short-term basis.
%
% ARGUMENTS:
%  - signal:    the audio signal
%  - fs:        the sampling frequency
%  - frame:     short-term window size (in seconds)
%  - step:      short-term step (in seconds)
%
% RETURNS:
%  - Features: a [MxN] matrix, where M is the number of features and N is
%  the total number of short-term windows. Each line of the matrix
%  corresponds to a seperate feature sequence
%
% (c) 2014 T. Giannakopoulos, A. Pikrakis


featureNames =... 
{...
    'Zero Crossing Rate',... 
    'Signal Energy',...
    'Signal Energy Entropy',...
    'Spectral Centroid',...
    'Spectral Spread',...
    'SpecEntropy',...
    'SpcFlux',...
    'SpecRollOff',...
    'MFCC1',...
    'MFCC2',...
    'MFCC3',...
    'MFCC4',...
    'MFCC5',...
    'MFCC6',...
    'MFCC7',...
    'MFCC8',...
    'MFCC9',...
    'MFCC10',...
    'MFCC11',...
    'MFCC12',...
    'MFCC13',...
    'Spectral Harmonic HR',...
    'Spectral Harmonic F0',...
    'Chroma1',...
    'Chroma2',...
    'Chroma3',...
    'Chroma4',...
    'Chroma5',...
    'Chroma6',...
    'Chroma7',...
    'Chroma8',...
    'Chroma9',...
    'Chroma10',...
    'Chroma11',...
    'Chroma12'...  
};






% if STEREO ...
if (size(signal,2) > 1)
    % convert to MONO
    signal = (sum(signal,2)/2); 
end


% convert window length and step from seconds to samples:
windowLength    = round(frame * fs);
step            = round(step  * fs);
curPos          = 1;
L               = length(signal);
% compute the total number of frames:
numOfFrames     = floor((L-windowLength)/step) + 1;
% number of features to be computed:
numOfFeatures   = 35;
Features        = zeros(numOfFeatures, numOfFrames);
Ham             = window(@hamming, windowLength);
mfccParams      = feature_mfccs_init(windowLength, fs);

for i=1:numOfFrames % for each frame
    
    % get current frame:
    frame  = signal(curPos:curPos+windowLength-1);
    frame  = frame .* Ham;
    frameFFT = CBSR_SignalSpectrum(frame, fs, '',0);
    
    if (sum(abs(frame))>eps)
        % compute time-domain features:
        Features(1,i) = feature_zcr(frame);
        Features(2,i) = feature_energy(frame);
        Features(3,i) = feature_energy_entropy(frame, 10);

        % compute freq-domain features: 
        if (i==1) 
            frameFFTPrev = frameFFT; 
        end;
        
        [Features(4,i) Features(5,i)]   = feature_spectral_centroid(frameFFT, fs);
        Features(6,i)                   = feature_spectral_entropy(frameFFT, 10);
        Features(7,i)                   = feature_spectral_flux(frameFFT, frameFFTPrev);
        Features(8,i)                   = feature_spectral_rolloff(frameFFT, 0.90);
%         [~,MFCCs,~ ]                    = Smartphone_SpectralAnalysis( frameFFT,'',0 );
        MFCCs                           = feature_mfccs(frameFFT, mfccParams);
        Features(9:21,i)                = MFCCs;
        
        [HR, F0]                        = feature_harmonic(frame, fs);
        Features(22, i)                 = HR;
        Features(23, i)                 = F0;        
        Features(23+1:23+12, i)         = feature_chroma_vector(frame, fs);
    else
        Features(:,i)                   = zeros(numOfFeatures, 1);
    end  
    
    curPos = curPos + step;
    frameFFTPrev = frameFFT;
end

Features(35, :) = medfilt1(Features(35, :), 3);

if plotNr > 0 && plotNr < 36
   figure; plot(Features(plotNr,:),'black'); title(featureNames(plotNr)); 
end


averagedFeatures = zeros(35,1);
for k=1:35
    averagedFeatures(k,1) = median(sort(Features(k,:),'descend'));
end


end
