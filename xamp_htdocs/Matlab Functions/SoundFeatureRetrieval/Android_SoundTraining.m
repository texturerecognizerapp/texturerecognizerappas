function [ tmp ] = Android_SoundTraining( querySound )
%   Description:        
%       This function calculates the training and test data sets for
%       further Machine Learning processing and evaluation
%
%   Input:  acceleration struct 
%   Output: 
%           ...
%
%   Algorithm
%
%
%
%
%   References
%
%           [1] .................
%

%   Author: .....................



    % Ensure correct number of inputs
    if( nargin~=  1), help Android_SoundTraining; 
        return; 
    end; 

    % Check, if CBTR_Constants has already been loaded for global variables
    if ( any(strcmp(who,'numTextures')) == 0)
       Smartphone_Constants 
    end
    
    load featureSpace_Training_max;
    load featureSpace_Training_min;
    
    tmp = Android_SoundProcessQuery(querySound,'',0);
    %tmp = abs(0.0001 .* randn(numSoundFeatures,1));
        
    %% only add if more than 10 queries per textures a required.
    % add random noise to previous instance line and save it, just makes it
    % harder to classify - random noise is a good test for robustness
    % for the entire algorithm
    tmp = tmp + abs(0.0001 .* randn(numel(tmp),1));
    %%%%%%%CBSR_Training_PlainFile = [CBSR_Training_PlainFile , tmp];
    
    for i=1:numel(minima)-12;
        tmp(i) = (tmp(i) - minima(i+12)) / (maxima(i+12) - minima(i+12));
    end
end

