function [vtf] = SpectralFlatness (X)
    XLog    = log(X+1e-20);
    vtf     = exp(mean(XLog,1)) ./ mean(X,1);
end