function [ queryFeatureVector ] = Android_SoundProcessQuery( dataMovement,ttitle,plotIt ) %
%   Description:
%       This function processes a datatrace
%
%   Input:  data trace
%           title
%           flag for plotting
%   Output:
%           ...
%
%   Algorithm
%
%
%
%
%   References
%
%           [1] .................
%

%   Author: .....................


% Ensure correct number of inputs
if( nargin~=  3), help Android_SoundProcessQuery;
    return;
end;


% Check, if Smartphone_Constants has already been loaded for global variables
if ( any(strcmp(who,'numTextures')) == 0)
    Smartphone_Constants
end



queryFeatureVector = zeros(numSoundFeatures,1);
% tic;
disp(' ')
disp('--------------------------------------------------------')
disp(ttitle);
disp('--------------------------------------------------------')







%% Features



%% Movement Features
[~,~,queryFeatureVector(9:43,1)] = Android_AudioFeatures(dataMovement,CBSR_SAMPLERATE,0.5,0.1,0);


% [loudness, sharpness] = CBSR_Perceptual_Analysis(dataMovement);    
% 
% queryFeatureVector(16,1) = loudness;
% queryFeatureVector(17,1) = sharpness;
% 
% [mfcc13] = CBSR_CepstralFeaturesNew(dataMovement,ttitle, 0);
% queryFeatureVector(18:30,1) = mfcc13(1:13);
% 
% [distanceHL, f1, f2, sp1] = CBSR_Frequency_Analysis(dataMovement,ttitle,0);       
% queryFeatureVector(31,1) = distanceHL;
% queryFeatureVector(32,1) = f1;
% queryFeatureVector(33,1) = f2;
% queryFeatureVector(34,1) = sp1;
% 
% [spread, skewness, slope, rolloff, specCentr, highSpecMean] = CBSR_SpectralShape_Analysis(dataMovement,ttitle,0); 
% queryFeatureVector(35,1) = spread;
% queryFeatureVector(36,1) = slope;
% queryFeatureVector(37,1) = skewness;
% queryFeatureVector(38,1) = rolloff;
% queryFeatureVector(39,1) = highSpecMean;
% 
% [mfccHarmonic] = CBSR_Harmonics(dataMovement,ttitle, 0);
% queryFeatureVector(40,1) = mfccHarmonic(1);
% queryFeatureVector(41,1) = mfccHarmonic(2);
% queryFeatureVector(42,1) = mfccHarmonic(3);
% queryFeatureVector(43,1) = mfccHarmonic(4);


% timeUsed = toc;
% disp(strcat('Calculation Time...',num2str(timeUsed),'sec'));



if plotIt == 1
    width = 8;
    height = 6;
    figure
    subplot(211)
    X = SpectrumFnc(dataTap,'',44100,1,0);
    X = X./max(X); 
    plot(X,'black');
    xlabel('Frequency (Hz)');
    ylabel('|Intensity|')
    axis([100 10000 0 1])

    subplot(212)
    X = SpectrumFnc(dataMovement,'',44100,1,0);
    X = X./max(X);
    plot(X,'black');
    xlabel('Frequency (Hz)');
    ylabel('|Intensity|')
    axis([100 10000 0 1])

    % to do export cutted version to pdf
    set(gcf,'PaperUnits','centimeters');
    % width height
    set(gcf,'PaperSize',[width height]);                  %[12 4.2]);
    % left bottom width height
    set(gcf,'PaperPosition',[0.0 0.0 width height]);  %[-0.5,0.20,12.7,4.0]);
    set(gcf,'PaperPositionMode','manual');
    print(gcf,'-dpdf',strcat('SoundSpectra',ttitle,'.pdf'));
end

end

