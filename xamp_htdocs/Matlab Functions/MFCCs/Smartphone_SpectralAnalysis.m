function [spectralCentroid,mfcCoeff,energyCoeff ] = Smartphone_SpectralAnalysis( x,ttitle,plotIt )
%   Description:
%       This function calculates ...
%
%   Input:  data trace
%           title
%           flag for plotting
%   Output:
%           ...
%
%   Algorithm
%
%
%
%
%   References
%
%           [1] .................
%

%   Author: .....................



% Ensure correct number of inputs
if( nargin~=  3), help Smartphone_SpectralAnalysis;
    return;
end;

ScList      = [];
SFlatList   = [];
SSpreadList = [];
SDecrList   = [];
binSumList  = [];
lpcList     = [];
energyList  = [];
frame       = int8(numel(x)/10);



%% Simple Spectral Features
for l = 1:frame:numel(x)-frame
    data = x(l:l+frame);
    spec = abs(dct(data,5000));
    %spec = spec ./max(spec);
    ScList      = [ScList,SpectralCentroid( abs(spec),10000)];
    SFlatList   = [SFlatList,SpectralFlatness(spec)];
    binList     = spectralGaussianBinning(spec);
    binSum      = sum(binList);
    binSumList  = [binSumList,binSum];
    %% Kuchenbecker Romano 30 energy values
    energyList = [energyList,get30Energies(spec)'];
end


if numel(ScList) == 0
    % set dummy value
    spectralCentroid = 100;
else
    sortedScList = sort(ScList,'descend');
    spectralCentroid = median( sortedScList );
end

if plotIt == 1
    spec = abs(dct(x, 5000));
    %figure;stem(spec,'black'); title(strcat('Spectrum of moving phase of...',ttitle,'...frame = 1 sec')); axis([0 2000 0 inf])
    figure; stem(ScList,'black'); title(strcat('SC (move)...',ttitle,'=',num2str(spectralCentroid))); axis([0 inf 0 800])
end





%% MFCC
[mfcCoeff] = getMfcc(x);

if plotIt == 2
    figure; stem(mfcCoeff(2:end),'black'); title(strcat('MFCC (move)...',ttitle,'...',num2str(std(mfcCoeff(2:end))))); axis([0 14 -inf inf])
end
energyCoeff = mean(energyList');





%% display section

% disp(strcat('  Spectral Centroid____',num2str(spectralCentroid)));
% disp(strcat('  MFCC_1_______________',num2str(mfcCoeff(1))));
% disp(strcat('  MFCC_2_______________',num2str(mfcCoeff(2))));
% disp(strcat('  MFCC_3_______________',num2str(mfcCoeff(3))));
% disp(strcat('  MFCC_4_______________',num2str(mfcCoeff(4))));
% disp(strcat('  MFCC_5_______________',num2str(mfcCoeff(5))));
% disp(strcat('  MFCC_6_______________',num2str(mfcCoeff(6))));
% disp(strcat('  MFCC_7_______________',num2str(mfcCoeff(7))));
% disp(strcat('  MFCC_8_______________',num2str(mfcCoeff(8))));
% disp(strcat('  MFCC_9_______________',num2str(mfcCoeff(9))));
% disp(strcat('  MFCC_10______________',num2str(mfcCoeff(10))));
% disp(strcat('  MFCC_11______________',num2str(mfcCoeff(11))));
% disp(strcat('  MFCC_12______________',num2str(mfcCoeff(12))));
% disp(strcat('  MFCC_13______________',num2str(mfcCoeff(13))));



end


function[coeff] = getMfcc(x)

Tw = 200;%25;              % analysis frame duration (ms)
Ts = 20;                  % analysis frame shift (ms)
alpha = 1;%0.97;          % preemphasis coefficient
R = [ 50 10000 ];           % frequency range to consider
M = 20;                   % number of filterbank channels
C = 13;                   % number of cepstral coefficients
L = 22;                   % cepstral sine lifter parameter
fs = 44100;               % Sample Rate Accelerometer
nbits = 10;

% hamming window (see Eq. (5.2) on p.73 of [1])
hamming = @(N)(0.54-0.46*cos(2*pi*[0:N-1].'/(N-1)));

% Feature extraction (feature vectors as columns)
%[ MFCCs, FBEs, frames ] = mfcc( x, fs, Tw, Ts, alpha, hamming, R, M, C, L );
[ MFCCs, FBEs, frames ] = mfccOneFrame( x, fs, Tw, Ts, alpha, hamming, R, M, C, L );
%coeff = mean(MFCCs');
coeff = MFCCs;

end




function [energies]=get30Energies(spec)

%calc energy spectrum
spec = spec.^2;

minF = 0;
maxF = 1500;
step = (maxF-minF) / 30;
energies = [];
for k=1:step:maxF-step+1
    e = sum(abs(spec(k:k+step)));
    energies = [energies,e];
end

end


function[binList] = spectralGaussianBinning(X)

a=0.12;
b= 10;

binList = [];
for b = 5:50:1000
    
    f = 1:1:1000;
    w_f = 0.1 * exp(-0.01*b) .*  exp( -(  ((f-b).^2) / (2*power(a*b,2)) )  );
    binList = [binList,X(1:1000)'.*w_f];
    
end



end

