

% 
% addpath('MatlabData/');
% addpath('Plots/');
% addpath('CBAR/');
% addpath('CBSR/');
% addpath('CBIR/');
% addpath('Database/');
% addpath('MatlabData/');


%% database path
dbPath                              = strcat(pwd,'Database\\');


%% General Settings
numTextures                         = 108;
numQueries                          = 10;
numInstances                        = numQueries * numTextures;

numAccFeatures                      = 29;
numSoundFeatures                    = 43;
numImageFeatures                    = 12;
numFrictionFeatures                 = 2;
numReflectanceFeatures              = 2;

numAccSoundFeatures                 = 10;

numCombinedFeatures                 = 9;

numFeatures                         = numAccFeatures + numSoundFeatures + numImageFeatures + numFrictionFeatures + numReflectanceFeatures + numAccSoundFeatures + numCombinedFeatures;

numSmartphoneFeatures = 55;














featureNames     = {};
featureNames{1}  = 'Waviness';
featureNames{2}  = 'Spikiness (1)';
featureNames{3}  = 'Self-Similarity';
featureNames{4}  = 'Stationarity';
featureNames{5}  = 'Regularity';
featureNames{6}  = 'Movement Spectral Centroid';
featureNames{7}  = 'MFCC Mean';
featureNames{8}  = 'MFCC(2)';
featureNames{9}  = 'MFCC(3)';
featureNames{10} = 'MFCC(4)';
featureNames{11} = 'MFCC(5)';
featureNames{12} = 'MFCC(6)';
featureNames{13} = 'MFCC(7)';
featureNames{14} = 'MFCC(8)';
featureNames{15} = 'MFCC(9)';
featureNames{16} = 'MFCC(10)';
featureNames{17} = 'MFCC(11)';
featureNames{18} = 'MFCC(12)';
featureNames{19} = 'MFCC STD';
featureNames{20} = 'Impact Spectral Centroid';
featureNames{21} = 'Impact Hardness';
featureNames{22} = 'Impact Energy (1)';
featureNames{23} = 'Impact Energy (2)';
featureNames{24} = 'Impact Energy (3)';
featureNames{25} = 'Impact Energy (4)';
featureNames{26} = 'Impact Energy (5)';
featureNames{27} = 'Impact Clamping';
featureNames{28} = 'Temporal Roughness';
featureNames{29} = 'Spectral Roughness)';
% Sound
featureNames{30} = 'Hardness Impact(Sound)';
featureNames{31} = 'ZCR Impact';
featureNames{32} = 'SC  Impact';
featureNames{33} = 'HighLowFrequencyRatio Impact';
featureNames{34} = 'HighMidFrequencyRatio Impact';
featureNames{35} = 'Distance High Low Impact';
featureNames{36} = 'Dominant Frequency 1 Impact';
featureNames{37} = 'Spread Impact';
featureNamesAudioMove =... 
{...
    'Zero Crossing Rate',... 
    'Signal Energy',...
    'Signal Energy Entropy',...
    'Spectral Centroid',...
    'Spectral Spread',...
    'SpecEntropy',...
    'SpcFlux',...
    'SpecRollOff',...
    'MFCC1',...
    'MFCC2',...
    'MFCC3',...
    'MFCC4',...
    'MFCC5',...
    'MFCC6',...
    'MFCC7',...
    'MFCC8',...
    'MFCC9',...
    'MFCC10',...
    'MFCC11',...
    'MFCC12',...
    'MFCC13',...
    'Spectral Harmonic HR',...
    'Spectral Harmonic F0',...
    'Chroma1',...
    'Chroma2',...
    'Chroma3',...
    'Chroma4',...
    'Chroma5',...
    'Chroma6',...
    'Chroma7',...
    'Chroma8',...
    'Chroma9',...
    'Chroma10',...
    'Chroma11',...
    'Chroma12'...  
};
for k=1:35
   featureNames{37+k} = featureNamesAudioMove{k}; 
end

%Image
featureNames{73} = 'Glossiness';
featureNames{74} = 'Regularity 1';
featureNames{75} = 'Regularity 2';
featureNames{76} = 'Linelikeness';
featureNames{77} = 'Directionality';
featureNames{78} = 'Coarseness';
featureNames{79} = 'Contrast';
featureNames{80} = 'Color';
featureNames{81} = 'Saturation';
featureNames{82} = 'Roughness';
featureNames{83} = 'NA';
featureNames{84} = 'Wiener Entropy';
featureNames{85} = 'Friction Mean';
featureNames{86} = 'Friction SlipStick';
featureNames{87} = 'Mean Reflectance';
featureNames{88} = 'VarReflectance';
featureNames{89} = 'Accel Sound Tapping 1';
featureNames{90} = 'Accel Sound Tapping 2';
featureNames{91} = 'Accel Sound Tapping 3';
featureNames{92} = 'Accel Sound Tapping 4';
featureNames{93} = 'Accel Sound Tapping 5';
featureNames{94} = 'Accel Sound Movement 1';
featureNames{95} = 'Accel Sound Movement 2';
featureNames{96} = 'Accel Sound Movement 3';
featureNames{97} = 'Accel Sound Movement 4';
featureNames{98} = 'Accel Sound Movement 5';
featureNames{99} = 'Combined_Friction';
featureNames{100} = 'Combined_Hardness';
featureNames{101} = 'Combined_Compliance';
featureNames{102} = 'Combined_MacroRough_Strength';
featureNames{103} = 'Combined_Regularity';
featureNames{104} = 'Combined_Anisotropy';
featureNames{105} = 'Combined_MicroRoughAcc';
featureNames{106} = 'Combined_MicroRoughSound';
featureNames{107} = 'Combined_Warmth';


%% SmartphoneFeatureNames

SmartphoneFeatureNames=cell(1,55);
SmartphoneFeatureNames{1} = 'Glossiness';
SmartphoneFeatureNames{2} = 'Regularity 1';
SmartphoneFeatureNames{3} = 'Regularity 2';
SmartphoneFeatureNames{4} = 'LineLikeness';
SmartphoneFeatureNames{5} = 'LineLikeness2';
SmartphoneFeatureNames{6} = 'Coarseness';
SmartphoneFeatureNames{7} = 'Contrast';
SmartphoneFeatureNames{8} = 'DominantColor';
SmartphoneFeatureNames{9} = 'Roughness';
SmartphoneFeatureNames{10} = 'Softness';
SmartphoneFeatureNames{11} = 'Directionality';
SmartphoneFeatureNames{12} = 'Wiener Entropy';
SmartphoneFeatureNames{13} = 'Hardness Impact(Sound)';
SmartphoneFeatureNames{14} = 'ZCR Impact';
SmartphoneFeatureNames{15} = 'SpecCentroidTapping';
SmartphoneFeatureNames{16} = 'HighLowFrequencyRatio Impact';
SmartphoneFeatureNames{17} = 'HighMidFrequencyRatio Impact';
SmartphoneFeatureNames{18} = 'Distance High Low Impact';
SmartphoneFeatureNames{19} = 'Dominant Frequency 1 Impact';
SmartphoneFeatureNames{20} = 'Spread Impact';
SmartphoneFeatureNames(21:55)= featureNamesAudioMove;

%% accel specific constants
CBAR_TAP_OFFSET                     = 1500;

CBAR_SAMPLERATE                     = 10000;
CBAR_ENERGYDETECTIONTHRESHOLD       = 5*10^-2;
CBAR_NOMOVEMENTTHRESHOLD            = 5*10^-3;
CBAR_REMOVALDISTANCE                = 5000;
CBAR_REMOVALFRAME1                  = 500;  % 1000
CBAR_REMOVALFRAME2                  = 500;  % 1000

%% sound specific constants
% energy detection threshold
CBSR_SAMPLERATE                     = 44100;
CBSR_ENERGYDETECTIONTHRESHOLD       = 5*10^-1;
CBSR_NOMOVEMENTTHRESHOLD            = 1*10^-3;
CBSR_REMOVALDISTANCE                = 5000;
CBSR_REMOVALFRAME1                  = 500;  % 1000
CBSR_REMOVALFRAME2                  = 500;  % 1000

%% image specific constants


%% friction specific constants











%% CBTR_MAIN global variables

accuracyCounter = 0;
sumConfMat       = zeros(numTextures,numTextures);


%% selectedFeatures
selectedFeatures = [ 6 20 21 28 30 32 79 80 85 87 88 ];
%selectedFeatures = [ 78 79 80   ];

%multcompare (40% Recall)
selectedFeatures = [ 1     3     4     6     8    23    24    25    26    28    29    30    39    41    45    46    47    48    79    81    82    85    86    87   88    90    91    92    93    94    95    96];
selectedFeatures = [4     6     9    24    25    27    28    29    39    79    87    88    90    93    98   101   102   103   105];
%combined
selectedFeatures = [ 80 87 99 100 101 102 103 105 106 107];
selectedFeatures = [ 29 33 30 31  ]; % MFCC1 MFCC4 MFCC2 MFCC3 Smartphone
% selectedFeatures = [ 7 8 9  ];
%% Used data sets, generated (training and testing data)

predictedIndices = zeros(numTextures,1);
predictedSimilarClassesFromClassification ={};



% todo
try
    x = load('featureSpace_Training.mat'); %queryFeatureVectors.mat
    featureSpace_Training = abs(x.featureSpace_Training);
    noiseVec = 0.000001*randn(size(featureSpace_Training));
    %%.. add minimal noise = harder to classify!!!
    %featureSpace_Training = featureSpace_Training + noiseVec;
    %featureSpace_Training = abs(featureSpace_Training);


    x = load('featureSpace_Testing.mat'); %queryFeatureVectors.mat
    featureSpace_Testing = abs(x.featureSpace_Testing);
    noiseVec = 0.000001*randn(size(featureSpace_Testing));
    %%.. add minimal noise = harder to classify!!!
    featureSpace_Testing = featureSpace_Testing + noiseVec;
    featureSpace_Testing = abs(featureSpace_Testing);
catch exc
    
end



% x = load('plainFile_SubjTest.mat'); %queryFeatureVectors.mat
% plainFileTesting = abs(x.plainFile_SubjTest);
% %%.. add minimal noise = harder to classify!!!
% plainFileTesting = plainFileTesting + noiseVec;
% plainFileTesting = abs(plainFileTesting);
% 
% x = load('plainFile_SubjTestWithoutNormalisation.mat'); %queryFeatureVectors.mat
% plainFileTestingWithoutNormalisation = abs(x.plainFile_SubjTestWithoutNormalisation);
% plainFileTestingWithoutNormalisation = abs(plainFileTestingWithoutNormalisation);
% 
% x = load('plainFileCombined1.mat');
% plainFileCombined1 = x.plainFileCombined;
% plainFileCombined1 = plainFileCombined1 + noiseVec;
% plainFileCombined1 = abs(plainFileCombined1);
% x = load('plainFileCombined2.mat');
% plainFileCombined2 = x.plainFileCombined;
% plainFileCombined2 = plainFileCombined2 + noiseVec;
% plainFileCombined2 = abs(plainFileCombined2);
% x = load('plainFileCombined3.mat');
% plainFileCombined3 = x.plainFileCombined;
% plainFileCombined3 = plainFileCombined3 + noiseVec;
% plainFileCombined3 = abs(plainFileCombined3);
% x = load('plainFileCombined4.mat');
% plainFileCombined4 = x.plainFileCombined;
% plainFileCombined4 = plainFileCombined4 + noiseVec;
% plainFileCombined4 = abs(plainFileCombined4);
% x = load('plainFileCombined5.mat');
% plainFileCombined5 = x.plainFileCombined;
% plainFileCombined5 = plainFileCombined5 + noiseVec;
% plainFileCombined5 = abs(plainFileCombined5);
% x = load('plainFileCombined6.mat');
% plainFileCombined6 = x.plainFileCombined;
% plainFileCombined6 = plainFileCombined6 + noiseVec;
% plainFileCombined6 = abs(plainFileCombined6);
% x = load('plainFileCombined7.mat');
% plainFileCombined7 = x.plainFileCombined;
% plainFileCombined7 = plainFileCombined7 + noiseVec;
% plainFileCombined7 = abs(plainFileCombined7);
% x = load('plainFileCombined8.mat');
% plainFileCombined8 = x.plainFileCombined;
% plainFileCombined8 = plainFileCombined8 + noiseVec;
% plainFileCombined8 = abs(plainFileCombined8);
% x = load('plainFileCombined9.mat');
% plainFileCombined9 = x.plainFileCombined;
% plainFileCombined9 = plainFileCombined9 + noiseVec;
% plainFileCombined9 = abs(plainFileCombined9);
% x = load('plainFileCombined10.mat');
% plainFileCombined10 = x.plainFileCombined;
% plainFileCombined10 = plainFileCombined10 + noiseVec;
% plainFileCombined10 = abs(plainFileCombined10);
% 
% 
% x = load('normalisationMaxima.mat');
% normMaxima = x.maxima;
% x = load('normalisationMinima.mat');
% normMinima = x.minima;

