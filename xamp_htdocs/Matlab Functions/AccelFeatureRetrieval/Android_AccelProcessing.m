function [ accelFeatureVector ] = Android_AccelProcessing( data )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if ( any(strcmp(who,'numTextures')) == 0)
    Smartphone_Constants
end

accelFeatureVector = zeros(1,1);

%% Preprocess data

[preprocessedData] = Android_AccelPreprocessing(data, 0);

%% Features

% acceleration spectral sum
[accelFeatureVector(1)] = CBAR_AccelerationSpectralSum(preprocessedData.raw);

end

