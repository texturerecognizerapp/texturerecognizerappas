function [ filteredData ] = movingAverageFilter( data, len, shift )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

movingAverage= data(1:length(data)-shift);
filteredData = data(1:length(data)-shift);

for i=len:length(data)
    summe = sum(data((i-len+1):i));
    movingAverage(i-len+1) = summe/len;
end

for i=1:length(filteredData)
    filteredData(i) = data(i+shift)-movingAverage(i);
end

end

