<?php

#**********************************************************
#Main script
#**********************************************************

#<1>set target path for storing photo uploads on the server
$photo_upload_path = "./upload/";
$photo_upload_path = $photo_upload_path. basename( $_FILES['uploadedfile']['name']); 

$processed_photo_output_path = "./output/";
$downloadFileName = "result.txt";
$processed_photo_output_path = $processed_photo_output_path.basename($downloadFileName);

#<3>modify maximum allowable file size to 10MB and timeout to 300s
ini_set('upload_max_filesize', '10M');  
ini_set('post_max_size', '10M');  
ini_set('max_input_time', 300);  
ini_set('max_execution_time', 300);  

#<4>Get and stored uploaded photos on the server
if(copy($_FILES['uploadedfile']['tmp_name'], $photo_upload_path)) {
	
	unlink($processed_photo_output_path);

	#<5> execute matlab image processing algorithm
	$command = "\"C:\Program Files\MATLAB\R2014b\bin\matlab\" -nojvm -nodesktop -nodisplay -r \"computeFeatures\"";
	exec($command);
	
} else{
    echo "There was an error uploading the file to $photo_upload_path !";
}

?>



