%% Constants

% audio features
fs = 44100;

%classification settings
kNN = 'k-nearest-neighbor';
kMeans = 'k-means';
Mahalanobis = 'Mahalanobis';

%folders and files
folder = './upload/';
audioFile  = 'audio.wav';
jsonClassificationSettingsFile = 'jsonClassificationSettings.txt';

%% features

%TODO add image features and maybe acceleration features depending on the
%features selected in the settings

% load files to compute features (currently only audio)
audioData = audioread(strcat(folder,audioFile));

% figure;
% plot(audioData)

% compute features (add more functions to compute image and maybe
% acceleration features)
features = Smartphone_SoundTraining(audioData(fs:2*fs));

features = [zeros(12,1); features];

% get relevant features 
% TODO move to section after classification settings and compute only
% features that are selected in the settings

% f1: new features to be classified
% f2: features from database
f1 = [features(21), features(22), features(25)];
load featureSpace_Training;
f2 = featureSpace_Training(:,[21,22,25]);

%% classification

%% classification settings

%default settings
classificationMethod = kNN;
classificationFeatures = {'ZeroCrossingRate'};
neighborhoodSize = 10;

%read settings from JSON file
settingsElements = readJSON(folder, jsonClassificationSettingsFile);

featureFieldNames = fieldnames(settingsElements.ClassificationFeatures);
numOfClassificationFeatures = length(featureFieldNames);

if(numOfClassificationFeatures > 0) 
    classificationFeatures = cell(1,numOfClassificationFeatures);
        
    for i=1:numOfClassificationFeatures
        classificationFeatures{i} = getfield(settingsElements.ClassificationFeatures,featureFieldNames{i});
    end
    
end

classificationMethod = settingsElements.ClassificationMethod;

if(strcmp(classificationMethod,kNN)) 
    neighborhoodSize = str2num(settingsElements.NeighborhoodSize);
end

%% get features specified by the settings
% uncomment to use this; otherwise only MFCC1, MFCC2, and MFCC5 are used
% (see code in features section)

% load SmartphoneFeatureNames;
% 
% featureNames = SmartphoneFeatureNames;
% 
% f1 = zeros(1,numel(classificationFeatures));
% f2 = zeros(size(featureSpace_Training,1),numel(classificationFeatures));
% 
% classificationFeatures
% 
% for i=1:numel(classificationFeatures)
%     
%     featureIndex = find(strcmp(classificationFeatures{i}, featureNames));
%     
%     if(~isempty(featureIndex))
%         
%         if(featureIndex < numel(features))
%             f1(i) = features(featureIndex);
%         else
%             error('Index too big for features array. Are all feature in the feature names cell array also contained in the features array?');
%         end
%         
%         if(featureIndex < size(featureSpace_Training,2))
%             f2(:,i) = featureSpace_Training(:,featureIndex);
%         else
%             error('Index too big for features array. Are all feature in the feature names cell array also contained in the database?');          
%         end
%     else
%         error(['unknown feature ', classificationFeatures{i}]);
%     end
% end

%% actual classification

load textureNames;

if(strcmp(classificationMethod, kNN))
    
    [indices, occurences] = classifyKNN(f1, f2, neighborhoodSize);
    
    names = textureNames(indices)
   
    jsonString = buildJSON(names, occurences);
    
    outputFileID = fopen([pwd, '/output/result.txt'], 'w');
    fprintf(outputFileID, '%s', jsonString);
    fclose(outputFileID);
end

if(strcmp(classificationMethod, kMeans))
    
    [indices, distances] = classifyDistToMeans(f1, f2, 'Euclidean');
    
    names = textureNames(indices)
   
    jsonString = buildJSON(names, distances);
    
    outputFileID = fopen([pwd, '/output/result.txt'], 'w');
    fprintf(outputFileID, '%s', jsonString);
    fclose(outputFileID);
    
end

if(strcmp(classificationMethod, Mahalanobis))
    
    [indices, distances] = classifyDistToMeans(f1, f2, 'Mahalanobis');
    
    names = textureNames(indices)
   
    jsonString = buildJSON(names, distances);
    
    outputFileID = fopen([pwd, '/output/result.txt'], 'w');
    fprintf(outputFileID, '%s', jsonString);
    fclose(outputFileID);
    
end

