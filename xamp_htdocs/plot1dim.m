function [  ] = plot1dim( folder, file )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    
    if exist([folder,file], 'file')
        M = importdata(strcat(folder,file));
        d = size(M);
        figure;
        plot(M(1:d(1), 1));
        title(file);
    end
    
end

