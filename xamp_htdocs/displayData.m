function [  ] = displayData( uploadFolder )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    accel = 'accel.txt';
    filteredAccel = 'filteredAccel.txt';
    externAccel = 'externAccel.txt';
    filteredExternAccel = 'filteredExternAccel.txt';
    grav = 'grav.txt';
    gyro = 'gyro.txt';
    magnet = 'magnet.txt';
    rotvec = 'rotvec.txt';
    fsr = 'fsr.txt';
    velocity = 'velocity.txt';
    parameters = 'parameters.txt';
    json = 'jsonFeatureLog.txt';
    
    impact = 'impact.wav';
    sound = 'sound.wav';
    
    display = 'display.jpg';
    macro = 'macro.jpg';
    
    plot4dim(uploadFolder, accel);
    plotdct4dim(uploadFolder, accel, 3);
    plot3dim(uploadFolder, filteredAccel);
    plotdct3dim(uploadFolder, filteredAccel);
    plot4dim(uploadFolder, externAccel);
    plotdct4dim(uploadFolder, externAccel, 4);
    plot1dim(uploadFolder, filteredExternAccel);
    plotdct1dim(uploadFolder, filteredExternAccel);
    plot4dim(uploadFolder, grav);
    plot4dim(uploadFolder, gyro);
    plot4dim(uploadFolder, magnet);
    plot4dim(uploadFolder, rotvec);
    plot1dim(uploadFolder, fsr);
    plot1dim(uploadFolder, velocity);
    
    %plotSound(uploadFolder, impact);
    %plotSound(uploadFolder, sound);
    
%     figure;
%     imshow([uploadFolder, display]);
%     title(display);
%     figure;
%     imshow([uploadFolder, macro]);
%     title(macro);
    
    %params = readParameters(uploadFolder, parameters)
    %json = readJSON(uploadFolder, json)
    
    fileID = fopen([pwd, '/output/result.txt'], 'w');
    fprintf(fileID, '%s', 'result');
    
end

