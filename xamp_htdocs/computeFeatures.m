function [] = computeFeatures( )
%
% Description
%       Loads the data recorded by the android device and uploaded in the
%       upload folder. Computes image and sound features and evaluates the
%       result based on the existing database.
%       It then saves the results in the outputfolder as a JSON file named
%       'result' which the android device will then receive.
%       If there already is a result.txt file in the output folder, it has
%       to be deleted first so the android device won't load the old file
%       before the new one is created.
%
% Input
%       None
%
% Output
%       result.txt file which will be safed in the outputfolder.
%
tic;

%% load constants, define variables
load featureSpace_Training.mat;
load featureSpace_Training_Max.mat;
load featureSpace_Training_Min;
load SmartphoneFeatureNames.mat;
load labelNames108.mat;
load textureNames.mat;
audioFileName='audio.wav';
imageFileName='picture.jpg';
accelFileName='accel.txt';
uploadFolder='./upload/';
outputFolder='C:\xampp\htdocs/output/';
outputFileName='result.txt';
jsonClassificationSettingsFile = 'jsonClassificationSettings.txt';
numberOfDatabaseEntries=1080;
dataSamplesPerTexture=10;
numberOfTextures=numberOfDatabaseEntries/dataSamplesPerTexture;
numberOfResults=numberOfDatabaseEntries/dataSamplesPerTexture;
settings='JSON'; 

%% settings for both feature selection and evaluation method
switch settings
    
    case 'default'
        % take default settings
        featureSelection      = {'DominantColor','DominantSaturation','MFCC1','MFCC2','MFCC5',...
                                 'SpectralCentroid', 'SpectralEntropy'};
        numberOfFeatures      =  size(featureSelection,2);
        classificationMethod  =  'kNN';
        neighborhoodSize      =  15;
        
    case 'JSON'
        % read settings from JSN file
        settingsElements      = readJSON(uploadFolder, jsonClassificationSettingsFile);
        featureSelection      = readJSONFeatures(settingsElements);
        numberOfFeatures      = size(featureSelection,2);
        classificationMethod  = settingsElements.ClassificationMethod;
        if(strcmp(classificationMethod,'k-nearest-neighbors'))
            neighborhoodSize  = str2double(settingsElements.NeighborhoodSize);
            classificationMethod = 'kNN';
        end
        
        classificationMethod
end

%% compute feature vector for the movement data and the image

featureVector = zeros(1,size(SmartphoneFeatureNames,2));
audioData  = audioread(audioFileName);
% dataSize=numel(data);
% data  = data(0.7*dataSize-1:0.8*dataSize);
% data  = awgn(data,50);
accelData = accelread(uploadFolder, accelFileName);
image = imread(imageFileName);
[height, width, ~] = size(image);
rescaleHeight = round(height/4:3*height/4);
rescaleWidth  = round(width/4:3*width/4);
image=image(rescaleHeight,rescaleWidth,:);
[featureVector(1:12)]  = Android_ImageProcessing(image,'',0);
[featureVector(13:55)] = Android_SoundProcessQuery(audioData,'',0);
[featureVector(56)] = Android_AccelProcessing(accelData);
features               = normalise(featureVector, maxima, minima);

%% constructing query and data vector

featureIndices = getFeatureIndices(featureSelection, SmartphoneFeatureNames)
queryVector    = features(featureIndices)
dataVector     = featureSpace_Training(:,featureIndices);

%% Classification
numberOfDataSamples = size(dataVector,1);

switch classificationMethod
    case 'DistanceToMean'
        %Euclidean distance to mean values
        meanValues = zeros((numberOfDataSamples/dataSamplesPerTexture),numberOfFeatures);        
        distance   = zeros((numberOfDataSamples/dataSamplesPerTexture),1);
        for i=1:numberOfFeatures
            for j=1:size(meanValues,1)
                meanValues(j,i) = mean(dataVector((j-1)*dataSamplesPerTexture+1:(j-1)*dataSamplesPerTexture+10,i));
            end
        end
        for a=1:size(meanValues,1)
            distance(a,1)   = norm(queryVector-meanValues(a,:));
        end
        [minValue,minIndex] = sort(distance,'ascend');
        resultIndices       = minIndex(1:numberOfResults);
        resultValue         = minValue(1:numberOfResults);

    case 'Mahalanobis'
        % TODO implement Mahalanobis
        meanValues = zeros((numberOfDataSamples/dataSamplesPerTexture),numberOfFeatures);
        covMat = zeros(size(queryVector,2),size(queryVector,2),size(meanValues,1));
                
        for a=1:size(meanValues,1)
            %covMat(:,:,a) = eye(size(queryVector,2),size(queryVector,2));
            for i=1:dataSamplesPerTexture
                covMat(:,:,a) = covMat(:,:,a) + dataVector((a-1)*dataSamplesPerTexture+i,:)' * dataVector((a-1)*dataSamplesPerTexture+i,:);
            end
            
            covMat(:,:,a) = (1/dataSamplesPerTexture) * covMat(:,:,a);
        end
                
        %covMat = eye(size(queryVector,2));
        distance   = zeros((numberOfDataSamples/dataSamplesPerTexture),1);
        for i=1:numberOfFeatures
            for j=1:size(meanValues,1)
                meanValues(j,i) = mean(dataVector((j-1)*dataSamplesPerTexture+1:(j-1)*dataSamplesPerTexture+10,i));
            end
        end
        
        for a=1:size(meanValues,1)
            distance(a,1) = sqrt((queryVector-meanValues(a,:)) * inv(covMat(:,:,a)) * (queryVector-meanValues(a,:))');
        end
        
        [minValue,minIndex] = sort(distance,'ascend');
        resultIndices       = minIndex(1:numberOfResults);
        resultValue         = minValue(1:numberOfResults);
        
    case 'EuclideanDistance'
        distance = zeros(numberOfDatabaseEntries,1);
        for i=1:numberOfDatabaseEntries
            distance(i,1) = norm(queryVector-dataVector(i,:));
        end
        [minValue,minIndex] = sort(distance,'ascend');
        resultIndices       = ceil(minIndex(1:numberOfResults)/dataSamplesPerTexture);
        resultValue         = minValue(1:numberOfResults);
        
    case 'kNN'
        distance = zeros(numberOfDatabaseEntries,1);
        for i=1:numberOfDatabaseEntries
            distance(i,1) = norm(queryVector-dataVector(i,:));
        end
        [ ~ , minIndex] = sort(distance, 'ascend');
        minTextureIndex = ceil(minIndex(1:neighborhoodSize)/dataSamplesPerTexture);
        [occurencesUnsorted,textureIndices] = hist(minTextureIndex,unique(minTextureIndex));
        ResultMatrix    = [occurencesUnsorted' textureIndices];
        [ ~ ,maxIndex]  = sort(ResultMatrix(:,1), 'descend');
        SortedResults   = ResultMatrix(maxIndex,:);
        resultIndices   = SortedResults(1:min(numberOfResults,size(SortedResults,1)),2);
        resultValue     = SortedResults(1:min(numberOfResults,size(SortedResults,1)),1);
        
    case 'k-means'
        
end

%% displaying and saving the result file
 for i=1:size(resultIndices,1)
     disp(['textureName:  ' textureNames{resultIndices(i,1)}]);
     disp(['Value:        ' num2str(resultValue(i,1))]);
 end


jsonString   = buildJSON(textureNames(resultIndices), resultValue);
outputfileID = fopen([outputFolder outputFileName], 'w');
fprintf(outputfileID, '%s', jsonString);
fclose(outputfileID);


%% display time used by the function
timeUsed = toc;
disp(['Calculation Time:   ' num2str(timeUsed) 'sec']);


end


function [normalisedFeatures] = normalise( features, maxima, minima )
normalisedFeatures=zeros(1,size(features,2));
for i=1:size(features,2)
    normalisedFeatures(i)=(features(i)-minima(i))/(maxima(i)-minima(i));
end
end

function [featureIndices]     = getFeatureIndices ( featureNames,SmartphoneFeatureNames)
%translates the feature names into their according indices
[occurences,~]       = ismember(SmartphoneFeatureNames,featureNames);
[~,featureIndices,~] = find(occurences);

end

function [ accelData]         = accelread(folder, file)
if exist([folder,file], 'file')
    M = importdata(strcat(folder,file));
    d = size(M);
    accelData = struct;
    accelData.X = M(1:d(1), 2);
    accelData.Y = M(1:d(1), 3);
    accelData.Z = M(1:d(1), 4);
end
end

function [ elements ]         = readJSON( folder, file )
if exist([folder,file], 'file')
    string = fileread([folder,file]);
    elements = JSON.parse(string);
end
end

function [JSONFeatures]       = readJSONFeatures( JSONSettings )

featureFieldNames = fieldnames(JSONSettings.ClassificationFeatures);
numOfFeatures = length(featureFieldNames);

JSONFeatures = cell(1,numOfFeatures);

for i=1:numOfFeatures
    JSONFeatures{i} = getfield(JSONSettings.ClassificationFeatures,featureFieldNames{i});
end

end
