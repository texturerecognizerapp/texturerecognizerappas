function [ ] = plot3dim( folder, file )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    if exist([folder,file], 'file')
        M = importdata(strcat(folder,file));
        d = size(M);
        figure;
        plot(M(1:d(1), 1));
        hold;
        plot(M(1:d(1), 2));
        plot(M(1:d(1), 3));
        title(file);
    end

end

