function [  ] = plot4dim( folder, file )
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
    
    if exist([folder,file], 'file')
        M = importdata(strcat(folder,file));
        d = size(M);
        figure;
        plot(M(1:d(1), 2));
        hold;
        plot(M(1:d(1), 3));
        plot(M(1:d(1), 4));
        title(file);
    end

end

