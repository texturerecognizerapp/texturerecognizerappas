<?php

#function for streaming file to client
function streamFile($location, $filename, $mimeType='application/octet-stream')
{ if(!file_exists($location))
  { header ("HTTP/1.0 404 Not Found");
    return;
  }
  
  $size=filesize($location);
  $time=date('r',filemtime($location));
  #html response header
  header('Content-Description: File Transfer');	
  header("Content-Type: $mimeType"); 
  header('Cache-Control: public, must-revalidate, max-age=0');
  header('Pragma: no-cache');  
  header('Accept-Ranges: bytes');
  header('Content-Length:'.($size));
  header("Content-Disposition: inline; filename=$filename");
  header("Content-Transfer-Encoding: binary\n");
  header("Last-Modified: $time");
  header('Connection: close');      

  ob_clean();
  flush();
  readfile($location);
	
}

	$photo_upload_path = "./upload/";

	$processed_photo_output_path = "./output/";
$downloadFileName = "result.txt";
$processed_photo_output_path = $processed_photo_output_path.basename($downloadFileName);

	$command = "\"C:\Program Files\MATLAB\R2014b\bin\matlab\" -nojvm -nodesktop -nodisplay -r \"computeFeatures()\"";
	exec($command);

	#<6>stream processed photo to the client
while(!file_exists($processed_photo_output_path)) {
	echo 'not exist';
  sleep(1);
}

echo $processed_photo_output_path;

streamFile($processed_photo_output_path, $downloadFileName,"application/octet-stream");

?>