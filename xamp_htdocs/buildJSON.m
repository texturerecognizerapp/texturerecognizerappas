function [ jsonString ] = buildJSON( names, values )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

    jsonString = '{';
    
    for i=1:length(names)
        
        jsonString = [jsonString, '"', num2str(i), '":{'];
        
        if i==length(names)
            jsonString = [jsonString, '"name":"', names{i}, '","value":', num2str(values(i)), '}'];
        else
             jsonString = [jsonString, '"name":"', names{i}, '","value":', num2str(values(i)), '},'];
        end
        
    end
    
    jsonString = [jsonString, '}'];

end

