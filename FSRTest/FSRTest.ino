int fsrPin1 = A1;
int fsrPin2 = A2;
int xPin = A3;
int yPin = A4;
int zPin = A5;

int fsrRead = 0;

int fsrRest = 0;

int fsrValue1 = 0;
int fsrValue2 = 0;
int fsrRestValue1 = 0;
int fsrRestValue2 = 0;
int fsrValue;
int xVal = 0;
int yVal = 0;
int zVal = 0;

void setup() {
  // put your setup code here, to run once:

  fsrRestValue1 = map(analogRead(fsrPin1), 0, 1023, -128, 127);
  fsrRestValue2 = map(analogRead(fsrPin2), 0, 1023, -128, 127);

  //fsrRestValue1 = (byte)(map(analogRead(fsrPin1), 0, 1023, -128, 127));
  //fsrRestValue2 = (byte)(map(analogRead(fsrPin2), 0, 1023, -128, 127));

  //Setup usb serial connection to computer
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:

  xVal = analogRead(xPin);
  yVal = analogRead(yPin);
  zVal = analogRead(zPin);
  fsrValue1 = analogRead(fsrPin1);
  fsrValue2 = analogRead(fsrPin2);
  fsrValue = (map(analogRead(fsrPin1), 0, 1023, -64, 63) - fsrRestValue1) - (map(analogRead(fsrPin2), 0, 1023, -64, 63) - fsrRestValue2);

  //fsrValue =  (byte)((map(analogRead(fsrPin1), 0, 1023, -128, 127)  - fsrRestValue1) - (map(analogRead(fsrPin1), 0, 1023, -128, 127) - fsrRestValue2));
  Serial.print(fsrValue1);
  Serial.print('\t');
  Serial.print(fsrValue2);
  Serial.print('\t');
  Serial.print(fsrValue);
  Serial.print('\t');
  Serial.print(xVal);
  Serial.print('\t');
  Serial.print(yVal);
  Serial.print('\t');
  Serial.print(zVal);
  Serial.print('\n');
  delay(100);

}
