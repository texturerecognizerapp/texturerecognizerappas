const int MAX = 1500;

enum LED {rgb, Rgb, rGb, rgB, RGB};
enum State {waiting, recording, sending, done}; 

int greenLEDPin = 4;
int blueLEDPin = 5;
int redLEDPin = 3;

int xPin = A3;
int yPin = A4;
int zPin = A5;
int fsrPin1 = A1;
int fsrPin2 = A2;
int xRest = 0;
int fsrRead = 0;
int yRest = 0;
int zRest = 0;
int fsrRest = 0;

bool allowRecording;
bool firstRecord;
bool isRecording;
bool dataAvailable;
bool recordingFinished ;
bool dataSent;

int counterAccel = 0;
int counterFSR = 0;

const int counterDividerFSR = 10;
int accelRestValue = 0;
int accelValue;
char accelValues[MAX];
int fsrRestDiff = 0;
int fsrValue = 0;
int fsrRestValue = 0;
char fsrValues[(MAX/counterDividerFSR)+1];

unsigned long time1 = 0;
unsigned long time2 = 0;

bool useDelay = false;
bool checkTime = true;

State state;

void setup() {
  delay(1000);

  pinMode(greenLEDPin, OUTPUT);
  pinMode(blueLEDPin, OUTPUT);
  pinMode(redLEDPin, OUTPUT);

  allowRecording = false;
  firstRecord = true;

  setState(waiting);
  
  //Setup usb serial connection to computer
  Serial.begin(9600);
  Serial1.begin(9600);
}

void loop() {

   /*if(((analogRead(fsrPin1)) - (analogRead(fsrPin2))) > 127)
    Serial.println(127);
   else if (((analogRead(fsrPin1)) - (analogRead(fsrPin2))) < -128) 
    Serial.println(-128);
   else
    Serial.println(((analogRead(fsrPin1)) - (analogRead(fsrPin2))));*/
  
  if(state == recording) {
    firstRecord = false;
    readSensors();
    if(useDelay) {
      delay(2);
    }
  }

  if(state == sending) {
    sendData();
  }

  if(dataSent) {
    setState(waiting);
  }

  serialEvent();
}

void serialEvent() {

 char c;

  if(Serial1.available()) {
    c = (char)Serial1.read();

    if(c == 'c') {
      char checkType;
      byte checkValue;

      checkType = 'a';
      if(abs(analogRead(yPin)) > 10) {
        checkValue = 1;
        Serial1.write(checkType);
        Serial1.write(checkValue);
        //Serial.write(checkType);
        /*if(checkValue == 1) {
          Serial.write('1');
        } else if(checkValue == 0) {
          Serial.write('0');
        }*/
      } else {
        checkValue = 0;
        Serial1.write(checkType);
        //Serial1.write(checkValue);
        /*if(checkValue == 1) {
          Serial.write('1');
        } else if(checkValue == 0) {
          Serial.write('0');
        }*/
      }

      checkType = 'f';

      if(abs(analogRead(fsrPin1)) > 50) {
        checkValue = 1;
        Serial1.write(checkType);
        Serial1.write(checkValue);
        //Serial.write(checkType);
        /*if(checkValue == 1) {
          Serial.write('1');
        } else if(checkValue == 0) {
          Serial.write('0');
        }*/
      } else {
        checkValue = 0;
        Serial1.write(checkType);
        Serial1.write(checkValue);
        //Serial.write(checkType);
        /*if(checkValue == 1) {
          Serial.write('1');
        } else if(checkValue == 0) {
          Serial.write('0');
        }*/
      }
    }
    if(!isRecording && (c == 'b')) {
      allowRecording = true;

      //accelRestValue = map(analogRead(yPin), -1023, 1023, -128, 127);
      accelRestValue = analogRead(yPin);

      fsrRestDiff = (analogRead(fsrPin1) - analogRead(fsrPin2));
      //fsrRestDiff = 15*(log(1+analogRead(fsrPin1)) - log(1+analogRead(fsrPin2)));
      
      setState(recording);
      if(checkTime) {
        time1 = millis();
      }
    } else if(c == 'e') {
      allowRecording = false;
      if(state != waiting) {
        setState(sending);
      }
    } else if(!isRecording) {
      
      for(int i=0; i<3; i++) {
        digitalWrite(redLEDPin, LOW);
        delay(200);
        digitalWrite(redLEDPin, HIGH);
        delay(200);
      }
    }
  } 
}


void readSensors() {

  if(counterAccel < MAX) {
    
    //accelValues[counterAccel] = (byte)(map(analogRead(yPin) - accelRestValue, -1023, 1023, -128, 127));
    accelValue = (analogRead(yPin) - accelRestValue);

    if(accelValue > 126)
      accelValues[counterAccel] = 126;
    else if (accelValue < -127)
      accelValues[counterAccel] = -127;
    else
      accelValues[counterAccel] = (char)accelValue;

    if((counterAccel % counterDividerFSR) == 0) {

      fsrValue = (analogRead(fsrPin1) - analogRead(fsrPin2));
      //fsrValue = 15*(log(analogRead(fsrPin1)) - log(analogRead(fsrPin2)));
      fsrValue = (fsrValue - fsrRestDiff);
      if(fsrValue > 126)
        fsrValues[counterFSR] = 126;
      else if (fsrValue < -127)
        fsrValues[counterFSR] = -127;
      else 
        fsrValues[counterFSR] = (char)fsrValue;

      /*Serial.print(fsrValue);
      Serial.print('\t');
      Serial.println((int)fsrValues[counterFSR]);*/
        
      //fsrValues[counterFSR] = (byte)(fsrValue - fsrRestDiff);
      /*if(fsrValues[counterFSR] > 127) {
        fsrValues[counterFSR] = 127;
      } else if(fsrValues[counterFSR] < -128) {
        fsrValues[counterFSR] = -128;
      }*/
      
      //fsrValues[counterFSR] = (byte)(map(fsrValue - fsrRestDiff, -1023, 1023, -128, 127));
      counterFSR++;
    }

    counterAccel++;

    dataAvailable = true;

  } else {
    if(checkTime) {
      Serial.println(counterFSR);
      Serial.print("Time diff: ");
      time2 = millis();
      Serial.println(time2-time1);
    }
    setState(sending);
  }
}

void sendData() {

  int i = 0;

  while(i<counterAccel) {
    Serial1.write(accelValues[i]);
    i++;
  }

  i=0;

  Serial1.write(-128);

  while(i<(counterFSR)) {
    Serial1.write(fsrValues[i]);
    i++;
  }

  Serial1.write(127);

  dataSent = true;
}

void setState(State s) {
  switch(s) {
    case waiting:
      state = waiting;

      for(int i = 0; i < MAX; i++) {
        accelValues[i] = 0;
        if((i % counterDividerFSR) == 0) {
          fsrValues[i] = 0;
        }
      }
      
      isRecording = false;
      dataAvailable = false;
      recordingFinished = false;
      dataSent = false;
      counterAccel = 0;
      counterFSR = 0;
      
      setLED(Rgb);

      if(allowRecording) {
        setState(recording);
        
        if(checkTime) {
          time1 = millis();
        }
      } else if(!allowRecording && !firstRecord) {
        setState(done);
      }
      
      break;
   case recording:
      state = recording;
   
      isRecording = true;
      dataAvailable = dataAvailable;
      recordingFinished = false;
      setLED(rGb);
      break;
   case sending:
      state = sending;
   
      isRecording = false;
      dataAvailable = dataAvailable;
      recordingFinished = true;
      setLED(rgB);
      break;
   case done:
      state = done;    

      setLED(rgb);

      delay(1000);

      setup();
  }
}

void setLED(LED s) {
  switch(s) {
    case rgb:
      digitalWrite(redLEDPin, LOW);
      digitalWrite(greenLEDPin, LOW);
      digitalWrite(blueLEDPin, LOW);
    case Rgb:
      digitalWrite(redLEDPin, HIGH);
      digitalWrite(greenLEDPin, LOW);
      digitalWrite(blueLEDPin, LOW);
      break;
    case rGb:
      digitalWrite(redLEDPin, LOW);
      digitalWrite(greenLEDPin, HIGH);
      digitalWrite(blueLEDPin, LOW);
      break;
   case rgB:
      digitalWrite(redLEDPin, LOW);
      digitalWrite(greenLEDPin, LOW);
      digitalWrite(blueLEDPin, HIGH);
      break;
   case RGB:
      digitalWrite(redLEDPin, HIGH);
      digitalWrite(greenLEDPin, HIGH);
      digitalWrite(blueLEDPin, HIGH);
      break;
  }
}


