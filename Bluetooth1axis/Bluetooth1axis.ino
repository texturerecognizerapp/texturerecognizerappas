const int MAX = 1500;

enum LED {rgb, Rgb, rGb, rgB, RGB};
enum State {waiting, recording, sending, done}; 

int greenLEDPin = 4;
int blueLEDPin = 5;
int redLEDPin = 3;

int xPin = A3;
int yPin = A4;
int zPin = A5;
int fsrPin1 = A1;
int fsrPin2 = A2;
int xRest = 0;
int fsrRead = 0;
int yRest = 0;
int zRest = 0;
int fsrRest = 0;

bool allowRecording;
bool firstRecord;
bool isRecording;
bool dataAvailable;
bool recordingFinished ;
bool dataSent;

int counterAccel = 0;
int counterFSR = 0;

const int counterDividerFSR = 10;
int accelRestValue = 0;
byte accelValues[MAX];
int fsrRestDiff = 0;
int fsrValue = 0;
int fsrRestValue = 0;
byte fsrValues[(MAX/counterDividerFSR)+1];

unsigned long time = 0;

bool useDelay = false;

State state;

void setup() {
  delay(1000);

  pinMode(greenLEDPin, OUTPUT);
  pinMode(blueLEDPin, OUTPUT);
  pinMode(redLEDPin, OUTPUT);

  allowRecording = false;
  firstRecord = true;

  setState(waiting);
  
  //Setup usb serial connection to computer
  Serial.begin(9600);
}

void loop() {
  
  if(state == recording) {
    firstRecord = false;
    readSensors();
    if(useDelay) {
      delay(2);
    }
  }

  if(state == sending) {
    sendData();
  }

  if(dataSent) {
    setState(waiting);
  }

  serialEvent();
}

void serialEvent() {

 char c;

  if(Serial.available()) {
    c = (char)Serial.read();

    if(c == 'c') {
      char checkType;
      byte checkValue;

      checkType = 'a';
      if(abs(analogRead(zPin)) > 10) {
        checkValue = 1;
        Serial.write(checkType);
        Serial.write(checkValue);
      } else {
        checkValue = 0;
        Serial.write(checkType);
        Serial.write(checkValue);
      }

      checkType = 'f';
      if(abs(analogRead(fsrPin1)) > 100) {
        checkValue = 1;
        Serial.write(checkType);
        Serial.write(checkValue);
      } else {
        checkValue = 0;
        Serial.write(checkType);
        Serial.write(checkValue);
      }
    }
    if(!isRecording && (c == 'b')) {
      allowRecording = true;

      accelRestValue = analogRead(zPin);

      fsrRestDiff = analogRead(fsrPin1) - analogRead(fsrPin2);
      
      setState(recording);
    } else if(c == 'e') {
      allowRecording = false;
      if(state != waiting) {
        setState(sending);
      }
    } else if(!isRecording) {
      
      for(int i=0; i<3; i++) {
        digitalWrite(redLEDPin, LOW);
        delay(200);
        digitalWrite(redLEDPin, HIGH);
        delay(200);
      }
    }
  } 
}


void readSensors() {

  if(counterAccel < MAX) {
    
    /*time = millis() - time;
    Serial.println(time);*/
    
    accelValues[counterAccel] = (byte)(analogRead(zPin) - accelRestValue);

    if((counterAccel % counterDividerFSR) == 0) {

      fsrValue = analogRead(fsrPin1) - analogRead(fsrPin2);
      
      fsrValues[counterFSR] = (byte)(map(fsrValue - fsrRestValue, -1023, 1023, -128, 127));
      counterFSR++;
    }

    counterAccel++;

    dataAvailable = true;

  } else {
    setState(sending);
  }
}

void sendData() {

  int i = 0;

  while(i<counterAccel) {
    Serial.write(accelValues[i]);
    i++;
  }

  i=0;

  Serial.write(126);

  while(i<(counterFSR)) {
    Serial.write(fsrValues[i]);
    i++;
  }

  Serial.write(127);

  dataSent = true;
}

void setState(State s) {
  switch(s) {
    case waiting:
      state = waiting;

      for(int i = 0; i < MAX; i++) {
        accelValues[i] = 0;
        if((i % counterDividerFSR) == 0) {
          fsrValues[i] = 0;
        }
      }
      
      isRecording = false;
      dataAvailable = false;
      recordingFinished = false;
      dataSent = false;
      counterAccel = 0;
      counterFSR = 0;
      
      setLED(Rgb);

      if(allowRecording) {
        setState(recording);
      } else if(!allowRecording && !firstRecord) {
        setState(done);
      }
      
      break;
   case recording:
      state = recording;
   
      isRecording = true;
      dataAvailable = dataAvailable;
      recordingFinished = false;
      setLED(rGb);
      break;
   case sending:
      state = sending;
   
      isRecording = false;
      dataAvailable = dataAvailable;
      recordingFinished = true;
      setLED(rgB);
      break;
   case done:
      state = done;    

      setLED(rgb);

      delay(1000);

      setup();
  }
}

void setLED(LED s) {
  switch(s) {
    case rgb:
      digitalWrite(redLEDPin, LOW);
      digitalWrite(greenLEDPin, LOW);
      digitalWrite(blueLEDPin, LOW);
    case Rgb:
      digitalWrite(redLEDPin, HIGH);
      digitalWrite(greenLEDPin, LOW);
      digitalWrite(blueLEDPin, LOW);
      break;
    case rGb:
      digitalWrite(redLEDPin, LOW);
      digitalWrite(greenLEDPin, HIGH);
      digitalWrite(blueLEDPin, LOW);
      break;
   case rgB:
      digitalWrite(redLEDPin, LOW);
      digitalWrite(greenLEDPin, LOW);
      digitalWrite(blueLEDPin, HIGH);
      break;
   case RGB:
      digitalWrite(redLEDPin, HIGH);
      digitalWrite(greenLEDPin, HIGH);
      digitalWrite(blueLEDPin, HIGH);
      break;
  }
}


